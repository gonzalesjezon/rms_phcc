@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print {
    @page {
       size: 7in 9.25in;
       margin: 27mm 16mm 27mm 16mm;
    }
    .font-family-1{
      font-size: 14pt;
    }
  }

  .font-family-1{
      font-size: 14pt;
    }
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="width:960px;margin: auto; font-size: 12px;font-family: Times New Roman, serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 32 <br> Revised 2018</div>
    <div class="col-sm-6"></div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h4 class="p-0 m-0 font-weight-bold">Republic of the Philippines</h4>
      <div style="font-size: 20pt;" class="font-weight-bold"><u>Philippine Competition Commission</u></div>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h3><b>OATH OF OFFICE</b></h3>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-1"></div>
    <div class="col-10">
      <p class="text-justify font-family-1" style="text-indent: 50px;">
        I, <b><u>{!! $oathoffice->applicant->getFullName() !!}</u></b> of <b><u>{!! $oathoffice->applicant->permanent_house_number !!} {!! $oathoffice->applicant->permanent_street !!} {!! $oathoffice->applicant->permanent_barangay !!} {!! $oathoffice->applicant->city !!}</u></b> having been appointed to the position of <b><u>{!! $oathoffice->applicant->job->plantilla_item->position->Name !!}</u></b> hereby solemnly swear, that I will faithfully discharge to the best of my ability, the duties of my present position and of all others that I may hereafter hold under the Republic of the Philippines; that I will bear true faith and allegiance to the same; that I will obey the laws, legal orders, and decrees promulgated by the duly constituted authorities of the Republic of the Philippines; and that I impose this obligation upon myself voluntarily, without mental reservation or purpose of evasion.
      </p>
      <p class="font-family-1" style="text-indent: 50px;">SO HELP ME GOD.</p>
    </div>
  </div>

  <div class="row mb-1 font-family-1">
    <div class="col-sm-5"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center"><b>{!! $oathoffice->applicant->getFullName() !!}</b></div>
  </div>

  <div class="row mb-4 font-family-1">
    <div class="col-sm-5"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center border-top border-dark">(Signature over Printed Name of the Appointee)</div>
  </div>

  <div class="row mb-1 font-family-1">
    <div class="col-sm-2 text-right">Government ID: </div>
    <div class="col-sm-3 border-bottom border-dark"></div>
  </div>

  <div class="row mb-1 font-family-1">
    <div class="col-sm-2 text-right">ID Number: </div>
    <div class="col-sm-3 border-bottom border-dark"></div>
  </div>

  <div class="row mb-1 font-family-1">
    <div class="col-sm-2 text-right">Date Issued: </div>
    <div class="col-sm-3 border-bottom border-dark"></div>
  </div>

  <hr>

  <div class="row mb-8 font-family-1">
    <div class="col-sm-12">
      <p style="text-indent: 8em;">Subscribed and sworn to before me this _______ day of ___________________, 20___ in __________________________________, Philippines.</p>
    </div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-6"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center">
</div>
  </div>

  <div class="row mb-4 font-family-1">
    <div class="col-sm-5"></div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3 text-center border-top border-dark">(Signature over Printed Name of the Appointing Officer/Authority/ Head of Office)</div>
  </div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection