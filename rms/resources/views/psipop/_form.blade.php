@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'psipop-form']) !!}

<div class="row">
    <div class="col-6">

        <div class="form-group row">
            {{ Form::label('', 'Type of Personnel', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="type_of_personnel" id="type_of_personnel">
                    <option value="0">Select</option>
                    @foreach(config('params.type_of_personnel') as  $key => $value)
                    <option value="{{ $key }}" {{ ($key == @$psipop->type_of_personnel) ? 'selected' : '' }} >{!! $value !!}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group row">
            {{ Form::label('', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('item_number', @$psipop->item_number, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Item Number',
                    'required' => 'true'
                    
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="position_id" id="position_id" >
                    <option value="0">Select position</option>
                    @foreach($positions as $position)
                    <option value="{{ $position->RefId }}" {{ ($position->RefId == @$psipop->position_id) ? 'selected' : '' }} >
                        {!! strtoupper($position->Name) !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

         <div class="form-group row">
            {{ Form::label('', 'Employee Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="employment_status_id" id="employment_status_id" >
                    <option value="0">Select employee status</option>
                    @foreach($employee_status as $emp_status)
                    <option value="{{ $emp_status->RefId }}" {{ ($emp_status->RefId == @$psipop->employment_status_id) ? 'selected' : '' }} >
                        {!! strtoupper($emp_status->Name) !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="office_id" id="office_id" >
                    <option value="0">Select office</option>
                    @foreach($offices as $office)
                    <option value="{{ $office->RefId }}" {{ ($office->RefId == @$psipop->office_id) ? 'selected' : '' }} >
                        {!! strtoupper($office->Name) !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="division_id" id="division_id" >
                    <option value="0">Select division</option>
                    @foreach($divisions as $division)
                    <option value="{{ $division->RefId }}" {{ ($division->RefId == @$psipop->division_id) ? 'selected' : '' }} >
                        {!! strtoupper($division->Name) !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

   
    </div>

    <div class="col-6">
    	<div class="form-group row">
            {{ Form::label('', 'Department', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="department_id" id="department_id" >
                    <option value="0">Select department</option>
                    @foreach($departments as $department)
                    <option value="{{ $department->RefId }}" {{ ($department->RefId == @$psipop->department_id) ? 'selected' : '' }} >
                        {!! strtoupper($department->Name) !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Salary/Job Grade', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="job_grade" id="job_grade" >
                    <option value="0">Select</option>
                    @foreach(config('params.job_grades') as $key => $grade)
                    <option value="{{ $key }}" {{ ($key == @$psipop->job_grade) ? 'selected' : '' }} >
                        {!! $grade !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Step', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="step" id="step" >
                    <option value="0">Select</option>
                    @foreach(config('params.steps') as $key => $step)
                    <option value="{{ $key }}" {{ ($key == @$psipop->step) ? 'selected' : '' }} >
                        {!! $step !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Basic Salary', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('basic_salary', number_format(@$psipop->basic_salary,2), [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => '0.00'
                    ])
                }}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
	<div class="col-6">
    	<div class="form-group row">
            {{ Form::label('', 'Name of Incumbent', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('name_of_incumbent', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

        <p class="font-weight-bold">Annual Salary:</p>

        <div class="form-group row">
            {{ Form::label('', 'Authorized', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('authorized', @$psipop->authorized, [
                    'class' => 'form-control form-control-sm',
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Actual', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('actual', @$psipop->actual, [
                    'class' => 'form-control form-control-sm',
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Area Code', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('area_code', @$psipop->area_code, [
                    'class' => 'form-control form-control-sm',
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'P/P/A Atribution', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('ppa_atribution', @$psipop->ppa_atribution, [
                    'class' => 'form-control form-control-sm',
                    ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
    	<div class="form-group row">
            {{ Form::label('', 'Sex', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

        <p>&nbsp;</p>

    	<div class="form-group row">
            {{ Form::label('', 'Date of Birth', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'TIN', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Date of Original Appointment', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Date of Last Promotion', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Civil Service Eligibility', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => 'true'
                    ])
                }}
            </div>
        </div>

    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}

@section('scripts')
    @include('psipop._form-script')
@endsection
