<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/bootstrap-clockpicker.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery-clockpicker.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#interview-form').parsley(); //frontend validation

    $('.clockpicker').clockpicker();

    $('#send_mail').click(function(){
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {

            var arr = {};
            $("[class*='form-control']").each(function () {
                var obj_name = $(this).attr("name");
                var value    = $(this).val();
                arr[obj_name] = value;
            });

            $.ajax({
                url:`{{ url('interviews/sendMail') }}`,
                data:{
                    'data':arr,
                    '_token':`{{ csrf_token() }}`
                },
                type:'POST',
                dataType:'JSON',
                success:function(result){
                    Swal.fire(
                      'Sent Successfully!',
                      'Your mail has been sent.',
                      'success'
                    )

                }
            })
          }

        });

    });

    $applicantId = `{!! @$interview->applicant_id !!}`;

    $('#select_position').change(function() {
      jobId = $(this).find(':selected').val();

      $.ajax({
        url:`{!! route('interviews.get-applicant') !!}`,
        data:{
          'job_id':jobId
        },
        type:'GET',
        dataType:'JSON',
        success:function(res){

          option = [];
          option += `<option>Select applicant</option>`;
          $.each(res.applicants, function(k,v) {
            firstName = (v.first_name) ? v.first_name : '';
            middleName = (v.middle_name) ? v.middle_name : '';
            lastName = (v.last_name) ? v.last_name : '';
            fullname = firstName+' '+middleName+' '+lastName;
            if($applicantId == v.id){
              option += `<option value="${v.id}" data-email="${v.email_address}" selected  >${fullname}</option>`;
            }else{
              option += `<option value="${v.id}" data-email="${v.email_address}" >${fullname}</option>`;
            }
          });
          $('#select_applicant').html(option);

        }
      });

    });

    $(document).on('change', '#select_applicant',function() {
      email = $(this).find(':selected').data('email');
      $('#email').val(email);
    });

    $('#select_position').trigger('change');

  });
</script>