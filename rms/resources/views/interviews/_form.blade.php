@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('css/bootstrap-clockpicker.min.css') }}" />
  	<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('css/jquery-clockpicker.min.css') }}" />
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id'=>'interview-form']) !!}

<div class="row">
	<div class="col-6">
		<div class="form-group row">
		  {{ Form::label('', 'Position', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <select name="job_id" class="select2 select2-sm" id="select_position">
		        <option value="0">Selet position</option>
		        @foreach($jobs as $job)
		        <option value="{{ $job->id }}" 
		          data-education="{!! @$job->education !!}"
		          data-experience="{!! @$job->experience !!}"
		          data-training="{!! @$job->training !!}"
		          data-eligibility="{!! @$job->eligibility !!}"
		          data-office="{!! @$job->psipop->office->Name !!}"
		          {{ ($job->id == @$interview->job_id) ? 'selected' :  '' }}
		          >
		          {{ $job->psipop->position->Name}}
		        </option>
		        @endforeach
		    </select>
		  </div>
		</div>

		<div class="form-group row">
		  {{ Form::label('', 'Applicant', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <select name="applicant_id" class="select2 select2-sm" id="select_applicant"></select>
		  </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-6">

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Interview Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{@$interview->interview_date}}" name="interview_date"
		                   class="form-control form-control-sm" >
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				<div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true" data-min-view="2">
						{{ Form::text('interview_time', @$interview->interview_time, [
							'class' => 'form-control form-control-sm',
							'required' => true
						]) }}
				    <span class="input-group-addon">
				        <span class="glyphicon glyphicon-time"></span>
				    </span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Location', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('interview_location', @$interview->interview_location, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Email Address', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('email', @$interview->applicant->email_address, [
					'id' => 'email',
					'class' => 'form-control form-control-sm',
					'readonly' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Confirmation', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
              <div class="switch-button switch-button-success switch-button-yesno">
              	 @if(@$interview->confirmed == 1)
                  <input type="checkbox" name="confirmed" id="confirmed" checked="true"><span>
                 @else
                 <input type="checkbox" name="confirmed" id="confirmed"><span>
                 @endif
                 <label for="confirmed"></label></span>
              </div>
          </div>
		</div>
	</div>

	<div class="col-6">
		<div class="form-group row">
			<label class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> If Reschedule </label>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{ @$examination->resched_interview_date }}" name="resched_interview_date"
		                   class="form-control form-control-sm">
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('resched_interview_time', @$interview->resched_interview_time, [
					'class' => 'form-control form-control-sm',
				]) }}
			</div>
		</div>

		<hr>

		<div class="form-group row">
			{{ Form::label('','Interview Status', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::select('interview_status', config('params.interview_status'), @$interview->interview_status,[
					'class' => 'form-control form-control-xs',
					'placeholder' => 'Select interview status'
				]) }}
			</div>
		</div>

		<div class="row">
			<div class="col-9 text-right">
				<a class="btn btn-success mt-2" style="color: #fff;height: 30px;" id="send_mail">
					<i class="mdi mdi-mail-send"></i>
					Notify
				</a>
			</div>
		</div>


	</div>
</div>

<hr>

<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('interviews._form-script')
@endsection
