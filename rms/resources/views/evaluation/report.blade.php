@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print{
    @page{
      size: a4;
    }
    .font-style{
      font-family: Trebuchet MS, Helvetica, sans-serif;
      font-size: 9pt;
    }
  }
  .font-style{
    font-family: Trebuchet MS, Helvetica, sans-serif;
    font-size: 9pt;
  }
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<div class="dropdown-divider d-print-none"></div>

<div class="font-style" style="width: 960px;margin: auto;">
<div class="row mb-4">
    <div class="col-6">
      <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
    </div>
    <div class="col-6 text-right">
        <span>
          <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
        North Avenue, Quezon City 1105 <br>
        <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
        <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
        </span>
    </div>
  </div>

<div class="row mb-4 text-center">
  <div class="col-sm-12"><h3><b>INDIVIDUAL ASSESSMENT FORM</b></h3></div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Name</b></div>
  <div class="col-3 ">: {{$applicant->getFullname()}}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Present Position</b></div>
  <div class="col-3 ">: {!! $applicant->job->psipop->position->Name !!}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Division/Office</b></div>
  <div class="col-5">: {{$applicant->job->psipop->division->Name}}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-2"><b>Vacant Position/JG/Div./Office</b></div>
  <div class="col-3">: {{$applicant->job->psipop->position->Name}}</div>
</div>

<div class="row mb-1 text-left">
  <div class="col-4"><b>CSC Qualification Standards</b></div>
  <div class="col-3">:</div>
</div>

<div class="row mb-1 text-left">
  <div class="offset-1 col-1">Education</div>
  <div class="col-8">
    {!! $applicant->job->education !!}
  </div>
</div>

<div class="row mb-1 text-left">
  <div class="offset-1 col-1">Experience</div>
  <div class="col-8">
    {!! $applicant->job->experience !!}
  </div>
</div>

<div class="row mb-1 text-left">
  <div class="offset-1 col-1">Eligibility</div>
  <div class="col-8">
    {!! $applicant->job->eligibility !!}
  </div>
</div>

<div class="row mb-4 text-left">
  <div class="offset-1 col-1">Training</div>
  <div class="col-8">
    {!! $applicant->job->training !!}
  </div>
</div>

<div class="row mb-4 text-left">
  <div class="col-2"><b>Evaluation Made as of:</b></div>
  <div class="col-3">: {{@$applicant->evaluation->created_at}}</div>
</div>

<div class="row">
  <div class="col text-light bg-secondary">I. PERFORMANCE (40%)</div>
</div>

<div class="form-group row">
  <div class="col">For Transferees:</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->performance }}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Rating 1 + Rating 2</div>
  </div>

  <label for=""><h4 class="mt-1">/</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_divide }}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_average }}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="offset-1"><h4 class="mt-1">X</h4></div>
  <div class="col text-center">
    {{ @$applicant->evaluation->performance_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    <span class="font-weight-bold">{{ @$applicant->evaluation->performance_score}} %</span>
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<div class="row">
  <div class="col">
    <label for="">* For Non-Transferee, a grade of Satisfactory rating is given.</label>
  </div>
</div>

<!-- Education & Training -->
<div class="row">
  <div class="col text-light bg-secondary">II. EDUCATION & TRAINING (20%)</div>
</div>

<div class="row">
  {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-3 mt-2']) }}
  <div class="col-4">
    {{ @$applicant->evaluation->eligibility}}
  </div>
</div>

<div class="row">
  {{ Form::label('training', 'Training', ['class'=>'col-3 mt-2']) }}
  <div class="col-4">
    {{ @$applicant->evaluation->training}}
  </div>
</div>

<div class="row">
  {{ Form::label('Seminar', 'Seminar', ['class'=>'col-3 mt-2']) }}
  <div class="col-4">
    {{ @$applicant->evaluation->seminar}}
  </div>
</div>

<div class="row">
  {{ Form::label('minimum_education_points', 'Minimum Educational Requirement', ['class'=>'col-3 mt-1']) }}
  <div class="col-2">
    <p>{{ @$applicant->evaluation->minimum_education_points}} PTS</p>
  </div>
</div>

<div class=" row">
  {{ Form::label('minimum_training_points', 'Minimum Training Requirement', ['class'=>'col-3 mt-1']) }}
  <div class="col-2">
   <p>{{ @$applicant->evaluation->minimum_training_points}} PTS</p>
  </div>
</div>

<div class="row">
  {{ Form::label('ratings_excess', 'Ratings in Excess of the Minimum:', ['class'=>'col-4 mt-1 font-weight-bold']) }}
</div>

<div class="row">
  {{ Form::label('education_points', 'Education', ['class'=>'col-2 mt-2']) }}
  <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
  <div class="col-2 text-center">
    {{ @$applicant->evaluation->education_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Points Rating</div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('training_points', 'Training', ['class'=>'col-2 mt-2']) }}
  <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->training_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Points Rating</div>
  </div>

  <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->education_training_total_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Total Points</div>
  </div>

  <div class="offset-1"><h4 class="mt-1">X</h4></div>
  <div class="col text-center">
    {{ @$applicant->evaluation->education_training_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    <span class="font-weight-bold">{{ @$applicant->evaluation->education_training_score}} %</span>
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Experience & Outstanding Accomplishments -->
<div class="form-group row">
  <div class="col text-light bg-secondary">III. EXPERIENCE & OUTSTANDING ACCOMPLISHMENTS (20%)</div>
</div>

<div class="row">
  {{ Form::label('relevant_positions_held', 'Relevant Positions Held', ['class'=>'col-3 mt-1']) }}
  <div class="col-4">
    <p>{{ @$applicant->evaluation->relevant_positions_held}}</p>
  </div>
</div>

<div class="row">
  {{ Form::label('minimum_experience_requirement', 'Minimum Experience Requirement', ['class'=>'col-3 mt-1']) }}
  <div class="col-4">
    <p>{{ @$applicant->evaluation->minimum_experience_requirement}} PTS</p>
  </div>
</div>

<div class="row">
  {{ Form::label('additional_points', 'Additional Points (in excess of the minimum requirement)', ['class'=>'col-3 mt-1']) }}
  <div class="col-3">
    <p>{{ @$applicant->evaluation->additional_points}} PTS</p>
  </div>
</div>

<div class="row">

  {{ Form::label('', '', ['class'=>'col-2 mt-2']) }}
  <div class="col text-center">
  </div>

  <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->experience_accomplishments_total_points}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Total Points</div>
  </div>

  <div class="offset-1"><h4 class="mt-1">X</h4></div>
  <div class="col text-center">
    {{ @$applicant->evaluation->experience_accomplishments_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    <span class="font-weight-bold">{{ @$applicant->evaluation->experience_accomplishments_score}} %</span>
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Potential -->
<div class="row">
  <div class="col text-light bg-secondary">IV. POTENTIAL (10%)</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->potential}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">R1 + R2 + R3 + R4 + R5</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->potential_average_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->potential_percentage_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Percentage Rating</div>
  </div>

  <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->potential_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    <span class="font-weight-bold">{{ @$applicant->evaluation->potential_score}} %</span>
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<!-- Potential -->
<div class="form-group row">
  <div class="col text-light bg-secondary">V. PSYCHOSOCIAL ATTRIBUTES & PERSONALITY TRAITS (10%)</div>
</div>

<div class="form-group row">
  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">R1 + R2 + R3 + R4 + R5</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_average_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Average Rating</div>
  </div>

  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_percentage_rating}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">Percentage Rating</div>
  </div>

  <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
  <div class="col text-center">
    {{ @$applicant->evaluation->psychosocial_percent}}
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">POINTS WEIGHT</div>
  </div>

  <label for=""><h4 class="mt-1">=</h4></label>
  <div class="col text-center">
    <span class="font-weight-bold">{{ @$applicant->evaluation->psychosocial_score}} %</span>
    <div class="border border-top-1 border-bottom-0 border-right-0 border-left-0 border-dark">&nbsp;</div>
  </div>
</div>

<hr>

<div class="form-group row">
  <div class="col-8 text-light bg-secondary">TOTAL</div>
  <div class="col text-light bg-secondary">{{ @$applicant->evaluation->total_percent}}</div>
  <div class="col-2 text-center text-light bg-secondary">{{ @$applicant->evaluation->total_score}} %</div>
</div>

<!-- <div class="form-group row text-center">
  {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
  {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
  {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col mt-2']) }}
</div> -->

<!-- <div class="form-group row text-center">
  <div class="col">
    {{ @$evaluation->evaluated_by}}
    <hr>
  </div>
  <div class="col">
    {{ @$evaluation->reviewed_by}}
    <hr>
  </div>
  <div class="col">
    {{  @$evaluation->noted_by}}
    <hr>
  </div>
</div> -->
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection