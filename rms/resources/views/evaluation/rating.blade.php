@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Individual Assesment</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <div class="row">
                        <div class="col">
                            <span class="text-bold"><h4>You are evaluating applicant: {{$applicant->getFullname()}}</h4></span>
                        </div>
                        <div class="tools mt-2">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle"
                                    aria-expanded="false">
                                <i class="icon icon-left mdi mdi-settings-square"> </i>
                                Options
                                <span class="icon-dropdown mdi mdi-chevron-down"> </span>
                            </button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="#" data-modal="md-flipH" class="dropdown-item md-trigger"><i class="icon icon-left mdi mdi-eye"></i>
                                    View MSP
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('evaluation._form-rating', [
                        'action' => $action,
                        'method' => 'POST',
                        'evaluation' => $evaluation
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection