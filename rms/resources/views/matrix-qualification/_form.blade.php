@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-form']) !!}

<div class="form-group row">
  {{ Form::label('', 'Position', ['class'=>'col-12 col-sm-1 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <select name="job_id" class="select2 select2-sm" id="select_position">
        <option value="0">Selet position</option>
        @foreach($jobs as $job)
        <option value="{{ $job->id }}" 
          data-education="{!! @$job->education !!}"
          data-experience="{!! @$job->experience !!}"
          data-training="{!! @$job->training !!}"
          data-eligibility="{!! @$job->eligibility !!}"
          data-office="{!! @$job->psipop->office->Name !!}"
          {{ ($job->id == @$matrix->job_id) ? 'selected' :  '' }}
          >
          {{ $job->psipop->position->Name}}
        </option>
        @endforeach
    </select>
  </div>
  
    {{ Form::label('', 'Division/Office', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-5">
      <div class="col-12 col-sm-8 col-lg-7">
        <input
          size="16"
          type="text"
          value=""
          class="form-control form-control-sm"
          placeholder="Division/Office"
          id="office"
          readonly
        >
      </div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('', 'Applicant', ['class'=>'col-12 col-sm-1 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <select name="applicant_id" class="select2 select2-sm" id="select_applicant"></select>
  </div>
</div>

<div class="row">
  <div class="col-1"></div>
  <div class="col-4">
    <h5 class="font-weight-bold">Applicant Qualifications</h5>
  </div>
  <div class="col-1"></div>
  <div class="col-5">
    <h5 class="font-weight-bold">Required Qualifications</h5>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <div class="form-group row">
      {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
      <div class="col-10">
        <div id="education"></div>
      </div>
    </div>

    <div class="form-group row">
      {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
      <div class="col-10">
        <div id="experience"></div>
      </div>
    </div>

    <div class="form-group row">
      {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
      <div class="col-10">
        <div id="training"></div>
      </div>
    </div>

    
    <div class="form-group row">
      {{ Form::label('', 'Total Hours', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
      <div class="col-4">
        <input
          size="16"
          type="text"
          value="{{ @$matrix->total_hours }}"
          class="form-control form-control-sm"
          placeholder="0.00"
          id="total_hours"
          name="total_hours"
        >
      </div>
    </div>

    <div class="form-group row">
      {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
      <div class="col-10">
        <div id="eligibility"></div>
      </div>
    </div>
  </div> 

  <div class="col-6">
    <div class="form-group row">
      <div class="col-10">
        <div id="job_education"></div>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-10">
        <div id="job_experience"></div>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-10">
        <div id="job_training"></div>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-10" style="padding-bottom: 18px;">
        &nbsp;
      </div>
    </div>

    <div class="form-group row">
      <div class="col-10">
        <div id="job_eligibility"></div>
      </div>
    </div>
  </div> 
</div>

<div class="row">
  <div class="col-6">
    <div class="row form-group">
      {{ Form::label('', 'IPS Rating', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-5">
          <input
            size="16"
            type="text"
            value="{{ @$matrix->ips_rating }}"
            class="form-control form-control-sm"
            placeholder="IPS Rating"
            name="ips_rating"
          >
        </div>

        <div class="col-12 col-sm-8 col-lg-5">
          <select class="select2 select2-sm" name="semester">
            <option>Select semester</option>
            <option value="1st Semester" {{ (@$matrix->semester == '1st Semester') ? 'selected' : '' }} >1st Semester</option>
            <option value="2nd Semester" {{ (@$matrix->semester == '2nd Semester') ? 'selected' : '' }} >2nd Semester</option>
          </select>
        </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <div class="row form-group">
      <div class="col-12 col-sm-2"></div>
      <div class="col-12 col-sm-8 col-lg-5">
        <select class="select2 select2-sm" name="matrix_year">
          <option>Select year</option>
          @foreach(range($latest_year,$earliest_year) as $i)
          <option value="{{$i}}" {{ ($i == @$matrix->matrix_year) ? 'selected' : '' }}>{{$i}}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <div class="form-group row">
      {{ Form::label('', 'Remarks', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
      <div class="col-10">
        <textarea name="remarks" rows="5" cols="60">
          {!! @$matrix->remarks !!}
        </textarea>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <div class="row form-group">
      {{ Form::label('', 'Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-5">
          <select class="select2 select2-sm" name="status">
            <option value="0">Select status</option>
            <option value="1" {{ (@$matrix->status == 1) ? 'selected' : '' }} >For Schedule of Interview</option>
          </select>
        </div>
    </div>
  </div>
</div>


<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <!-- wysiwyg -->
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      // instansiate summernote(wysiwyg) editor
      const $education = $('#education');
      const $experience = $('#experience');
      const $eligibility = $('#eligibility');
      const $training = $('#training');

      const $jobEducation = $('#job_education');
      const $jobExperience = $('#job_experience');
      const $jobEligibility = $('#job_eligibility');
      const $jobTraining = $('#job_training');
      const $remarks = $('#remarks');

      // place data into summernote(wysiwyg) editor
      $education.summernote('code', ``);
      $experience.summernote('code', ``);
      $eligibility.summernote('code', ``);
      $training.summernote('code', ``);
      

      $education.summernote('disable');
      $experience.summernote('disable');
      $eligibility.summernote('disable');
      $training.summernote('disable');

      $education.summernote({ height: 10 });
      $experience.summernote('disable');
      $eligibility.summernote('disable');
      $training.summernote('disable');

      $jobEducation.summernote('disable');
      $jobExperience.summernote('disable');
      $jobEligibility.summernote('disable');
      $jobTraining.summernote('disable');


      // remove tools on summernote(wysiwyg) editor
      $('.note-toolbar').remove();
      $('.note-editable').css('height', '4rem');

      $applicantId = `{!! @$matrix->applicant_id !!}`;

      $('#select_position').change(function() {
        jobId = $(this).find(':selected').val();

        $.ajax({
          url:`{!! route('matrix-qualification.get-applicant') !!}`,
          data:{
            'job_id':jobId
          },
          type:'GET',
          dataType:'JSON',
          success:function(res){

            option = [];
            option += `<option>Select applicant</option>`;
            $.each(res.applicants, function(k,v) {
              firstName = (v.first_name) ? v.first_name : '';
              middleName = (v.middle_name) ? v.middle_name : '';
              lastName = (v.last_name) ? v.last_name : '';
              fullname = firstName+' '+middleName+' '+lastName;
              if($applicantId == v.id){
                option += `<option value="${v.id}" selected  >${fullname}</option>`;
              }else{
                option += `<option value="${v.id}"  >${fullname}</option>`;
              }
            });
            $('#select_applicant').html(option);

          }
        });

        education   = $(this).find(':selected').data('education');
        experience  = $(this).find(':selected').data('experience');
        training    = $(this).find(':selected').data('training');
        eligibility = $(this).find(':selected').data('eligibility');
        office      = $(this).find(':selected').data('office');

        $('#office').val(office);
        $jobEducation.summernote('code', education);
        $jobExperience.summernote('code', experience);
        $jobTraining.summernote('code', training);
        $jobEligibility.summernote('code', eligibility);
      });

      $('#select_position').trigger('change');
    });
  </script>
@endsection
