@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Matrix of Qualification</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit a matrix of qualification in the form below.</span>
                </div>
                <div class="card-body">
                    @include('matrix-qualification._form', [
                        'action' => ['MatrixQualificationController@update', $matrix->id],
                        'method' => 'PATCH',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
