@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Create Access Module</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an access module in the form below.</span></div>
                <div class="card-body">
                    @include('access_modules._form', [
                        'action' => 'AccessModuleController@store',
                        'method' => 'POST',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
