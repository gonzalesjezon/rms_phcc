@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
     <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection


{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="row">
    <div class="col-6">
        <div class="form-group row">
          {{ Form::label('', 'Position', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
          <div class="col-12 col-sm-8 col-lg-6">
            <select name="job_id" class="select2 select2-sm" id="select_position">
                <option value="0">Selet position</option>
                @foreach($jobs as $job)
                <option value="{{ $job->id }}" 
                  data-education="{!! @$job->education !!}"
                  data-experience="{!! @$job->experience !!}"
                  data-training="{!! @$job->training !!}"
                  data-eligibility="{!! @$job->eligibility !!}"
                  data-office="{!! @$job->psipop->office->Name !!}"
                  {{ ($job->id == @$erasure->applicant->job_id) ? 'selected' :  '' }}
                  >
                  {{ $job->psipop->position->Name}}
                </option>
                @endforeach
            </select>
          </div>
        </div>

        <div class="form-group row">
          {{ Form::label('', 'Applicant', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
          <div class="col-12 col-sm-8 col-lg-6">
            <select name="applicant_id" class="select2 select2-sm" id="select_applicant"></select>
          </div>
        </div>
    </div>
</div>

<div class="form-group row">
   {{ Form::label('', 'ERASURE(S)/ALTERATION(S) MADE', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="row">
    <div class="col-2"></div>
    <div class="col-3"><label class="text-sm-right">From Date </label></div>
    <div class="col-3"><label class="text-sm-right">To Date </label></div>
</div>

<div class="row form-group" >
    <div class="col-2"></div>
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$erasure->from_date}}" name="from_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$erasure->to_date}}" name="to_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('particulars') ? 'has-error' : ''}}">
            {{ Form::label('particulars', 'Particular', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="particulars" name="particulars"></div>
                <textarea name="particulars" class="d-none"></textarea>
                <input type="hidden" name="particulars" id="particulars-text">
                {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Appointing Officer:', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('appointing_officer', @$erasure->appointing_officer, [
                'class' => 'form-control form-control-sm',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Date Sign:', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{@$erasure->sign_date}}" name="sign_date"
                   class="form-control form-control-sm"
                   required="true"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation

        let data = {
          '#particulars': `{!! @$erasure->particulars !!}`,
        };


        // initialize editors for each data element
        App.textEditors(Object.keys(data));

        // when validation fails, get data from hidden input texts
        // and set as value for wysiwyg editor
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            if (data[key] === '') {
              data[key] = $(`${key}-text`).html();
            }
            setData(key, data[key]);
          }
        }

        // set data for wysiwyg editors
        function setData(selector = '', data = '') {
          $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
        }

        $('.note-toolbar').remove();
        // on form submit, get data from wysiwyg editors
        // pass to hidden input elements
        $('#job-submit').click(function() {
          for (let key in data) {
            if (data.hasOwnProperty(key)) {
              let element = getData(key);
              $(`${key}-text`).val(element);
            }
          }
        });

        function getData(selector = '') {
          let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
          return data.html();
        }

       $applicantId = `{!! @$erasure->applicant_id !!}`;

        $('#select_position').change(function() {
          jobId = $(this).find(':selected').val();

          $.ajax({
            url:`{!! route('erasure_alterations.get-applicant') !!}`,
            data:{
              'job_id':jobId
            },
            type:'GET',
            dataType:'JSON',
            success:function(res){

              option = [];
              option += `<option>Select applicant</option>`;
              $.each(res.applicants, function(k,v) {
                firstName = (v.first_name) ? v.first_name : '';
                middleName = (v.middle_name) ? v.middle_name : '';
                lastName = (v.last_name) ? v.last_name : '';
                fullname = firstName+' '+middleName+' '+lastName;
                if($applicantId == v.id){
                  option += `<option value="${v.id}" data-email="${v.email_address}" selected  >${fullname}</option>`;
                }else{
                  option += `<option value="${v.id}" data-email="${v.email_address}" >${fullname}</option>`;
                }
              });
              $('#select_applicant').html(option);

            }
          });

        });

        $('#select_position').trigger('change');
      });
    </script>
@endsection
