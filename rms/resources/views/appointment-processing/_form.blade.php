@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
 <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
<style type="text/css">
    .select2-container--default .select2-selection--single{
        height: 3rem !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 3.13816rem !important;
        font-size: 1rem !important;
        height: 2rem !important;
    }

      .v-top{
            vertical-align: top !important;
        }
</style>

@endsection

@if(@$applicants)

{!! Form::open(['action' => $getAction, 'method' => 'GET', 'id' => 'selection-form']) !!}

<div class="form-group row">
    <div class="col-3">
        {{ Form::label('applicant', 'Appointment Checklists for Applicant', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
    <div class="col-3">
        <select class="select2 select2-sm" name="app_id" id="applicant_id">
            <option>Select applicant</option>
            @foreach($applicants as $applicant)
            <option value="{{ $applicant->id }}"
                data-position="{{ $applicant->job->psipop->position->Name }}"
                data-office="{{ $applicant->job->psipop->office->Name }}"
                data-item="{{ $applicant->job->psipop->item_number }}"
                data-job_grade="{{ config('params.job_grades.'.$applicant->job->psipop->job_grade) }}"
                data-salary="{{ number_format($applicant->job->psipop->basic_salary ,2) }}"
                data-publish_date="{{ date('m/d/Y', strtotime($applicant->job->publish_date)) }}"
                data-deadline_date="{{ date('m/d/Y', strtotime($applicant->job->deadline_date)) }}"
                data-empstatus="{{ config('params.employee_status.'.@$applicant->appointment_form->employee_status_id) }}"
                data-publication="{{ config('params.publication.'.$applicant->publication) }}"
                {{ ($applicant->id == @$app_selected->id) ? 'selected' : '' }}
                >
                {{ $applicant->getFullName() }}</option>
            @endforeach
        </select>
    </div>
</div>

@else

<div class="form-group row">
    <div class="col-3">
        {{ Form::label('applicant', 'Appointment Checklists for Applicant', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
    <div class="col-3">
        <input type="text" class="form-control form-control-sm" value="{{ @$processing->applicant->getFullName() }}" readonly>
    </div>
</div>

@endif

{!! Form::close() !!}

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}


<?php
    $data = "";
    if(@$app_selected){
        $data = $app_selected;
    }else{
        $data = @$processing->applicant;
    }

?>

<input type="hidden" name="applicant_id" value="{{ @$app_selected->id }}">
<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="p-1 font-weight-bold">Qualification Standards</td>
                    <td style="background-color: #d0caca;" colspan="4" class="p-0">&nbsp;</td>
                </tr>
                <tr class="text-center">
                    <td>Criteria</td>
                    <td>Requirements</td>
                    <td style="width: 18em;">Appointee's Qualification <br> (Provide Specific Details)</td>
                    <td>QS MET</td>
                    <td>Remarks</td>
                </tr>
                <tr>
                    <td class="v-top">Education</td>
                    <td class="v-top">
                        {!! 
                            @$data->job->education 
                        !!}
                    </td> 
                    <td class="v-top">
                        @if(@$data->getHighestEduc)
                            @foreach(@$data->getHighestEduc as $value)
                            {!! $value->course !!} {!! $value->school_name !!} {!! ($value->ongoing == 1) ? '(ongoing)' : '' !!}
                            @endforeach
                        @endif
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if(@$processing->educ_check == 1)
                                    <input type="checkbox" name="educ_check" id="swt0" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="educ_check" id="swt0"><span>
                                    @endif
                                  <label for="swt0"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="educ_remarks" class="form-control form-control-xs" rows="10">{{@$processing->educ_remarks}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td class="v-top">Experience</td>
                    <td class="v-top">
                        {!! @$data->job->experience !!}
                    </td>
                    <td class="v-top">
                        
                            @if(@$data->workexperience)
                                <?php
                                  $subYrExp       = 0;
                                  $subMonExp      = 0;
                                  $subYrExpPriv   = 0;
                                  $subMonExpPriv  = 0;

                                  $totalYrExp    = 0;
                                  $totalMonthExp = 0;

                                  $govPos     = [];
                                  $privatePos = [];
                                ?>

                                @foreach(@$data->workexperience as $key => $value)
                                
                                  <?php
                                      $date1 = new DateTime($value->inclusive_date_from);
                                      $toDate = ($value->present_work == 1) ? date('Y-m-d') : $value->inclusive_date_to;
                                      $date2 = new DateTime($toDate);

                                      $interval = $date2->diff($date1);

                                      $yearExp = $interval->format('%y');
                                      $monthExp = $interval->format('%m');

                                      if($value->govt_service == 1){
                                        $subYrExp += $yearExp;
                                        $subMonExp += $monthExp;
                                        $govPos[$key] = $value->position_title.' ('.$yearExp.' year, '.$monthExp.' months)';

                                      }else{

                                        $subYrExpPriv += $yearExp;
                                        $subMonExpPriv += $monthExp;
                                        $privatePos[$key] = $value->position_title.' ('.$yearExp.' year, '.$monthExp.' months)';

                                      }

                                      $totalMonthExp  = $subMonExp;
                                      $totalYrExp = $subYrExp;

                                      if($totalMonthExp >= 12){
                                        $totalYrExp += 1;
                                        $totalMonthExp -=12;
                                      }

                                  ?>

                                @endforeach

                                <!-- Government -->
                                @if(count($govPos) > 0)
                                <!-- List the government position -->
                                    @foreach($govPos as $value)
                                    {!! $value !!}
                                    @endforeach

                                @endif

                                <br>

                            @endif
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$processing->exp_check == 1)
                                <input type="checkbox" name="exp_check" id="swt1" checked="true"><span>
                                @else
                                <input type="checkbox" name="exp_check" id="swt1"><span>
                                @endif
                                  <label for="swt1"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="exp_remarks" class="form-control form-control-xs" rows="10">{!! @$processing->exp_remarks !!}</textarea>
                    </td>
                </tr>
                <tr>
                    <td class="v-top">Training</td>
                    <td class="v-top">
                        {!! @$data->job->training !!}
                    </td>
                    <td class="v-top">
                        {!! @@$data->matrix->training_remarks!!}
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if(@$processing->training_check == 1)
                                    <input type="checkbox" name="training_check" id="swt2" checked="true"><span>
                                    @else
                                    <input type="checkbox" name="training_check" id="swt2"><span>
                                    @endif
                                  <label for="swt2"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="training_remarks" class="form-control form-control-xs" rows="10">{{@$processing->training_remarks}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td class="v-top">Eligibility</td>
                    <td class="v-top">
                        {!! @$data->job->eligibility !!}
                    </td>
                    <td class="v-top">
                        @if(@$data->eligibility)
                            @foreach(@$data->eligibility as $value)
                            <span class="pl-2">{!! $value->eligibility_ref  !!}</span> <br>
                            @endforeach
                        @endif
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$processing->eligibility_check == 1)
                                  <input type="checkbox" name="eligibility_check" id="swt3" checked="true"><span>
                                @else
                                    <input type="checkbox" name="eligibility_check" id="swt3"><span>
                                @endif
                                  <label for="swt3"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="eligibility_remarks" class="form-control form-control-xs" rows="10">{!! @$processing->eligibility_remarks !!}</textarea>
                    </td>
                </tr>
                <tr>
                    <td class="v-top">Others if applicable <br> (e.g., Age, Term of Office)</td>
                    <td class="v-top"></td>
                    <td>
                        <textarea name="other_qualification" class="form-control form-control-xs" rows="10">{!! @$processing->other_qualification !!}</textarea>
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-2">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$processing->other_check)
                                <input type="checkbox" name="other_check" id="swt4" checked="true"><span>
                                @else
                                <input type="checkbox" name="other_check" id="swt4"><span>
                                @endif
                                  <label for="swt4"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                        <textarea name="other_remarks" class="form-control form-control-xs" rows="10">{!! @$processing->other_remarks !!}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="p-1 font-weight-bold" style="width: 35em;">Common Requirements for Regular Appointments</td>
                    <td style="background-color: #d0caca;" class="p-0">&nbsp;</td>
                </tr>
                <tr class="text-center">
                    <td>Requirements</td>
                    <td>Detail/Compliance</td>
                </tr>
                <tr>
                    <td>CS Form 33-A (revised 2017) in triplicate copies</td>
                    <td>
                        <textarea name="ra_form_33" class="form-control form-control-xs">{{@$processing->ra_form_33}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Employement Status</td>
                    <td>
                        <textarea name="ra_employee_status" class="form-control form-control-xs">{{@$processing->ra_employee_status}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Nature of Appointment</td>
                    <td>
                        <textarea name="ra_nature_appointment" class="form-control form-control-xs">{{@$processing->ra_nature_appointment}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Appointing Authority</td>
                    <td>
                        <textarea name="ra_appointing_authority" class="form-control form-control-xs">{{@$processing->ra_appointing_authority}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Date of Signing</td>
                    <td>
                        <div class="form-group row">
                            <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-8">
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <input size="16" type="text" name="ra_date_sign"
                                           class="form-control form-control-sm" value="{{@$processing->ra_date_sign}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Date of Pulbication/Posting of Vacant Position</td>
                    <td>
                        <div class="form-group row">
                            <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-8">
                                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                    <input size="16" type="text" name="ra_date_publication"
                                           class="form-control form-control-sm" value="{{@$processing->ra_date_publication}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Certification by PSB Chairman (at the back of appoitnment) or a copy of the proceedings of PSB's Deliberation</td>
                    <td>
                        <textarea name="ra_certification" class="form-control form-control-xs">{{@$processing->ra_certification}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Personal Data Sheet (ra Form 212, Revised 2017) Completely Filled with Picture Attached</td>
                    <td>
                        <textarea name="ra_pds" class="form-control form-control-xs">{{@$processing->ra_pds}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Certificate of Eligiblity/License (Authenticated Copy)</td>
                    <td>
                        <textarea name="ra_eligibility" class="form-control form-control-xs">{{@$processing->ra_eligibility}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Position Description Form (PDF)</td>
                    <td>
                        <textarea name="ra_position_description" class="form-control form-control-xs">{{@$processing->ra_position_description}}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="p-1 font-weight-bold" style="width: 35em;">Additional and Specific Cases</td>
                    <td style="background-color: #d0caca;" class="p-0">&nbsp;</td>
                </tr>
                <tr class="text-center">
                    <td>Requirements</td>
                    <td>Detail/Compliance</td>
                </tr>
                <tr>
                    <td>
                        Erasures/alterations on the appointment and other supporting documents (Changes duly initialed by authorized officials and accompanies by a communication authenticating changes made)
                    </td>
                    <td>
                        <textarea name="ar_01" class="form-control form-control-xs">{{@$processing->ar_01}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Appointee with decided administrative/criminal case (certified true copy of decision rendered submitted)</td>
                    <td>
                        <textarea name="ar_02" class="form-control form-control-xs">{{@$processing->ar_02}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Discrepancy in name/place of birth (Requirements and procedures as amended by CSC Resolution No,991907 dated August 27, 1999)</td>
                    <td>
                        <textarea name="ar_03" class="form-control form-control-xs">{{@$processing->ar_03}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>COMELEC Ban (Exemption from COMELEC)</td>
                    <td>
                        <textarea name="ar_04" class="form-control form-control-xs">{{@$processing->ar_04}}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        Non-Disciplinary Demotion
                        <ul>
                            <li>Certification of the Agency Head that demotion is not a result of an administrative case</li>
                            <li>Written consent by the employee interposing no object to the demotion</li>
                        </ul>
                    </td>
                    <td>
                        <textarea name="ar_05" class="form-control form-control-xs">{{@$processing->ar_05}}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();

        $('#applicant_id').change(function() {
            $('#selection-form').submit();
        });
      });
    </script>
@endsection
