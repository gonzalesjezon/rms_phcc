@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit {{ $title }}</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit the appointment processing in the form below.</span>
                </div>
                <div class="card-body">
                    @include('appointment-processing._form', [
                        'action' => ['AppointmentProcessingController@update', $processing->id],
                        'method' => 'PATCH',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
