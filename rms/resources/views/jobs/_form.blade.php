@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method ,'id' => 'create-form']) !!}
<div class="row">
    <div class="col-6">

        <div class="form-group row {{ $errors->has('plantilla_item_id') ? 'has-error' : ''}}">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="psipop_id" id="psipop_id" 
                {{ (@$job->psipop_id) ? 'disabled' : '' }}>
                    <option value="0">Select position</option>
                    @foreach($psipops as $psipop)
                    <option value="{{ $psipop->id }}" 
                        data-empstatus="{{ $psipop->employment_status->Name }}"
                        data-office="{{ $psipop->office->Name }}"
                        data-division="{{ $psipop->division->Name }}"
                        data-department="{{ @$psipop->department->Name }}"
                        data-jobgrade="{{ config('params.job_grades.'.$psipop->job_grade) }}"
                        data-basic_salary="{{ number_format($psipop->basic_salary,2) }}"
                        data-annual_salary="{{ number_format($psipop->basic_salary * 12,2) }}"
                        {{ ($psipop->id == @$job->psipop_id) ? 'selected' : '' }} >
                        {!! strtoupper($psipop->position->Name) !!}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Employee Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '',[
                        'class' => 'form-control form-control-sm',
                        'readOnly' => true,
                        'id' => 'empstatus'

                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '',[
                        'class' => 'form-control form-control-sm',
                        'readOnly' => true,
                        'id' => 'office'

                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '',[
                        'class' => 'form-control form-control-sm',
                        'readOnly' => true,
                        'id' => 'division'

                    ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">

        <div class="form-group row">
            {{ Form::label('', 'Job Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$currentItem->job_grade->Name, [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true,
                    'id' => 'jobgrade'
                ])
                }}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('monthly_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('monthly_basic_salary', 'Monthly Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                <input type="text" name="monthly_basic_salary" 
                    id="basic_salary" 
                    class="form-control form-control-sm" 
                    placeholder="PHP 0"  
                    readonly>
            </div>
        </div>

        <div class="form-group row {{ $errors->has('annual_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('annual_basic_salary', 'Annual Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'PHP 0',
                    'readOnly' => true,
                    'id' => 'annual_basic_salary'
                ])
                }}
                {!! $errors->first('annual_basic_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        @if($status == 'non-plantilla')
        <div class="form-group row {{ $errors->has('daily_salary') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
            {{ Form::label('daily_salary', 'Daily Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('daily_salary', @$job->daily_salary, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Daily Salary',
                    'required' => false,
                    'id' => 'daily_salary'
                ])
                }}
                {!! $errors->first('daily_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
         @endif

         <div class="form-group row {{ $errors->has('reporting_line') ? 'has-error' : ''}}">
            {{ Form::label('reportling_line', 'Place of Assignment', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                <input type="text" name="reporting" id="reporting" value="{{@$job->reporting_line}}" class="form-control form-control-sm" >
                {!! $errors->first('reporting_line', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

    </div>
</div>

<hr>

<div class="row">
    <div class="col-6">

        <div class="form-group row ">
            <div class="col-10">
                {{ Form::label('', 'Benefits and Allowances :', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
            </div>
            <div class="col-2">
                {{ Form::label('', 'Other Compensation :', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('pera_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'PERA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('pera_amount', number_format(2000 * 12,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'pera_amount',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('pera_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('clothing_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Clothing Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('clothing_amount', number_format(6000,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'clothing_amount',
                    'readonly' => 'true',
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('clothing_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('midyear_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Mid-Year Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('midyear_amount', number_format(@$currentItem->SalaryAmount ,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'midyear_amount',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('midyear_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('yearend_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Year-End Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('yearend_amount', number_format(@$currentItem->SalaryAmount ,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'yearend_amount',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('yearend_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('cashgift_amount') ? 'has-error' : '' }} ">
            {{ Form::label('', 'Cash Gift', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('cashgift_amount', number_format(5000,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'cashgift_amount',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('cashgift_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

    </div>


    <div class="col-6">
        <div class="form-group row {{ $errors->has('representation_amount') ? 'has-error' : '' }} mt-8">
            {{ Form::label('', 'Representation', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('representation_amount', number_format(@$job->representation_amount,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'representation_amount',
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('representation_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('transportation_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Transportation', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('transportation_amount', number_format(@$job->transportation_amount,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'transportation_amount',
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('transportation_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('eme_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'EME', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('eme_amount', number_format(@$job->eme_amount,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'eme_amount',
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('rata_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('communication_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Communication', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('communication_amount', number_format(@$job->communication_amount,2),[
                    'class' => 'form-control form-control-sm',
                    'id' => 'communication_amount',
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('rata_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group row ">
    {{ Form::label('', 'DUTIES RESPONSIBILITIES:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="row ">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_1') ? 'has-error' : ''}}">
            {{ Form::label('compentency_1', 'General Functions', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_1" name="compentency_1"></div>
                <textarea name="compentency_1" class="d-none"></textarea>
                <input type="hidden" name="compentency_1" id="compentency_1-text">
                {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_2') ? 'has-error' : ''}}">
            <label class="col-12 col-sm-4 col-form-label text-sm-right">
                Specific Duties & <br> Responsibilities
            </label>
            <div class="col-8">
                <div id="compentency_2" name="compentency_2"></div>
                <textarea name="compentency_2" class="d-none"></textarea>
                <input type="hidden" name="compentency_2" id="compentency_2-text">
                {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="form-group row ">
    {{ Form::label('', 'PREFERRED QUALIFICATIONS:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('education') ? 'has-error' : ''}}">
            {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="education" name="education"></div>
                {{ Form::textarea('education', @$job->education,['id'=>'education-text', 'class'=>'d-none']) }}
                {!! $errors->first('education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('training') ? 'has-error' : ''}}">
            {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="training" name="training"></div>
                {{ Form::textarea('training', @$job->training,['id'=>'training-text', 'class'=>'d-none']) }}
                {!! $errors->first('training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('experience') ? 'has-error' : ''}}">
            {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="experience" name="experience"></div>
                {{ Form::textarea('experience', @$job->experience,['id'=>'experience-text', 'class'=>'d-none']) }}
                {!! $errors->first('experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('eligibility') ? 'has-error' : ''}}">
            {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="eligibility" name="eligibility"></div>
                {{ Form::textarea('eligibility', @$job->eligibility,['id'=>'eligibility-text', 'class'=>'d-none']) }}
                {!! $errors->first('eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group row ">
    {{ Form::label('', 'CSC MINIMUM QUALIFICATIONS:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('csc_education') ? 'has-error' : ''}}">
            {{ Form::label('csc_education', 'Education', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="csc_education" name="csc_education"></div>
                {{ Form::textarea('csc_education', @$job->csc_education,['id'=>'csc_education-text', 'class'=>'d-none']) }}
                {!! $errors->first('csc_education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('csc_training') ? 'has-error' : ''}}">
            {{ Form::label('csc_training', 'Training', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="csc_training" name="csc_training"></div>
                {{ Form::textarea('csc_training', @$job->csc_training,['id'=>'csc_training-text', 'class'=>'d-none']) }}
                {!! $errors->first('csc_training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('csc_experience') ? 'has-error' : ''}}">
            {{ Form::label('csc_experience', 'Experience', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="csc_experience" name="csc_experience"></div>
                {{ Form::textarea('csc_experience', @$job->csc_experience,['id'=>'csc_experience-text', 'class'=>'d-none']) }}
                {!! $errors->first('csc_experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('csc_eligibility') ? 'has-error' : ''}}">
            {{ Form::label('csc_eligibility', 'Eligibility', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="csc_eligibility" name="csc_eligibility"></div>
                {{ Form::textarea('csc_eligibility', @$job->csc_eligibility,['id'=>'csc_eligibility-text', 'class'=>'d-none']) }}
                {!! $errors->first('csc_eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-4">
        <div class="form-group row">
            <label class="col-12 col-sm-5 col-form-label text-sm-right font-weight-bold">Publish</label>
            <div class="col-12 col-sm-8 col-lg-6 pt-1">
                <div class="switch-button switch-button-success switch-button-yesno">
                    <input type="checkbox" name="publish" id="swt8" {{ @$job->publish ? 'checked' : '' }}><span>
                                        <label for="swt8"></label></span>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-12 col-sm-5 col-form-label text-sm-right"> Deadline Date </label>
            <div class="col-12 col-sm-7">
                <div data-min-view="2" data-date-format="mm/dd/yyyy" class="input-group date datetimepicker">
                    <input size="16" type="text" 
                            value="{{ date('m/d/Y', strtotime(@$job->deadline_date)) }}" 
                            name="deadline_date"
                           class="form-control form-control-sm"
                           placeholder="mm/dd/yyyy" 
                           >
                    <div class="input-group-append">
                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-12 col-sm-5 col-form-label text-sm-right"> Application <br> address to: </label>
            <div class="col-12 col-sm-7">
                <select class="select2 select2-sm" name="appointer_id">
                    <option value="0">Select</option>
                    @foreach($employeeinfo as $info)
                        @if(!empty($info->employee))
                        <option value="{{ $info->RefId }}" {{ ($info->RefId == @$job->appointer_id) ? 'selected' : '' }}>{!! $info->employee->getFullName() !!} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-7 col-lg-8">

        <div class="form-group row">
            <div class="col-12">
                <div class="row">
                    <label class="col-12 col-sm-2 col-form-label text-sm-right">PCC Website</label>
                    <div class="col-12 col-sm-8 col-lg-2 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_1" id="swt9" {{ @$job->publication_1 ? 'checked' : '' }}><span>
                                                <label for="swt9"></label></span>
                        </div>
                    </div>

                    <label class="col-12 col-sm-2 col-form-label text-left pl-0">Posted</label>
                    <div class="col-3">
                        <div data-min-view="2" data-date-format="mm/dd/yyyy" class="input-group date datetimepicker">
                            <input size="16" type="text" 
                                    value="{{ date('m/d/Y', strtotime(@$job->posted_from)) }}" 
                                    name="posted_from"
                                   class="form-control form-control-sm"
                                   placeholder="From">
                            <div class="input-group-append">
                                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="col-3">
                        <div data-min-view="2" data-date-format="mm/dd/yyyy" class="input-group date datetimepicker">
                            <input size="16" type="text" 
                                    value="{{ date('m/d/Y', strtotime(@$job->posted_to)) }}" 
                                    name="posted_to"
                                   class="form-control form-control-sm"
                                   placeholder="To">
                            <div class="input-group-append">
                                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Newspaper</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_2" id="swt10" {{ @$job->publication_2 ? 'checked' : '' }}><span>
                                                <label for="swt10"></label></span>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <label class="col-12 col-sm-2 col-form-label text-sm-right">CSC Bulletin</label>
                    <div class="col-12 col-sm-8 col-lg-2 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_3" id="swt11" {{ @$job->publication_3 ? 'checked' : '' }}><span>
                                                <label for="swt11"></label></span>
                        </div>
                    </div>
                    <label class="col-12 col-sm-2 col-form-label text-left pl-0">Approved Date</label>
                    <div class="col-3">
                        <div data-min-view="2" data-date-format="mm/dd/yyyy" class="input-group date datetimepicker">
                            <input size="16" type="text" 
                                value="{{ date('m/d/Y', strtotime(@$job->approved_date)) }}" 
                                name="approved_date"
                                class="form-control form-control-sm"
                                placeholder="mm/dd/yyyy" 
                                   >
                            <div class="input-group-append">
                                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-2 col-form-label text-sm-right">Other</label>
                    <div class="col-12 col-sm-8 col-lg-2 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_4" id="swt12" {{ @$job->publication_4 ? 'checked' : '' }}><span>
                                                <label for="swt12"></label></span>
                        </div>
                    </div>
                    <label class="col-12 col-sm-2 col-form-label text-left pl-0">Pls. Specify</label>
                    <div class="col-4">
                        <input type="text" name="other_specify" id="other_specify" value="{{@$job->other_specify}}" class="form-control form-control-sm"
                        placeholder="(Other Specify)" 
                         >
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<hr>

<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('jobs._form-script')
@endsection
