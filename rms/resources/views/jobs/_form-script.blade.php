<script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#job-form').parsley(); //frontend validation

    let data = {
      '#education': `{!! @$job->education !!}`,
      '#experience': `{!! @$job->experience !!}`,
      '#training': `{!! @$job->training !!}`,
      '#eligibility': `{!! @$job->eligibility !!}`,
      '#compentency_1': `{!! @$job->compentency_1 !!}`,
      '#compentency_2': `{!! @$job->compentency_2 !!}`,

      '#csc_education': `{!! @$job->csc_education !!}`,
      '#csc_experience': `{!! @$job->csc_experience !!}`,
      '#csc_training': `{!! @$job->csc_training !!}`,
      '#csc_eligibility': `{!! @$job->csc_eligibility !!}`,
    };

    // initialize editors for each data element
    App.textEditors(Object.keys(data));

    // when validation fails, get data from hidden input texts
    // and set as value for wysiwyg editor
    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        if (data[key] === '') {
          data[key] = $(`${key}-text`).html();
        }
        setData(key, data[key]);
      }
    }

    // set data for wysiwyg editors
    function setData(selector = '', data = '') {
      $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
    }

    $('.note-toolbar').remove();
    // on form submit, get data from wysiwyg editors
    // pass to hidden input elements
    $('#job-submit').click(function() {
      for (let key in data) {
        if (data.hasOwnProperty(key)) {
          let element = getData(key);
          $(`${key}-text`).val(element);
        }
      }
    });

    function getData(selector = '') {
      let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
      return data.html();
    }

    // delete form data on clear form
    $('#clear-form').click(function() {
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          getData(key, '');
        }
      }
    });

    // $('#station_line').on('keyup',function(){
    //     $('#station').val($(this).val());
    // })

    $(".onlyNumber").keyup(function(){
      amount  = $(this).val();
      if(amount == 0){
        $(this).val('');
      }else{
        plainAmount = amount.replace(/\,/g,'')
        $(this).val(commaSeparateNumber(plainAmount));
      }
    });

  });

  function computeAllowances(basic_amount){
    let peraAmount = $('#pera_amount').val().replace(/\,/g,'');
    let cgAmount = $('#cashgift_amount').val().replace(/\,/g,'');

    $('#midyear_amount').val(commaSeparateNumber(basic_amount));
    $("#yearend_amount").val(commaSeparateNumber(basic_amount));
  }

  $(document).on('change', '#psipop_id', function() {
    empStatus   = $(this).find(':selected').data('empstatus');
    office      = $(this).find(':selected').data('office');
    division    = $(this).find(':selected').data('division');
    department  = $(this).find(':selected').data('department');
    jobGrade    = $(this).find(':selected').data('jobgrade');
    basicSalary = $(this).find(':selected').data('basic_salary');
    annualSalary = $(this).find(':selected').data('annual_salary');

    $('#empstatus').val(empStatus);
    $('#office').val(office);
    $('#division').val(division);
    $('#department').val(department);
    $('#jobgrade').val(jobGrade);
    $('#basic_salary').val(basicSalary);
    $('#midyear_amount').val(basicSalary);
    $('#yearend_amount').val(basicSalary);
    $('#annual_basic_salary').val(annualSalary)
  });

  $('#psipop_id').trigger('change');
</script>