@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print {
    @page {
       size: 7in 9.25in;
       margin: 27mm 16mm 27mm 16mm;
    }
  }
</style>
@endsection

@section('content')
<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="width:960px;margin: auto; font-size: 12px;font-family: Times New Roman, serif;">
  <div class="row mb-1">
    <div class="col-6">
        <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
    </div>
    <div class="col-6 text-right">
        <span>
          <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
        North Avenue, Quezon City 1105 <br>
        <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
        <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
        </span>
    </div>
  </div>
  <div class="border-bottom border-dark mb-1"></div>

  <div class="row mb-6">
    <div class="col-3 font-weight-bold">CS Form No. 4 <br> Revised 2018</div>
    <div class="col-6"></div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h4 class="p-0 m-0 font-weight-bold">Republic of the Philippines</h4>
      <div style="font-size: 20pt;" class="font-weight-bold"><u>Philippine Competition Commission</u></div>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-12 text-center">
      <h3><b>CERTIFICATION OF ASSUMPTION TO DUTY</b></h3>
    </div>
  </div>

  <div class="row mb-2" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-9 text-justify">
      <p style="text-indent: 0.5in;line-height:0.7cm;">This is to certify that {!! ($assumption->applicant->gender == 'male') ?  'Mr.' : 'Ms.' !!} <u>{!! $assumption->applicant->getFullName() !!}</u> has assumed the duties and responsibilities as <u>{!! $assumption->applicant->job->psipop->position->Name !!}</u> of the <u>{!! $assumption->applicant->job->psipop->division->Name !!}</u>  effective <u>{!! date('F d, Y', strtotime($assumption->assumption_date)) !!}</u>.</p>
    </div>
  </div>

  <div class="row mb-2" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-9 text-justify">applicant
      <p style="text-indent: 0.5in;line-height:0.7cm;">This certification is issued in connection with the issuance of the appointment of Mr. Herrera as Attorney IV.</p>
    </div>
  </div>

  <div class="row mb-2" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-9 text-justify">
      <p style="text-indent: 0.5in;line-height:0.7cm;">Done this {{ date('d', time()) }}  day of {{ date('F Y', time()) }} in North Avenue, Quezon City.</p>
    </div>
  </div>

  <div style="height: 5em;"></div>

  <div class="row mb-1" style="font-size: 14pt;">
    <div class="col-6"></div>
    <div class="col-3"></div>
    <div class="col-3 text-center">{!! $assumption->head_of_office !!}</div>
  </div>

  <div class="row mb-4" style="font-size: 14pt;">
    <div class="col-7"></div>
    <div class="col-3 text-center border-top border-dark">Head Office/Department/Unit</div>
  </div>

  <div class="row form-group mb-8" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-1">Date: </div>
    <div class="border-bottom border-dark col-2 text-center">{!! $assumption->assumption_date !!}</div>
  </div>

  <div class="row mb-8" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-2 text-left">Attested By </div>
  </div>


  <div class="row mb-1" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-3 text-center">{!! $assumption->attested_by !!}</div>
  </div>

  <div class="row" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-3 text-center border-top border-dark font-weight-bold">HRMO</div>
  </div>

  <div style="height: 10em;"></div>

  <div class="row mb-1" style="font-size: 10pt;">
    <div class="col-1"></div>
    <div class="col-6" >
      201 File <br>
      Admin <br>
      COA <br>
      CSC
    </div>
  </div>

  <div class="row mb-4" style="font-size: 10pt;">
    <div class="col-8"></div>
    <div class="col-2 border border-dark font-weight-bold">
      <i>
        For submission to CSCFO
      with 30 days from the
      date of assumption of the appointee
      </i>
    </div>
  </div>


</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection