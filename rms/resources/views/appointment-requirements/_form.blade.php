@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
<style type="text/css">
    .select2-container--default .select2-selection--single{
        height: 3rem !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 3.13816rem !important;
        font-size: 1rem !important;
        height: 2rem !important;
    }
</style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form', 'files' => true]) !!}

<div class="form-group row">
    <div class="col-2">
        {{ Form::label('applicant', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
    <div class="col-3">
        
        <select class="select2 select2-sm" name="applicant_id" id="applicant_id" 
          {{ (@$requirment->applicant_id) ? 'disabled' : '' }}
        >
            <option>Select applicant</option>
            @foreach($applicants as $applicant)
            <option value="{{ $applicant->id }}"
                data-position="{{ $applicant->job->psipop->position->Name }}"
                data-office="{{ $applicant->job->psipop->office->Name }}"
                data-item="{{ $applicant->job->psipop->item_number }}"
                data-job_grade="{{ config('params.job_grades.'.$applicant->job->psipop->job_grade) }}"
                data-salary="{{ number_format($applicant->job->psipop->basic_salary ,2) }}"
                data-publish_date="{{ date('m/d/Y', strtotime($applicant->job->publish_date)) }}"
                data-deadline_date="{{ date('m/d/Y', strtotime($applicant->job->deadline_date)) }}"
                data-empstatus="{{ config('params.employee_status.'.@$applicant->appointment_form->employee_status_id) }}"
                data-publication="{{ config('params.publication.'.$applicant->publication) }}"
                data-form_status="{{ $applicant->appointment_form->form_status }}"
                {{ ($applicant->id == @$requirement->applicant_id) ? 'selected' : '' }}
                >
                {{ $applicant->getFullName() }}</option>
            @endforeach
        </select>
    </div>
</div>
<hr>
<div class="row">
  <div class="col-12">
    <h5 class="font-weight-bold p-0 m-0">REQUIREMENTS FOR ASSUMPTION TO DUTY AND PROCESSING OF CLAIMS FOR ORIGINAL APPOINTMENT/REEMPLOYMENT</h5>
  </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td></td>
                    <td class="font-weight-bold">Document </td>
                    <td class="font-weight-bold">No of Copies</td>
                    <td class="font-weight-bold">Submitted</td>
                </tr>
                
                <tr class="status-2 d-none">
                    <td>1</td>
                    <td class="font-weight-bold">Clearance Certificate</td>
                    <td>
                        <input type="text" name="clearance_copies" class="form-control form-control-sm" value="{{ @$requirement->clearance_copies}}">
                    </td>
                    <td >
                      <div class="col-12 col-sm-8 col-lg-6 offset-3">
                          <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$requirement->clearance_status == 1)
                                <input type="checkbox" name="clearance_status" id="swt1" checked="true"><span>
                                @else
                                <input type="checkbox" name="clearance_status" id="swt1"><span>
                                @endif
                              <label for="swt1"></label></span>
                          </div>
                        </div>
                    </td>
                </tr>
                <tr class="status-1 d-none">
                    <td>1</td>
                    <td class="font-weight-bold">Personal Data Sheet (include assumption to duty at PCC)</td>
                    <td>
                        <input type="text" name="pds_copies" class="form-control form-control-sm" value="{{ @$requirement->pds_copies}}">
                    </td>
                    <td >
                      <div class="col-12 col-sm-8 col-lg-6 offset-3">
                          <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$requirement->pds_status == 1)
                                <input type="checkbox" name="pds_status" id="swt2" checked="true"><span>
                                @else
                                <input type="checkbox" name="pds_status" id="swt2"><span>
                                @endif
                              <label for="swt2"></label></span>
                          </div>
                        </div>
                    </td>
                </tr>

                <tr class="status-2 d-none">
                    <td >2</td>
                    <td class="font-weight-bold"> Latest Personal Data Sheet (include assumption to duty at PCC)</td>
                    <td>
                        <input type="text" name="pds_copies" class="form-control form-control-sm" value="{{ @$requirement->pds_copies}}">
                    </td>
                    <td >
                      <div class="col-12 col-sm-8 col-lg-6 offset-3">
                          <div class="switch-button switch-button-success switch-button-yesno">
                                @if(@$requirement->pds_status == 1)
                                <input type="checkbox" name="pds_status" id="swt2" checked="true"><span>
                                @else
                                <input type="checkbox" name="pds_status" id="swt2"><span>
                                @endif
                              <label for="swt2"></label></span>
                          </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>2</td>
                    <td><b>CSC Form No. 211 (Medical Certificate) -with notation : Fit to Work</b></td>
                    <td>
                        <input type="text" name="medical_copies" class="form-control form-control-sm" value="{{ @$requirement->medical_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->medical_status == 1)
                                  <input type="checkbox" name="medical_status" id="swt3" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="medical_status" id="swt3"><span>
                                  @endif
                                <label for="swt3"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td><b>Oath of Office (Notarized)</b></td>
                    <td>
                        <input type="text" name="oath_copies" class="form-control form-control-sm" value="{{ @$requirement->oath_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->oath_status == 1)
                                  <input type="checkbox" name="oath_status" id="swt4" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="oath_status" id="swt4"><span>
                                  @endif
                                <label for="swt4"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="status-2 d-none">
                    <td>4</td>
                    <td><b>  Service Record (reflects record of transfer to PCC)</b></td>
                    <td>
                        <input type="text" name="service_record_copies" class="form-control form-control-sm" value="{{ @$requirement->service_record_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->service_record_status == 1)
                                  <input type="checkbox" name="service_record_status" id="swt5" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="service_record_status" id="swt5"><span>
                                  @endif
                                <label for="swt5"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="status-1 d-none">
                    <td>4</td>
                    <td><b> NBI Clearance</b></td>
                    <td>
                        <input type="text" name="nbi_copies" class="form-control form-control-sm" value="{{ @$requirement->nbi_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->nbi_status)
                                  <input type="checkbox" name="nbi_status" id="swt6" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="nbi_status" id="swt6"><span>
                                  @endif
                                <label for="swt6"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td>5</td>
                    <td><b>Certified true copy of pre-audited disbursement voucher of last salary from previous agency and/or Certification by the Chief Accountant of Last Salary received from </b></td>
                    <td>
                        <input type="text" name="disbursement_voucher_copies" class="form-control form-control-sm" value="{{ @$requirement->disbursement_voucher_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->disbursement_voucher_status)
                                  <input type="checkbox" name="disbursement_voucher_status" id="swt7" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="disbursement_voucher_status" id="swt7"><span>
                                  @endif
                                <label for="swt7"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>5</td>
                    <td><b>GSIS Enrolment Form</b></td>
                    <td>
                        <input type="text" name="gsis_enrollment_copies" class="form-control form-control-sm" value="{{ @$requirement->gsis_enrollment_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->gsis_enrollment_status)
                                  <input type="checkbox" name="gsis_enrollment_status" id="swt8" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="gsis_enrollment_status" id="swt8"><span>
                                  @endif
                                <label for="swt8"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td>6</td>
                    <td><b>  Certificate of other Allowances/Bonuses received for the year</b> <br> (Mid-Year Bonus, Year-End Bonus, Uniform Allowance, Productivity Enhancement Incentive, Performance-Based Bonus, etc.)</td>
                    <td>
                        <input type="text" name="certificate_allowance_copies" class="form-control form-control-sm" value="{{ @$requirement->certificate_allowance_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->certificate_allowance_status)
                                  <input type="checkbox" name="certificate_allowance_status" id="swt9" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="certificate_allowance_status" id="swt9"><span>
                                  @endif
                                <label for="swt9"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>6</td>
                    <td><b> BIR Form No. 2316 (Cumulative Income & Withholding Tax from Previous Employer) for the year</b></td>
                    <td>
                        <input type="text" name="bir_form_2316_copies" class="form-control form-control-sm" value="{{ @$requirement->bir_form_2316_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_form_2316_status)
                                  <input type="checkbox" name="bir_form_2316_status" id="swt10" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bir_form_2316_status" id="swt10"><span>
                                  @endif
                                <label for="swt10"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                
                <tr class="status-2 d-none">
                    <td>7</td>
                    <td><b> Certificate of Leave Balances and Availment/Non-Availment of Forced Leave and Special Leave Privilege</b></td>
                    <td>
                        <input type="text" name="cert_leave_balances_copies" class="form-control form-control-sm" value="{{ @$requirement->cert_leave_balances_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->cert_leave_balances_status)
                                  <input type="checkbox" name="cert_leave_balances_status" id="swt11" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="cert_leave_balances_status" id="swt11"><span>
                                  @endif
                                <label for="swt11"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>7</td>
                    <td><b>  BIR Form No. 2305 (Certificate of Update of Exemption and of Employer’s and Employee’s Information)</b></td>
                    <td>
                        <input type="text" name="bir_form_2305_copies" class="form-control form-control-sm" value="{{ @$requirement->bir_form_2305_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_form_2305_status)
                                  <input type="checkbox" name="bir_form_2305_status" id="swt12" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bir_form_2305_status" id="swt12"><span>
                                  @endif
                                <label for="swt12"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                
                <tr class="status-2 d-none">
                    <td>8</td>
                    <td><b> BIR Form No. 2316 (Cumulative Income & Withholding Tax from Previous Employer) for the year</b></td>
                    <td>
                        <input type="text" name="bir_form_2316_copies" class="form-control form-control-sm" value="{{ @$requirement->bir_form_2316_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_form_2316_status)
                                  <input type="checkbox" name="bir_form_2316_status" id="swt13" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bir_form_2316_status" id="swt13"><span>
                                  @endif
                                <label for="swt13"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>8</td>
                    <td><b>  BIR Form No. 1905 (Application for Registration Information Update)</b></td>
                    <td>
                        <input type="text" name="bir_form_1905_copies" class="form-control form-control-sm" value="{{ @$requirement->bir_form_1905_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_form_1905_status)
                                  <input type="checkbox" name="bir_form_1905_status" id="swt14" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bir_form_1905_status" id="swt14"><span>
                                  @endif
                                <label for="swt14"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                
                <tr class="status-2 d-none">
                    <td>9</td>
                    <td><b>  BIR Form No. 2305 (Certificate of Update of Exemption and of Employer’s and Employee’s Information)</b></td>
                    <td>
                        <input type="text" name="bir_form_2305_copies" class="form-control form-control-sm" value="{{ @$requirement->bir_form_2305_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_form_2305_status)
                                  <input type="checkbox" name="bir_form_2305_status" id="swt15" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bir_form_2305_status" id="swt15"><span>
                                  @endif
                                <label for="swt15"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>9</td>
                    <td><b> Philhealth Member Registration Form</b></td>
                    <td>
                        <input type="text" name="philhealth_copies" class="form-control form-control-sm" value="{{ @$requirement->philhealth_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->philhealth_status)
                                  <input type="checkbox" name="philhealth_status" id="swt16" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="philhealth_status" id="swt16"><span>
                                  @endif
                                <label for="swt16"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td>10</td>
                    <td><b>  BIR Form No. 1905 (Application for Registration Information Update)</b></td>
                    <td>
                        <input type="text" name="bir_form_1905_copies" class="form-control form-control-sm" value="{{ @$requirement->bir_form_1905_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_form_1905_status)
                                  <input type="checkbox" name="bir_form_1905_status" id="swt17" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bir_form_1905_status" id="swt17"><span>
                                  @endif
                                <label for="swt17"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>10</td>
                    <td><b> HDMF Membership Registration/Updating Form</b></td>
                    <td>
                        <input type="text" name="hdmf_copies" class="form-control form-control-sm" value="{{ @$requirement->hdmf_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->hdmf_status)
                                  <input type="checkbox" name="hdmf_status" id="swt18" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="hdmf_status" id="swt18"><span>
                                  @endif
                                <label for="swt18"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                
                <tr class="status-2 d-none">
                    <td>11</td>
                    <td><b> Philhealth Member Registration Form</b></td>
                    <td>
                        <input type="text" name="philhealth_copies" class="form-control form-control-sm" value="{{ @$requirement->philhealth_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->philhealth_status)
                                  <input type="checkbox" name="philhealth_status" id="swt20" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="philhealth_status" id="swt20"><span>
                                  @endif
                                <label for="swt20"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>11</td>
                    <td><b> Statement of Assets, Liabilities and Net Worth (Notarized & Typewritten as of date of assumption to duty)</b></td>
                    <td>
                        <input type="text" name="saln_copies" class="form-control form-control-sm" value="{{ @$requirement->saln_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->saln_status)
                                  <input type="checkbox" name="saln_status" id="swt21" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="saln_status" id="swt21"><span>
                                  @endif
                                <label for="swt21"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td>12</td>
                    <td><b> HDMF Membership Registration/Updating Form</b></td>
                    <td>
                        <input type="text" name="hdmf_copies" class="form-control form-control-sm" value="{{ @$requirement->hdmf_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->hdmf_status)
                                  <input type="checkbox" name="hdmf_status" id="swt22" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="hdmf_status" id="swt22"><span>
                                  @endif
                                <label for="swt22"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>12</td>
                    <td><b> Report on Assumption to Duty / First Day of Service*</b></td>
                    <td>
                        <input type="text" name="assumption_copies" class="form-control form-control-sm" value="{{ @$requirement->assumption_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->assumption_status)
                                  <input type="checkbox" name="assumption_status" id="swt23" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="assumption_status" id="swt23"><span>
                                  @endif
                                <label for="swt23"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td>13</td>
                    <td><b> Statement of Assets, Liabilities and Net Worth (Notarized & Typewritten as of date of assumption to duty)</b></td>
                    <td>
                        <input type="text" name="saln_copies" class="form-control form-control-sm" value="{{ @$requirement->saln_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->saln_status)
                                  <input type="checkbox" name="saln_status" id="swt24" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="saln_status" id="swt24"><span>
                                  @endif
                                <label for="swt24"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>13</td>
                    <td><b> Position Description Form*</b></td>
                    <td>
                        <input type="text" name="position_description_copies" class="form-control form-control-sm" value="{{ @$requirement->position_description_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->position_description_status)
                                  <input type="checkbox" name="position_description_status" id="swt25" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="position_description_status" id="swt25"><span>
                                  @endif
                                <label for="swt25"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td>14</td>
                    <td><b> Landbank Application Forms*, with photocopy of two (2) valid IDs</b></td>
                    <td>
                        <input type="text" name="land_bank_copies" class="form-control form-control-sm" value="{{ @$requirement->land_bank_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->land_bank_status)
                                  <input type="checkbox" name="land_bank_status" id="swt26" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="land_bank_status" id="swt26"><span>
                                  @endif
                                <label for="swt26"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>

                
                <tr class="status-2 d-none">
                    <td>15</td>
                    <td><b> Report on Assumption to Duty / First Day of Service*</b></td>
                    <td>
                        <input type="text" name="assumption_copies" class="form-control form-control-sm" value="{{ @$requirement->assumption_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->assumption_status)
                                  <input type="checkbox" name="assumption_status" id="swt27" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="assumption_status" id="swt27"><span>
                                  @endif
                                <label for="swt27"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td style="vertical-align: top;">15</td>
                    <td>
                        <ol style="list-style-type: lower-roman;">
                            <li>
                              Original copies of the following for authentication:
                              <ol style="list-style-type: lower-alpha;">
                                 <li> Diploma/Certificate of Graduation</li>
                                 <li>Transcript of Records</li>
                                 <li>Certificate of Training/Seminar Programs Attended within the last 5 years</li>
                              </ol>
                            </li>
                            <li>Authenticated copy of Certificate of Eligibility/Bar Membership</li>
                        </ol>
                    </td>
                    <td>
                        <input type="text" name="_original_copies" class="form-control form-control-sm" value="{{ @$requirement->original_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->original_status)
                                  <input type="checkbox" name="original_status" id="swt28" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="original_status" id="swt28"><span>
                                  @endif
                                <label for="swt28"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                
                <tr class="status-2 d-none">
                    <td>16</td>
                    <td><b> Position Description Form*</b></td>
                    <td>
                        <input type="text" name="position_description_copies" class="form-control form-control-sm" value="{{ @$requirement->position_description_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->position_description_status)
                                  <input type="checkbox" name="position_description_status" id="swt29" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="position_description_status" id="swt29"><span>
                                  @endif
                                <label for="swt29"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>16</td>
                    <td><b> Passport-sized photo for Personal Data Sheet and LandBank forms</b></td>
                    <td>
                        <input type="text" name="passport_copies" class="form-control form-control-sm" value="{{ @$requirement->passport_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->passport_status)
                                  <input type="checkbox" name="passport_status" id="swt30" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="passport_status" id="swt30"><span>
                                  @endif
                                <label for="swt30"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td>17</td>
                    <td><b> ID Pictures with the following sizes:</b>
                      <ol style="list-style-type: lower-alpha;">
                        <li>Passport-sized photo for Personal Data Sheets</li>
                        <li>1x1 photos</li>
                      </ol>
                    </td>
                    <td>
                        <input type="text" name="id_picture_copies" class="form-control form-control-sm" value="{{ @$requirement->id_picture_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->id_picture_status)
                                  <input type="checkbox" name="id_picture_status" id="swt31" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="id_picture_status" id="swt31"><span>
                                  @endif
                                <label for="swt31"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="status-1 d-none">
                    <td>17</td>
                    <td><b> Certificate of Live Birth (Original) and/or Marriage Certificate (if applicable)</b></td>
                    <td>
                        <input type="text" name="bday_copies" class="form-control form-control-sm"  value="{{ @$requirement->bday_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bday_status)
                                  <input type="checkbox" name="bday_status" id="swt32" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bday_status" id="swt32"><span>
                                  @endif
                                <label for="swt32"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                

                
                <tr class="status-2 d-none">
                    <td style="vertical-align: top;">18</td>
                    <td>
                        <ol style="list-style-type: lower-roman;">
                            <li>
                              Original copies of the following for authentication:
                              <ol style="list-style-type: lower-alpha;">
                                 <li> Diploma/Certificate of Graduation</li>
                                 <li>Transcript of Records</li>
                                 <li>Certificate of Training/Seminar Programs Attended within the last 5 years</li>
                              </ol>
                            </li>
                            <li>Authenticated copy of Certificate of Eligibility/Bar Membership</li>
                        </ol>
                    </td>
                    <td>
                        <input type="text" name="original_copies" class="form-control form-control-sm" value="{{ @$requirement->original_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->original_status)
                                  <input type="checkbox" name="original_status" id="swt33" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="original_status" id="swt33"><span>
                                  @endif
                                <label for="swt33"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>19</td>
                    <td><b> Certificate of Live Birth (Original) and/or Marriage Certificate (if applicable)</b></td>
                    <td>
                        <input type="text" name="bday_copies" class="form-control form-control-sm" value="{{ @$requirement->bday_copies}}">
                    </td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bday_status)
                                  <input type="checkbox" name="bday_status" id="swt34" checked="true"><span>
                                  @else
                                  <input type="checkbox" name="bday_status" id="swt34"><span>
                                  @endif
                                <label for="swt34"></label></span>
                            </div>
                        </div>
                    </td>
                </tr>
                
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();

        $('#applicant_id').change(function (){
          formStatus = $(this).find(':selected').data('form_status');

          $('.status-1').addClass('d-none');
          $('.status-2').addClass('d-none');
          switch(formStatus){
            case 1:
              $('.status-1').removeClass('d-none');
              break;
            case 2:
              $('.status-2').removeClass('d-none');
              break;
          }

        });

        $('#applicant_id').trigger('change');
      });
    </script>
@endsection


