@extends('layouts.app')

@section('content')

    <div class="page-head">
        <h2 class="page-head-title">Appointment - Requirements</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('appointment-requirements.index' ) }}">
                        Applicants ( {{ ($requirement) ? $requirement->applicant->getFullName() : '' }} )</a></li>
            </ol>
        </nav>
    </div>

    <!-- Applicant Form -->
    @if(!$documentView)
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                {{ ($requirement) ? $requirement->applicant->getFullName() : '' }}
                <span class="card-subtitle">
                Applied For: {{ $requirement->applicant->job->psipop->position_title }} / Applied On: {{ $requirement->created_at }}
                </span>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Attached Documents
                    <span class="card-subtitle">All applicant documents attached below</span>
                </div>
                <div class="card-body">
                @if($requirement)

                    @if($requirement->rar_path)
                        <div class="row">
                            <div class="col-2 mt-1"><span class="badge badge-info">Applicant Files</span></div>
                            <div class="col-0 p-1">
                                <a href="{{ asset('documents/' . $requirement->pds_path) }}" download >
                                    <span class="badge badge-success">Download</span>
                                </a>
                            </div>
                        </div>
                    @endif
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
        });
    </script>
@endsection
