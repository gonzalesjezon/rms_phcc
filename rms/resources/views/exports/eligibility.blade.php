<table class="table">
	<thead class="text-center" style="font-weight: bold;">
		<tr>
			<th>Position</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Eligibility</th>
			<th>Rating</th>
			<th>Exam Date</th>
			<th>Exam Place</th>
			<th>Licence Number</th>
			<th>Licence Validity</th>
		</tr>
	</thead>
	<tbody>
		@if(count($eligibility['eligibility']) > 1)
			@foreach($eligibility['eligibility'] as $i => $value)
			<tr>
				<td>{!! $eligibility['position'] !!}</td>
				<td>{!! $eligibility['last_name'] !!}</td>
				<td>{!! $eligibility['first_name'] !!}</td>
				<td>{!! $eligibility['middle_name'] !!}</td>

				<td>{!! $eligibility['eligibility'][$i]['eligibility_ref'] !!}</td>
				<td>{!! $eligibility['eligibility'][$i]['rating'] !!}</td>
				<td>{!! $eligibility['eligibility'][$i]['exam_date'] !!}</td>
				<td>{!! $eligibility['eligibility'][$i]['exam_place'] !!}</td>
				<td>{!! $eligibility['eligibility'][$i]['license_number'] !!}</td>
				<td>{!! $eligibility['eligibility'][$i]['license_validity'] !!}</td>
			</tr>
			@endforeach
		@endif
	</tbody>
</table>