<table class="table">
	<thead class="text-center" style="font-weight: bold;">
		<tr>
			<th>Position</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Title</th>
			<th>Inclusive Date From</th>
			<th>Inclusive Date To</th>
			<th>Number of Hours</th>
			<th>LD Type</th>
			<th>Sponsored By</th>
		</tr>
	</thead>
	<tbody>
		@if(count($training['training']) > 0)
			@foreach($training['training'] as $i => $value)
			<tr>
				<td>{!! $training['position'] !!}</td>
				<td>{!! $training['last_name'] !!}</td>
				<td>{!! $training['first_name'] !!}</td>
				<td>{!! $training['middle_name'] !!}</td>
				<td>{!! $training['training'][$i]['title_learning_programs'] !!}</td>
				<td>{!! $training['training'][$i]['inclusive_date_from'] !!}</td>
				<td>{!! $training['training'][$i]['inclusive_date_to'] !!}</td>
				<td>{!! $training['training'][$i]['number_hours'] !!}</td>
				<td>{!! $training['training'][$i]['ld_type'] !!}</td>
				<td>{!! $training['training'][$i]['sponsored_by'] !!}</td>
			</tr>
			@endforeach
		@endif
	</tbody>
</table>