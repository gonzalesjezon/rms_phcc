<table class="table">
	<thead class="text-center" style="font-weight: bold;">
		<tr>
			<th>Position</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Extension Name</th>
			<th>Nick Name</th>
			<th>Email Address</th>
			<th>Mobile Number</th>
			<th>Telephone Number</th>
			<th>Publication</th>
			<th>Birth Date</th>
			<th>Place of Birth</th>
			<th>Gender</th>
			<th>Civil Status</th>
			<th>Citizenship</th>
			<th>Height</th>
			<th>Weight</th>
			<th>Blood Type</th>
			<th>House Number</th>
			<th>Street</th>
			<th>Subd.</th>
			<th>Brgy</th>
			<th>City</th>
			<th>Province</th>
			<th>Zip Code</th>
			<th>Permanent House No.</th>
			<th>Permanent Street</th>
			<th>Permanent Subd.</th>
			<th>Permanent Brgy.</th>
			<th>Permanent City</th>
			<th>Permanent Province</th>
			<th>Permanent Zip Code</th>

			<!-- Education -->
			<th>School Name</th>
			<th>Course</th>
			<th>Ongoing</th>
			<th>Attendance From</th>
			<th>Attendance To</th>
			<th>Level</th>
			<th>Graduated</th>
			<th>Award</th>
			<th>Educ Level</th>

			<!-- Eligibility -->
			<th>Eligibility</th>
			<th>Rating</th>
			<th>Exam Date</th>
			<th>Exam Place</th>
			<th>Licence Number</th>
			<th>Licence Validity</th>

			<!-- Experience -->
			<th>Inclusive Date From</th>
			<th>Inclusive Date To</th>
			<th>Present Work</th>
			<th>Position Title</th>
			<th>Department</th>
			<th>Monthly Salary</th>
			<th>Salary Grade</th>
			<th>Status of Appointment</th>
			<th>Govt Service</th>

			<!-- Training -->
			<th>Title</th>
			<th>Date From</th>
			<th>Date To</th>
			<th>Number of Hours</th>
			<th>LD Type</th>
			<th>Sponsored By</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{!! $applicants['job_name'] !!}</td>
			<td>{!! $applicants['last_name'] !!}</td>
			<td>{!! $applicants['first_name'] !!}</td>
			<td>{!! $applicants['middle_name'] !!}</td>
			<td>{!! $applicants['extension_name'] !!}</td>
			<td>{!! $applicants['nickname'] !!}</td>
			<td>{!! $applicants['email_address'] !!}</td>
			<td>{!! $applicants['mobile_number'] !!}</td>
			<td>{!! $applicants['telephone_number'] !!}</td>
			<td>{!! $applicants['publication'] !!}</td>
			<td>{!! date('Y-m-d',strtotime($applicants['birthday'])) !!}</td>
			<td>{!! $applicants['birth_place'] !!}</td>
			<td>{!! $applicants['gender'] !!}</td>
			<td>{!! $applicants['civil_status'] !!}</td>
			<td>{!! $applicants['citizenship'] !!}</td>
			<td>{!! $applicants['height'] !!}</td>
			<td>{!! $applicants['weight'] !!}</td>
			<td>{!! $applicants['blood_type'] !!}</td>
			<td>{!! $applicants['house_number'] !!}</td>
			<td>{!! $applicants['street'] !!}</td>
			<td>{!! $applicants['subdivision'] !!}</td>
			<td>{!! $applicants['barangay'] !!}</td>
			<td>{!! $applicants['city'] !!}</td>
			<td>{!! $applicants['province'] !!}</td>
			<td>{!! $applicants['zip_code'] !!}</td>
			<td>{!! $applicants['permanent_house_number'] !!}</td>
			<td>{!! $applicants['permanent_street'] !!}</td>
			<td>{!! $applicants['permanent_subdivision'] !!}</td>
			<td>{!! $applicants['permanent_barangay'] !!}</td>
			<td>{!! $applicants['permanent_city'] !!}</td>
			<td>{!! $applicants['permanent_province'] !!}</td>
			<td>{!! $applicants['permanent_zip_code'] !!}</td>

			<!-- Education Primary -->
			<td>{!! $applicants['education'][0]['school_name'] !!}</td>
			<td>{!! (@$applicants['education'][0]['ongoing']) ? 1 : 0 !!}</td>
			<td>{!! $applicants['education'][0]['course'] !!}</td>
			<td>{!! $applicants['education'][0]['attendance_from'] !!}</td>
			<td>{!! $applicants['education'][0]['attendance_to'] !!}</td>
			<td>{!! $applicants['education'][0]['level'] !!}</td>
			<td>{!! $applicants['education'][0]['graduated'] !!}</td>
			<td>{!! $applicants['education'][0]['awards'] !!}</td>
			<td>{!! $applicants['education'][0]['educ_level'] !!}</td>

			<!-- Education -->

			<!-- Eligibility -->
			<td>{!! $applicants['eligibility'][0]['eligibility_ref'] !!}</td>
			<td>{!! $applicants['eligibility'][0]['rating'] !!}</td>
			<td>{!! $applicants['eligibility'][0]['exam_date'] !!}</td>
			<td>{!! $applicants['eligibility'][0]['exam_place'] !!}</td>
			<td>{!! $applicants['eligibility'][0]['license_number'] !!}</td>
			<td>{!! $applicants['eligibility'][0]['license_validity'] !!}</td>

			<!-- Experience -->
			<td>{!! $applicants['work_experience'][0]['inclusive_date_from'] !!}</td>
			<td>{!! $applicants['work_experience'][0]['inclusive_date_to'] !!}</td>
			<td>{!! ($applicants['work_experience'][0]['present_work']) ? 1 : 0 !!}</td>
			<td>{!! $applicants['work_experience'][0]['position_title'] !!}</td>
			<td>{!! $applicants['work_experience'][0]['department'] !!}</td>
			<td>{!! $applicants['work_experience'][0]['monthly_salary'] !!}</td>
			<td>{!! $applicants['work_experience'][0]['salary_grade'] !!}</td>
			<td>{!! $applicants['work_experience'][0]['status_of_appointment'] !!}</td>
			<td>{!! ($applicants['work_experience'][0]['govt_service']) ? 1 : 0 !!}</td>

			<!-- Training -->
			<td>{!! $applicants['training'][0]['title_learning_programs'] !!}</td>
			<td>{!! $applicants['training'][0]['inclusive_date_from'] !!}</td>
			<td>{!! $applicants['training'][0]['inclusive_date_to'] !!}</td>
			<td>{!! $applicants['training'][0]['number_hours'] !!}</td>
			<td>{!! $applicants['training'][0]['ld_type'] !!}</td>
			<td>{!! $applicants['training'][0]['sponsored_by'] !!}</td>
		</tr>

		@php

		$countEducation 	= count($applicants['education']);
		$countEligibility 	= count($applicants['eligibility']);
		$countWExperience 	= count($applicants['work_experience']);
		$countTraining 		= count($applicants['training']);

		$maxCount = max(array($countEducation,$countEligibility,$countWExperience,$countTraining));

		@endphp

		@for($i = 1; $i <= $maxCount; $i++)

		<tr>
			<td colspan="32"></td>
			<!-- Education Secondary -->
			<td>{!! @$applicants['education'][$i]['school_name'] !!}</td>
			<td>{!! @$applicants['education'][$i]['course'] !!}</td>
			<td>{!! (@$applicants['education'][$i]['ongoing']) ? 1 : 0 !!}</td>
			<td>{!! @$applicants['education'][$i]['attendance_from'] !!}</td>
			<td>{!! @$applicants['education'][$i]['attendance_to'] !!}</td>
			<td>{!! @$applicants['education'][$i]['level'] !!}</td>
			<td>{!! @$applicants['education'][$i]['graduated'] !!}</td>
			<td>{!! @$applicants['education'][$i]['awards'] !!}</td>
			<td>{!! @$applicants['education'][$i]['educ_level'] !!}</td>
			<!-- Education -->

			<!-- Eligibility -->
			<td>{!! @$applicants['eligibility'][$i]['eligibility_ref'] !!}</td>
			<td>{!! @$applicants['eligibility'][$i]['rating'] !!}</td>
			<td>{!! @$applicants['eligibility'][$i]['exam_date'] !!}</td>
			<td>{!! @$applicants['eligibility'][$i]['exam_place'] !!}</td>
			<td>{!! @$applicants['eligibility'][$i]['license_number'] !!}</td>
			<td>{!! @$applicants['eligibility'][$i]['license_validity'] !!}</td>

			<!-- Work Experience -->
			<td>{!! @$applicants['work_experience'][$i]['inclusive_date_from'] !!}</td>
			<td>{!! @$applicants['work_experience'][$i]['inclusive_date_to'] !!}</td>
			<td>{!! (@$applicants['work_experience'][$i]['present_work'] ) ? 1 : 0 !!}</td>
			<td>{!! @$applicants['work_experience'][$i]['position_title'] !!}</td>
			<td>{!! @$applicants['work_experience'][$i]['department'] !!}</td>
			<td>{!! @$applicants['work_experience'][$i]['monthly_salary'] !!}</td>
			<td>{!! @$applicants['work_experience'][$i]['salary_grade'] !!}</td>
			<td>{!! @$applicants['work_experience'][$i]['status_of_appointment'] !!}</td>
			<td>{!! (@$applicants['work_experience'][$i]['govt_service']) ? 1 : 0 !!}</td>

			<!-- Training -->
			<td>{!! @$applicants['training'][$i]['title_learning_programs'] !!}</td>
			<td>{!! @$applicants['training'][$i]['inclusive_date_from'] !!}</td>
			<td>{!! @$applicants['training'][$i]['inclusive_date_to'] !!}</td>
			<td>{!! @$applicants['training'][$i]['number_hours'] !!}</td>
			<td>{!! @$applicants['training'][$i]['ld_type'] !!}</td>
			<td>{!! @$applicants['training'][$i]['sponsored_by'] !!}</td>
		</tr>

		@endfor

	
	</tbody>
</table>