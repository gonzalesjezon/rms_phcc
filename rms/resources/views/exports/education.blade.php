<table class="table">
	<thead class="text-center" style="font-weight: bold;">
		<tr>
			<th>Position</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>School Name</th>
			<th>Course</th>
			<th>Attendance From</th>
			<th>Attendance To</th>
			<th>Level</th>
			<th>Graduated</th>
			<th>Award</th>
			<th>Educ Level</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{!! $education['position'] !!}</td>
			<td>{!! $education['last_name'] !!}</td>
			<td>{!! $education['first_name'] !!}</td>
			<td>{!! $education['middle_name'] !!}</td>
			<td>{!! $education['primary']['school_name'] !!}</td>
			<td>{!! $education['primary']['course'] !!}</td>
			<td>{!! $education['primary']['attendance_from'] !!}</td>
			<td>{!! $education['primary']['attendance_to'] !!}</td>
			<td>{!! $education['primary']['level'] !!}</td>
			<td>{!! $education['primary']['graduated'] !!}</td>
			<td>{!! $education['primary']['awards'] !!}</td>
			<td>1</td>
		</tr>

		<tr>
			<td></td>
			<td>{!! $education['last_name'] !!}</td>
			<td>{!! $education['first_name'] !!}</td>
			<td>{!! $education['middle_name'] !!}</td>
			<td>{!! $education['secondary']['school_name'] !!}</td>
			<td>{!! $education['secondary']['course'] !!}</td>
			<td>{!! $education['secondary']['attendance_from'] !!}</td>
			<td>{!! $education['secondary']['attendance_to'] !!}</td>
			<td>{!! $education['secondary']['level'] !!}</td>
			<td>{!! $education['secondary']['graduated'] !!}</td>
			<td>{!! $education['secondary']['awards'] !!}</td>
			<td>2</td>
		</tr>

		@if(count($education['vocational']) > 1)
			@foreach($education['vocational'] as $i => $value)
				<tr>
					<td></td>
					<td>{!! $education['last_name'] !!}</td>
					<td>{!! $education['first_name'] !!}</td>
					<td>{!! $education['middle_name'] !!}</td>
					<td>{!! $education['vocational'][$i]['school_name'] !!}</td>
					<td>{!! $education['vocational'][$i]['course'] !!}</td>
					<td>{!! $education['vocational'][$i]['attendance_from'] !!}</td>
					<td>{!! $education['vocational'][$i]['attendance_to'] !!}</td>
					<td>{!! $education['vocational'][$i]['level'] !!}</td>
					<td>{!! $education['vocational'][$i]['graduated'] !!}</td>
					<td>{!! $education['vocational'][$i]['awards'] !!}</td>
					<td>3</td>
				</tr>
			@endforeach
		@endif

		@if(count($education['college']) > 1)
			@foreach($education['college'] as $i => $value)
				<tr>
					<td></td>
					<td>{!! $education['last_name'] !!}</td>
					<td>{!! $education['first_name'] !!}</td>
					<td>{!! $education['middle_name'] !!}</td>
					<td>{!! $education['college'][$i]['school_name'] !!}</td>
					<td>{!! $education['college'][$i]['course'] !!}</td>
					<td>{!! $education['college'][$i]['attendance_from'] !!}</td>
					<td>{!! $education['college'][$i]['attendance_to'] !!}</td>
					<td>{!! $education['college'][$i]['level'] !!}</td>
					<td>{!! $education['college'][$i]['graduated'] !!}</td>
					<td>{!! $education['college'][$i]['awards'] !!}</td>
					<td>4</td>

				</tr>
			@endforeach
		@endif

		@if(count($education['graduate']) > 1)
			@foreach($education['graduate'] as $i => $value)
				<tr>
					<td></td>
					<td>{!! $education['last_name'] !!}</td>
					<td>{!! $education['first_name'] !!}</td>
					<td>{!! $education['middle_name'] !!}</td>
					<td>{!! $education['graduate'][$i]['school_name'] !!}</td>
					<td>{!! $education['graduate'][$i]['course'] !!}</td>
					<td>{!! $education['graduate'][$i]['attendance_from'] !!}</td>
					<td>{!! $education['graduate'][$i]['attendance_to'] !!}</td>
					<td>{!! $education['graduate'][$i]['level'] !!}</td>
					<td>{!! $education['graduate'][$i]['graduated'] !!}</td>
					<td>{!! $education['graduate'][$i]['awards'] !!}</td>
					<td>5</td>
				</tr>
			@endforeach
		@endif

	</tbody>
</table>