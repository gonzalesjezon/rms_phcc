<table class="table">
	<thead class="text-center" style="font-weight: bold;">
		<tr>
			<th>Position</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Inclusive Date From</th>
			<th>Inclusive Date To</th>
			<th>Position Title</th>
			<th>Department</th>
			<th>Monthly Salary</th>
			<th>Salary Grade</th>
			<th>Status of Appointment</th>
			<th>Govt Service</th>
		</tr>
	</thead>
	<tbody>
		@if(count($experience['work_experience']) > 0)
			@foreach($experience['work_experience'] as $i => $value)
			<tr>
				<td>{!! $experience['position'] !!}</td>
				<td>{!! $experience['last_name'] !!}</td>
				<td>{!! $experience['first_name'] !!}</td>
				<td>{!! $experience['middle_name'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['inclusive_date_from'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['inclusive_date_to'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['position_title'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['department'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['monthly_salary'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['salary_grade'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['status_of_appointment'] !!}</td>
				<td>{!! $experience['work_experience'][$i]['govt_service'] !!}</td>
			</tr>
			@endforeach
		@endif
	</tbody>
</table>