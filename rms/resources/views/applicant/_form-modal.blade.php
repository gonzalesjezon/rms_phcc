 <!-- IMPORT MODAL -->
  {!! Form::open(['action' => 'ApplicantController@saveToDb', 'method' => 'POST','id' => 'modal-form', 'files' => true ,
      'data-parsley-namespace' => "data-parsley-", 'data-parsley-validate' => '',
      'class' => 'form-horizontal group-border-dashed'
  ]) !!}
    <div class="modal-container colored-header colored-header-success custom-width modal-effect-9" id="form-success" style="perspective: none;">
        <div class="modal-content">
          <div class="modal-header modal-header-colored pb-0">
            <h3 class="modal-title">Import Form</h3>
            <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
          </div>
          <div class="modal-header p-4">
            <h6>You may import the applicant information details and requirements in the form below.</h6>
          </div>
          <div class="modal-body form">
            <div class="row mb-2">
              <div class="col-md-8">
                1. Personal Information
              </div>
              <div class="col-md-4">
                  <input id="personal_info" type="file" name="personal_info" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="personal_info" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
              </div>
            </div>

            <div class="row mb-2">
              <div class="col-md-8">
                <span style="font-size: .9em;">2. Applicant Requirements</span>
              </div>
              <div class="col-md-4">
                <input id="pds_path" type="file" name="performance_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="performance_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
              </div>
            </div>

            <!-- <div class="row mb-2">
              <div class="col-md-8">
                <span style="font-size: .9em;">2. Fully accomplished Personal Data Sheet (PDS) with recent passport-sized picture (CS Form No. 212, Revised 2017)</span>
              </div>
              <div class="col-md-4">
                <input id="pds_path" type="file" name="pds_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="pds_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
              </div>
            </div>

            <div class="row mb-2">
              <div class="col-md-8">
                <span style="font-size: .9em;">3. Performance rating in the last rating period (if applicable)</span>
              </div>
              <div class="col-md-4">
                <input id="pds_path" type="file" name="performance_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="performance_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
              </div>
            </div>

            <div class="row mb-2">
              <div class="col-md-8">
                <span style="font-size: .9em;">4. Photocopy of certificate of eligibility/rating/license </span>
              </div>
              <div class="col-md-4">
                <input id="coe_path" type="file" name="coe_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="coe_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
              </div>
            </div>

            <div class="row">
              <div class="col-md-8">
                <span style="font-size: .9em;">5. Photocopy of Transcript of Records</span>
              </div>
              <div class="col-md-4">
                <input id="tor_path" type="file" name="tor_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
                <label for="tor_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                <span class="badge badge-primary"></span>
              </div>
            </div> -->

          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-success modal-close" type="submit" data-dismiss="modal">Proceed</button>
          </div>
        </div>
      </div>
      <div class="modal-overlay"></div>

{{ Form::hidden('_token',csrf_token())}}
{!! Form::close() !!}