
    <div class="form-group row">
        {{ Form::label('application_letter_path', 'Application Letter', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="application_letter_path" type="file" name="application_letter_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="application_letter_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->application_letter_path }}</span>
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('pds_path', 'Personal Data Sheet', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="pds_path" type="file" name="pds_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="pds_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->pds_path }}</span>
        </div>
    </div>

   <!--  <div class="form-group row">
        {{ Form::label('curriculum_vitae_path', 'Curriculum Vitae', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="curriculum_vitae_path" type="file" name="curriculum_vitae_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="curriculum_vitae_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary"></span>
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('info_sheet_path', 'Information Sheet', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="info_sheet_path" type="file" name="info_sheet_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="info_sheet_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary"></span>
        </div>
    </div> -->

    <!-- <div class="row">
        <div class="col-sm-3 text-sm-right">
            <i>(Download Info Sheet Form)</i> <a href="{{ asset('documents/information_sheet.pdf') }}" download><span class="badge badge-success">Download </span></a>
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('employment_certificate_path', 'Employment Certificate', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="employment_certificate_path" type="file" name="employment_certificate_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="employment_certificate_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->employment_certificate_path }}</span>
        </div>
    </div> -->

    <!-- <div class="form-group row">
        {{ Form::label('tor_path', 'Transcript of Records', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="tor_path" type="file" name="tor_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="tor_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->tor_path }}</span>
        </div>
    </div> -->

    <div class="form-group row">
        {{ Form::label('coe_path', 'Certificate of Eligibility', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="coe_path" type="file" name="coe_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="coe_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->coe_path }}</span>
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('training_certificate_path', 'Training Certificate', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="training_certificate_path" type="file" name="training_certificate_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="training_certificate_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->training_certificate_path }}</span>
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('performance_path', 'Performance Rating (if applicable)', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            <input id="performance_path" type="file" name="performance_path" data-multiple-caption="{count} files selected" multiple=""
                   class="inputfile">
            <label for="performance_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
            <span class="badge badge-primary">{{ $applicant->performance_path }}</span>
        </div>
    </div>

    
