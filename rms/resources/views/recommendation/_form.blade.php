@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="form-group row">
    {{ Form::label('title', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::label('reference_no', $recommendation->applicant->getFullName(), ['class'=>'col-12 col-sm-2 col-form-label text-sm-right'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Chairperson', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::text('sign_one', @$recommendation->sign_one, ['class'=>'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Director IV - AO', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::text('sign_two', @$recommendation->sign_two, ['class'=>'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Head of Requesitioning Office', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::text('sign_three', @$recommendation->sign_three, ['class'=>'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Human Resources Development Division', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::text('sign_four', @$recommendation->sign_four, ['class'=>'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'EA Representative', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::text('sign_five', @$recommendation->sign_five, ['class'=>'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Prepared By', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        {{ Form::text('prepared_by', @$recommendation->prepared_by, ['class'=>'form-control form-control-sm'])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right font-weight-bold']) }}
    <div class="col-3 offset-1">
        <select class="form-control form-control-xs" name="recommend_status">
            <option>Select</option>
            <option value="1" {{ (@$recommendation->recommend_status == 1) ? 'selected' : '' }} >For Job Offer</option>
        </select>
    </div>
</div>

<input type="hidden" name="id" value="{{@$recommendation->id}}">
<input type="hidden" name="applicant_id" value="{{@$recommendation->applicant_id}}">

<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Save', ['id' => 'recommendation-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}


@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <!-- wysiwyg -->
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
            type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();

        // readonly summernote(wysiwyg) editor
        // $('#education').summernote('disable');
        // $('#experience').summernote('disable');
        // $('#eligibility').summernote('disable');
        // $('#training').summernote('disable');

        // remove tools on summernote(wysiwyg) editor
        // $('.note-toolbar').remove();

        // $.fn.niftyModal('setDefaults', {
        //   overlaySelector: '.modal-overlay',
        //   contentSelector: '.modal-content',
        //   closeSelector: '.modal-close',
        //   classAddAfterOpen: 'modal-show',
        // });

        // print modal
        // $('#print-button').click(function() {
        //   printElement(document.getElementById('printThis'));
        // })

        // function printElement(elem) {
        //   const domClone = elem.cloneNode(true);
        //   const printSection = document.createElement('div');
        //   printSection.id = 'printSection';
        //   printSection.appendChild(domClone);
        //   $('body').append(printSection);
        //   $('.be-wrapper, .modal-buttons').hide();
        //   window.print();
        //   $('.be-wrapper, .modal-buttons').show();
        //   $('#printSection').remove();
        // }

      });
    </script>
@endsection


