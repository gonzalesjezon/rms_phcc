@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Recommendation</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create a recommendation in the form below.</span></div>
                <div class="card-body">
                    @include('recommendation._form', [
                        'action' => ['RecommendationController@update', $recommendation->id],
                        'method' => 'PATCH',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
