@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="row mb-4">
    <div class="col text-center"><h3><strong>CERTIFICATION</strong></h3></div>
</div>
<div class="row">
    <div class="col">
        <p style="text-indent: 10em;">The PCC HRM Promotion and Selection Board recommended the appointment of <strong class="text-uppercase">{{ $recommendation->applicant->getFullName() }}</strong> for the position of
        <strong class="text-uppercase">{{ $recommendation->applicant->job->psipop->position_title }} {{$recommendation->applicant->job->psipop->item_number}} {{$recommendation->applicant->job->psipop->salary_grade}}).</strong>
        </p>
    </div>
</div>

<div class="row text-center mb-6">
    <div class="col"><strong>PCC HRM PROMOTION AND SELECTION BOARD</strong></div>
</div>

<div class="row text-center">
    <div class="col">
        {{ $recommendation->sign_one }}
    </div>
</div>

<div class="row text-center mb-8">
    <div class="col-4 offset-4 border border-top-1 border-left-0 border-right-0 border-bottom-0 border-dark">
        <strong>Chairperson</strong>
    </div>
</div>

<div class="row text-center">
    <div class="col">{{ $recommendation->sign_two }}</div>
    <div class="col">{{ $recommendation->sign_three }}</div>
    <div class="col">{{ $recommendation->sign_four }}</div>
    <div class="col">{{ $recommendation->sign_five }}</div>
</div>

<div class="row text-center mb-8">
    <div class="col">
        <hr>
        Director IV - AO
    </div>
    <div class="col">
        <hr>
        Head of Requisitioning Office
    </div>
    <div class="col">
        <hr>
        Human Resource Development Division
    </div>
    <div class="col">
        <hr>
        EA Representative
    </div>
</div>

<div class="row text-left">
    <div class="col"><strong>Prepared by:</strong> {{ $recommendation->prepared_by}}</div>
</div>

<div class="form-group row text-right">
<div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
  {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
  {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection