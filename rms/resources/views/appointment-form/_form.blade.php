@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
<style type="text/css">
    .select2-container--default .select2-selection--single{
        height: 3rem !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 3.13816rem !important;
        font-size: 1rem !important;
        height: 2rem !important;
    }
</style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

<div class="form-group row">
    {{ Form::label('applicant_name', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        <select class="select2 select2-sm" name="applicant_id" id="applicant_id">
            <option>Select applicant</option>
            @foreach($applicants as $applicant)
            <option value="{{ $applicant->id }}"
                data-position="{{ $applicant->job->psipop->position->Name }}"
                data-office="{{ $applicant->job->psipop->office->Name }}"
                data-item="{{ $applicant->job->psipop->item_number }}"
                data-job_grade="{{ config('params.job_grades.'.$applicant->job->psipop->job_grade) }}"
                data-salary="{{ number_format($applicant->job->psipop->basic_salary ,2) }}"
                data-publish_date="{{ date('m/d/Y', strtotime($applicant->job->publish_date)) }}"
                data-deadline_date="{{ date('m/d/Y', strtotime($applicant->job->deadline_date)) }}"
                {{ ($applicant->id == @$form->applicant_id) ? 'selected' : '' }}
                >
                {{ $applicant->getFullName() }}</option>
            @endforeach
        </select>
    </div>

    <div class="col-7 text-center">
        {{ Form::label('','PUBLICATION',['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('position', 'Position', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'position'
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Publication Date From </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'publish_date'
            ])
        }}
    </div>
</div>

<div class="form-group row {{ $errors->has('employee_status') ? 'has-error' : ''}}">

    {{ Form::label('office', 'Office/Department Unit', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'office'
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">To </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'deadline_date'
            ])
        }}
    </div>
</div>

<!-- <div class="form-group row">
    <div class="col-5 text-center">
        {{ Form::label('','CERTIFICATION', ['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div> -->

<div class="form-group row">
    {{ Form::label('item_no', 'Item Number', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'item_number'
            ])
        }}
    </div>

    {{ Form::label('', 'Publish At', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('publication',config('params.publication.'.@$applicant->publication) , [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>

</div>

<div class="form-group row">

    {{ Form::label('salary_grade', 'Job Grade', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'job_grade'
            ])
        }}
    </div>

    {{ Form::label('', 'Posted In', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('posted_in',@$form->posted_in , [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

</div>

<div class="form-group row">

    {{ Form::label('', 'Monthly Rate', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'salary'
            ])
        }}
    </div>

    {{ Form::label('', 'Signatory HRMO', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('hrmo', @$form->hrmo, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

</div>

<div class="form-group row">

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Date Issued</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="date_issued"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->date_issued}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Deliberation Date</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="chairperson_deliberation_date"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->chairperson_deliberation_date}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

</div>

<div class="row form-group">

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Assessment Date</label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="assessment_date"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{@$form->assessment_date}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    {{ Form::label('', 'Appointing Officer', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('appointing_officer', @$form->appointing_officer, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

    
</div>


<div class="row form-group">

    {{ Form::label('', 'Employee Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('employee_status_id', config('params.employee_status'), @$form->employee_status_id,[
                'class' => 'select2 select2-sm',
                'placeholder' => 'Select employee status'
            ])
        }}
    </div>

    {{ Form::label('', 'Chairperson, HRMPSB/ Placement', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('chairperson', @$form->chairperson, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

    
</div>

<div class="row form-group">

    {{ Form::label('', 'Vice', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('vice', @$form->vice, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>

    {{ Form::label('', 'Who', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('who', @$form->who, [
                'class' => 'form-control form-control-sm',
            ])
        }}
    </div>
</div>



<div class="row form-group">

    {{ Form::label('', 'Nature of Appointment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('nature_of_appointment', config('params.nature_of_appointment'), @$form->nature_of_appointment,[
                'class' => 'select2 select2',
                'placeholder' => 'Select nature of appointment'
            ])
        }}
    </div>

    {{ Form::label('', 'Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('form_status', $form_status, @$form->form_status,[
                'class' => 'select2 select2-sm',
                'placeholder' => 'Select status',
                'id' => 'form_status'
            ])
        }}

        <div class="row">
            <div class="col-12 text-right">
                <a class="btn btn-success mt-2 d-none" style="color: #fff;height: 30px;" id="send_mail">
                    <i class="mdi mdi-mail-send"></i>
                    Notify
                </a>
            </div>
        </div>
    </div>
</div>

</div>



<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation

        $('#form_status').change(function(){
            val = $(this).find(':selected').val();
            $('#send_mail').addClass('d-none');
            if(val > 2)
            {
              $('#send_mail').removeClass('d-none');
            }

        });

        $('#send_mail').click(function(){
          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
          }).then((result) => {
            if (result.value) {

              var arr = {};
              $("[class*='form-control']").each(function () {
                  var obj_name = $(this).attr("name");
                  var value    = $(this).val();
                  arr[obj_name] = value;
              });

              $.ajax({
                  url:`{{ url('appointment-form/sendMail') }}`,
                  data:{
                      'data':arr,
                      '_token':`{{ csrf_token() }}`
                  },
                  type:'POST',
                  dataType:'JSON',
                  success:function(result){
                      Swal.fire(
                        'Sent Successfully!',
                        'Your mail has been sent.',
                        'success'
                      )

                  }
              })
            }

          });

      });

    $('#applicant_id').change(function() {
        position = $(this).find(':selected').data('position');
        office = $(this).find(':selected').data('office');
        item = $(this).find(':selected').data('item');
        jobGrade = $(this).find(':selected').data('job_grade');
        basicSalary = $(this).find(':selected').data('salary');
        publishDate = $(this).find(':selected').data('publish_date');
        deadlineDate = $(this).find(':selected').data('deadline_date');

        $('#position').val(position);
        $('#office').val(office);
        $('#item_number').val(item);
        $('#job_grade').val(jobGrade);
        $('#salary').val(basicSalary);
        $('#publish_date').val(publishDate);
        $('#deadline_date').val(deadlineDate);
    });

    $('#applicant_id').trigger('change');
});
    </script>
@endsection
