@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Create {{ $title }}</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create a new interview evaluation rating in the form below.</span>
                </div>
                <div class="card-body">
                    @include('interview_evaluation_ratings._form', [
                        'action' => 'InterviewEvaluationRatingsController@store',
                        'method' => 'POST',
                        'jobs' => $jobs
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
