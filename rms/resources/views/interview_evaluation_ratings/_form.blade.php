@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('css/bootstrap-clockpicker.min.css') }}" />
  	<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('css/jquery-clockpicker.min.css') }}" />
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'files' => true, 'id'=>'interview-form']) !!}

<div class="row">
	<div class="col-6">
		<div class="form-group row">
		  {{ Form::label('', 'Position', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <select name="job_id" class="select2 select2-sm" id="select_position">
		        <option value="0">Selet position</option>
		        @foreach($jobs as $job)
		        <option value="{{ $job->id }}" 
		          data-education="{!! @$job->education !!}"
		          data-experience="{!! @$job->experience !!}"
		          data-training="{!! @$job->training !!}"
		          data-eligibility="{!! @$job->eligibility !!}"
		          data-office="{!! @$job->psipop->office->Name !!}"
		          {{ ($job->id == @$interview->applicant->job_id) ? 'selected' :  '' }}
		          >
		          {{ $job->psipop->position->Name}}
		        </option>
		        @endforeach
		    </select>
		  </div>
		</div>

		<div class="form-group row">
		  {{ Form::label('', 'Applicant', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <select name="applicant_id" class="select2 select2-sm" id="select_applicant"></select>
		  </div>
		</div>

		<div class="form-group row">
			{{ Form::label('', 'Rater', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <select name="rated_by" class="select2 select2-sm" id="rated_by">
		    	<option>Select</option>
		    	@foreach($raters as $employee)
		    	<option value="{{ $employee->RefId }}" {{ ($employee->RefId == @$interview->rated_by) ? 'selected' : '' }}>
		    		{{ $employee->getFullName() }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
	</div>

	<div class="col-6">
		<!-- <div class="form-group row">
			{{ Form::label('', 'Date Interview', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
		    <input type="text" class="form-control form-control-sm" id="interview_date" readonly>
		  </div>
		</div> -->

		<div class="form-group row">
			{{ Form::label('', 'Recommendation', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <select name="status" class="select2 select2-sm" id="status">
		    	<option>Select</option>
		    	@foreach(config('params.recommendations') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->status) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>

		<div class="form-group row">
			{{ Form::label('', 'Attachment', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
		  <div class="col-12 col-sm-8 col-lg-6">
		    <input id="attachment_path" type="file" name="attachment_path" data-multiple-caption="{count} files selected" multiple=""
                 class="inputfile">
        <label for="attachment_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
        <span class="badge badge-primary">{{ @$interview->attachment_path }}</span>
		  </div>
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-12">
		<h4 class="font-weight-bold">POTENTIAL</h4>
	</div>
</div>

<div class="row">
	<div class="col-7 text-center">
		<h4 class="font-weight-bold">FACTOR</h4>
	</div>
	<div class="col-5 text-center">
		<h4 class="font-weight-bold">Rating Scales</h4>
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Job Knowledge</span> - the degree of understanding of the job and related functions and the quickness with which employee/applicant has developed understanding of functional tasks.
			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_one" class="select2 select2-sm" id="rating_one">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_one) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>

	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Initiative</span> - the degree to which the applicant is self-starting and assumes responsibilities when specific directions are lacking, how well the applicant follows through on assignments, taking appropriate independent action when necessary, and the relative amount of supervision required.

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_two" class="select2 select2-sm" id="rating_two">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_two) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Behavior</span> - professionalism, enthusiasm, dedication and interest displayed regarding position duties and responsibilities.
			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_three" class="select2 select2-sm" id="rating_three">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_three) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Planning and Organizing Ability</span> - ability to establish a course of action for self and/or others to accomplish a specific goal; planning proper assignments of personnel and appropriate allocation of resources.

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_four" class="select2 select2-sm" id="rating_four">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_four) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Judgment</span> - the ability to develop alternative course of action and make rational and realistic decisions which are based on logical assumptions and factual information and considerations. 

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_five" class="select2 select2-sm" id="rating_five">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_five) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Reliability </span>- ability to demonstrate willingness to work on rush jobs, can be relied upon to finish assigned tasks.

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_six" class="select2 select2-sm" id="rating_six">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_six) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<hr>

<div class="row">
	<div class="col-12">
		<h4 class="font-weight-bold">PSYCHOSOCIAL ATTRIBUTES
</h4>
	</div>
</div>

<div class="row">
	<div class="col-7 text-center">
		<h4 class="font-weight-bold">FACTOR</h4>
	</div>
	<div class="col-5 text-center">
		<h4 class="font-weight-bold">Rating Scales</h4>
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Interpersonal Relations</span> - how well the applicant gets along with other individuals in the performance of job duties, effectiveness of relations with co-workers, subordinates, supervisors, employee cooperativeness, tact and courtesy.


			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_seven" class="select2 select2-sm" id="rating_seven">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_seven) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Emotional Stability</span> – mental state of calmness and composure, the ability to remain stable in times of stress.

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_eight" class="select2 select2-sm" id="rating_eight">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_eight) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Conscientiousness</span> - being responsible, dependable, organized and persistent.

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_nine" class="select2 select2-sm" id="rating_nine">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_nine) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>

<div class="row p-2">
	<div class="col-7">
		<div class="form-group row border border-dark">
			<p class="p-2">
				<span class="font-weight-bold">Cooperation</span> – the ability to work together in common with commonly agreed-upon goals and possibly methods, instead of working separately in competition.

			</p>
		</div>
	</div>

	<div class="col-5">
		<div class="row form-group">
			<label class="col-12 col-sm-3">&nbsp;</label>
			<div class="col-12 col-sm-8 col-lg-6">
		    <select name="rating_ten" class="select2 select2-sm" id="rating_ten">
		    	<option>Select</option>
		    	@foreach(config('params.rating_scales') as $key => $value)
		    	<option value="{{ $key }}" {{ ($key == @$interview->rating_ten) ? 'selected' : '' }}>{{ $value }}</option>
		    	@endforeach
		    </select>
		  </div>
		</div>
		
	</div>
</div>


<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
<script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
        type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/bootstrap-clockpicker.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery-clockpicker.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    //initialize the javascript
    App.init();
    App.formElements();
    $('#interview-form').parsley(); //frontend validation

    $('.clockpicker').clockpicker();

    $applicantId = `{!! @$interview->applicant_id !!}`;

    $('#select_position').change(function() {
      jobId = $(this).find(':selected').val();

      $.ajax({
        url:`{!! route('interviews.get-applicant') !!}`,
        data:{
          'job_id':jobId
        },
        type:'GET',
        dataType:'JSON',
        success:function(res){

          option = [];
          option += `<option>Select applicant</option>`;
          $.each(res.applicants, function(k,v) {
            firstName = (v.first_name) ? v.first_name : '';
            middleName = (v.middle_name) ? v.middle_name : '';
            lastName = (v.last_name) ? v.last_name : '';
            fullname = firstName+' '+middleName+' '+lastName;
            if($applicantId == v.id){
              option += `<option value="${v.id}" data-email="${v.email_address}" selected  >${fullname}</option>`;
            }else{
              option += `<option value="${v.id}" data-email="${v.email_address}" >${fullname}</option>`;
            }
          });
          $('#select_applicant').html(option);

        }
      });

    });

    $('#select_position').trigger('change');

  });
</script>
@endsection
