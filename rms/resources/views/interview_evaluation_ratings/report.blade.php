@extends('layouts.print')

@section('css')
 <style type="text/css">
 	.table > thead > tr >td, .table > tbody > tr >td {
 		padding: 1px;
 		border: 1px solid #333;
 	}
 </style>
@endsection

@section('content')
 <div class="form-group row text-right d-print-none">
    <div class="col col-10 col-lg-9 offset-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>

 <div id="reports" style="width: 860px;margin: auto;font-size: 10pt;font-family: Arial, Helvetica, sans-serif;">
	
	<img src="{{ asset('img/pcc-logo-small.png') }}" width="25%">

	<div class="row mb-6">
		<div class="col-12 text-center">
			<h4 class="font-weight-bold">INTERVIEW/EVALUATION RATING FORM</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<p class="font-weight-bold">Direction:</p>
			<p>Using the rating scales provided below, assess the applicant based on the criteria:</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-4"></div>
		<div class="col-4">
			<table style="width: 50rem;">
				<tbody>
					<tr>
						<td>1</td>
						<td style="">-</td>
						<td>Poor</td>
					</tr>
					<tr>
						<td>2</td>
						<td>-</td>
						<td>Below Average</td>
					</tr>
					<tr>
						<td>3</td>
						<td>-</td>
						<td>Average</td>
					</tr>
					<tr>
						<td>4</td>
						<td>-</td>
						<td>Above Average</td>
					</tr>
					<tr>
						<td>5</td>
						<td>-</td>
						<td>Excellent</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-4"></div>
	</div>

	<div class="row">
		<div class="col-12">
			<p class="font-weight-bold">I. POTENTIAL RATING</p>
			<p class="pl-7 text-justify">(Potential refers to the applicant’s capacity to perform the duties and his/her readiness to assume the responsibilities of the position, as well as those of higher positions.)</p>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<table class="table">
				<tbody>
					<tr class="text-center">
						<td rowspan="2" style="width: 250px;">FACTOR</td>
						<td colspan="5">RATING</td>
						<td rowspan="2">AVERAGE RATING</td>
					</tr>
					<tr class="text-center">
						<td style="height: 70px;vertical-align: top;">Rater 1</td>
						<td style="height: 70px;vertical-align: top;">Rater 2</td>
						<td style="height: 70px;vertical-align: top;">Rater 3</td>
						<td style="height: 70px;vertical-align: top;">Rater 4</td>
						<td style="height: 70px;vertical-align: top;">Rater 5</td>
					</tr>
					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Job Knowledge</span> - the degree of
							understanding of the job and related
							functions and the quickness with which
							employee/applicant has developed
							understanding of functional tasks.
							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Initiative</span> - the degree to which the applicant
								is self-starting and assumes responsibilities
								when specific directions are lacking, how well
								the applicant follows through on assignments,
								taking appropriate independent action when
								necessary, and the relative amount of
								supervision required

							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Behavior</span> - professionalism, enthusiasm,
								dedication and interest displayed regarding
								position duties and responsibilities
							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Planning and Organizing Ability</span> - ability to
								establish a course of action for self and/or
								others to accomplish a specific goal; planning
								proper assignments of personnel and
								appropriate allocation of resources.
							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Judgment</span> - the ability to develop alternative
								course of action and make rational and
								realistic decisions which are based on logical
								assumptions and factual information and
								considerations

							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Reliability</span> - ability to demonstrate willingness
								to work on rush jobs, can be relied upon to
								finish assigned tasks
							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="6">
							<span class="font-weight-bold">FINAL RATING</span>
							<td></td>
						</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>

	<div style="page-break-after: always;"></div>

	<img src="{{ asset('img/pcc-logo-small.png') }}" width="25%">

	<div class="row">
		<div class="col-12">
			<p class="font-weight-bold">II. PSYCHOSOCIAL ATTRIBUTES</p>
			<p class="pl-7 text-justify">(Psychosocial Attributes refer to the characteristics of a person which involve both psychological and social aspects. Psychological includes the way the person perceives things, ideas, beliefs and understanding and how he/she acts and relates these things to others in social situations.)</p>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-12">
			<table class="table">
				<tbody>
					<tr class="text-center">
						<td rowspan="2" style="width: 250px;">FACTOR</td>
						<td colspan="5">RATING</td>
						<td rowspan="2">AVERAGE RATING</td>
					</tr>
					<tr class="text-center">
						<td style="height: 70px;vertical-align: top;">Rater 1</td>
						<td style="height: 70px;vertical-align: top;">Rater 2</td>
						<td style="height: 70px;vertical-align: top;">Rater 3</td>
						<td style="height: 70px;vertical-align: top;">Rater 4</td>
						<td style="height: 70px;vertical-align: top;">Rater 5</td>
					</tr>
					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Interpersonal Relations</span> - how well the
								applicant gets along with other individuals in
								the performance of job duties, effectiveness
								of relations with co-workers, subordinates,
								supervisors, employee cooperativeness, tact
								and courtesy.

							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Emotional Stability</span> – mental state of
								calmness and composure, the ability to
								remain stable in times of stress.
							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Conscientiousness</span> - being responsible,
								dependable, organized and persistent.
							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td>
							<p class="p-1">
								<span class="font-weight-bold">Cooperation</span> – the ability to work together in
								common with commonly agreed-upon goals
								and possibly methods, instead of working
								separately in competition.

							</p>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td colspan="6">
							<span class="font-weight-bold">FINAL RATING</span>
							<td></td>
						</td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>

	<div class="row mb-3">
		<div class="col-3">
			<span class="font-weight-bold pl-2">Applicant Name</span>
		</div>
		<div class="1">:</div>
		<div class="col-6 border-bottom border-dark"></div>
	</div>

	<div class="row mb-3">
		<div class="col-3">
			<span class="font-weight-bold pl-2">Position Applied/JG</span>
		</div>
		<div class="1">:</div>
		<div class="col-6 border-bottom border-dark"></div>
	</div>

	<div class="row mb-1">
		<div class="col-3">
			<span class="font-weight-bold pl-2">Recommendation (Please √)</span>
		</div>
		<div class="1">:</div>
	</div>

	<table class="table">
		<tbody>
			<tr class="text-center font-weight-bold">
				<td style="height: 50px;">RATER</td>
				<td>Recommended</td>
				<td>Recommended with
					Reservation/s</td>
				<td>Not Recommended</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>

	<div class="row mb-1">
		<div class="col-4">
			<span class="font-weight-bold pl-2">Name and Signature of Rater</span>
		</div>
		<div class="1">:</div>
		<div class="col-6 border-bottom border-dark"></div>
	</div>


</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection