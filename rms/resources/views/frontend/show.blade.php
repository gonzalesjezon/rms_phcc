@extends('layouts.print')

@section('css')
<style type="text/css">
	.v-top{
		vertical-align: top !important;
	}

	.v-middle{
		vertical-align: middle !important;
	}

	.table > thead > tr > td, .table > tbody > tr > td{
		padding: 1px;
		border: 1px solid #333;
	} 
</style>
@endsection

@section('content')
<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

		<div class="row mb-4" >
			<div class="col-12">
				<img src="{{ asset('img/pcc-logo-small.png') }}" height="96">
			</div>
		</div>

		<div class="row mb-2">
			<div class="col-12">
				<table class="table table-bordered mb-6" style="width: 812px;">
					<thead>
						<tr class="text-center">
							<td colspan="7" >
								<h4 class="font-weight-bold">Job Details</h4>
							</td>
						</tr>
						<tr class="text-center">
							<td class="v-middle" style="height: 30px;">Position Title</td>
							<td class="v-middle">Item Number</td>
							<td class="v-middle">Job Grade</td>
							<td class="v-middle">Annual Salary</td>
							<td class="v-middle">Status of Employment</td>
							<td class="v-middle">Office</td>
							<td class="v-middle">Division</td>
						</tr>
					</thead>
					<tbody>
						<tr class="text-center">
							<td class="v-top"  nowrap >{!! @$jobs->psipop->position->Name !!}</td>
							<td class="v-top" nowrap >{!! (@$jobs->status == 'plantilla') ? @$jobs->psipop->Name : 'NA' !!}</td>
							<td class="v-top" nowrap >{!! ($jobs->status == 'plantilla') ? config('params.job_grades.'.@$jobs->psipop->job_grade) : 'NA' !!}</td>
							<td class="v-top">
								{!! number_format(@$jobs->psipop->basic_salary,2) !!}
							</td>
							<td class="v-top">
								{!! config('params.employee_status.'.@$jobs->psipop->employment_status_id) !!}
							</td>
							<td class="v-top">{!! @$jobs->psipop->office->Name!!}</td>
							<td class="v-top">{!! @$jobs->psipop->division->Name !!}</td>
						</tr>
					</tbody>
				</table>

				
				<table class="table table-bordered mb-6" style="width: 812px;">
					<thead class="font-weight-bold text-center">
						<tr>
							<td colspan="4" style="height: 30px;vertical-align: middle;">CSC MINIMUM QUALIFICATIONS</td>
						</tr>
						<tr>
							<td style="height: 30px;" class="v-middle">Education</td>
							<td style="height: 30px;" class="v-middle">Training</td>
							<td style="height: 30px;" class="v-middle">Work Experience</td>
							<td style="height: 30px;" class="v-middle">Eligibility</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{!! @$jobs->csc_education !!}</td>
							<td>{!! @$jobs->csc_training !!}</td>
							<td>{!! @$jobs->csc_work_experience !!}</td>
							<td>{!! @$jobs->csc_eligibility !!}</td>
						</tr>
					</tbody>
				</table>
				

				<table class="table table-bordered" style="width: 812px;">
					<thead>
						<tr class="text-center">
							<td colspan="5"  class="font-weight-bold" style="height: 30px;vertical-align: middle;">PREFERRED QUALIFICATION</td>
						</tr>
						<tr class="text-center">
							<td>EDUCATION</td>
							<td>TRAINING</td>
							<td>WORK EXPERIENCE</td>
							<td>ELIGIBILITY</td>
							<td>COMPETENCY</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="v-top">{!! $jobs->education !!}</td>
							<td class="v-top">{!! $jobs->training !!}</td>
							<td class="v-top">{!! $jobs->experience !!}</td>
							<td class="v-top">{!! $jobs->eligibility !!}</td>
							<td class="v-top">
								@if($jobs->compentency_1)
									<div class="pl-2 text-justify">{!! $jobs->compentency_1 !!}</div>
								@endif
							</td>
						</tr>
					</tbody>
				</table>

				<table class="border-0" style="width: 812px;">
					<tbody class="border-0">
						<tr>
							<td>
								<h5 class="font-weight-bold" style="font-size: 15px;">Duties and Responsibilities</h5> 

								@if($jobs->compentency_1)
									<p class="font-weight-bold mb-0 pb-0 pl-4">A. General Functions :</p>
									<div class="pl-6 text-justify">{!! $jobs->compentency_1 !!}</div>
								@endif

								<div style="height:2em;"></div>

								@if($jobs->compentency_2)
									<p class="font-weight-bold mb-0 pb-0 pl-4">B. Specific Duties and Responsibilities :</p>
									<div class="pl-6 text-justify">{!! $jobs->compentency_2 !!}</div>
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="mb-4 font-weight-bold">Interested and qualified applicants may 
					<a href="{{ route('frontend.create',['id'=>$jobs->id ]) }}">Apply Now.</a>
				</div>

				<p class="font-weight-bold mb-0 pb-0">Please also attach following documents through the link on or before {{ date('F, d Y',strtotime($jobs->deadline_date)) }}</p>
				<p class="font-weight-bold ">The assessment process will start the following day</p>

				<div class="mb-1">
			
				<ol>
          <li>Letter of Application</li>
          <li>Latest Personal Data Sheet with Work Experience Form (can be downloaded through www.csc.gov.ph)</li>
          <li>Update Service Record/Employment Certificate (preferably with statement of duties and responsibilities)</li>
          <li>Transcript of Records/Diploma</li>
          <li>Copy of Certificate of Eligibility/Board Rating  </li>
          <li>Copies of Certificates of Training/Seminars Attended</li>
        </ol>
	       <b>Address your application letter to:</b>
				</div>

				<div class="mt-3">
					<p class="text-justify mb-0 pb-0" >
						The Executive Director
					</p>
					<p class="mb-0 pb-0">Philippines Competition Commission</p>
					<p>25/F Vertis North Corporate Center 1, North Avenue, Quezon City 1105</p>
				</div>
			</div>
		</div>
	

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection