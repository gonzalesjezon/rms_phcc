@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

<div class="row mb-6">
	<div class="col-12 border border-primary border-left-0 border-right-0">
		<h3 class="p-2 m-0 text-primary">{!! strtoupper($jobs->psipop->position->Name) !!}</h3>
	</div>
</div>

<div class="row mb-2">
	<div class="col-4">Job Grade</div>
	<div class="col-4">{!! config('params.job_grades.'.$jobs->psipop->job_grade) !!}</div>
</div>

<div class="row mb-2 bg-grey">
	<div class="col-4">PLANTILLA ITEM NO</div>
	<div class="col-4">{!! $jobs->psipop->item_number!!}</div>
</div>

<div class="row mb-2">
	<div class="col-4">Annual Basic Salary</div>
	<div class="col-4">{!! number_format($jobs->psipop->basic_salary * 12,2) !!}</div>
</div>

<div class="row mb-2 bg-grey">
	<div class="col-4">Office/Division</div>
	<div class="col-8">{!! $jobs->psipop->division->Name !!}</div>
</div>

<div class="row mb-2">
	<div class="col-4">Education</div>
	<div class="col-8">{!! $jobs->education!!}</div>
</div>

<div class="row mb-2 bg-grey">
	<div class="col-4">Experience</div>
	<div class="col-8">{!! $jobs->experience!!}</div>
</div>

<div class="row mb-2">
	<div class="col-4">Training</div>
	<div class="col-8">{!! $jobs->training!!}</div>
</div>

<div class="row mb-2 bg-grey">
	<div class="col-4">Eligibility</div>
	<div class="col-8">{!! $jobs->eligibility !!}</div>
</div>

<div class="row mb-2">
	<div class="col-4">Date Posted</div>
	<div class="col-8">{!! date('F d, Y',strtotime($jobs->created_at))!!}</div>
</div>


<div class="row mb-2">
	<div class="col-4 font-weight-bold">Address your applicantion letter to:</div>
</div>

<div class="row mb-2">
	<div class="col-8">
		The Executive Director <br>
		Philippine Competition Commission <br>
		25/F Vertis North Corporate Center 1, North Avenue Quezon City 1105
	</div>
</div>

<div class="row mb-2">
	<div class="col-4 font-weight-bold">Address the following documents:</div>
</div>

<div class="row mb-2">
	<div class="col-8">
		<ul>
			<li>Letter of Application</li>
			<li>Latest Personal Data Sheet or Curriculum Vitae with Work Experience Form (can be downloaded through www.csc.gov.ph</li>
			<li>Updated Service Record/Employment Certificate (preferably with statement of duties and responsibilities)</li>
			<li>Transcript of Records/Diploma</li>
			<li>Copy of Certificate of Eligibility/Board Rating</li>
			<li>Copies of Certificates of Training/Seminars Attended</li>
		</ul>
	</div>
</div>

<div class="row mb-2">
	<div class="col-8">Email your application and attachments to hrdd@phcc.gov.ph</div>
</div>

<div class="row mb-2">
	<div class="col-8 font-weight-bold">*Applications with incomplete documents will not be processed.</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection