{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-2 offset-1 text-center">NAME OF SCHOOL (Write in Full)</div>
  <div class="col-2 offset-1">DEGREE/COURSE (Write in Full)</div>
  <div class="col-2 text-center">Period of Attendance</div>
  <div class="col-1">Highest Level/Units Earned</div>
  <div class="col-1">Year Graduated</div>
  <div class="col-2">Scholarship/Academic <br> Honors Received</div>
</div>

<!-- <div class="form-group row">
  <div class="col-form-label col-1 font-weight-bold mt-4">
    Primary
  </div>
  <div class="col-3 mt-4">
    {{ Form::text('education[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('education[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    From
    {{ Form::text('education[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    To
    {{ Form::text('education[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('education[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('education[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('education[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>

</div> -->

<!-- {{--SECONDARY--}}
<div class="form-group row">
  {{ Form::label('primary_name', 'Secondary', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('education[1][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('education[1][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('education[1][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('education[1][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('education[1][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('education[1][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('education[1][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div> -->

{{--VOCATIONAL--}}
  
<div class="row form-group">
  <div class="col-1">
    <a href="#" id="add_vocational" class="btn btn-info">
      <i class="fa fa-plus"></i> Add </a>
  </div>
</div>

<div class="row">
  <div class="col-2">
    <select class="form-control-xs form-control" id="educ_level" >
      <option>Select education level</option>
      <option value="1">Primary</option>
      <option value="2">Secondary</option>
      <option value="3">Vocational</option>
      <option value="4">College</option>
      <option value="5">Graduate Studies</option>
    </select>
  </div>

</div>

<div class="form-group row vocational">
  <!-- {{ Form::label('primary_name', 'Vocational', ['class'=>'col-form-label col-1 font-weight-bold col']) }} -->
  <!-- <div class="col-3 pr-0">
    {{ Form::text('education[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 font-weight-bold"> -->
      <!-- <div class="custom-control custom-checkbox mt-2">
        <input class="custom-control-input" type="checkbox" id="check0" name="graduate[0][ongoing]" >
        <label class="custom-control-label" for="check0" style="font-size: 7pt;" >Ongoing</label>
      </div> -->
  <!-- </div>
  <div class="col-2 pr-0 pl-0">
    {{ Form::text('education[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold pr-0">
    <input size="16" type="text"  name="education[0][attendance_from]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="From">
  </div>
  <div class="col-1 text-center font-weight-bold pr-0 ">
    <input size="16" type="text"  name="education[0][attendance_to]"
                           class="form-control form-control-sm datetimepicker date" style="font-size: 8pt;" data-min-view="2" data-date-format="yyyy-mm-dd" placeholder="To">
  </div>
  <div class="col-1 pr-0">
    {{ Form::text('education[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 pr-0">
    {{ Form::text('education[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('education[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
  </div> -->
</div>
<!-- 
{{--COLLEGE--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_college" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<div class="form-group row college">
  <label class="col-1 col-form-label text-sm-right">College <span style="color:red;">*</span> </label>
  <div class="col-3">
    {{ Form::text('college[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('college[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('college[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('college[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('college[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('college[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('college[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
  </div>
</div>

{{--GRADUATE STUDIES--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_graduate_studies" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<div class="form-group row graduate-studies">
  <label class="col-form-label col-1 font-weight-bold col">Graduate <br> Studies</label>
  <div class="col-3">
    {{ Form::text('graduate[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('graduate[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('graduate[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('graduate[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('graduate[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('graduate[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('graduate[0][awards]', '', [
            'class' => 'form-control form-control-sm col-9 pr-0 mr-1',
        ])
    }}
  </div>
</div>
 -->
<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
  </div>
</div>
