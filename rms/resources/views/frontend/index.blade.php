@extends('frontend.layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

@section('content')
    <div class="bg-light">
        <div class="main-content container-fluid bg-light ">
          @include('layouts._flash-message')
            <div class="card card-table card-border-color card-border-color-primary">
                <div class="card-header">
                  <h3 class="mb-4">CAREERS</h3>
                </div>
                <div class="card-body">
                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                        <thead>
                            <tr class="text-center">
                                <th>Item Number</th>
                                <th>Position Title</th>
                                <th>Job Grade</th>
                                <th>Annual Salary</th>
                                <th>Required Qualifications</th>
                                <th>Job Descriptions</th>
                                <th>Deadline of Application</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($jobs as $key => $job)
                            <tr>
                                <td style="vertical-align: top" class="text-center" >
                                    {{@$job->psipop->item_number}} <br> {{ config('params.employee_status.'.$job->employee_status_id)}}
                                </td>
                                <td style="vertical-align: top;">{{@$job->psipop->position->Name}}</td>
                                <td style="vertical-align: top" class="text-center">
                                  {{ config('params.job_grades.'.@$job->psipop->job_grade) }}
                                </td>
                                <td style="vertical-align: top;">{!! number_format($job->psipop->basic_salary,2) !!}</td>
                                <td style="vertical-align: top">
                                  @if($job->education)
                                    <p style="text-indent: 20px;text-align: justify;">
                                      {!! $job->education !!}
                                    </p>
                                  @endif

                                  @if($job->experience)
                                    <p style="text-indent: 20px;text-align: justify;">{!! $job->experience !!}</p>
                                  @endif

                                  @if($job->training)
                                    <p style="text-indent: 20px;text-align: justify;">{!! $job->training !!}</p>
                                  @endif

                                  @if($job->eligibility)
                                    <p style="text-indent: 20px;text-align: justify;">{!! $job->eligibility !!}</p>
                                  @endif
                                </td>
                                <td style="vertical-align: top" class="text-center">
                                     <a href="{{ route('frontend.show',['id'=>$job->id] ) }}" class="btn btn-info mt-3" target="_blank"><i class="icon icon-left mdi mdi-eye"></i> View</a>
                                </td>
                                <td style="vertical-align: top;" class="text-center">
                                    <span style="font-size: 12px;">{{ $job->deadline_date }}</span> <br>
                                    <button data-modal="md-flipH" class="btn btn-info md-trigger show" data-job_id="{{$job->id}}"> Apply Now</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=" form-group row">
              <div class="col-md-12">
                  <h4 class="font-weight-bold">​Attach the following documents​</h4>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-12">
                <ol>
                  <li>Letter of Application</li>
                  <li>Latest Personal Data Sheet or Curriculum Vitae with Work Experience Form (can be downloaded through www.csc.gov.ph)</li>
                  <li>Updated Service Record/Employment Certificate (preferably with statement of duties and responsibilities)</li>
                  <li>Transcript of Records/Diploma</li>
                  <li>Copy of Certificate of Employment  </li>
                  <li>Copy of Certificate of Eligibility/Board Rating</li>
                  <li>Copies of Certificates of Training/Seminars Attended</li>
                </ol>

              </div>
            </div>

            <div class=" row">
              <div class="col-md-12">
                  <h5 >​Email your application and the attachments to hrdd@phcc.gov.ph.</h5>
              </div>
            </div>

            <div class=" row">
              <div class="col-md-12">
                  <h5 class="font-weight-bold">​*Applications with incomplete documents will not be processed.</h5>
              </div>
            </div>

        </div>
    </div>

@include('frontend._notification')
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}"
            type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();

      $.fn.niftyModal('setDefaults', {
          overlaySelector: '.modal-overlay',
          contentSelector: '.modal-content',
          closeSelector: '.modal-close',
          classAddAfterOpen: 'modal-show',
      });

      var jobId;
      $(document).on('click','.show',function(){
        jobId = $(this).data('job_id');
      });

      var check;
      $('input[name=i_agree]').on('change',function(){
        check = $(this).val();
      });

      $('#btn-submit').on('click',function(){

         if(check == 'yes'){
            href = window.location+'/create?id='+jobId;
            window.open(href, '_blank');
            $('.mdi-close').trigger('click');
         }else{
            return false;
         }

      })

    });
  </script>
@endsection
