@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => 'SelectionLineUpController@getSelected', 'method' => 'GET', 'id' => 'form']) !!}
<div class="form-group row">
    {{ Form::label('', 'Position', ['class'=>'col-12 col-sm-1 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-3">
      <select name="job_id" id="job_id" class="form-control form-control-xs" {{ (@$jobSelected) ? 'disabled' : '' }}>
        <option value="0">Select position</option>
        @if(@$positions)
          @foreach($positions as $position)
          <option value="{{$position->job_id }}" {{ ($position->job_id == @$jobSelected) ? 'selected' : '' }} >{{ @$position->job->psipop->position->Name }}</option>
          @endforeach
        @endif
      </select>
    </div>
</div>
{!! Form::close() !!}

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'form-2']) !!}
<input type="hidden" name="job_id" value="{{ @$jobSelected}}">

<table class="table table-bordered">
  <thead class="text-center">
    <tr>
      <th rowspan="2">PSB MEMBERS</th>
      <th colspan="{{ (@$applicant) ? count($applicant) : 0 }}">Applicants</th>
    </tr>
    <tr>
      @if(@$applicant)
      @foreach($applicant as  $value)
        <input type="hidden" name="id[{{$value->applicant_id}}]" value="{{ (@$status == 'edit') ? $value->id : '' }}">
        <th>{!! $value->applicant->getFullName() !!}</th>
      @endforeach
      @endif
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>EE Representative</td>
      @if(@$applicant)
      @foreach($applicant as $key => $value)
      <td>
        <div class="col-12 col-sm-8 col-lg-6 pt-1 offset-4">
            <div class="switch-button switch-button-success switch-button-yesno">
              @if($value->er_representative_selected)
                <input type="checkbox" name="er_representative[{{$value->applicant_id}}]" id="swt1{{$value->applicant_id}}" checked>
              @else
                <input type="checkbox" name="er_representative[{{$value->applicant_id}}]" id="swt1{{$value->applicant_id}}">
              @endif
                <span><label for="swt1{{$value->applicant_id}}"></label></span>
            </div>
            <input type="hidden" name="applicant_id[{{$key}}]" value="{{$value->applicant_id}}">
        </div>
      </td>
      @endforeach
      @else
      <td></td>
      @endif
    </tr>
    <tr>
      <td>HRDD</td>
      @if(@$applicant)
      @foreach(@$applicant as $value)
      <td>
        <div class="col-12 col-sm-8 col-lg-6 pt-1 offset-4">
            <div class="switch-button switch-button-success switch-button-yesno">
              @if($value->hrdd_selected)
                <input type="checkbox" name="hrdd[{{$value->applicant_id}}]" id="swt2{{$value->applicant_id}}" checked>
              @else
                <input type="checkbox" name="hrdd[{{$value->applicant_id}}]" id="swt2{{$value->applicant_id}}">
              @endif
                <span><label for="swt2{{$value->applicant_id}}"></label></span>
            </div>
        </div>
        <input type="hidden" name="applicant_id[{{$key}}]" value="{{$value->applicant_id}}">
      </td>
      @endforeach
      @else
      <td></td>
      @endif
    </tr>
    <tr>
      <td>Chairperson & AO Director</td>
      @if(@$applicant)
      @foreach(@$applicant as $value)
      <td>
        <div class="col-12 col-sm-8 col-lg-6 pt-1 offset-4">
            <div class="switch-button switch-button-success switch-button-yesno">
                @if($value->chairperson_selected)
                <input type="checkbox" name="chairperson[{{$value->applicant_id}}]" id="swt3{{$value->applicant_id}}" checked>
                @else
                <input type="checkbox" name="chairperson[{{$value->applicant_id}}]" id="swt3{{$value->applicant_id}}">
                @endif
                <span><label for="swt3{{$value->applicant_id}}"></label></span>
            </div>
        </div>
        <input type="hidden" name="applicant_id[{{$key}}]" value="{{$value->applicant_id}}">
      </td>
      @endforeach
      @else
      <td></td>
      @endif
    </tr>
    <tr>
      <td>
        <span class="mb-2">Head of Requisitioning Office</span> <br>
        <u>Reason for Recommendation</u> <br>
        <span> - Please see attached memo.</span>
      </td>
      @if(@$applicant)
      @foreach(@$applicant as $value)
      <td>
        <div class="col-12 col-sm-8 col-lg-6 pt-1 offset-4">
            <div class="switch-button switch-button-success switch-button-yesno">
                @if($value->hrmo_selected)
                <input type="checkbox" name="hrmo[{{$value->applicant_id}}]" id="swt4{{$value->applicant_id}}" checked>
                @else
                <input type="checkbox" name="hrmo[{{$value->applicant_id}}]" id="swt4{{$value->applicant_id}}">
                @endif
                <span><label for="swt4{{$value->applicant_id}}"></label></span>
            </div>
        </div>
        <input type="hidden" name="applicant_id[{{$key}}]" value="{{$value->applicant_id}}">
      </td>
      @endforeach
      @else
      <td></td>
      @endif
    </tr>
  </tbody>
</table>

<div class="form-group row text-right">
  <div class="col col-sm-12">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}


@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <!-- wysiwyg -->
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      $('#job_id').change(function() {
        $('#form').submit();
      });
    });

  </script>
@endsection
