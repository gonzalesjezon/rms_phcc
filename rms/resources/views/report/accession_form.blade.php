@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print {
    @page {
       size: 7in 9.25in;
       margin: 27mm 16mm 27mm 16mm;
    }
  }
</style>
@endsection

@section('content')
<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="width:960px;margin: auto; font-size: 12pt;font-family: Times New Roman, serif;">
  <div class="row mb-1">
    <div class="col-6">
        <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
    </div>
    <div class="col-6 text-right">
        <span>
          <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
        North Avenue, Quezon City 1105 <br>
        <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
        <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
        </span>
    </div>
  </div>
  <div class="border-bottom border-dark mb-1"></div>

  <div class="row mb-4 mt-6">
  	<div class="col-md-12 text-center">
  		<strong class="text-uppercase">{!! date('F Y') !!} REPORT on ACCESSION</u></strong>
  	</div>
  </div>

  <table class="table table-bordered mb-6">
  	<thead class="text-center">
  		<tr>
  			<th>Name</th>
  			<th>Position Title</th>
  			<th>Status of Appointment</th>
  			<th>Mode of Accession</th>
  			<th>Effectivity Date of Appointment</th>
  		</tr>
  	</thead>
  </table>

  <div class="row mb-8">
  	<div class="col-4">Prepared By</div>
  	<div class="col-4"></div>
  	<div class="col-4">Submitted By</div>
  </div>

  <div class="row mb-4">
  	<div class="col-4">
  		Camille Rae G. Echague <br>
		Human Resource Management Officer II <br>
		Human Resource Development Division <br>
  	</div>
  	<div class="col-4"></div>
  	<div class="col-4">
  		Antonia Lynnely L. Bautista <br>
		Chief Administrative Officer <br>
		Human Resource Development Division
  	</div>
  </div>


</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection