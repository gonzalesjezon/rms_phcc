@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div class="row">
	<div class="col-12 text-center">
		<h4 class="font-weight-bold pb-0">APPOINTMENT PROCESSING CHECKLISTS</h4>
		<span>(REGULATED/NON-ACCREDITED)</span>
	</div>
</div>
@if($appointment)
<div class="row">
    <div class="col-12">
    	<label class="font-weight-bold card-title">Common Requirements for Regular Appointments</label>
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td></td>
                    <td class="p-1">Requirements</td>
                    <td class="p-1">HRMO</td>
                    <td class="p-1">CSFO</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td class="p-1">
                        <b>APPOINTMENT FORMS</b> (CS Form No. 33-B, Revised 2017) <br> - Original CSC copy of appointment form
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form33_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="p-1">
                       <b>PLANTILLA OF CASUAL APPOINTMENT</b> (CSC Form No. 34-B or D) <br> - Original CSC copy
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form34b_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td class="p-1">
                       <b>PERSONAL DATA SHEET</b> (CS Form No. 212, Revised 2017)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form212_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td class="p-1">
                       <b>ORIGINAL COPY OF AUTHENTICATED CERTIFICATE OF ELIGIBILITY/ RATING/ LICENSE</b> <br> - Except if the eligibility has been previously authenticated in 2004 or onward and recorded
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->eligibility_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td class="p-1">
                       <b>POSITION DESCRIPTION FORM</b> (DBM-CSC Form No. 1, Revised 2017)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form1_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td class="p-1">
                      <b>OATH OF OFFICE</b> (CS Form No. 32, Revised 2017)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form32_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>7</td>
                    <td class="p-1">
                      <b>CERTIFICATE OF ASSUMPTION TO DUTY</b> (CS Form No. 4)
                    </td>
                    <td class="p-1 text-center">{{ ($appointment->form4_hrmo == 1) ? 'YES' : 'N/A' }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2" style="vertical-align: middle;" class="text-center">
                        <b>{{ $appointment->applicant->getFullName() }}</b>
                    </td>
                    <td>
                        <p style="font-size: 10px;">
                            This is to certify that I have checked the veracity, <br> authenticity and completeness of all the <br>requirements in support of the appointments attached herein.
                        </p>
                        <p  style="font-size: 10px;" class="text-center pt-5">
                            LOLITA E. ETRATA <br> <span class="border-top border-dark" > Highest Ranking HRMO </span>
                        </p>
                    </td>
                    <td>
                        <p style="font-size: 10px;" >
                            This is to certify that I have checked  all the <br>  requirements in support  of the appointments  <br>attached herein and found these <br> to be  [  ] complete /  [  ]   lacking.
                        </p>
                        <p  style="font-size: 10px;" class="text-center">
                            <br> <span class="border-top border-dark" >CSC FO Receiving Officer</span>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endif

<div class="form-group row text-right d-print-none">
<div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
  {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
  {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection