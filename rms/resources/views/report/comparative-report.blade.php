@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print{
    @page{
      size:a4 landscape;
    }

    .table>thead>tr>th, .table>tbody>tr>td{
      padding: 2px !important;
    }

    .font-style{
      font-family: Trebuchet MS, Helvetica, sans-serif;
      font-size: 8pt;
    }
    .bg-secondary{
      background-color: #878787 !important;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 2px !important;
    border:1px solid #000;
  }
  .font-style{
    font-family: Trebuchet MS, Helvetica, sans-serif;
    font-size: 8pt;
  }
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<div class="dropdown-divider d-print-none"></div>

<div id="reports" class="font-style" style="width: 960px;margin: auto;">
    <div class="row mb-4">
      <div class="col-6">
        <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
      </div>
      <div class="col-6 text-right">
          <span>
            <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
          North Avenue, Quezon City 1105 <br>
          <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
          <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
          </span>
      </div>
    </div>

    <div class="row mb-1 ">
        <div class="col-2"><b>Position</b></div>
        <div class="col-6"><b>{{@$job->psipop->position->Name}}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2"><b>Job Grade</b></div>
        <div class="col-6"><b>{{ config('params.job_grades.'.@$job->psipop->job_grade)}}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2"><b>Item Number</b></div>
        <div class="col-6"><b>{{@$job->psipop->item_number}}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2"><b>Dvision/Office</b></div>
        <div class="col-6"><b>{{@$job->psipop->division->Name}}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-3"><b>Qualification Standards</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2" style="text-indent: 20px;">Education</div>
        <div class="col-6"><b>{!! @$job->education !!}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2" style="text-indent: 20px;">Experience</div>
        <div class="col-6"><b>{!! @$job->experience !!}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2" style="text-indent: 20px;">Training</div>
        <div class="col-6"><b>{!! @$job->training !!}</b></div>
    </div>
    <div class="row mb-1 ">
        <div class="col-2" style="text-indent: 20px;">Eligibility</div>
        <div class="col-6"><b>{!! @$job->eligibility !!}</b></div>
    </div>

    <div class="row mb-1 text-center ">
      <div class="col-sm-12"><h4 class="font-weight-bold" style="text-decoration: underline;">COMPARATIVE RANKING SUMMARY</h4></div>
    </div>

    <table id="table1" class="table table-bordered " >
    <thead class="text-center">
    <tr class="bg-secondary">
      <th rowspan="2">NAME</th>
      <th colspan="2" scope="colgroup">PERFORMANCE</th>
      <th colspan="3" scope="colgroup">EDUCATION AND TRAINING</th>
      <th colspan="5" scope="colgroup">EXPERIENCE AND OUTSTANDING ACCOMPLISHMENTS</th>
      <th colspan="2" scope="colgroup">PYSCHOSOCIAL</th>
      <th colspan="2" scope="colgroup">POTENTIAL</th>
      <th rowspan="2">TOTAL <br> RATING</th>
      <th rowspan="2">RANK</th>
    </tr>
    <tr >
      <th>POINTS</th>
      <th scope="col" class="bg-secondary">40%</th>
      <th scope="col">POINTS (Education)+</th>
      <th scope="col"><i>POINTS (Training)</i></th>
      <th scope="col" class="bg-secondary">20%</th>
      <th scope="col">POINTS (Relevant)+</th>
      <th scope="col" style="width: 20px;"></th>
      <th scope="col"><i>POINTS  (Specialized)</i></th>
      <th scope="col" style="width: 20px;"></th>
      <th scope="col" class="bg-secondary">20%</th>
      <th scope="col">POINTS</th>
      <th scope="col" class="bg-secondary">10%</th>
      <th scope="col">POINTS</th>
      <th scope="col" class="bg-secondary">10%</th>
    </tr>
    </thead>
    <tbody class="text-center">
      @if(count($evaluations) > 0)
        @foreach($evaluations as $key => $evaluation)
        <tr>
          <td nowrap class="text-left">{{$evaluation->applicant->getFullname()}}</td>
          <td>{{$evaluation->performance_average}}</td>
          <td class="bg-secondary">{{$evaluation->performance_score}}</td>
          <td>{{ $evaluation->education_points  + $evaluation->minimum_education_points}}</td>
          <td >{{$evaluation->training_points + $evaluation->minimum_training_points}}</td>
          <td  class="bg-secondary">{{$evaluation->education_training_score}}</td>
          <td >{{$evaluation->minimum_experience_requirement}}</td>
          <td></td>
          <td>{{$evaluation->additional_points}}</td>
          <td></td>
          <td class="bg-secondary">{{$evaluation->experience_accomplishments_score}}</td>
          <td>{{$evaluation->psychosocial_percentage_rating}}</td>
          <td class="bg-secondary">{{$evaluation->psychosocial_score}}</td>
          <td>{{$evaluation->potential_percentage_rating}}</td>
          <td class="bg-secondary">{{$evaluation->potential_score}}</td>
          <td>{{$evaluation->total_score}}</td>
          <td class="text-center">{!! $key+1 !!}</td>
        </tr>
        @endforeach
      @endif
    </tbody>
  </table>

  <div class="row mb-1 ">
    <div class="col">
      Reference (based n Merit Selection Plan):
    </div>
  </div>

  <div class="row mb-1 ">
    <div class="col col-sm-2">Performance</div>
    <div class="col col-sm-1">40%</div>
    <div class="col-sm-9">
      indicates the employee's efficiency in discharging his/her duties and responsibilities.
    </div>
  </div>

  <div class="row mb-2 ">
    <div class="col col-sm-2">Education & Training</div>
    <div class="col col-sm-1">20%</div>
    <div class="col-sm-9">
      includes educational background, trainings/seminars attended relevant to the duties of the position to be filled. To
      enhance the effectiveness of the employee in achieving objectives and goals, PCC gives credit to candidates with
      credentials in higher education. However, only relevant educational degree or units/trainings in excess of minimum
      requirements shall be given extra points. For extra points, only the trainings/seminars for the least five(5) years
      are credited and not to exceed ten(10) points.
    </div>
  </div>

  <div class="row mb-2 ">
    <div class="col col-sm-2">Experience</div>
    <div class="col col-sm-1">20%</div>
    <div class="col-sm-9">
      refers to occupational history, work experience related/relevant to the area of knowledge or activity required for
      the
      position.
    </div>
  </div>

  <div class="row mb-2 ">
    <div class="col col-sm-2">Psychosocial Attributes</div>
    <div class="col col-sm-1">10%</div>
    <div class="col-sm-9">
      refers to the characteristics or traits of a person which involve both physical and social aspects. Pyschological
      attributes include the way the applicant/candidate perceives things, ideas, beliefs and understanding and how the
      applicant/candidate acts and relates them to others and social situations. This shall be measured in the panel
      interview to be conducted by the internal selection committee.
    </div>
  </div>

  <div class="row mb-2 ">
    <div class="col col-sm-2">Potential</div>
    <div class="col col-sm-1">10%</div>
    <div class="col-sm-9">
      refers to the employee's capacity to perform the duties and assume the responsibilities of the position to be filled
      and those of higher positions. This shall be measured in the panel interview to be conducted by the internal
      selection
      committee.
    </div>
  </div>

  <!-- <div class="row mb-8 ">
    <div class="col col-sm-2">Total</div>
    <div class="col col-sm-1">100%</div>
  </div>

  <div class="row mb-4 ">
    <div class="col">Evaluated By:</div>
  </div>

  <div class="form-group row text-center ">
    <div class="col-3">
      <hr>
      ISC Chairperson
    </div>
    <div class="col-3">
      <hr>
      ISC Member
    </div>
    <div class="col-3">
      <hr>
      ISC Member
    </div>
    <div class="col-3">
      <hr>
      EA Representative
    </div>
  </div> -->
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection