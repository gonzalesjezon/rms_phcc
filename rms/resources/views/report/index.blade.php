@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
    <style type="text/css">
        .select2-container--default .select2-selection--single{
            height: 3rem !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 3.13816rem !important;
            font-size: 1rem !important;
            height: 2rem !important;
        }
    </style>
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Reports</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
          </div>
          <div class="card-body p-2">
            <div class="form-group row">
              {{ Form::label('reports_name', 'Reports Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('reports_name', $reports, '', [
                        'class' => 'select2 select2-sm',
                        'placeholder' => 'Select report',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row d-none hide hide-all" id="select_position">
              {{ Form::label('select_job', 'Position', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm unselect" name="select_job" id="select_job">
                  <option value="0">Select position</option>
                  @foreach($jobs as $job)
                  <option value="{{$job->id}}">{{$job->psipop->position->Name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row d-none hide hide-all" id="select_applicant">
              {{ Form::label('applicant_name', 'Name', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('name', $applicants, '', [
                        'class' => 'form-control form-control-xs unselect',
                        'placeholder' => 'Select applicant',
                        'required' => true,
                        'id' => 'select_app'
                    ])
                }}
              </div>
            </div>

            <div class="row form-group d-none hide hide-all" id="get_month">
              {{ Form::label('', 'Select Month', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('', $months, '', [
                        'class' => 'form-control form-control-xs unselect',
                        'placeholder' => 'Select month',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="row form-group hide hide-all d-none issued transmittal" >
              {{ Form::label('', 'HRMO', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm unselect" id="hrmo">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group hide hide-all d-none issued" >
              {{ Form::label('', 'Agency Head', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm unselect" id="agency_head">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="row form-group d-none hide hide-all select_period">
              {{ Form::label('', 'Transaction Period', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd HH:ii" class="input-group date datetimepicker">
                  <input size="16" type="text" value=""
                         class="form-control form-control-sm" id="from_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>

              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd HH:ii" class="input-group date datetimepicker">
                  <input size="16" type="text" value=""
                         class="form-control form-control-sm" id="to_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>

            <div class="row form-group d-none hide hide-all" id="display_signatory">
              {{ Form::label('', 'Signatory', [
                  'class'=>'col-12 col-sm-2 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-5">
                <select class="select2 select2-sm unselect" id="select_signatory">
                  @foreach($employees as  $employee)
                    @if($employee)
                      <option value="{{ $employee->RefId }}" data-sign="{{ $employee->getFullName().'|'.@$employee->employeeinfo->position->Name  }}">{!! $employee->getFullName() !!}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-sm-2 col-form-label text-sm-right">Print Date </label>
              <div class="col-12 col-sm-8 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                  <input size="16" type="text" value="" name="print_date"
                         class="form-control form-control-sm" id="print_date">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>

<!--             <div class="form-group row">
              <label class="col-12 col-sm-3 col-md-1 col-form-label text-sm-right"> Sex </label>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-female"></span></div><span class="icon-class"> </span>
                </div>
              </div>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-male"></span></div><span class="icon-class"> </span>
                </div>
              </div>
            </div> -->

            <hr>

            <div class="form-group row">
              <div class="col-4 offset-6">
                <buton class="btn btn-secondary" id="preview">Preview</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      var reportName;
      var bool = false;
      $(document).on('change','#reports_name',function(){
          bool = false;
          printDate = '';
          $('#select_job').prop('selectedIndex',0);
          $('#select_app').prop('selectedIndex',0);

          $('.unselect').val([]);

          reportName = $(this).find(':selected').val();
          $('.hide-all').addClass('d-none');

          switch(reportName){
            case 'selection_lineup':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              $('#get_month').addClass('d-none');
              bool = true;
              break;
            case 'checklist':
              $('#select_applicant').removeClass('d-none');
              $('#select_position').addClass('d-none');
              $('#get_month').addClass('d-none');
              bool = true;
              break;
            case 'resignation_acceptance':
              $('#select_applicant').removeClass('d-none');
              $('#select_position').addClass('d-none');
              $('#get_month').addClass('d-none');
              bool = true;
              break;
            case 'preliminary_evaluation':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              $('#get_month').addClass('d-none');
              bool = true;
              break;
            case 'comparative-report':
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              $('#get_month').addClass('d-none');
              bool = true;
              break;
            case 'oath_office':
              $('#select_position').addClass('d-none');
              $('#get_month').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'appointment_form_regulated':
              $('#select_position').addClass('d-none');
              $('#get_month').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              bool = true;
              break;
            case 'medical_certificate':
              $('#select_position').addClass('d-none');
              $('#select_applicant').removeClass('d-none')
              $('#get_month').addClass('d-none');;
              bool = true;
              break;
            case 'erasures_alteration':
              $('#select_position').addClass('d-none');
              $('#select_applicant').removeClass('d-none');
              $('#get_month').addClass('d-none');
              bool = true;
              break;
            case 'publication_vacant_position':
              $('.select_period').removeClass('d-none');
              $('#display_signatory').removeClass('d-none');
              bool = true;
              break;
            case 'matrix_qualification':
              $('#get_month').addClass('d-none');
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'psb_matrix_qualification':
              $('#get_month').addClass('d-none');
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'matrix_qualification_3':
              $('#get_month').addClass('d-none');
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'psb_matrix_recommended':
              $('#get_month').addClass('d-none');
              $('#select_position').removeClass('d-none');
              $('#select_applicant').addClass('d-none');
              bool = true;
              break;
            case 'appointment-transmital':
              $('.select_period').removeClass('d-none');
              $('.transmittal').removeClass('d-none');
              bool = true;
              break;
            case 'appointments_issued':
              $('.issued').removeClass('d-none');
              $('.select_period').removeClass('d-none');
              bool = true;
              break;
            default:
              $('.hide').addClass('d-none')
              bool = false;
              break;
          }
      });

      var jobId;
      $(document).on('change','#select_job',function(){
          jobId = $(this).find(':selected').val();
          bool = true;

      })

      var appId;
      $(document).on('change','#select_applicant',function(){
          appId = $(this).find(':selected').val();
          bool = true;

      });

      var month;
      $(document).on('change','#get_month',function(){
          month = $(this).find(':selected').val();
          bool = true;

      })

      var printDate;
      $(document).on('change','#print_date',function(){
        printDate = $(this).val();
      });

      var id;
      var param;
      var param2;
      var selected_month;
      $(document).on('click','#preview',function(){

          let hrmo        = $('#hrmo :selected').data('sign');
          let agencyHead  = $('#agency_head :selected').data('sign');
          let cscOfficial = $('#csc_official :selected').data('sign');
          let signatoryId = $('#select_signatory :selected').val();
          let fromDate    = $('#from_date').val();
          let toDate      = $('#to_date').val();

          if(month){
            selected_month = month;
          }else{
            if(jobId){
              id = jobId;
            }else{
              id = appId;
            }
          }

          arrSign = [];
          switch(reportName)
          {
            case 'appointments_issued':
              arrSign = {
                'hrmo':hrmo, 
                'agency_head':agencyHead,
                'from':fromDate,
                'to':toDate,
              };
              break;

            case 'appointment-transmital':
              arrSign = {
                'hrmo':hrmo, 
                'from':fromDate,
                'to':toDate,
              };
              break;

            case 'publication_vacant_position':
              arrSign = {
                'hrmo':hrmo, 
                'from':fromDate,
                'to':toDate,
              };
              break;
          }


          param = (id) ? 'id='+id : '';
          param2 = (selected_month) ? 'month='+selected_month : '';
          date = (printDate) ? 'date='+printDate : '';
          signatory = (signatoryId) ? 'signatory_id='+signatoryId+'&' : '';
          sign = (!Array.isArray(arrSign) || !arrSign.length) ? 'sign='+JSON.stringify(arrSign) : '';

          var href = '';
          if(bool == true){
            if(param || date || param2 || sign){
              href = window.location+'/'+reportName+'?'+param+date+param2+signatory+sign;
            }else{
              alert('Select position or applicant first!');
              return false;
            }
          }else{

              href = window.location+'/'+reportName;

          }

          window.open(href, '_blank');
      });

    });
  </script>
@endsection
