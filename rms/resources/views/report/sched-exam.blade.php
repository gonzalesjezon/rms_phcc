<link rel="stylesheet" href="{{ URL::asset('beagle-assets/css/app.css') }}" type="text/css"/>
<div style="width: 760px; margin: auto;">

	<div class="row form-group">
		<div class="col-12">
			<p>{{ date('F d, Y',time())}}</p>
			<p class="mt-4">Name</p>
			<p class="mt-4">Dear Mr/Ms</p>
			<p class="mt-4">Good day!</p>
			<p class="mt-4">
				Thank you for your interest in exploring career opportunities in PCW. Upon initial assessment, you qualified to our vacant [[POSITION]] position. We are inviting you to take the technical and psychological exam on [[EXAM DATE]] at [[EXAM TIME]], 1145 J.P. Laurel St., San Miguel Manila 1005 Philippines. Please come on time, late comers will not be entertained.
			</p>
			<p class="mt-2">Please reply with your name to confirm attendance.</p>
			<p class="mt-5">Best regards,</p>
			<p class="mt-5">Human Resource Management and Development Section <br>
			   Philippine Commission on Women </p>
		</div>
	</div>
</div>
