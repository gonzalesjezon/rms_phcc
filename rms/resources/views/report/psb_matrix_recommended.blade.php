@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
	@media print{
    @page{
      size: legal landscape;
    }
    .table>thead>tr>th{
      vertical-align: middle !important;
      border: 1px solid #000;
          padding: 2px !important;
    }

    .table>tbody>tr>td{
      vertical-align: top !important;
      border: 1px solid #000;
      padding: 2px !important;
    }

    .font-style{
        font-family: Trebuchet MS, Helvetica, sans-serif;
        font-size: 9pt !important;
      }
  }
  .table>thead>tr>th{
    vertical-align: middle !important;
      padding: 2px !important;
      border: 1px solid #000;
  }

  .table>tbody>tr>td{
    vertical-align: top !important;
    padding: 2px !important;
    border: 1px solid #000;
  }

  .font-style{
      font-family: Trebuchet MS, Helvetica, sans-serif;
    font-size: 9pt !important;
   }
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<div class="dropdown-divider d-print-none"></div>

<div id="reports" class="font-style" style="width: 960px;margin: auto;">

  <div class="row mb-4">
    <div class="col-6">
      <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
    </div>
    <div class="col-6 text-right">
        <span>
          <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
        North Avenue, Quezon City 1105 <br>
        <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
        <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
        </span>
    </div>
  </div>

  <div class="dropdown-divider"></div>

   <div class="row mb-8 text-center">
    <div class="col-sm-12"><h4 class="font-weight-bold" >MATRIX OF QUALIFICATIONS</h4></div>
  </div>

  <div class="row mb-1">
      <div class="col-6"><b>Position for Consideration</b> : <b style="padding-left: 10px;">{{$job->psipop->position->Name}}</b></div>
      <div class="col-"></div>
      <div class="col-2 text-right"><b>Dvision/Office</b> :</div>
      <div class="col-4"><b>{{$job->psipop->division->Name}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-3"><b>Qualification Standards</b> :</div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Education</div>
      <div class="col-8"><b>{!! $job->education !!}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Experience</div>
      <div class="col-8"><b>{!! $job->experience !!}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Training</div>
      <div class="col-8"><b>{!! $job->training !!}</b></div>
  </div>
  <div class="row mb-2">
      <div class="col-2" style="text-indent: 20px;">Eligibility</div>
      <div class="col-8"><b>{!! $job->eligibility !!}</b></div>
  </div>

  <table id="table1" class="table table-bordered mb-4">
	  <thead class="text-center" style="background-color: #f9f973d1;">
	  <tr>
	    <th colspan="1">NAME</th>
	    <th rowspan="2" style="width: 50px;">AGE</th>
	    <th rowspan="2" style="width: 250px;">EDUCATIONAL BACKGROUND</th>
	    <th rowspan="2" style="width: 250px;">WORK EXPERIENCE</th>
	    <th rowspan="2" style="width: 100px;">ELIGIBILITY </th>
	    <th rowspan="2" style="width: 200px;">TRAINING</th>
	    <th rowspan="2" style="width: 150px;">REMARKS</th>
	  </tr>
	  <tr>
	  	<th nowrap>CURRENT POSITION</th>
	  </tr>
	  </thead>
	  <tbody>
        @if(count($matrixes) > 0)
    	  	@foreach($matrixes as $matrix)
    	  	<tr>
    	  		<td>{!! $matrix->applicant->getFullName() !!} <br> 
              @if($matrix->applicant->latestExperience)
              <span class="font-italic">{!! $matrix->applicant->latestExperience->position_title !!}</span> <br>
              <span class="font-italic">{!! $matrix->applicant->latestExperience->department !!}</span>
              @endif
            </td>
    	  		<td class="text-center">{!!  Carbon\Carbon::today()->diffInYears($matrix->applicant->birthday) !!}/{!! ($matrix->applicant->gender == 'male') ? 'M' : 'F' !!}
            </td>
    	  		<td>
    	  			@if($matrix->applicant->getHighestEduc)
    	  				@foreach($matrix->applicant->getHighestEduc as $value)
    	  				<p class="pl-1">{!! $value->course !!} <br> {!! $value->school_name !!} {!! ($value->ongoing == 1) ? '(ongoing)' : '' !!}</p>
    	  				@endforeach
    	  			@endif
    	  		</td>
    	  		<td>
    	  			@if($matrix->applicant->workexperience)

                @php 
                  $subYrExp       = 0;
                  $subMonExp      = 0;
                  $subYrExpPriv   = 0;
                  $subMonExpPriv  = 0;

                  $totalYrExp    = 0;
                  $totalMonthExp = 0;

                  $govPos     = [];
                  $privatePos = [];
                @endphp

    	  				@foreach($matrix->applicant->workexperience as $key => $value)
                
                  @php
                  $date1 = new DateTime($value->inclusive_date_from);
                  $toDate = ($value->present_work == 1) ? date('Y-m-d') : $value->inclusive_date_to;
                  $date2 = new DateTime($toDate);

                  $interval = $date2->diff($date1);

                  $yearExp = $interval->format('%y');
                  $monthExp = $interval->format('%m');

                  if($value->govt_service == 1){
                    $subYrExp += $yearExp;
                    $subMonExp += $monthExp;
                    $govPos[$key] = $value->position_title.' ('.$yearExp.' year, '.$monthExp.' months)';

                  }else{

                    $subYrExpPriv += $yearExp;
                    $subMonExpPriv += $monthExp;
                    $privatePos[$key] = $value->position_title.' ('.$yearExp.' year, '.$monthExp.' months)';

                  }

                  $totalMonthExp  = $subMonExp;
                  $totalYrExp = $subYrExp;

                  if($totalMonthExp >= 12){
                    $totalYrExp += 1;
                    $totalMonthExp -=12;
                  }

                  @endphp

    	  				@endforeach

                <p>
                 Total of {!! $totalYrExp !!} year {!! ($totalMonthExp) ? ' and '.$totalMonthExp. ' months' : '' !!} of work experience: 
                </p> 
                <!-- Government -->
                @if(count($govPos) > 0)

                <!-- List the government position -->

                Government Sector:
                @foreach($govPos as $value)
                {!! $value !!}
                @endforeach

                @endif

                <br>
                <div class="mb-4"></div>
                <!-- Private -->
                @if(count($privatePos) > 0)
                <!-- List the government position -->
                Private Sector:
                @foreach($privatePos as $value)
                {!! $value !!} <br>
                @endforeach

                @endif
                <div class="mb-2"></div>

    	  			@endif
    	  		</td>
    	  		<td>
    	  			@if($matrix->applicant->eligibility)
    	  				@foreach($matrix->applicant->eligibility as $value)
    	  				<span class="pl-2">{!! $value->eligibility_ref  !!}</span> <br>
    	  				@endforeach
    	  			@endif


    	  		</td>
    	  		<td>
    	  			<p class="pl-2" >{!! @$matrix->training_remarks!!}</p>
    	  		</td>
    	  		<td>
              @if($matrix->remarks)
              <p class="pl-2">{!! @$matrix->remarks !!}</p>
              @endif

              <p class="pl-2">
                Meets the minimum Qualification Standards requirements
              </p>   
            </td>
    	  	</tr>
    	  	@endforeach
        @endif
	  </tbody>
	</table>

	<div class="row ml-2 mb-5">
		<div class="col-3 font-weight-bold ">Prepared By:</div>
	</div>

	<div class="row ml-2 mb-5">
		<div class="col-3 ">Camille Rae G. Echague</div>
	</div>

	<div style="page-break-before: always;"></div>

	<div  class="border border-dark rounded">
      <div class="row mb-4">
          <div class="col text-center"><h3><strong>CERTIFICATION</strong></h3></div>
      </div>
      <div class="row mb-4">
          <div class="col text-center">The PCC HRM Promotion and Selection Board recommended the appointment of
          	 <span class="text-uppercase font-weight-bold"><u>{!! ($recommended) ? $recommended->applicant->getFullName() : '' !!}</u></span>
              <span class="text-uppercase"><u><strong></strong></u></span> for the position of <u class="font-weight-bold">{!! @$recommended->applicant->job->plantilla_item->position->Name !!}</u> Item No. <u class="font-weight-bold">{!! @$recommended->applicant->job->plantilla_item->Name!!}</u>, JG - <u class="font-weight-bold">{!! @$recommended->applicant->job->plantilla_item->salary_grade->Name!!}</u>
              <br />
          </div>
      </div>

      <div class="row text-center mb-2">
          <div class="col"><strong>PCC HRM PROMOTION AND SELECTION BOARD</strong></div>
      </div>

      <div class="row">
      	<div class="col-4">&nbsp;</div>
          <div class="col-4 border border-dark border-top-0 border-left-0 border-right-0"></div>
          <div class="col-4">&nbsp;</div>
      </div>

      <div class="row mb-8 font-weight-bold">
      	<div class="col-4">&nbsp;</div>
      	<div class="col-4 text-center">Chairperson</div>
      	<div class="col-4">&nbsp;</div>
      </div>



      <!-- <div class="row text-center font-weight-bold mb-2" style="margin-left: -5px;margin-right: -5px;">
          <div class="col-3">
          	<div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              Director IV - AO
          </div>
          <div class="col-3">
          	<div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              Head of Requisitioning Office
          </div>
          <div class="col-3">
          	<div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              Human Resource Development Division
          </div>
          <div class="col-3">
          	<div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              EA Representative
          </div>
      </div> -->
 </div>


</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection