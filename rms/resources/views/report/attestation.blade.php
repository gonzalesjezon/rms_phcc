@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
  @media print {
    @page {
       size: 7in 9.25in;
       margin: 27mm 16mm 27mm 16mm;
    }
  }
</style>
@endsection

@section('content')
<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="width:960px;margin: auto; font-size: 12pt;font-family: Times New Roman, serif;">
  <div class="row mb-1">
    <div class="col-6">
        <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
    </div>
    <div class="col-6 text-right">
        <span>
          <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
        North Avenue, Quezon City 1105 <br>
        <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
        <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
        </span>
    </div>
  </div>
  <div class="border-bottom border-dark mb-1"></div>

  <div class="row mb-2" style="font-size: 14pt;">
    <div class="col-1"></div>
    <div class="col-9 text-justify">
      <p style="text-indent: 0.5in;line-height:0.7cm;"></u>.</p>
    </div>
  </div>

  <div class="row mb-2">
  	<div class="col-10 text-right">{{ date('F d, Y',time()) }}</div>
  </div>

  <div class="row mb-2">
  	<div class="col-6">
  		<p class="font-weight-bold p-0 m-0">MS. JANINE P. DE VERA</p>
  		<p class="p-0 m-0">No. 15 Hill Drive, Beverly Hills</p>
  		<p class="p-0 m-0">Antipolo City, Rizal</p>
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-12 text-center">
  		<strong class="text-uppercase">RE: <u>ATTESTATION OF APPOINTMENT</u></strong>
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-12">
  		<p class="mb-6">Congratulations!</p>
		<p class="text-justify">
			We are pleased to inform you that the Civil Service Commission has approved your attestation to the <b>Economist III</b> position, JG-<b>9</b>, Item No. <b>PHCC-ECO3-2-2016</b>, <b>Permanent</b>, under the <b>Merger and Acquisition Office</b>
		</p>
  	</div>
  </div>

  <div class="row mb-3">
  	<div class="col-8">
  		<p>A copy of the attested appointment paper is attached for your reference.</p>
  	</div>
  </div>

  <div class="row mb-6">
  	<div class="col-6">
  		<p>Thank you for your continued hard work and support.</p>
  	</div>
  </div>

  <div class="row mb-6">
  	<div class="col-8"></div>
  	<div class="col-4">
  		<p>Very truly yours,</p>
  	</div>
  </div>

  <div class="row mb-6">
  	<div class="col-8"></div>
  	<div class="col-4">
  		<p class="font-weight-bold m-0 p-0">Kenneth V. Tanate, PhD.</p>
  		<p class="m-0 p-0">Executive Director </p>
  	</div>
  </div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection