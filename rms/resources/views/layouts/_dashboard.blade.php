<body>
   <div class="be-wrapper be-fixed-sidebar">
        @include('layouts._top-bar')
        @include('layouts._left-sidebar')
        @include('layouts._main-content')
        @include('layouts._right-sidebar')
   </div>
   <script src="{{ URL::asset('beagle-assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/js/app.js') }}" type="text/javascript"></script>
   @yield('scripts')
</body>
