<!-- Left Sidebar -->
<div class="be-left-sidebar">
  <div class="left-sidebar-wrapper">
    <a href="#" class="left-sidebar-toggle">Dashboard</a>
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">
            <li class="divider">Menu</li>
            <li class="{{ ($module == 'dashboard') ? 'active' : '' }}">
              <a href="{{ route('dashboard') }}"><i class="icon mdi mdi-home"> </i><span>Dashboard</span></a>
            </li>

            @php
              $modules = [];

              if(Auth::id() != 1){
                $modules = Auth::user()->access_module->access_rights->where('to_view',1)->pluck('module_id')->toArray();
              }

            @endphp

            <!-- MODULE 1 -->
            @if(in_array(1,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="{{ ($module == 'psipop') ? 'active' : '' }}" ><a href="{{ route('psipop.index') }}"><i class="icon mdi mdi-collection-item"> </i>
                <span>Itemization & Plantilla</span></a></li>
            @endif

            <!-- MODULE 2 -->
            @if(in_array(2,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="{{ ($module == 'jobs') ? 'active' : '' }}">
              <a href="{{ route('jobs.index') }}"><i class="icon mdi mdi-stackoverflow"> </i><span>Job Posting</span></a>
            </li>
            @endif

            @if(in_array(4,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="parent">
              <a href="#"><i class="icon mdi mdi-face"> </i><span>Applicants</span></a>
              <ul class="sub-menu">
                <!-- MODULE 4 -->
                <li class="{{ ($module == 'applicant') ? 'active' : '' }}">
                  <a href="{{ route('applicant.index') }}">List applicants</a>
                </li>
              </ul>
            </li>
            @endif

            @if(in_array(5,$modules) || in_array(6,$modules) || in_array(7,$modules) || in_array(8,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="parent">
              <a href="#"><i class="icon mdi mdi-star-circle"> </i><span>Evaluation</span></a>
              <ul class="sub-menu">
                <!-- MODULE 5 -->
                @if(in_array(5,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'matrix-qualification') ? 'active' : '' }}">
                  <a href="{{ route('matrix-qualification.index') }}">Matrix of Qualification</a>
                </li>
                @endif
                <!-- MODULE 6 -->
                @if(in_array(6,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'interviews') ? 'active' : '' }}">
                  <a href="{{ route('interviews.index') }}">Interview</a>
                </li>
                @endif

                <li class="{{ ($module == 'interview_evaluation_ratings') ? 'active' : '' }}">
                  <a href="{{ route('interview_evaluation_ratings.index') }}">Interview Evaluation Rating</a>
                </li>

                <!-- MODULE 7 -->
                @if(in_array(7,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'evaluation') ? 'active' : '' }}">
                  <a href="{{ route('evaluation.index') }}">Individual Assesment</a>
                </li>
                @endif
                <!-- MODULE 8 -->
                <!-- @if(in_array(8,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li><a href="{{ route('comparative-ranking.index')}}">Comparative Ranking</a></li>
                @endif -->

                <!-- @if(in_array(8,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_')) -->
                <li class="{{ ($module == 'selection-lineup') ? 'active' : '' }}">
                  <a href="{{ route('selection-lineup.index')}}">PSB Selection Line Up</a></li>
                <!-- @endif -->
              </ul>
            </li>
            @endif

            <!-- MODULE 9 -->
            <!-- @if(in_array(9,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
             <li><a href="{{ route('recommendation.index') }}"><i class="icon mdi mdi-assignment-account"> </i>
                <span>Recommendation</span></a></li>
            @endif -->

            <!-- MODULE 10 -->
            @if(in_array(10,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="parent">
              <a href="#"><i class="icon mdi mdi-badge-check"> </i><span>Job Offer</span></a>
              <ul class="sub-menu">
                <li class="{{ ($module == 'joboffer') ? 'active' : '' }}">
                  <a href="{{ route('joboffer.index') }}">Plantilla</a></li>
                <!-- <li><a href="{{ route('joboffer.index', ['status'=>'non-plantilla']) }}">Non-Plantilla</a></li> -->
              </ul>
            </li>
            @endif

            @if(in_array(11,$modules) || in_array(12,$modules) || in_array(13,$modules) || in_array(17,$modules) || in_array(18,$modules) || in_array(19,$modules) || in_array(20,$modules) || in_array(21,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="parent">
              <a href="#"><i class="icon mdi mdi-calendar"> </i><span>Appointment</span></a>
              <ul class="sub-menu">
                <!-- MODULE 11 -->
                @if(in_array(11,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'appointment-form') ? 'active' : '' }}">
                  <a href="{{ route('appointment-form.index') }}">Form</a></li>
                @endif
                <!-- MODULE 12 -->
                @if(in_array(12,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'appointment-issued') ? 'active' : '' }}">
                  <a href="{{ route('appointment-issued.index') }}">Transmital</a></li>
                @endif
                <!-- MODULE 13 -->
                @if(in_array(13,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'appointment-processing') ? 'active' : '' }}">
                  <a href="{{ route('appointment-processing.index') }}">Processing Checklist</a></li>
                @endif

                <!-- MODULE 17 -->
                @if(in_array(17,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'appointment-casual') ? 'active' : '' }}">
                  <a href="{{ route('appointment-casual.index') }}">Plantilla of Casual Appointment</a></li>
                @endif
                <!-- MODULE 18 -->
                @if(in_array(18,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'position-descriptions') ? 'active' : '' }}">
                  <a href="{{ route('position-descriptions.index')}}">Position Description</a></li>
                @endif
                <!-- MODULE 19 -->
                @if(in_array(19,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'oath-office') ? 'active' : '' }}">
                  <a href="{{ route('oath-office.index') }}">Oath of Office</a></li>
                @endif
                <!-- MODULE 20 -->
                @if(in_array(20,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'erasure_alterations') ? 'active' : '' }}">
                  <a href="{{ route('erasure_alterations.index') }}">Erasures & Alteration</a></li>
                @endif
                <!-- MODULE 21 -->
                @if(in_array(21,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'acceptance_resignation') ? 'active' : '' }}">
                  <a href="{{ route('acceptance_resignation.index')}}">Acceptance of Resignation</a></li>
                @endif
              </ul>
            </li>
            @endif

            <!-- <li class="parent">
              <a href="#"><i class="icon mdi mdi-assignment-check"> </i><span>Checklist</span></a>
              <ul class="sub-menu">
                <li><a href="{{ route('appointment.index') }}">Summary</a></li>
              </ul>
            </li> -->

            <!-- MODULE 14 -->
            @if(in_array(14,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="{{ ($module == 'appointment-requirements') ? 'active' : '' }}">
              <a href="{{ route('appointment-requirements.index') }}">
              <i class="icon mdi mdi-collection-text"> </i><span>Pre Emp. Requirements</span>
            </a></li>
            @endif
            <!-- MODULE 15 -->
            @if(in_array(15,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="{{ ($module == 'assumption') ? 'active' : '' }}">
              <a href="{{ route('assumption.index') }}">
              <i class="icon mdi mdi-face"> </i><span>Assumption to Duty</span></a></li>
            @endif

            @if(in_array(23,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <!-- MODULE 23 -->
            <li class="{{ ($module == 'report') ? 'active' : '' }}">
              <a href="{{ route('report.index') }}"><i class="icon mdi mdi-assignment-o"> </i>
                <span>Reports</span></a></li>
            @endif

            @if(in_array(24,$modules) || in_array(25,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
            <li class="parent">
              <a href="#"><i class="icon mdi mdi-settings"> </i><span>User Management</span></a>
              <ul class="sub-menu">
                <!-- MODULE 25 -->
                @if(in_array(25,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'access_modules') ? 'active' : '' }}">
                  <a href="{{ route('access_modules.index') }}">Access Module</a></li>
                @endif
                <!-- MODULE 24 -->
                @if(in_array(24,$modules) || Auth::id() == config('params._SUPER_ADMIN_ID_'))
                <li class="{{ ($module == 'users') ? 'active' : '' }}">
                  <a href="{{ route('users.index') }}">User</a></li>
                @endif
              </ul>
            </li>
            @endif

           <!--  @if(Auth::id() == config('params._SUPER_ADMIN_ID_'))
              <li class="parent">
                <a href="#"><i class="icon mdi mdi-settings"> </i><span>Configurations</span></a>
                <ul class="sub-menu">
                  <li><a href="{{ route('config.index') }}">List configurations</a></li>
                </ul>
              </li>
              <li class="divider">Features</li>
              <li class="parent">
                <a href="#"><i class="icon mdi mdi-inbox"> </i><span>Public Pages</span></a>
                <ul class="sub-menu">
                  <li><a href="{{ route('menu') }}">Menu</a></li>
                  <li><a href="{{ route('careers') }}">Careers</a></li>
                </ul>
              </li>
            @endif -->
          </ul>
        </div>
      </div>
    </div>

    <div class="progress-widget">
      <div class="progress-data"><span class="progress-value">70%</span><span class="name">Current Project</span>
      </div>
      <div class="progress">
        <div style="width: 60%;" class="progress-bar progress-bar-primary"></div>
      </div>
    </div>
  </div>
</div>
<!-- /. Left Sidebar -->
