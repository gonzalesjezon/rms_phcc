@section('css')
     <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
<style type="text/css">
    .select2-container--default .select2-selection--single{
        height: 3rem !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 3.13816rem !important;
        font-size: 1rem !important;
        height: 2rem !important;
    }
</style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {!! Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) !!}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="applicant_id" id="applicant_id">
                    <option>Select applicant</option>
                    @foreach($applicants as $applicant)
                    <option value="{{ $applicant->id }}"
                        data-position="{{ $applicant->job->psipop->position->Name }}"
                        data-division="{{ @$applicant->job->psipop->division->Name }}"
                        data-department="{{ @$applicant->job->psipop->department->Name }}"
                        data-office="{{ $applicant->job->psipop->office->Name }}"
                        data-item="{{ $applicant->job->psipop->item_number }}"
                        data-job_grade="{{ config('params.job_grades.'.$applicant->job->psipop->job_grade) }}"
                        data-salary="{{ number_format($applicant->job->psipop->basic_salary ,2) }}"
                        data-publish_date="{{ date('m/d/Y', strtotime($applicant->job->publish_date)) }}"
                        data-deadline_date="{{ date('m/d/Y', strtotime($applicant->job->deadline_date)) }}"
                        data-empstatus="{{ config('params.employee_status.'.@$applicant->appointment_form->employee_status_id) }}"
                        data-noa="{{ config('params.nature_of_appointment.'.$applicant->appointment_form->nature_of_appointment) }}"
                        {{ ($applicant->id == @$posdesc->applicant_id) ? 'selected' : '' }}
                        >
                        {{ $applicant->getFullName() }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'position'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Item Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'item'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Job / Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'job_grade'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Monthly Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'salary'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <b>Position Title</b>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Immediate Supervisor', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('immediate_supervisor', @$posdesc->immediate_supervisor, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <b>Position Title, and Item of those Directly Supervised :</b>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Item Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('item_number', @$posdesc->item_number, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row">
           &nbsp;
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'office'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'division'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Department', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'id' => 'department'
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Place of Work', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            &nbsp;
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Next Higher Supervisor', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('higher_supervisor', @$posdesc->higher_supervisor, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            &nbsp;
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('position_title', @$posdesc->position_title, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>
</div>

<div class="row mb-5">
    <div class="col-12">
        {{ Form::label('used_tools', 'MACHINE, EQUIPMENT, TOOLS, ETC., USED REGULARLY IN PERFORMANCE OF WORK:', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group row {{ $errors->has('used_tools') ? 'has-error' : ''}}">
            <div class="col-9 offset-1">
                <div id="used_tools" name="used_tools"></div>
                {{ Form::textarea('used_tools', '',['id'=>'used_tools-text', 'class'=>'d-none']) }}
                {!! $errors->first('used_tools', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6 text-center font-weight-bold">Internal</div>
    <div class="col-6 text-center font-weight-bold">External</div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Executive/Managerial', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="managerial">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->managerial == $key ) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Supervisors', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="supervisor">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->supervisor == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Non Supervisors', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="non_supervisor">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->non_supervisor == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Staff', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="staff">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->staff == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::label('', 'Working Condition', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office Work', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="office_work">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->office_work == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Field Work', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="field_work">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->field_work == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'General Public', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="general_public">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->general_public == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Other Agencies', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select class="select2 select2-sm" name="other_agency">
                    <option value="0">Select</option>
                    @foreach($option_1 as $key => $value)
                    <option value="{{$key}}" {{ (@$posdesc->other_agency == $key) ? 'selected' : '' }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Other/s (Please Specify)', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('other_contacts', @@$posdesc->other_contacts, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-6">&nbsp;</div>
        </div>

        <div class="form-group row">
            <div class="col-6">&nbsp;</div>
        </div>

        <div class="form-group row">
            <div class="col-6">&nbsp;</div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Other/s (Please Specify)', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('other_condition', @$posdesc->other_condition, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>
</div>

<div class="row mb-5">
    <div class="col-12">
        {{ Form::label('description_function_unit', 'BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE UNIT OR SECTION:', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group row {{ $errors->has('description_function_unit') ? 'has-error' : ''}}">
            <div class="col-9 offset-1">
                <div id="description_function_unit" name="description_function_unit"></div>
                <textarea name="description_function_unit" class="d-none" id="description_function_unit-text"></textarea>
                <!-- <input type="hidden" name="description_function_unit" id="description_function_unit-text"> -->
                {!! $errors->first('description_function_unit', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row mb-5">
    <div class="col-12">
        {{ Form::label('description_function_position', 'BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE POSITION (Job Summary):', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
        <div class="form-group row {{ $errors->has('description_function_position') ? 'has-error' : ''}}">
            <div class="col-9 offset-1">
                <div id="description_function_position" name="description_function_position"></div>
                <textarea name="description_function_position" class="d-none" id="description_function_position-text"></textarea>
                <!-- <input type="hidden" name="description_function_position" id="description_function_position-text"> -->
                {!! $errors->first('description_function_position', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row mb-3">
    <div class="col-12">
        {{ Form::label('', 'Qualification Standards', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
    </div>
</div>

<div class="row">
    <div class="col-3 text-center">
        <b>Education</b>
    </div>
    <div class="col-3 text-center">
        <b>Experience</b>
    </div>
    <div class="col-3 text-center">
        <b>Training</b>
    </div>
    <div class="col-3 text-center">
        <b>Eligibility</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-3">
        <div id="applicant_education"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-3">
        <div id="applicant_experience"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-3">
        <div id="applicant_training"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-3">
        <div id="applicant_eligibility"></div>
        <textarea class="d-none"></textarea>
    </div>
</div>

<div class="row">
    <div class="col-6 text-center">
        <b>Core Compentencies</b>
    </div>
    <div class="col-6 text-center">
        <b>Compentecy Level</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div id="core_compentency"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-6">
        <div id="compentency_1" name="compentency_1"></div>
        {{ Form::textarea('compentency_1', '',['id'=>'compentency_1-text', 'class'=>'d-none']) }}
        {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="row">
    <div class="col-6 text-center">
        <b>Leader Compentencies</b>
    </div>
    <div class="col-6 text-center">
        <b>Compentecy Level</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div id="leader_compentency"></div>
        <textarea class="d-none"></textarea>
    </div>

    <div class="col-6">
        <div id="compentency_2" name="compentency_2"></div>
        {{ Form::textarea('compentency_2', '',['id'=>'compentency_2-text', 'class'=>'d-none']) }}
        {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="row mb-3">
    <div class="col-12">
        {{ Form::label('', 'STATEMENT OF DUTIES AND RESPONSIBILITIES (Technical Competencies):', ['class'=>'col-12 col-sm-8 col-form-label text-sm-left font-weight-bold']) }}
    </div>
</div>

<div class="row">
    <div class="col-6 text-center">
        <b>Percentage of Worktime</b>
    </div>
    <div class="col-6 text-center">
        <b>Duties and Responsibilities</b>
    </div>
</div>

<div class="row form-group">
    <div class="col-6 ">
        {{ Form::text('percentage_work_time', @$posdesc->percentage_work_time, [
            'class' => 'form-control form-control-sm col-sm-6 offset-3',
        ])
        }}
    </div>

    <div class="col-6">
        <div id="responsibilities" name="responsibilities"></div>
        {{ Form::textarea('responsibilities', '',['id'=>'responsibilities-text', 'class'=>'d-none']) }}
        {!! $errors->first('responsibilities', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="row form-group">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Supervisor Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('supervisor_name', @$posdesc->supervisor_name, [
                    'class' => 'form-control form-control-sm',
                ])
                }}
            </div>
        </div>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="posdesc_id" value="{{@$posdesc->id}}">
    <input type="hidden" name="applicant_id" value="{{$applicant->id}}">
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation

        let data = {
          '#used_tools': `{!! @$posdesc->used_tools !!}`,
          '#description_function_unit': `{!! @$posdesc->description_function_unit !!}`,
          '#description_function_position': `{!! @$posdesc->description_function_position !!}`,
          '#applicant_education': ``,
          '#applicant_experience': ``,
          '#applicant_training': ``,
          '#applicant_eligibility': ``,
          '#core_compentency': `{!! @$applicant->job->compentency_1 !!}`,
          '#leader_compentency': `{!! @$applicant->job->compentency_2 !!}`,
          '#compentency_1': `{!! @$posdesc->compentency_1 !!}`,
          '#compentency_2': `{!! @$posdesc->compentency_2 !!}`,
          '#responsibilities': `{!! @$posdesc->responsibilities !!}`,
        };

        // initialize editors for each data element
        App.textEditors(Object.keys(data));

        // when validation fails, get data from hidden input texts
        // and set as value for wysiwyg editor
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            if (data[key] === '') {
              data[key] = $(`${key}-text`).html();
            }
            setData(key, data[key]);
          }
        }

        // set data for wysiwyg editors
        function setData(selector = '', data = '') {
          $(selector).next('.note-editor').find('.note-editing-area > .note-editable').html(data);
        }

        $('.note-toolbar').remove();
        // on form submit, get data from wysiwyg editors
        // pass to hidden input elements
        $('#appointment-submit').click(function() {
          for (let key in data) {
            if (data.hasOwnProperty(key)) {
              let element = getData(key);
              $(`${key}-text`).val(element);
            }
          }
        });

        function getData(selector = '') {
          let data = $(selector).next('.note-editor').find('.note-editing-area > .note-editable');
          return data.html();
        }

        $('#applicant_id').change(function() {
            position = $(this).find(':selected').data('position');
            office = $(this).find(':selected').data('office');
            item = $(this).find(':selected').data('item');
            jobGrade = $(this).find(':selected').data('job_grade');
            basicSalary = $(this).find(':selected').data('salary');
            publishDate = $(this).find(':selected').data('publish_date');
            deadlineDate = $(this).find(':selected').data('deadline_date');
            empStatus = $(this).find(':selected').data('empstatus');
            noa = $(this).find(':selected').data('noa');
            division = $(this).find(':selected').data('division');
            department = $(this).find(':selected').data('department');

            $('#position').val(position);
            $('#office').val(office);
            $('#item').val(item);
            $('#job_grade').val(jobGrade);
            $('#salary').val(basicSalary);
            $('#publish_date').val(publishDate);
            $('#deadline_date').val(deadlineDate);
            $('#empstatus').val(empStatus)
            $('#nature_of_appointment').val(noa);
            $('#division').val(division);
            $('#department').val(department);
        });

        $('#applicant_id').trigger('change');


    });
    </script>
@endsection
