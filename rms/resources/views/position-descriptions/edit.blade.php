@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit {{ $title }}</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can edit the position description in the form below.</span>
                </div>
                <div class="card-body">
                    @include('position-descriptions._form', [
                        'action' => ['PositionDescriptionController@update', $posdesc->id],
                        'method' => 'PATCH',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
