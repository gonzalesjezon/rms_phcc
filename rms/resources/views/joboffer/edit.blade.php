@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit {{ $title }}</h2>
    </div>

    <!-- Job Offer Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create a job offer in the form below.</span></div>
                <div class="card-body">
                    @include('joboffer._form', [
                        'action' => ['JobOfferController@update', $joboffer->id],
                        'method' => 'PATCH',
                        'joboffer' => $joboffer,
                        'status' => $status,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
