@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
</style>
@endsection

@section('content')
<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<div id="reports" style="width: 960px;margin: auto; font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-4">
	  <div class="col-6">
	    <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
	  </div>
	  <div class="col-6 text-right">
	      <span>
	        <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
	      North Avenue, Quezon City 1105 <br>
	      <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
	      <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
	      </span>
	  </div>
	</div>
	<hr>

	<div class="row form-group">
		<div class="col-6"></div>
		<div class="col-6">{{ date('F d, Y',time()) }}</div>
	</div>

	<div class="row form-group">
		<div class="col-6">
			<p class="p-0 m-0 font-weight-bold text-uppercase">{{ $joboffer->applicant->getFullName() }}</p>
			<p class="p-0 m-0">{{ $joboffer->applicant->house_number }} {{ $joboffer->applicant->street }}</p>
			<p class="p-0 m-0">{{ $joboffer->applicant->subdivision }}</p>
			<p class="p-0 m-0 mb-7">{{ $joboffer->applicant->city }}</p>
			<p >Dear <strong>Mr/Ms. {{$joboffer->applicant->last_name}}</strong>,</p>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<p class="text-justify" style="text-indent: 3em;">
				On behalf of Philippine Competition Commission (PCC), I am please to inform you that the PCC Human Resource Merit Promotion and Selection Board has selected you to serve as <b>{{ $joboffer->applicant->job->psipop->position->Name }}</b> under <b>{{ $joboffer->applicant->job->psipop->division->Name }}</b>. The annual compensation package is broken down as follows:
			</p>
		</div>
	</div>

	<div id="table" class="offset-1" style="width: 80%;">
	<div class="row border border-dark">
		<div class="col-6 border-right border-dark"><b>Basic Salary</b></div>
		<div class="col-6 text-right">{{ number_format($joboffer->applicant->job->monthly_basic_salary,2)}}</div>
	</div>

	<div class="row border border-dark border-top-0">
		<div class="col-6"><b>Allowances, Bonus and Other Benefits*</b></div>
		<div class="col-6 text-right"></div>
	</div>

	<div class="row border border-dark border-top-0 border-bottom-0">
		<div class="col-6 border-bottom border-right border-dark">Personal Economic Relief Allowance</div>
		<div class="col-6 text-right border-bottom border-dark">{{ number_format($joboffer->pera_amount,2)}}</div>
	</div>

	<div class="row border border-dark border-top-0 border-bottom-0">
		<div class="col-6 border-bottom border-right border-dark">Clothing Allowance</div>
		<div class="col-6 text-right border-bottom border-dark">{{ number_format($joboffer->clothing_allowance,2)}}</div>
	</div>

	<div class="row border border-dark border-top-0">
		<div class="col-6 border-right border-dark">Mid Year Bonus</div>
		<div class="col-6 text-right">{{ number_format($joboffer->year_end_bonus,2)}}</div>
	</div>

	<div class="row border border-dark border-top-0">
		<div class="col-6 border-right border-dark">Year End Bonus</div>
		<div class="col-6 text-right">{{ number_format($joboffer->year_end_bonus,2)}}</div>
	</div>

	<div class="row border border-dark border-top-0">
		<div class="col-6 border-right border-dark">Cash Gift</div>
		<div class="col-6 text-right">{{ number_format($joboffer->applicant->job->cashgift_amount,2)}}</div>
	</div>

	<div class="row border border-dark border-top-0 mb-4">
		<div class="col-6 border-right border-dark"><b>Total **</b></div>
		<div class="col-6 text-right">
			<?php
				$total = 0;
				$basic = ($joboffer->applicant->job->monthly_basic_salary) ? $joboffer->applicant->job->monthly_basic_salary : 0;
				$pera = ($joboffer->pera_amount) ? $joboffer->pera_amount : 0;
				$clothing = ($joboffer->clothing_allowance) ? $joboffer->clothing_allowance : 0;
				$yearend = ($joboffer->year_end_bonus) ? $joboffer->year_end_bonus : 0;
				$midyear = ($joboffer->year_end_bonus) ? $joboffer->year_end_bonus : 0;
				$cashgift = ($joboffer->cash_gift) ? $joboffer->cash_gift : 0;

				$total = $basic + $pera + $clothing + $yearend + $cashgift + $midyear;

			 ?>

			 {{ number_format($total,2)}}
		</div>
	</div>

	</div>

	<div class="row">
		<div class="col-12">
			<p class="text-justify" style="text-indent: 3em;">
				Should you decide to accept the offer, kindly signify your acceptance by signing on the conforme space below and notify the Commission of your earliest availability to assume the position.
			</p>

			<p style="text-indent: 3em;">
				Please return a copy of the signed letter to us thru email address: <u>hrdd@phcc.gov.ph</u>
			</p>

			<p style="text-indent: 3em;">
				Thank you and we look forward to working with you.
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-8"></div>
		<div class="col-4">
			<p class="mb-4">Very truly yours,</p>
			<p class="mb-0 pb-0"><b>Kenneth V. Tanate</b></p>
			<p class="m-0 p-0">Executive Director</p>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-3">
			<p class="mb-4">Conforme,</p>
			<p class="mb-0 pb-0 border border-top-1 border-dark border-bottom-0 border-left-0 border-right-0"><b> Mr/Ms {{ $joboffer->applicant->getFullName() }}</b></p>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<p class="m-0 p-0" style="font-size: 10px;">
				* Subject to CSC-DBM rules and regulations
			</p>
			<p class="m-0 p-0" style="font-size: 10px;">
				**This does not include other allowances, benefits and incentives which may be authorized for payrment by existing rules and regulartions.
			</p>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection