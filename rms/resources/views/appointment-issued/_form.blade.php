@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-assets/lib/select2/css/select2.min.css') }}"/>
<style type="text/css">
    .select2-container--default .select2-selection--single{
        height: 3rem !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 3.13816rem !important;
        font-size: 1rem !important;
        height: 2rem !important;
    }
</style>
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

<div class="form-group row">
    {{ Form::label('applicant_name', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        <select class="select2 select2-sm" name="applicant_id" id="applicant_id">
            <option>Select applicant</option>
            @foreach($applicants as $applicant)
            <option value="{{ $applicant->id }}"
                data-position="{{ $applicant->job->psipop->position->Name }}"
                data-office="{{ $applicant->job->psipop->office->Name }}"
                data-item="{{ $applicant->job->psipop->item_number }}"
                data-job_grade="{{ config('params.job_grades.'.$applicant->job->psipop->job_grade) }}"
                data-salary="{{ number_format($applicant->job->psipop->basic_salary ,2) }}"
                data-publish_date="{{ date('m/d/Y', strtotime($applicant->job->publish_date)) }}"
                data-deadline_date="{{ date('m/d/Y', strtotime($applicant->job->deadline_date)) }}"
                data-empstatus="{{ config('params.employee_status.'.@$applicant->appointment_form->employee_status_id) }}"
                data-publication="{{ config('params.publication.'.$applicant->publication) }}"
                {{ ($applicant->id == @$issued->applicant_id) ? 'selected' : '' }}
                >
                {{ $applicant->getFullName() }}</option>
            @endforeach
        </select>
    </div>

    <div class="col-7 text-center">
        {{ Form::label('','PUBLICATION',['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('position', 'Position', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('position', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'position'
            ])
        }}
    </div>

    {{ Form::label('', 'Mode of Publication', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('publication',  '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'publication'
            ])
        }}
    </div>
</div>

<div class="form-group row {{ $errors->has('employee_status') ? 'has-error' : ''}}">
    {{ Form::label('status', 'Employee Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '',[
                'class' => 'form-control form-control-sm',
                'readonly' => true,
                'id' => 'emp_status'
            ])
        }}
    </div>

    {{ Form::label('', 'Nature of Appointment', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('nature_of_appointment', config('params.nature_of_appointment'), @$issued->nature_of_appointment,[
                'class' => 'select2 select2-sm',
                'placeholder' => 'Select nature of appointment',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('office', 'Office/Department/Unit', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('office', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'office'
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Date Issued </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="date_issued"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->date_issued}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

</div>

<div class="form-group row">
    {{ Form::label('item_no', 'Item No.', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('item_no', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'item'
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Period of Employment From</label>
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="period_of_employment_from"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->period_of_employment_from}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>


</div>

<div class="form-group row">

    {{ Form::label('', 'Job Grade.', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'job_grade'
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">To</label>
    <div class="col-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="period_of_employment_to"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->period_of_employment_to}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

</div>

<div class="form-group row">

    {{ Form::label('', 'Monthly Rate', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('monthlyrate', '', [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
                'id' => 'salary'
            ])
        }}
    </div>


</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation

        $('#applicant_id').change(function() {
            position = $(this).find(':selected').data('position');
            office = $(this).find(':selected').data('office');
            item = $(this).find(':selected').data('item');
            jobGrade = $(this).find(':selected').data('job_grade');
            basicSalary = $(this).find(':selected').data('salary');
            publishDate = $(this).find(':selected').data('publish_date');
            deadlineDate = $(this).find(':selected').data('deadline_date');
            empStatus = $(this).find(':selected').data('empstatus');
            publication = $(this).find(':selected').data('publication');

            $('#position').val(position);
            $('#office').val(office);
            $('#item').val(item);
            $('#job_grade').val(jobGrade);
            $('#salary').val(basicSalary);
            $('#publish_date').val(publishDate);
            $('#deadline_date').val(deadlineDate);
            $('#emp_status').val(empStatus);
            $('#publication').val(publication);
        });

        $('#applicant_id').trigger('change');
      });
    </script>
@endsection
