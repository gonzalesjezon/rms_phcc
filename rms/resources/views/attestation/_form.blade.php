@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'attestation-form']) !!}
<div class="form-group row">
   {{ Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::label('', $appointmentform->applicant->getFullName(), ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Signatory', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('attestation_signatory', @$attestation->attestation_signatory, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Signatory',
                'required' => 'true'
            ])
        }}
    </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Date Sign </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ @$attestation->attestation_date_sign }}" name="attestation_date_sign"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>



<div class="form-group row text-right">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();
    });
  </script>
@endsection
