@extends('layouts.print')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome.css') }}" >
<style type="text/css">
</style>
@endsection

@section('content')

<div class="row text-right d-print-none">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
    <button class="btn btn-primary btn-space" id="evaluation-report" type="submit"><i class="mdi mdi-print"></i> Print</button>
    <!-- {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }} -->
  </div>
</div>

<br>

<div id="reports" style="width: 960px;margin: auto; font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-1">
	    <div class="col-6">
	        <img src="{{ asset('img/pcc-logo-small.png') }}" height="80px;">
	    </div>
	    <div class="col-6 text-right">
	        <span>
	          <i class="fas fa-map-marker-alt"></i> 25/F Vertis North Corporate Center I <br>
	        North Avenue, Quezon City 1105 <br>
	        <i class="fa fa-envelope"></i> queries@phcc.gov.ph <br>
	        <i class="fa fa-phone fa-rotate-90"></i> (+632) 7719 PCC (7719 - 722
	        </span>
	    </div>
	 </div>
	<hr>

	<div class="row form-group">
		<div class="col-6"></div>
		<div class="col-6">{{ date('F d, Y',time()) }}</div>
	</div>

	<div class="row form-group">
		<div class="col-6">
			<p class="p-0 m-0 font-weight-bold text-uppercase">{{ $attestation->applicant->getFullName() }}</p>
			<p class="p-0 m-0">{{ $attestation->applicant->house_number }} {{ $attestation->applicant->street }}</p>
			<p class="p-0 m-0">{{ $attestation->applicant->subdivision }}</p>
			<p class="p-0 m-0 mb-7">{{ $attestation->applicant->city }}</p>
			<p >Dear <strong>Mr/Ms. {{$attestation->applicant->last_name}}</strong>,</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-12 text-center">
			<strong class="text-uppercase">RE: <u>ATTESTATION OF APPOINTMENT</u></strong>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<p class="mb-6">Congratualations!</p>
			<p class="text-justify">
				We are pleased to inform you that the Civil Service Commission has approved your attestation to the  <b>{{ $attestation->applicant->job->plantilla_item->position->Name }}</b> position, JG- {{ $attestation->applicant->job->plantilla_item->job_grade->Name }}, Item No. {{ $attestation->applicant->job->plantilla_item->Name }}, {{ config('params.employee_status.'.$attestation->applicant->appointment_form->employee_status_id) }} under the <b>{{ $attestation->applicant->job->plantilla_item->division->Name }}</b>. The annual compensation package is broken down as follows:
			</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-12">

			<p style="text-indent: 3em;">
				A copy of the attested appointment paper is attached for your reference.
			</p>

			<p style="text-indent: 3em;">
				Thank you for your continued hard work and support
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-8"></div>
		<div class="col-4">
			<p class="mb-4">Very truly yours,</p>
			<p class="mb-0 pb-0"><b>Kenneth V. Tanate</b></p>
			<p class="m-0 p-0">Executive Director</p>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection