@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Preliminary Evaluation</h2>
    </div>

    <!-- Matrix Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle" style="display: initial !important;">You can create matrix of qualification.</span>
                </div>
                <div class="card-body">
                    @include('preliminary_evaluation._form', [
                        'action' => $action,
                        'actionQualified' => $actionQualified,
                        'method' => 'POST',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
