@extends('layouts.app')

@section('content')

<body>
    <div class="be-wrapper be-nosidebar-left">
        <nav class="navbar navbar-expand fixed-top be-top-header">
            <div class="container-fluid">
                <div class="be-navbar-header"><a href="index.html" class="navbar-brand"></a>
                </div>
                <div class="be-right-navbar">
                    <ul class="nav navbar-nav float-right be-user-nav">
                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                               class="nav-link dropdown-toggle">
                                <img src="{{ URL::asset('beagle-assets/img/avatar.png') }}" alt="Avatar">
                                <span class="user-name">Túpac Amaru</span></a>
                            <div role="menu" class="dropdown-menu">
                                <div class="user-info">
                                    <div class="user-name">Túpac Amaru</div>
                                    <div class="user-position online">Available</div>
                                </div>
                                <a href="pages-profile.html" class="dropdown-item"><span class="icon mdi mdi-face"></span>
                                    Account</a><a href="#" class="dropdown-item"><span class="icon mdi mdi-settings"></span>
                                    Settings</a><a href="pages-login.html" class="dropdown-item">
                                    <span class="icon mdi mdi-power"></span> Logout</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav float-right be-icons-nav">
                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                class="nav-link dropdown-toggle">
                                <span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
                            <ul class="dropdown-menu be-notifications">
                                <li>
                                    <div class="title">Notifications<span class="badge badge-pill">3</span></div>
                                    <div class="list">
                                        <div class="be-scroller">
                                            <div class="content">
                                                <ul>
                                                    <li class="notification notification-unread">
                                                        <a href="#">
                                                            <div class="image"><img src="{{ URL::asset('beagle-assets/img/avatar2.png') }}"
                                                                                    alt="Avatar"></div>
                                                            <div class="notification-info">
                                                                <div class="text"><span class="user-name">Jessica Caruso</span>
                                                                    accepted your invitation to join the team.</div>
                                                                <span class="date">2 min ago</span>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="notification">
                                                        <a href="#">
                                                            <div class="image">
                                                                <img src="{{ URL::asset('beagle-assets/img/avatar3.png') }}" alt="Avatar">
                                                            </div>
                                                            <div class="notification-info">
                                                                <div class="text"><span class="user-name">Joel King</span>
                                                                    is now following you</div>
                                                                <span class="date">2 days ago</span>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="notification">
                                                        <a href="#">
                                                            <div class="image">
                                                                <img src="{{ URL::asset('beagle-assets/img/avatar4.png') }}" alt="Avatar">
                                                            </div>
                                                            <div class="notification-info">
                                                                <div class="text"><span class="user-name">John Doe</span>
                                                                    is watching your main repository</div>
                                                                <span class="date">2 days ago</span>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="notification">
                                                        <a href="#">
                                                            <div class="image">
                                                                <img src="{{ URL::asset('beagle-assets/img/avatar5.png') }}"
                                                                     alt="Avatar"></div>
                                                            <div class="notification-info"><span class="text">
                                                                    <span class="user-name">Emily Carter</span>
                                                                    is now following you</span><span class="date">
                                                                    5 days ago</span></div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer"> <a href="#">View all notifications</a></div>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                <span class="icon mdi mdi-apps"></span></a>
                            <ul class="dropdown-menu be-connections">
                                <li>
                                    <div class="list">
                                        <div class="content">
                                            <div class="row">
                                                <div class="col"><a href="#" class="connection-item">
                                                        <img src="{{ URL::asset('beagle-assets/img/github.png') }}" alt="Github">
                                                        <span>GitHub</span></a></div>
                                                <div class="col"><a href="#" class="connection-item">
                                                        <img src="{{ URL::asset('beagle-assets/img/bitbucket.png') }}" alt="Bitbucket">
                                                        <span>Bitbucket</span></a></div>
                                                <div class="col"><a href="#" class="connection-item">
                                                        <img src="{{ URL::asset('beagle-assets/img/slack.png') }}" alt="Slack">
                                                        <span>Slack</span></a></div>
                                            </div>
                                            <div class="row">
                                                <div class="col"><a href="#" class="connection-item">
                                                        <img src="{{ URL::asset('beagle-assets/img/dribbble.png') }}" alt="Dribbble">
                                                        <span>Dribbble</span></a></div>
                                                <div class="col"><a href="#" class="connection-item">
                                                        <img src="{{ URL::asset('beagle-assets/img/mail_chimp.png') }}" alt="Mail Chimp">
                                                        <span>Mail Chimp</span></a></div>
                                                <div class="col"><a href="#" class="connection-item">
                                                        <img src="{{ URL::asset('beagle-assets/img/dropbox.png') }}" alt="Dropbox">
                                                        <span>Dropbox</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer"> <a href="#">More</a></div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <a href="#" id="sub-navigation" data-toggle="collapse" aria-expanded="false"
                   data-target="#be-navbar-collapse" class="be-toggle-top-header-menu collapsed">Wihout Sidebars</a>
                <div id="be-navbar-collapse" class="navbar-collapse collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="index.html" class="nav-link">Home</a></li>
                        <li class="nav-item"><a href="ui-general.html" class="nav-link">UI Elements</a></li>
                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                               class="nav-link dropdown-toggle">Forms <span class="mdi mdi-caret-down"></span></a>
                            <div role="menu" class="dropdown-menu"><a href="form-elements.html" class="dropdown-item">
                                    Elements</a><a href="form-validation.html" class="dropdown-item">Validation</a>
                                <a href="form-wizard.html" class="dropdown-item">Wizard</a>
                                <a href="form-wysiwyg.html" class="dropdown-item">WYSIWYG Editor</a></div>
                        </li>
                        <li class="nav item"><a href="tables-general.html" class="nav-link">Tables</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="be-content">
            <!-- Main Content -->
            <div class="main-content container-fluid">
                <div class="page-head mt-5">
                    <h1 class="page-head-title">Careers</h1>
                    <hr class="my-4">
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header"><span class="title">Address your APPLICATION LETTER to</span></div>
                            <div class="card-body">
                                <div class="card card-border-color card-border-color-primary">
                                    <div class="card-body">
                                        <span class="d-block"><strong>{{ $config['application_recipient_name']}}</strong></span>
                                        <span class="d-block">{{ $config['application_recipient_title']}}</span>
                                        <span class="d-block">{{ $config['application_recipient_department']}}</span>
                                        <br>
                                        <address>
                                            <strong>{{ $config['application_recipient_organization']}} <br> </strong>
                                            {{ $config['application_recipient_address']}}
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header"><span class="title">
                                    Attach the Following Requirements: (INCOMPLETE requirements shall be excluded
                                    from the screening)</span></div>
                            <div class="card-body">
                                <div class="card card-border-color card-border-color-danger">
                                    <div class="card-body">
                                        <span class="d-block">
                                            <ul>
                                                @foreach($requirements as $requirement)
                                                    <li>{{ $requirement }}</li>
                                                @endforeach
                                            </ul>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Vacant Position -->
                <div class="row">
                    @foreach($jobs as $job)
                        @if($job->publish)
                            <div class="col-lg-4 col-md-6 col-sm-1">
                                <div class="card card card-border-color card-border-color-primary">
                                    <div class="card card-border card-contrast">
                                        <div class="card-header card-header-contrast">
                                            {{ $job->title }}
                                            <div class="tools">
                                                <a href="{{ route('applicant.create', [
                                                        'id' => $job->id,
                                                        'title' => $job->title
                                                    ])}}" class="btn btn-primary">Apply</a>
                                            </div>
                                            <span class="card-subtitle">
                                                Created: {!! \Carbon\Carbon::parse($job->created_at)->format('d/m/Y') !!}
                                                | Expires: {!! $job->expires !!}
                                                | {{ strtoupper($job->status) }} Position</span>
                                        </div>
                                        <div class="card-body">
                                            <h4>Job Description</h4><hr>
                                            <p>{!! $job->description !!}</p>

                                            @if($job->education)
                                                <h4>Education</h4><hr>
                                                <p>{!! $job->education !!}</p>
                                            @endif

                                            @if($job->experience)
                                                <h4>Job Experience</h4><hr>
                                                <p>{!! $job->experience !!}</p>
                                            @endif

                                            @if($job->trainings)
                                                <h4>Trainings</h4><hr>
                                                <p>{!! $job->trainings !!}</p>
                                            @endif

                                            @if($job->status === 'non-plantilla')
                                                @if($job->trainings)
                                                    <h4>Duties & Responsibilities</h4><hr>
                                                    <p>{!! $job->trainings !!}</p>
                                                @endif
                                                @if($job->key_competencies)
                                                    <h4>Key Competencies</h4><hr>
                                                    <p>{!! $job->key_competencies !!}</p>
                                                @endif
                                            @endif

                                            @if($job->requirements)
                                                <h4>Requirements</h4><hr>
                                                <p>{!! $job->requirements !!}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('beagle-assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/prettify/prettify.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
          //initialize the javascript
          App.init();

          //Runs prettify
          prettyPrint();
        });
    </script>
</body>
@endsection
