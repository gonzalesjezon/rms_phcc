@extends('layouts.print')

@section('css')
@endsection

@section('content')
  <div  class="border border-dark rounded">
      <div class="row mb-4">
          <div class="col text-center"><h3><strong>CERTIFICATION</strong></h3></div>
      </div>
      <div class="row mb-4">
          <div class="col text-center">The PCC HRM Promotion and Selection Board recommended the appointment of
              <span class="text-uppercase"><u><strong>APPLICANT NAME</strong></u></span> for the position of <u class="font-weight-bold">POSITION TITLE</u> Item No. <u class="font-weight-bold">ITEM NUMBER</u>, JG - <u class="font-weight-bold">JOB GRADE</u>
              <br />
          </div>
      </div>

      <div class="row text-center mb-2">
          <div class="col"><strong>PCC HRM PROMOTION AND SELECTION BOARD</strong></div>
      </div>

      <div class="row">
        <div class="col-4">&nbsp;</div>
          <div class="col-4 border border-dark border-top-0 border-left-0 border-right-0"></div>
          <div class="col-4">&nbsp;</div>
      </div>

      <div class="row mb-8 font-weight-bold">
        <div class="col-4">&nbsp;</div>
        <div class="col-4 text-center">Chairperson</div>
        <div class="col-4">&nbsp;</div>
      </div>



      <div class="row text-center font-weight-bold mb-2" style="margin-left: -5px;margin-right: -5px;">
          <div class="col-3">
            <div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              Director IV - AO
          </div>
          <div class="col-3">
            <div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              Head of Requisitioning Office
          </div>
          <div class="col-3">
            <div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              Human Resource Development Division
          </div>
          <div class="col-3">
            <div class=" border border-dark border-top-0 border-left-0 border-right-0"></div>
              EA Representative
          </div>
      </div>
 </div>

  <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection