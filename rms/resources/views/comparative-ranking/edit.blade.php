@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Comparative Ranking</h2>
    </div>

    <!-- Applicant Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an comparative ranking in the form below.</span></div>
                <div class="card-body">
                    @include('comparative-ranking._form', [
                        'action' => ['ComparativeRankingController@update',@$evaluation->id],
                        'method' => 'PATCH',
                        'evaluation' => $evaluation
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
