@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />

  <style type="text/css">
    .table>thead>tr>th{
      padding: 2px !important;
    }
  </style>
@endsection

<table id="table1" class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 11px !important;">
  <thead class="text-center">
  <tr >
    <th rowspan="2">NAME</th>
    <th colspan="2" scope="colgroup">PERFORMANCE</th>
    <th colspan="3" scope="colgroup">EDUCATION AND TRAINING</th>
    <th colspan="4" scope="colgroup">EXPERIENCE AND OUTSTANDING ACCOMPLISHMENTS</th>
    <th colspan="2" scope="colgroup">PYSCHOSOCIAL</th>
    <th colspan="2" scope="colgroup">POTENTIAL</th>
    <th rowspan="2" scope="colgroup">RANK</th>
    <th rowspan="2">RECOMMENDED</th>
  </tr>
  <tr>
    <th>POINTS</th>
    <th scope="col">40%</th>
    <th scope="col">POINTS(Education)+</th>
    <th scope="col"><i>POINTS(Training)</i></th>
    <th scope="col">20%</th>
    <th colspan="2" scope="col">POINTS(Relevant)+</th>
    <th scope="col"><i>POINTS <br> (Specialized)</i></th>
    <th scope="col">20%</th>
    <th scope="col">POINTS</th>
    <th scope="col">10%</th>
    <th scope="col">POINTS</th>
    <th scope="col">10%</th>
  </tr>
  </thead>
  <tbody>
  {!! Form::open(['action' => $action, 'method' => $method, 'id' => 'matrix-form']) !!}
    <tr>
      <td nowrap>{{$evaluation->applicant->getFullname()}}</td>
      <td>{{$evaluation->performance_score}}</td>
      <td>{{$evaluation->performance_percent}}</td>
      <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
      <td>{{$evaluation->education_training_total_points}}</td>
      <td>{{$evaluation->education_training_score}}</td>
      <td>{{$evaluation->experience_accomplishments_total_points}}</td>
      <td></td>
      <td></td>
      <td>{{$evaluation->experience_accomplishments_score}}</td>
      <td>{{$evaluation->psychosocial_percentage_rating}}</td>
      <td>{{$evaluation->psychosocial_score}}</td>
      <td>{{$evaluation->psychosocial_percentage_rating}}</td>
      <td>{{$evaluation->psychosocial_score}}</td>
      <td></td>
      <td>
          <div class="col-12 col-sm-8 col-lg-6 pt-1">
              <div class="switch-button switch-button-success switch-button-yesno">
                  @if($evaluation->recommended == 1)
                  <input type="checkbox" name="recommended" id="swt0" checked="true"><span>
                  @else
                  <input type="checkbox" name="recommended" id="swt0"><span>
                  @endif
                  <label for="swt0"></label></span>
              </div>
          </div>
          <input type="hidden" name="applicant_id" value="{{$evaluation->applicant_id}}">
          <input type="hidden" name="id" value="{{$evaluation->id}}">
          <input type="hidden" name="job_id" value="{{$evaluation->applicant->job_id}}">
        </td>
    </tr>
  </tbody>
</table>


<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <!-- wysiwyg -->
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

    });
  </script>
@endsection
