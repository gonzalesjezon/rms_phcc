@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css"
          href="{{ URL::asset('beagle-assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/jqvmap/jqvmap.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
@endsection

@section('content')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-4">
                <div class="widget widget-tile mb-1"><b>Vacant Position</b></div>
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline"></div>
                    <div class="data-info">
                        <div class="desc">Plantilla</div>
                        <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span
                                    data-toggle="counter" data-end="40" class="number">0</span>
                        </div>
                    </div>
                    <div class="data-info">
                        <div class="desc">Non-Plantilla</div>
                        <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span
                                    data-toggle="counter" data-end="25" class="number">0</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="widget widget-tile mb-1"><b>Applicants</b></div>
                <div class="widget widget-tile">
                    <div id="spark2" class="chart sparkline"></div>
                    <div class="data-info">
                        <div class="desc">Plantilla</div>
                        <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span
                                    data-toggle="counter" data-end="120" class="number">0</span>
                        </div>
                    </div>
                    <div class="data-info">
                        <div class="desc">Non-Plantilla</div>
                        <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span
                                    data-toggle="counter" data-end="80" class="number">0</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="widget widget-tile mb-1"><b>Evaluation</b></div>
                <div class="widget widget-tile">
                    <div id="spark3" class="chart sparkline"></div>
                    <div class="data-info">
                        <div class="desc">Plantilla</div>
                        <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span
                                    data-toggle="counter" data-end="30" class="number">0</span>
                        </div>
                    </div>
                    <div class="data-info">
                        <div class="desc">Non-Plantilla</div>
                        <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span
                                    data-toggle="counter" data-end="50" class="number">0</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="widget widget-tile mb-1"><b>Recommendation</b></div>
                <div class="widget widget-tile">
                    <div id="spark4" class="chart sparkline"></div>
                    <div class="data-info">
                        <div class="desc">Plantilla</div>
                        <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span
                                    data-toggle="counter" data-end="30" class="number">0</span>
                        </div>
                    </div>
                    <div class="data-info">
                        <div class="desc">Non-Plantilla</div>
                        <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span
                                    data-toggle="counter" data-end="50" class="number">0</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="widget widget-tile mb-1"><b>Newly Hired</b></div>
                <div class="widget widget-tile">
                    <div id="spark5" class="chart sparkline"></div>
                    <div class="data-info">
                        <div class="desc">Plantilla</div>
                        <div class="value"><span class="indicator indicator-negative mdi mdi-chevron-down"></span><span
                                    data-toggle="counter" data-end="4" class="number">0</span>
                        </div>
                    </div>
                    <div class="data-info">
                        <div class="desc">Non-Plantilla</div>
                        <div class="value"><span class="indicator indicator-negative mdi mdi-chevron-down"></span><span
                                    data-toggle="counter" data-end="8" class="number">0</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Charts row 2 -->
        <div class="row">
            <div class="col-8">
                <div class="card">
                    {!! Form::open(['action' => 'DashboardController@index','method' => 'POST',
                        'id' => 'dashboard-form-statistics', 'class' => 'form-horizontal group-border-dashed'
                    ]) !!}
                    <div class="row">
                        <div class="col">
                            <div class="card-header card-header-divider">Statistics
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        Applicants <span class="icon-dropdown mdi mdi-chevron-down"></span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Vacant Position</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Applicant</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Evaluation</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Recommendation</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Newly Hired</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <!-- table -->
                            <div class="col">
                                <div class="card">
                                    <div class="card-header">
                                        Applicants
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-sm table-hover table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th class="number">Male</th>
                                                <th class="number">Female</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Plantilla</td>
                                                <td class="number">10</td>
                                                <td class="number">20</td>
                                                <td class="number">30</td>
                                            </tr>
                                            <tr>
                                                <td>Non-Plantilla</td>
                                                <td class="number">15</td>
                                                <td class="number">12</td>
                                                <td class="number">27</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card">
                                    <div class="card-header card-header-divider">
                                        <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span
                                                    class="icon mdi mdi-refresh-sync"></span><span
                                                    class="icon mdi mdi-close"></span>
                                        </div>
                                        <span class="title">Chart</span>
                                        <span class="card-subtitle"></span>
                                    </div>
                                    <div class="card-body">
                                        <canvas id="plantilla-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/jquery.flot.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/jquery.flot.pie.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/jquery.flot.time.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/jquery.flot.resize.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/plugins/curvedLines.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-flot/plugins/jquery.flot.tooltip.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery.sparkline/jquery.sparkline.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jqvmap/jquery.vmap.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/jqvmap/maps/jquery.vmap.world.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/chartjs/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-charts-chartjs.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/dashboard/home.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //initialize the javascript
            App.init();
            App.dashboard();
            Webapp.charts();
        });
    </script>
@endsection
