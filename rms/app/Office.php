<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'office';

    protected $fillable = [

		'Code',
        'Name'
    ];

    public function psipop(){
    	return $this->belongsTo('App\PSIPOP');
    }
}
