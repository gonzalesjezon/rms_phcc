<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErasureAlteration extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'erasure_alterations';
    protected $fillable = [

		'applicant_id',
		'from_date',
		'to_date',
		'particulars',
		'appointing_officer',
		'sign_date'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
