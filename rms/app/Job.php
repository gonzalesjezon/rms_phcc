<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Database\Eloquent\SoftDeletes;
class Job extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'psipop_id',
        'description',
        'education',
        'experience',
        'training',
        'eligibility',
        'duties_responsibilities',
        'key_competencies',
        'monthly_basic_salary',
        'daily_salary',
        'pera_amount',
        'clothing_amount',
        'midyear_amount',
        'yearend_amount',
        'cashgift_amount',
        'eme_amount',
        'communication_amount',
        'status',
        'requirements',
        'compentency_1',
        'compentency_2',
        'compentency_3',
        'compentency_4',
        'compentency_5',
        'expires',
        'deadline_date',
        'publish_date',
        'publish',
        'station',
        'reporting_line',
        'publication_1',
        'publication_2',
        'publication_3',
        'publication_4',
        'approved_date',
        'other_specify',
        'appointer_id',

        'representation_amount',
        'transportation_amount',
        'csc_education',
        'csc_experience',
        'csc_training',
        'csc_eligibility',
        'posted_from',
        'posted_to',

        'created_by',
        'updated_by'

    ];

    /**
     * Relation: Job has many applicants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }

    public function psipop(){
        return $this->belongsTo('App\PSIPOP','psipop_id');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function division(){
        return $this->belongsTo('App\Division');
    }

    public function appointer(){
        return $this->belongsTo('App\EmployeeInformation','appointer_id');
    }


}
