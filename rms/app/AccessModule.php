<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessModule extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'access_modules';

    protected $fillable = [
		'access_name',
    ];

    public function access_rights(){
    	return $this->hasMany('App\AccessRight');
    }

}
