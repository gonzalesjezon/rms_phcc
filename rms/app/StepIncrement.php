<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepIncrement extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'stepincrement';

    protected $fillable = [

		'Code',
		'Name',

    ];
}
