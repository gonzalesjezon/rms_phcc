<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentIssued extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'appointment_issued';
    protected $fillable = [

		'applicant_id',
		'date_issued',
		'period_of_employment_from',
		'period_of_employment_to',
		'nature_of_appointment',
		'deliberation_date',
		'assessment_date'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
