<?php

namespace App\Imports;

use App\Applicant;
use App\Education;
use App\Eligibility;
use App\WorkExperience;
use App\Training;
use App\PlantillaItem;
use App\Position;
use App\Job;
use Auth;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ApplicantsImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
    	$position = Position::where('Name',$rows[0]['position'])->first();
    	$plantilla_item = PlantillaItem::where('PositionRefId',$position->RefId)->first();
    	$job = Job::where('plantilla_item_id',$plantilla_item->RefId)->select('id')->first();

    	$ctr = 0;
    	$applicant = new Applicant;
    	foreach ($rows as $key => $row) {
    		
    		if($ctr == 0){
	    		$applicant = Applicant::create([
	    			'job_id' 						=> $job->id,
		            'reference_no' 					=> uniqid(),
					'created_by' 					=> Auth::id(),
					'last_name' 					=> $row['last_name'],
					'first_name' 					=> $row['first_name'],
					'middle_name' 					=> $row['middle_name'],
					'extension_name' 				=> $row['extension_name'],
					'nickname' 						=> $row['nick_name'],
					'email_address' 				=> $row['email_address'],
					'mobile_number' 				=> $row['mobile_number'],
					'telephone_number' 				=> $row['telephone_number'],
					'publication' 					=> $row['publication'],
					'birth_place' 					=> $row['place_of_birth'],
					'birthday' 						=> date('Y-m-d',strtotime($row['birth_date'])),
					'gender' 						=> $row['gender'],
					'civil_status' 					=> $row['civil_status'],
					'citizenship' 					=> $row['citizenship'],
					'height' 						=> $row['height'],
					'weight' 						=> $row['weight'],
					'blood_type' 					=> $row['blood_type'],
					'house_number' 					=> $row['house_number'],
					'street' 						=> $row['street'],
					'subdivision' 					=> $row['subd'],
					'barangay' 						=> $row['brgy'],
					'city' 							=> $row['city'],
					'province' 						=> $row['province'],
					'zip_code' 						=> $row['zip_code'],
					'permanent_house_number' 		=> $row['permanent_house_no'],
					'permanent_street' 				=> $row['permanent_street'],
					'permanent_subdivision' 		=> $row['permanent_subd'],
					'permanent_barangay' 			=> $row['permanent_brgy'],
					'permanent_city' 				=> $row['permanent_city'],
					'permanent_province' 			=> $row['permanent_province'],
					'permanent_zip_code' 			=> $row['permanent_zip_code'],
	    		]);
	    		$ctr = 1;
	    	}

	    	if($row['school_name']){
		    	Education::create([
		    		'applicant_id' 		=> $applicant->id,
		    		'school_name' 		=> $row['school_name'],
					'course' 			=> $row['course'],
					'attendance_from' 	=> $row['attendance_from'],
					'attendance_to' 	=> $row['attendance_to'],
					'level' 			=> $row['level'],
					'graduated' 		=> $row['graduated'],
					'award' 			=> $row['award'],
		            'educ_level'        => $row['educ_level'],
		            'ongoing'        	=> $row['ongoing'],
		    	]);
	    	}

	    	if($row['eligibility']){
		    	Eligibility::create([
		    		'applicant_id' 			=> $applicant->id,
		    		'eligibility_ref' 		=> $row['eligibility'],
					'rating' 				=> $row['rating'],
					'exam_date' 			=> $row['exam_date'],
					'exam_place' 			=> $row['exam_place'],
					'license_number' 		=> $row['licence_number'],
					'license_validity' 		=> $row['licence_validity'],
		    	]);
	    	}

	    	if($row['position_title']){
		    	WorkExperience::create([
		    		'applicant_id' 					=> $applicant->id,
		    		'inclusive_date_from' 			=> $row['inclusive_date_from'],
					'inclusive_date_to' 			=> $row['inclusive_date_to'],
					'present_work' 					=> $row['present_work'],
					'position_title' 				=> $row['position_title'],
					'department' 					=> $row['department'],
					'monthly_salary' 				=> $row['monthly_salary'],
					'salary_grade' 					=> $row['salary_grade'],
					'status_of_appointment' 		=> $row['status_of_appointment'],
		            'govt_service'                  => $row['govt_service'],
		    	]);
	    	}

	    	if($row['title']){
		    	Training::create([
		    		'applicant_id' 					=> $applicant->id,
		    		'title_learning_programs' 		=> $row['title'],
					'inclusive_date_from' 			=> $row['date_from'],
					'inclusive_date_to' 			=> $row['date_to'],
					'number_hours' 				    => $row['number_of_hours'],
					'ld_type' 						=> $row['ld_type'],
					'sponsored_by' 					=> $row['sponsored_by'],
		    	]);
	    	}
    	}

    }
}