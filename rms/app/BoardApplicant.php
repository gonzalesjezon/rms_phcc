<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardApplicant extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'boarding_applicants';
    protected $fillable = [

		'applicant_id',
		'start_date',
		'start_time',
		'board_status',
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
