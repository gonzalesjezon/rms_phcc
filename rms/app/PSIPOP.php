<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Database\Eloquent\SoftDeletes;
class PSIPOP extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    protected $table = 'psipop';

    protected $fillable = [
        'applicant_id',
		'employment_status_id',
        'type_of_personnel',
		'office_id',
		'division_id',
		'department_id',
        'position_id',
		'job_grade',
		'step',
		'position_title',
        'item_number',
        'status',

        'area_code',
        'authorized',
        'actual',
        'level',
        'ppa_atribution',

    ];

    public function position(){
        return $this->belongsTo('App\Position','position_id');
    }

    public function office(){
    	return $this->belongsTo('App\Office','office_id');
    }

    public function division(){
    	return $this->belongsTo('App\Division','division_id');
    }

    public function department(){
    	return $this->belongsTo('App\Department','department_id');
    }

    public function job(){
        return $this->belongsTo('App\Job');
    }

    public function applicant(){
        return $this->belongsTo('App\Applicant','applicant_id');
    }

    public function employment_status(){
        return $this->belongsTo('App\EmployeeStatus','employment_status_id');
    }


}
