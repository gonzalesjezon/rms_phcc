<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentForm extends Model
{
	protected $primaryKey = 'id';
    protected $table = 'appointment_forms';
    protected $fillable = [
		'applicant_id',
		'employee_status_id',
		'nature_of_appointment',
		'appointing_officer',
		'hrmo',
		'chairperson',
		'date_sign',
		'hrmo_assessment_date',
		'chairperson_deliberation_date',
		'form_status',
		'period_emp_from',
		'period_emp_to',
		'date_issued',
		'vice',
		'who',
		'assessment_date',
		'posted_in'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
