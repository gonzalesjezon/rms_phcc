<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'rms_modules';

    protected $fillable = [
		'name',
		'description'
    ];
}
