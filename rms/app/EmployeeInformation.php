<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeInformation extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'empinformation';

    protected $fillable = [

		'EmployeesRefId',
		'PositionRefId',
		'PositionItemRefId',

    ];

    public function employee(){
    	return $this->belongsTo('App\Employee','EmployeesRefId');
    }

    public function position(){
    	return $this->belongsTo('App\Position','PositionRefId');
    }
}
