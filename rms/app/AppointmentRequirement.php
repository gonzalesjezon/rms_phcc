<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentRequirement extends Model
{
	/**
     * The upload storage path for documents
     * @var string
     *
     */
    public $documentStorage = 'documents/';

    protected $primaryKey = 'id';
    protected $table = 'appointment_requirements';
    protected $fillable = [

		'applicant_id',
        'pds_status',
        'medical_status',
        'oath_status',
        'nbi_status',
        'gsis_enrollment_status',
        'bir_form_2316_status',
        'bir_form_2305_status',
        'bir_form_1905_status',
        'philhealth_status',
        'hdmf_status',
        'saln_status',
        'assumption_status',
        'position_description_status',
        'land_bank_status',
        'original_status',
        'passport_status',
        'bday_status',

        'pds_copies',
        'medical_copies',
        'oath_copies',
        'nbi_copies',
        'gsis_enrollment_copies',
        'bir_form_2316_copies',
        'bir_form_2305_copies',
        'bir_form_1905_copies',
        'philhealth_copies',
        'hdmf_copies',
        'saln_copies',
        'assumption_copies',
        'position_description_copies',
        'land_bank_copies',
        'original_copies',
        'passport_copies',
        'bday_copies',

        'clearance_copies',
        'clearance_status',
        'service_record_copies',
        'service_record_status',
        'disbursement_voucher_copies',
        'disbursement_voucher_status',
        'certificate_allowance_copies',
        'certificate_allowance_status',
        'cert_leave_balances_copies',
        'cert_leave_balances_status',
        'id_picture_copies',
        'id_picture_status',

        'rar_path',

		'tin_number',
		'pagibig_number'

    ];

    /**
     * List of applicant documents for upload
     *
     * @var array
     */
    protected $documents = [
        'rar_path',
    ];

    /**
     * Saves the document file name to model object
     * deletes existing media file
     *
     * @param object $request Illuminate\Http\Request object
     *
     * @return object App\Applicant model
     */
    public function saveDocumentFileNames($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $file = $request->file($document);
                $md5Name = md5_file($file->getRealPath());
                $this->$document = $md5Name . '.'
                    . $file->getClientOriginalExtension();
            }
        }

        return $this;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param object $request Illuminate\Http\Request object
     *
     */
    public function uploadDocumentFiles($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $request->file($document)->storeAs(
                    $this->documentStorage,
                    $this->$document
                );
            }
        }
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param string $fileName path and name of media file
     *
     * @return bool
     */
    public function deleteMediaFile($fileName)
    {
        if (Storage::delete($fileName)) {
            return true;
        }

        return false;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param array $oldAttributes media object attribute names
     *
     * @return int number of deleted files
     */
    public function deleteAllMediaFiles($oldAttributes = []): int
    {
        $count = 0;
        // delete images
        foreach ($this->images as $image) {
            if ($this->deleteMediaFile($this->imageStorage . $oldAttributes[$image])) {
                $count++;
            }
        }

        // delete documents
        foreach ($this->documents as $document) {
            if ($this->deleteMediaFile($this->documentStorage . $oldAttributes[$document])) {
                $count++;
            }
        }

        return $count;
    }

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
