<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlantillaItem extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'positionitem';

    protected $fillable = [

		'Code',
		'Name',
		'PositionRefId',
		'StepIncrementRefId',
		'SalaryAmount',
		'OfficeRefId',
		'JobGradeRefId',
		'DivisionRefId',
        'DepartmentRefId',
        'has_occupied',
        'applicant_id',

    ];

    public function position(){
    	return $this->belongsTo('App\Position','PositionRefId');
    }

    public function office(){
    	return $this->belongsTo('App\Office','OfficeRefId');
    }

    public function division(){
    	return $this->belongsTo('App\Division','DivisionRefId');
    }

    public function department(){
        return $this->belongsTo('App\Department','DepartmentRefId');
    }

    public function step_increment(){
    	return $this->belongsTo('App\StepIncrement','StepIncrementRefId');
    }

    public function job_grade(){
    	return $this->belongsTo('App\JobGrade','JobGradeRefId');
    }

    public function employee(){
        return $this->belongsTo('App\Employee','applicant_id');
    }

}
