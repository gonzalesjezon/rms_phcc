<?php

namespace App\Exports;

use App\Applicant;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ApplicantExport implements FromView
{
	public function __construct($data){
		$this->data = $data;
	}
	
    public function view(): View
    {
        return view('exports.applicants', [
            'applicants' => $this->data
        ]);

    }
}