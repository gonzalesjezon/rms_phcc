<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{

    protected $primaryKey = 'id';

    protected $table = 'educations';

    protected $fillable = [
    	'applicant_id',
    	'school_name',
    	'course',
    	'attendance_from',
    	'attendance_to',
    	'level',
    	'graduated',
    	'awards',
    	'educ_level',
        'ongoing'
    ];

    public function applicants(){
    	return $this->belongsTo('App\Applicant');
    }

    protected $education = "";

    public function getEducation()
    {
        return $this->education = $this->school_name.' '.$this->course;
    }
}
