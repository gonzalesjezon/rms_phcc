<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Assumption;
use App\AppointmentForm;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AssumptionController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Assumption to Duty');
        $this->middleware('auth');
        $this->module = 'assumption';
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $assumptions = Assumption::with([
                'applicant.job.psipop' => function ($query) {
                    $query->where('type_of_personnel', 1);
                }
            ]
        )->paginate($perPage);

        return view('assumption.index', [
            'assumptions' => $assumptions,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('assumption.create',[
            'action' => 'AssumptionController@store',
            'applicants' => $applicants,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assumption = new Assumption;
        $assumption->fill($request->all());
        $assumption->created_by = Auth::id();
        $assumption->save();

        return redirect()
            ->route('assumption.edit',[
                'id' => $assumption->id
            ])->with('success', 'Assumption has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function show(Assumption $assumption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);

        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $assumption = Assumption::find($id);

        return view('assumption.edit')->with([
            'assumption' => $assumption,
            'module' => $this->module,
            'applicants' => $applicants
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assumption = Assumption::find($id);
        $assumption->fill($request->all());
        $assumption->updated_by = Auth::id();

        $assumption->save();

       return redirect()
            ->route('assumption.edit',[
                'id' => $assumption->id
            ])->with('success', 'Assumption has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assumption $assumption
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $assumption = Assumption::find($id);
        $assumption->delete();
        return redirect('/assumption')->with('success', 'Assumption was successfully deleted.');
    }

    public function assumptionReport(Request $request){
        $assumption = Assumption::find($request->id);

        return view('assumption.report',[
            'assumption' => $assumption
        ]);
    }
}
