<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\SelectionLineup;
use App\Office;
use App\PSIPOP;
use App\Job;
use App\Evaluation;
use App\JobOffer;
use Auth;

class SelectionLineUpController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'PSB Selecion Line Up');
        $this->middleware('auth');
        $this->module = 'selection-lineup';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 50;

        $selection = SelectionLineup::latest()
            ->select('job_id')
            ->groupBy('job_id')
            ->paginate($perPage);

        return view('selection-lineup.index',[
            'selection' => $selection,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = Evaluation::select('job_id')
        ->groupBy('job_id')
        ->get();

        return view('selection-lineup.create',[
            'positions'=> $positions,
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->applicant_id as $key => $applicant_id) {
            $selection = SelectionLineup::find($request->id[$applicant_id]);
            if(empty($selection)){
                $selection = new SelectionLineup;
            }

            $selection->er_representative_selected  = (isset($request->er_representative[$applicant_id])) ? 1 : 0;
            $selection->chairperson_selected        = (isset($request->chairperson[$applicant_id])) ? 1 : 0;
            $selection->hrdd_selected               = (isset($request->hrdd[$applicant_id])) ? 1 :0;

            if(isset($request->hrmo[$applicant_id])){
                $selection->hrmo_selected = 1;
                $check = JobOffer::where('applicant_id',$applicant_id)->first();
                if(!$check){
                    $recommend = new JobOffer();
                    $recommend->applicant_id = $applicant_id;
                    $recommend->created_by   = Auth::id();
                    $recommend->save();
                }
            }else{
                $selection->hrmo_selected = 0;
            }
            $selection->job_id          = $request->job_id;
            $selection->applicant_id    = $applicant_id;
            $selection->created_by      = Auth::id();
            $selection->save();
        }

        return redirect('/selection-lineup')->with('success', 'PSB Selection Line Up successfully saved.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applicant = SelectionLineup::where('job_id',$id)->get();
        $positions = Evaluation::select('job_id')
        ->groupBy('job_id')
        ->get();

        return view('selection-lineup.create')->with([
            'positions'     => $positions,
            'jobSelected'   => $id,
            'applicant'     => $applicant,
            'status'        => 'edit',
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SelectionLineup::where('job_id',$id)->delete();
        return redirect('selection-lineup')->with('success', 'Selection Line up deleted!');
    }

    /*
     * Get all positions inside evaulations based in office
     */
    public function getSelected(Request $request){

        $positions = Evaluation::select('job_id')
        ->groupBy('job_id')
        ->get();

        $applicant = Evaluation::where('job_id',$request->job_id)
        ->get();

        return view('selection-lineup.create')->with([
            'positions'     => $positions,
            'jobSelected'   => $request->job_id,
            'applicant'     => $applicant,
            'module' => $this->module
        ]);
    }

}
