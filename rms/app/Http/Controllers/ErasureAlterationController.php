<?php

namespace App\Http\Controllers;

use App\ErasureAlteration;
use App\AppointmentForm;
use App\Applicant;
use App\Job;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ErasureAlterationController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Erasures and Alteration');
        $this->middleware('auth');
        $this->module = 'erasure_alterations';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);
        $perPage = 100;
        $erasures = ErasureAlteration::latest()
        ->paginate($perPage);

        return view('erasure_alterations.index', [
            'erasures' => $erasures,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->check($this->module);

        return view('erasure_alterations.create', [
            'jobs' => Job::where('publish',1)
                        ->getModels(),
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $erasure = new ErasureAlteration();
        $erasure->fill($request->all());
        $erasure->created_by = Auth::id();
        $erasure->save();

       return redirect()
       ->route('erasure_alterations.edit',[
            'id' => $erasure
       ])->with('success', 'Erasure and alteration was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function show(ErasureAlteration $erasureAlteration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);
        $erasure = ErasureAlteration::find($id);

        return view('erasure_alterations.edit')->with([
            'erasure' => $erasure,
            'jobs' => Job::where('publish',1)
                        ->getModels(),
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $erasure = ErasureAlteration::find($id);
        $erasure->fill($request->all());
        $erasure->updated_by = Auth::id();
        $erasure->save();

       return redirect()
       ->route('erasure_alterations.edit',[
            'id' => $erasure
       ])->with('success', 'Erasure and alteration was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ErasureAlteration::destroy($id);
        return redirect('erasure_alterations')->with('success', 'Erasure and alteration record deleted!');
    }

    public function getApplicant(Request $request){

        # Get all applicant where status is equal to 1 and jobid is equal to requested job id
        # return array
        $applicantId = AppointmentForm::with([
            'applicant' => function($qry) use($request){
                $qry = $qry->where('job_id',$request->job_id);
            }
        ])
        ->where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        # List of selected applicant
        $applicants = Applicant::whereIn('id',$applicantId)
        ->get();

        return json_encode([
            'applicants' => $applicants
        ]);

    }


    public function report(Request $request){

        $erasures = ErasureAlteration::find($request->id)->first();
        return view('erasure_alterations.report',[
            'erasures' => $erasures
        ]);
    }
}
