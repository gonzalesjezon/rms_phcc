<?php

namespace App\Http\Controllers;

use App\OathOffice;
use App\Applicant;
use App\AppointmentForm;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class OathOfficeController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Oath of Office');
        $this->middleware('auth');
        $this->module = 'oath-office';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);

        $perPage = 100;
        $oathoffice = OathOffice::with([
                'applicant.job.psipop' => function ($query) {
                    $query->where('type_of_personnel',1);
                }
            ]
        )->paginate($perPage);

        return view('oath-office.index', [
            'oathoffices' => $oathoffice,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('oath-office.create')->with([
            'applicants' => $applicants,
            'action' => 'OathOfficeController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $oathOffice = new OathOffice;
        $oathOffice->fill($request->all());
        $oathOffice->created_by = Auth::id();
        $oathOffice->save();

        return redirect()
            ->route('oath-office.edit',[
                'id' => $oathOffice->id
            ])->with('success', 'The oath of office has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function show(OathOffice $oathOffice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $this->check($this->module);
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $oathOffice = OathOffice::find($id);

        return view('oath-office.edit')->with([
            'applicants' => $applicants,
            'module' => $this->module,
            'oathOffice' => $oathOffice
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oathOffice = OathOffice::find($id);
        $oathOffice->fill($request->all());
        $oathOffice->updated_by = Auth::id();
        $oathOffice->save();

       return redirect()
            ->route('oath-office.edit',[
                'id' => $oathOffice->id
            ])->with('success', 'The oath of office has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OathOffice::destroy($id);
        return redirect('/oath-office')->with('success', 'Oath of Office was successfully deleted.');
    }

    public function oathOfficeReport(Request $request){

        $oathoffice = OathOffice::find($request->id);

        return view('oath-office.report',[
            'oathoffice' => $oathoffice,
        ]);
    }
}
