<?php

namespace App\Http\Controllers;

use App\Examination;
use App\Interview;
use App\Job;
use App\SelectionLineup;
use App\Applicant;
use App\Appointment;
use App\MatrixQualification;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class InterviewController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Schedule of Interview');
        $this->middleware('auth');
        $this->module = 'interviews';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $interview = Interview::latest()
        ->paginate($perPage);

        return view('interviews.index',[
            'interviews' => $interview,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->check($this->module);

        return view('interviews.create',[
            'method' => 'POST',
            'module' => $this->module,
            'jobs' => Job::where('publish',1)
                        ->getModels()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview =  new Interview();
        $interview->fill($request->all());
        $interview->notify = ($request->notify) ? 1 : 0;
        $interview->created_by = Auth::id();
        $interview->save();

        return redirect()
            ->route('interviews.edit',[
                'interview_id' => $interview->id,
            ])->with('success', 'The interview status was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->check($this->module);
        $interview = Interview::find($id);

        return view('interviews.edit',[
            'interview' => $interview,
            'module' => $this->module,
            'jobs' => Job::where('publish',1)
                            ->getModels()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $interview = Interview::find($id);
        $interview->fill($request->all());
        $interview->notify = ($request->notify) ? 1 : 0;
        $interview->updated_by = Auth::id();
        $interview->save();

        return redirect()
            ->route('interviews.edit', [
                'interview_id' => $interview->id,
            ])
            ->with('success', 'The interview status was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interview = Interview::find($id);
        $interview->delete();
        return redirect('interviews')->with('success', 'Interview schedule deleted!');
    }

    public function mail($to, $subject, $message){

        Mail::to($to)->send(new SendMailable($message));

    }

    public function sendMail(Request $request)
    {
        $interview = new Interview;
        $interview->job_id                     = $request->data['job_id'];
        $interview->applicant_id               = $request->data['applicant_id'];
        $interview->interview_date             = $request->data['interview_date'];
        $interview->interview_time             = $request->data['interview_time'];
        $interview->interview_location         = $request->data['interview_location'];
        $interview->interview_location         = $request->data['interview_location'];
        $interview->email                      = $request->data['email'];
        $interview->resched_interview_date     = $request->data['resched_interview_date'];
        $interview->resched_interview_time     = $request->data['resched_interview_time'];

        $message['status'] = $request->data['interview_status'];
        $message['data']   = $interview;
        $message['type']   = 'interview';

        $this->mail($request->data['email'], 'Subject', $message);

        return json_encode([
            'status' => true,
        ]);
    }

    public function getApplicant(Request $request){

        # Get all applicant where status is equal to 1 and jobid is equal to requested job id
        # return array
        $applicantId = MatrixQualification::where('job_id',$request->job_id)
        ->where('status',1)
        ->pluck('applicant_id')
        ->toArray();

        # List of selected applicant
        $applicants = Applicant::whereIn('id',$applicantId)
        ->get();

        return json_encode([
            'applicants' => $applicants
        ]);

    }
}
