<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Recommendation;
use App\Appointment;
use App\Evaluation;
use App\JobOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class RecommendationController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Recommendation');
        $this->middleware('auth');
        $this->module = 'recommendation';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);

        $perPage = 100;
        $recommendations = Recommendation::latest()
            ->paginate($perPage);

        return view('recommendation.index', [
            'recommendations' => $recommendations,
            'action' => 'RecommendationController@store',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);
        $recommendation = Recommendation::find($id)->first();

        return view('recommendation.edit')->with([
            'recommendation' => $recommendation,
            'action' => 'RecommendationController@update',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $recommend = Recommendation::find($request->id);
        if(empty($recommend)){
            $recommend = new Recommendation();
        }
        $recommend->fill($request->all());
        $recommend->updated_by = Auth::id();

        if($recommend->save()){
            $joboffer = JobOffer::where('applicant_id',$request->applicant_id)->first();
            if(empty($joboffer)){
                $joboffer =  new JobOffer();
                $joboffer->fill($request->all());
                $joboffer->created_by = Auth::id();
                $joboffer->save();
            }
        }

        return redirect()
            ->route('recommendation.edit',[
                'id' => $recommend->id,
            ])->with('success', 'The recommendation was successfully updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = explode('-', $id);

        $Id = $data[0];
        $applicant_id = $data[1];

        Evaluation::where('applicant_id',$applicant_id)
                 ->update(['recommended' => 0]);

        Recommendation::destroy($id);
        return redirect('/recommendation')->with('success', 'Recommendation data deleted!');
    }

    public function report(Request $request){

        $recommend  = Recommendation::find($request->id)->first();
        return view('recommendation.report',[
            'recommendation' => $recommend
        ]);
    }
}
