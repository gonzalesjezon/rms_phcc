<?php

namespace App\Http\Controllers;

use App\MatrixQualification;
use App\Job;
use App\Applicant;
use App\Interview;
use App\Evaluation;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
class MatrixQualificationController extends Controller
{
     /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Matrix of Qualification');
        $this->middleware('auth');
        $this->module = 'matrix-qualification';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $matrix = MatrixQualification::latest()
        ->paginate($perPage);

        return view('matrix-qualification.index',[
            'matrix' => $matrix,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->check($this->module);
        $jobs = Job::where('publish',1)
        ->getModels();

        return view('matrix-qualification.create',[
            'jobs' => $jobs,
            'module' => $this->module,
            'earliest_year' => $this->earliestYear(),
            'latest_year' => $this->latestYear(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $matrix = new MatrixQualification;
        $matrix->fill($request->all());
        $matrix->created_by = Auth::id();
        $matrix->save();

        return redirect()
            ->route('matrix-qualification.index')
            ->with('success', 'The matrix of qualification was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);
        $matrix = MatrixQualification::find($id);
        return view('matrix-qualification.edit',[
            'matrix' => $matrix,
            'jobs' => $jobs = Job::where('publish',1)
                        ->getModels(),
            'module' => $this->module,
            'earliest_year' => $this->earliestYear(),
            'latest_year' => $this->latestYear(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $matrix = MatrixQualification::find($id);
        $matrix->fill($request->all());
        $matrix->status = ($request->status) ? 1 : 0;
        $matrix->updated_by = Auth::id();
        $matrix->save();

        $jobs = Job::where('publish',1)
        ->getModels();

        return redirect()
            ->route('matrix-qualification.edit', [
                'matrix' => $matrix,
            ])
            ->with('success', 'The examination schedule was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $matrix = MatrixQualification::find($id);
        $matrix->delete();
        return redirect('matrix-qualification')->with('success', 'Matrix of qualicafication deleted!');
    }

    public function getApplicant(Request $request){

        $applicants = Applicant::where('job_id',$request->job_id)
        ->get();

        return json_encode([
            'applicants' => $applicants
        ]);

    }

}
