<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\JobOffer;
use App\AppointmentForm;
use App\Office;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class JobOfferController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Job Offer');
        $this->middleware('auth');
        $this->module = 'joboffer';
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $this->check($this->module);

        $status = 'plantilla';

        if(in_array($request->status, ['plantilla', 'non-plantilla'])) {
            $status = $request->status;
        }

        $perPage = 100;
        $jobOffers = JobOffer::with([
                'applicant.job.psipop' => function ($query) {
                    $query->where('type_of_personnel', 1);
                }
            ]
        )->paginate($perPage);


        return view('joboffer.index', [
            'jobOffers' => $jobOffers,
            'status' => $status,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $applicants = Applicant::where('qualified',1)
        ->orderBy('last_name','asc')
        ->get();

        return view('jobOffer.create',[
            'module' => $this->module,
            'applicants' => $applicants
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jobOffer = new JobOffer;
        $jobOffer->fill($request->all());
        $jobOffer->created_by = Auth::id();
        $jobOffer->save();

        return redirect()
            ->route('joboffer.edit',[
                'id' => $jobOffer->id
            ])->with('success', 'Job Offer has successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function show(JobOffer $jobOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);

        $status = 'plantilla';
        $jobOffer = JobOffer::find($id);

        return view('joboffer.edit')->with([
            'joboffer' => $jobOffer,
            'status' => $status,
            'module' => $this->module,
            'applicants' => Applicant::where('qualified',1)
                ->orderBy('last_name','asc')
                ->get()
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $joboffer = JobOffer::find($request->joboffer_id);
        $joboffer->fill($request->all());
        $joboffer->updated_by = Auth::id();

        if($joboffer->save()){
            // $appointment = $this->storeAppointment($request);
        }

       return redirect('/joboffer')->with('success', 'Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobOffer $jobOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobOffer $jobOffer, $id)
    {
        JobOffer::destroy($id);
        return redirect('/joboffer')->with('success', 'Appointment was successfully deleted.');
    }

    public function report(Request $request){

        $joboffer  = JobOffer::find($request->id)->first();
        return view('joboffer.report',[
            'joboffer' => $joboffer
        ]);
    }


}
