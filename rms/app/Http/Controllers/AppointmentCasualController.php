<?php

namespace App\Http\Controllers;

use App\AppointmentCasual;
use App\Applicant;
use App\AppointmentForm;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;

class AppointmentCasualController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Plantilla of Casual Appointment');
        $this->middleware('auth');
        $this->module = 'appointment-casual';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $casuals = AppointmentCasual::latest()
            ->paginate($perPage);

        return view('appointment-casual.index', [
            'casuals' => $casuals,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check($this->module);
        
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('appointment-casual.create')->with([
            'applicants' => $applicants,
            'action' => 'AppointmentCasualController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $casual = new AppointmentCasual;
        $casual->fill($request->all());
        $casual->created_by = Auth::id();
        $casual->save();
        return redirect()
            ->route('appointment-casual.edit',[
                'id' => $casual->id
            ])
            ->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $casual = AppointmentCasual::find($id);

        return view('appointment-casual.edit',[
            'casual' => $casual,
            'applicants' => $applicants,
            'module' => $this->module
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $casual = AppointmentCasual::find($id);
        $casual->fill($request->all());
        $casual->updated_by = Auth::id();
        $casual->save();
        return redirect()
            ->route('appointment-casual.edit',[
                'id' => $casual->id
            ])
            ->with('success', 'The Appointment was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentCasual::destroy($id);
        return redirect('/appointment-casual')->with('success', 'Appointment data deleted!');
    }
}
