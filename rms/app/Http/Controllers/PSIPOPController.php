<?php

namespace App\Http\Controllers;

use App\PSIPOP;
use App\Department;
use App\Division;
use App\Office;
use App\Section;
use App\Position;
use App\EmployeeStatus;
use App\PlantillaItem;
use App\StepIncrement;
use Auth;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class PSIPOPController extends Controller
{
    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'careers']]);
        View::share('title', 'Itemization & Plantilla of Personnel');
        $this->module = 'psipop';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->check($this->module);

        $perPage = 200;
        $psipop = PSIPOP::paginate($perPage);

        return view('psipop.index', [
            'psipop' => $psipop,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->check($this->module);

        return view('psipop.create',[
            'positions' => Position::orderBy('Name','asc')->get(),
            'employee_status' => EmployeeStatus::orderBy('Name','asc')->get(),
            'offices'   => Office::orderBy('Name','asc')->get(),
            'divisions' => Division::orderBy('Name','asc')->get(),
            'departments' => Department::orderBy('Name','asc')->get(),
            'module' => $this->module,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $psipop =  new PSIPOP();
        $psipop->fill($request->all());
        $psipop->basic_salary     = ($request->basic_salary) ? str_replace(',', '', $request->basic_salary) : 0;
        $psipop->created_by = Auth::id();
        $psipop->save();

        return redirect()
            ->route('psipop.edit', [
                'id' => $psipop->id,
            ])
            ->with('success', 'The itemization & plantilla of personnel was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PSIPOP  $psipop
     * @return \Illuminate\Http\Response
     */
    public function show(PSIPOP $pSIPOP)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PSIPOP  $psipop
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        // $this->check($this->module);

        $psipop  = PSIPOP::findOrFail($id);

        return view('psipop.edit',[
            'positions' => Position::orderBy('Name','asc')->get(),
            'employee_status' => EmployeeStatus::orderBy('Name','asc')->get(),
            'offices'   => Office::orderBy('Name','asc')->get(),
            'divisions' => Division::orderBy('Name','asc')->get(),
            'departments' => Department::orderBy('Name','asc')->get(),
            'psipop' => $psipop,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PSIPOP  $psipop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $psipop = PSIPOP::findOrFail($id);
        $psipop->fill($request->all());
        $psipop->basic_salary     = ($request->basic_salary) ? str_replace(',', '', $request->basic_salary) : 0;
        $psipop->updated_by = Auth::id();
        $psipop->save();

        return redirect()
            ->route('psipop.edit', [
                'id' => $psipop->id,
            ])
            ->with('success', 'The itemization & plantilla of personnel was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PSIPOP  $psipop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $psipop = PSIPOP::find($id);
        $psipop->delete();
        
        return redirect('psipop')->with('success', 'Record successfully deleted!');
    }


}
