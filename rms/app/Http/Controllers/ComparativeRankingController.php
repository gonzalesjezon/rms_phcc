<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Evaluation;
use App\MatrixQualification;
use App\JobOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class ComparativeRankingController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Comparative Ranking');
        $this->middleware('auth');
        $this->module = 'comparative-ranking';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $evaluation = Evaluation::latest()
            ->paginate($perPage);

        return view('comparative-ranking.index', [
            'evaluation' => $evaluation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->check($this->module);

        $evaluation = Evaluation::where('applicant_id',$id)->first();

        return view('comparative-ranking.edit',[
            'evaluation' => $evaluation
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $evaluation = Evaluation::find($request->id);
        if(empty($evaluation)){
            $evaluation =  new Evaluation;
        }
        $evaluation->fill($request->all());
        $evaluation->recommended = ($request->recommended) ? 1 : 0;
        $evaluation->updated_by = Auth::id();
        $evaluation->save();


        if ($evaluation->recommended == 1) {
            $check = JobOffer::where('applicant_id',$request->applicant_id)->first();
            if(!$check){
                $recommend = new JobOffer();
                $recommend->fill($request->all());
                $recommend->created_by = Auth::id();
                $recommend->save();
            }
        }

        return redirect('comparative-ranking')->with('success', 'Comparative ranking successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Evaluation::where('applicant_id',$id)->update(['recommended'=> 0]);
        JobOffer::where('applicant_id',$id)->delete();
        return redirect('comparative-ranking')->with('success', 'Comparative ranking successfully deleted!');
    }

    /**
     * Displays Matrix Qualification Report
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function report(Request $request)
    {

        $evaluations = Evaluation::find($request->id)->first();
        return view('comparative-ranking.report')->with([
            'evaluation' => $evaluations,
        ]);
    }


}
