<?php

namespace App\Http\Controllers;

use App\Job;
use App\Evaluation;
use App\PreliminaryEvaluation;
use App\Applicant;
use App\MatrixQualification;
use App\Recommendation;
use App\Appointment;
use App\AppointmentForm;
use App\AppointmentCasual;
use App\AppointmentIssued;
use App\SelectionLineup;
use App\ErasureAlteration;
use App\AcceptanceResignation;
use App\EmployeeInformation;
use App\JobOffer;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use DateTime;

class ReportController extends Controller
{
    /**
     * @var array list of available report documents
     */
    protected $reports = [
        // 'preliminary_evaluation' => 'Preliminary Evaluation',
        // 'selection_lineup' => 'Selection Line Up',
        // 'checklist' => 'Checklist',
        'appointment-transmital' => 'CS Form No. 1 Appointment Transmittal and Action Form',
        'appointments_issued' => 'CS Form No. 2 Report on Appointment Issued',
        // 'absence_qualified_eligible' => 'CS Form No. 5 Certification of the  Absence of a Qualified Eligible',
        'absence_qualified_eligible' => 'CS Form No. 5 Certification that there is no applicant who meets all the qualifications requirements',
        'publication_vacant_position' => 'CS Form No. 9 Request for Publication of Vacant Positions',
        // 'appointment_form_regulated' => 'CS Form No. 33-A Revised 2018 Appointment Form',
        'appointments_casual' => 'CS Form No. 34-A Plantilla of Casual Appointment Regulated',
        'medical_certificate' => 'CS Form No. 211 Medical Certificate',
        'comparative-report' => 'Comparative Report',
        'matrix_qualification' => 'Matrix of Qualifications - Long List',
        'psb_matrix_qualification' => 'Matrix of Qualifications - Short List',
        'matrix_qualification_3' => 'Matrix of Qualifications - Selection',
        'psb_matrix_recommended' => ' Matrix of Qualifications PSB Recommended',
        'accession_form' => 'Report on Accession',
        'separation_form' => 'Report on Separation',
        'notice_of_appointment' => 'Notice Of Appointment',
        'attestation'  => 'Attestation'
        // 'dibar' => 'CS Form No. 8 Report on DIBAR',
        // 'oath_office' => 'CS Form No. 32 Oath of Office',

        // 'position_description' => 'DBM-CSC Form No. 1 Position Description Forms',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Reports');
        $this->middleware('auth');
        $this->module = 'report';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);
        $jobs = Job::with([
            'psipop' => function ($query){
                $query = $query->where('type_of_personnel',1);
            }
        ])->where('publish',1)->getModels();

        $applicants = DB::table('applicants')
            ->where('qualified',1)
            ->select(DB::raw('CONCAT(`first_name`, " " ,`last_name`) as fullname, id'))
            ->orderBy('first_name','asc')
            ->get()->pluck('fullname', 'id')->toArray();

        $employees = Employee::orderBy('FirstName','asc')->getModels();

        return view('report.index', [
            'reports' => $this->reports,
            'jobs' => $jobs,
            'applicants' => $applicants,
            'months' => config('params.months'),
            'employees' => $employees,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('report.sched-exam');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointmentTransmital(Request $request){
        $date = json_decode($request->sign);

        $arr = array(@$date->from,@$date->to);

        $appointment = AppointmentIssued::whereBetween('created_at',$arr)
        ->get();

        return view('report.appointment-transmital',[
            'appointment' => $appointment,
            'signatory'  => json_decode($request->sign),
        ]);
    }

    public function appointmentIssued(Request $request){
        $date = new DateTime($request->date);
        $print_date = $date->format('F Y');

        $date = json_decode($request->sign);

        $appointment = AppointmentIssued::where('created_at','>=',$date->from)
        ->where('created_at','<=',$date->to)
        ->getModels();

        return view('report.appointments_issued')->with([
            'print_date' => $print_date,
            'appointments' => $appointment,
            'signatory'  => json_decode($request->sign),
        ]);
    }

    public function appointmentCasual(Request $request){
        $casual = AppointmentCasual::getModels();
        return view('report.appointments_casual',[
            'casuals' => $casual
        ]);
    }


    public function absenceQualifiedEligible(Request $request){
        return view('report.absence_qualified_eligible');
    }

    public function dibarReport(Request $request){
        return view('report.dibar');
    }

    public function sepearationForm(Request $request){
        return view('report.separation_form');
    }

    public function accessionForm(Request $request){
        return view('report.accession_form');
    }

    public function vacantPosition(Request $request){

        $date = json_decode($request->sign);

        $jobs = Job::where('publish',1)
        ->where('created_at','>=',@$date->from)
        ->where('created_at','<=',@$date->to)
        ->get();

        $employeeinfo = EmployeeInformation::where('EmployeesRefId',$request->signatory_id)->first();

        return view('report.publication_vacant_position',[
            'jobs' => $jobs,
            'employeeinfo' => $employeeinfo
        ]);
    }

    public function oathOffice(Request $request){

        $applicant = Applicant::find($request->id);

        return view('report.oath_office',[
            'applicant' => $applicant,
        ]);
    }

    public function matrixQReport(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $matrixes = MatrixQualification::whereIn('applicant_id',$applicant)->get();
        $job = Job::find($request->id);

        return view('report.matrix_qualification',[
            'matrixes' => $matrixes,
            'job' => $job
        ]);
    }

    public function matrixQPSBReport(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $matrixes = MatrixQualification::whereIn('applicant_id',$applicant)->where('status',1)->get();
        $recommended = Evaluation::whereIn('applicant_id',$applicant)->where('recommended',1)->first();
        $job = Job::find($request->id);

        return view('report.psb_matrix_qualification',[
            'matrixes' => $matrixes,
            'job' => $job,
            'recommended' => $recommended
        ]);
    }

    public function matrixQThree(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $job = Job::find($request->id);
        $selection = SelectionLineup::where('job_id',$request->id)->get();

        return view('report.matrix_qualification_3',[
            'job'        => $job,
            'selections' => $selection,
            'counted'    => count($selection)
        ]);
    }

    public function matrixRecommended(Request $request){
        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $matrixes = MatrixQualification::whereIn('applicant_id',$applicant)->where('status',1)->get();
        $recommended = JobOffer::whereIn('applicant_id',$applicant)->first();
        $job = Job::find($request->id);

        return view('report.psb_matrix_recommended',[
            'matrixes' => $matrixes,
            'job' => $job,
            'recommended' => $recommended
        ]);
    }

    public function appointmentFormRegulated(Request $request){
        $applicant = AppointmentForm::where('applicant_id',$request->id)->first();
        $numberInWord = $this->convert_number_to_words(@$applicant->applicant->job->monthly_basic_salary);

        return view('report.appointment_form_regulated',[
            'applicant' => $applicant,
            'number_in_word' => $numberInWord
        ]);
    }

    public function medicalCertificate(Request $request){

        $applicant = Applicant::find(@$request->id)->first();
        return view('report.medical_certificate',[
            'applicant' => $applicant
        ]);
    }

    public function preliminaryEvaluation(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $preliminary = PreliminaryEvaluation::whereIn('applicant_id',$applicant)->getModels();
        $jobs = Job::where('id',$request->id)->first();

        return view('report.preliminary_evaluation')
        ->with([
            'preliminary' => $preliminary,
            'jobs' => $jobs,

        ]);
    }

    public function selectionLineup(Request $request){

        $applicants = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $job = Job::find($request->id);
        $recommend = SelectionLineup::whereIn('applicant_id',$applicants)->getModels();

        return view('report.selection_lineup')
        ->with([
            'recommend' => $recommend,
            'jobs' => $job
        ]);
    }

    public function checklistReport(Request $request){

        $appointment = new Appointment();
        $applicant = new Applicant();

        if ($request->id) {
            $applicant = Applicant::where('id', $request->id)
                ->first();
            $appointment = Appointment::where('applicant_id', $request->id)
                ->first();
        }

        return view('report.checklist')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
        ]);
    }

    public function comparativeReport(Request $request)
    {

        $evaluations = Evaluation::where('job_id',$request->id)->orderBy('total_score','desc')->getModels();
        $job = Job::find($request->id);
        return view('report.comparative-report')->with([
            'evaluations' => $evaluations,
            'job' => $job
        ]);
    }

    public function noticeOfAppointment(Request $request){
        return view('report.notice_of_appointment');
    }

    public function attestation(Request $request){
        return view('report.attestation');
    }

}
