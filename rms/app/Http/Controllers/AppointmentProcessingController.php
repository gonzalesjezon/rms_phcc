<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\AppointmentProcessing;
use App\AppointmentForm;
use App\Appointment;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\AppointmentChecklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentProcessingController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Processing Checklist');
        $this->middleware('auth');
        $this->module = 'appointment-processing';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $appointments = AppointmentProcessing::latest()
            ->paginate($perPage);

        return view('appointment-processing.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check($this->module);

        $arrID = JobOffer::where('joboffer_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('appointment-processing.create')->with([
            'applicants' => $applicants,
            'action' => 'AppointmentProcessingController@store',
            'module' => $this->module,
            'app_selected' => Applicant::find(@$request->app_id)
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $processing = new AppointmentProcessing;
        $processing->fill($request->all());
        $processing->educ_check = ($request->educ_check) ? 1 : 0;
        $processing->exp_check = ($request->exp_check) ? 1 : 0;
        $processing->training_check = ($request->training_check) ? 1 : 0;
        $processing->eligibility_check = ($request->eligibility_check) ? 1 : 0;
        $processing->other_check = ($request->other_check) ? 1 : 0;
        $processing->created_by = Auth::id();
        $processing->save();

        return redirect()
            ->route('appointment-processing.edit',[
                'id' => $processing
            ])
            ->with('success', 'The appointment processing has successfully created!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrID = JobOffer::where('joboffer_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $processing = AppointmentProcessing::find($id);

        return view('appointment-processing.edit',[
            'processing' => $processing,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $processing = AppointmentProcessing::find($id);
        $processing->fill($request->all());
        $processing->educ_check = ($request->educ_check) ? 1 : 0;
        $processing->exp_check = ($request->exp_check) ? 1 : 0;
        $processing->training_check = ($request->training_check) ? 1 : 0;
        $processing->eligibility_check = ($request->eligibility_check) ? 1 : 0;
        $processing->other_check = ($request->other_check) ? 1 : 0;
        $processing->updated_by = Auth::id();
        $processing->save();

        return redirect()
            ->route('appointment-processing.edit',[
                'id' => $processing
            ])
            ->with('success', 'The appointment processing has successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = AppointmentProcessing::find($id);
        $appointment->delete();
        return redirect('/appointment-processing')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){

        $appointment = AppointmentProcessing::find($request->id);

        return view('appointment-processing.report')->with([
            'appointment' => $appointment,
        ]);
    }


    public function selectApplicant(Request $request)
    {

        return redirect()
            ->route('appointment-processing.create',[
                'app_id' => $request->app_id
            ]);
    }



}
