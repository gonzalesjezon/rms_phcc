<?php

namespace App\Http\Controllers;

use App\AppointmentIssued;
use App\AppointmentProcessing;
use App\AppointmentForm;
use App\JobOffer;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AppointmentIssuedController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Transmital');
        $this->middleware('auth');
        $this->module = 'appointment-issued';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $issued = AppointmentIssued::latest()
            ->paginate($perPage);

        return view('appointment-issued.index', [
            'issued' => $issued,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check($this->module);
        
        $arrID = JobOffer::where('joboffer_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('appointment-issued.create')->with([
            'applicants' => $applicants,
            'action' => 'AppointmentIssuedController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $issued = new AppointmentIssued;
        $issued->fill($request->all());
        $issued->created_by = Auth::id();
        $issued->save();

        return redirect()
            ->route('appointment-issued.edit',[
                'id' => $issued
            ])
            ->with('success', 'The appointment issued has successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function show(AppointmentIssued $appointmentIssued)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrID = JobOffer::where('joboffer_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $issued = AppointmentIssued::find($id);

        return view('appointment-issued.edit',[
            'applicants' => $applicants,
            'issued' => $issued,
            'module' => $this->module
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $issued = AppointmentIssued::find($id);
        $issued->fill($request->all());
        $issued->updated_by = Auth::id();
        $issued->save();

        return redirect()
            ->route('appointment-issued.edit',[
                'id' => $issued
            ])
            ->with('success', 'The appointment issued has successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $issued = AppointmentIssued::find($id);
        $issued->delete();
        return redirect('/appointment-issued')->with('success', 'Transmital data deleted!');
    }

}
