<?php

namespace App\Http\Controllers;

use App\AppointmentForm;
use App\Applicant;
use App\PositionDescription;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PositionDescriptionController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Position Description');
        $this->middleware('auth');
        $this->module = 'position-descriptions';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->check($this->module);
        $perPage = 100;
        $appointments = PositionDescription::latest()
            ->paginate($perPage);

        return view('position-descriptions.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check($this->module);
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('position-descriptions.create')->with([
            'action' => 'PositionDescriptionController@store',
            'option_1' => config('params.option_1'),
            'module' => $this->module,
            'applicants' => $applicants
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $posdesc = new PositionDescription;
        $posdesc->fill($request->all());
        $posdesc->created_by = Auth::id();
        $posdesc->save();
        return redirect()
        ->route('position-descriptions.edit',[
            'id' => $posdesc
        ])->with('success', 'The Postion Description was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function show(PositionDescription $positionDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $posdesc = PositionDescription::find($id);

        return view('position-descriptions.edit')->with([
            'option_1' => config('params.option_1'),
            'module' => $this->module,
            'applicants' => $applicants,
            'posdesc' => $posdesc
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $posdesc = PositionDescription::find($id);
        $posdesc->fill($request->all());
        $posdesc->updated_by = Auth::id();
        $posdesc->save();
        return redirect()
        ->route('position-descriptions.edit',[
            'id' => $posdesc
        ])->with('success', 'The Postion Description was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PositionDescription::destroy($id);
        return redirect('/position-descriptions')->with('success', 'Position description data deleted!');
    }

    public function posDescriptionReport(Request $request)
    {
    
        $posdesc = PositionDescription::find($request->id);

        return view('position-descriptions.report',[
            'posdesc' => $posdesc
        ]);
    }
}
