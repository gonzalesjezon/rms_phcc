<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Appointment;
use App\AppointmentForm;
use App\Attestation;
use App\AppointmentRequirement;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\OathOffice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Auth;

class AppointmentFormController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Form');
        $this->middleware('auth');
        $this->module = 'appointment-form';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('appointment-form.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $this->check($this->module);

        $arrID = JobOffer::where('joboffer_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        return view('appointment-form.create')->with([
            'applicants' => $applicants,
            'form_status' => config('params.form_status'),
            'action' => 'AppointmentFormController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $form = new AppointmentForm;
        $form->fill($request->all());
        $form->created_by = Auth::id();
        $form->save();

        return redirect()
            ->route('appointment-form.edit',[
                'id' => $form
            ])
            ->with('success', 'The appointment form has successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrID = JobOffer::where('joboffer_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $form = AppointmentForm::find($id);

        return view('appointment-form.edit',[
            'applicants' => $applicants,
            'form' => $form,
            'form_status' => config('params.form_status'),
            'module' => $this->module
        ]);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = AppointmentForm::find($id);
        $form->fill($request->all());
        $form->updated_by = Auth::id();
        $form->save();

        return redirect()
            ->route('appointment-form.edit',[
                'id' => $form
            ])
            ->with('success', 'The appointment form has successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = AppointmentForm::find($id);
        $form->delete();
        return redirect('/appointment-form')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){

        $form = AppointmentForm::find($request->id);
        $numberInWord = $this->convert_number_to_words(@$form->applicant->job->monthly_basic_salary);

        return view('appointment-form.report',[
            'applicant' => $form,
            'number_in_word' => $numberInWord
        ]);
    }


    public function sendMail(Request $request)
    {
        $applicant = Applicant::find($request)->first();

        $message['status'] = $request->data['form_status'];
        $message['data']   = $applicant;
        $message['type']   = 'appointment-form';

        $this->mail($request->data['email'], 'Subject', $message);

        return json_encode([
            'status' => true,
        ]);
    }

    public function mail($to, $subject, $message){

        Mail::to($to)->send(new SendMailable($message));

    }

}
