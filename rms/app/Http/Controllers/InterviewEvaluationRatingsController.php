<?php

namespace App\Http\Controllers;

use App\InterviewEvaluationRating;
use App\Interview;
use App\Job;
use App\Applicant;
use App\Employee;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class InterviewEvaluationRatingsController extends Controller
{
    public function __construct()
    {
        View::share('title', 'Interview Evaluation Rating');
        $this->middleware('auth');
        $this->module = 'interview_evaluation_ratings';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $ratings = InterviewEvaluationRating::latest()
        ->paginate($perPage);

        return view('interview_evaluation_ratings.index',[
            'ratings' => $ratings,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->check($this->module);

        return view('interview_evaluation_ratings.create',[
            'method' => 'POST',
            'module' => $this->module,
            'jobs' => Job::where('publish',1)
                        ->getModels(),
            'raters' => Employee::orderBy('LastName','asc')->get()

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview =  new InterviewEvaluationRating();
        $interview->fill($request->all());
        $interview->saveImageFileNames($request);
        $interview->saveDocumentFileNames($request);
        $interview->created_by = Auth::id();
        if($interview->save()){
            $interview->uploadImageFiles($request);
            $interview->uploadDocumentFiles($request);
        }

        return redirect()
            ->route('interview_evaluation_ratings.edit',[
                'interview_id' => $interview->id,
            ])->with('success', 'The interview evaluation rating was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $this->check($this->module);
        $interview = InterviewEvaluationRating::find($id);

        return view('interview_evaluation_ratings.edit',[
            'interview' => $interview,
            'module' => $this->module,
            'jobs' => Job::where('publish',1)
                            ->getModels(),
            'raters' => Employee::orderBy('LastName','asc')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $interview = InterviewEvaluationRating::find($id);
        $interview->fill($request->all());
        $interview->saveImageFileNames($request);
        $interview->saveDocumentFileNames($request);
        $interview->updated_by = Auth::id();
        if($interview->save()){
            $interview->uploadDocumentFiles($request);
            $interview->uploadImageFiles($request);
        }

        return redirect()
            ->route('interview_evaluation_ratings.edit', [
                'interview_id' => $interview->id,
            ])
            ->with('success', 'The interview evaluation rating was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interview = InterviewEvaluationRating::find($id);
        $interview->delete();
        return redirect('interviews')->with('success', 'Interview evaluation rating deleted!');
    }

 

    public function getApplicant(Request $request){

        # Get all applicant where status is equal to 1 and jobid is equal to requested job id
        # return array
        $applicantId = Interview::where('job_id',$request->job_id)
        ->where('interview_status',1)
        ->pluck('applicant_id')
        ->toArray();

        # List of selected applicant
        $applicants = Applicant::whereIn('id',$applicantId)
        ->get();

        return json_encode([
            'applicants' => $applicants
        ]);

    }

    public function report(Request $request){
        $rating =InterviewEvaluationRating::find($request->id);
        return view('interview_evaluation_ratings.report',[
            'rating' => $rating
        ]);
    }
}
