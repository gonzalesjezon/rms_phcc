<?php

namespace App\Http\Controllers;

use App\AppointmentRequirement;
use App\Appointment;
use App\Applicant;
use App\AppointmentForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentRequirementController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Pre Employment Requirements');
        $this->middleware('auth');
        $this->module = 'appointment-requirements';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->check($this->module);

        $perPage = 100;
        $appointments = AppointmentRequirement::latest()
            ->paginate($perPage);

        return view('appointment-requirements.index', [
            'appointments' => $appointments,
            'module' => $this->module
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check($this->module);

        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();


        return view('appointment-requirements.create')->with([
            'applicants' => $applicants,
            'action' => 'AppointmentRequirementController@store',
            'module' => $this->module
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requirement = new AppointmentRequirement;
        $requirement->fill($request->all());
        $requirement->pds_status =($request->pds_status) ? 1 : 0;
        $requirement->medical_status =($request->medical_status) ? 1 : 0;
        $requirement->oath_status =($request->oath_status) ? 1 : 0;
        $requirement->nbi_status =($request->nbi_status) ? 1 : 0;
        $requirement->gsis_enrollment_status =($request->gsis_enrollment_status) ? 1 : 0;
        $requirement->bir_form_2316_status =($request->bir_form_2316_status) ? 1 : 0;
        $requirement->bir_form_2305_status =($request->bir_form_2305_status) ? 1 : 0;
        $requirement->bir_form_1905_status =($request->bir_form_1905_status) ? 1 : 0;
        $requirement->philhealth_status =($request->philhealth_status) ? 1 : 0;
        $requirement->hdmf_status =($request->hdmf_status) ? 1 : 0;
        $requirement->saln_status =($request->saln_status) ? 1 : 0;
        $requirement->assumption_status =($request->assumption_status) ? 1 : 0;
        $requirement->position_description_status =($request->position_description_status) ? 1 : 0;
        $requirement->land_bank_status =($request->land_bank_status) ? 1 : 0;
        $requirement->original_status =($request->original_status) ? 1 : 0;
        $requirement->passport_status =($request->passport_status) ? 1 : 0;
        $requirement->bday_status =($request->bday_status) ? 1 : 0;
        $requirement->clearance_status = ($request->clearance_status) ? 1 : 0;
        $requirement->service_record_status = ($request->service_record_status) ? 1 : 0;
        $requirement->disbursement_voucher_status = ($request->disbursement_voucher_status) ? 1 : 0;
        $requirement->certificate_allowance_status = ($request->certificate_allowance_status) ? 1 : 0;
        $requirement->cert_leave_balances_status = ($request->cert_leave_balances_status) ? 1 : 0;
        $requirement->id_picture_status = ($request->id_picture_status) ? 1 : 0;
        $requirement->original_copies = ($request->_original_copies) ? $request->_original_copies : $request->original_copies;
        $requirement->save();
        // $requirement->saveDocumentFileNames($request);
        return redirect()
            ->route('appointment-requirements.edit',[
                'id' => $requirement->id
            ])
            ->with('success', 'The Appointment - Requirements was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $this->check($this->module);
        $requirement = AppointmentRequirement::where('applicant_id',$id)->first();
        return view('appointment-requirements.show', [
            'requirement' => $requirement,
            'documentView' => $request->document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrID = AppointmentForm::where('form_status',1)
        ->pluck('applicant_id')
        ->toArray();

        $applicants = Applicant::whereIn('id', $arrID)
        ->get();

        $requirement = AppointmentRequirement::find($id);
        return view('appointment-requirements.edit',[
            'requirement' => $requirement,
            'module' => $this->module,
            'applicants' => $applicants
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $requirement = AppointmentRequirement::find($id);
        $requirement->fill($request->all());
        $requirement->pds_status =($request->pds_status) ? 1 : 0;
        $requirement->medical_status =($request->medical_status) ? 1 : 0;
        $requirement->oath_status =($request->oath_status) ? 1 : 0;
        $requirement->nbi_status =($request->nbi_status) ? 1 : 0;
        $requirement->gsis_enrollment_status =($request->gsis_enrollment_status) ? 1 : 0;
        $requirement->bir_form_2316_status =($request->bir_form_2316_status) ? 1 : 0;
        $requirement->bir_form_2305_status =($request->bir_form_2305_status) ? 1 : 0;
        $requirement->bir_form_1905_status =($request->bir_form_1905_status) ? 1 : 0;
        $requirement->philhealth_status =($request->philhealth_status) ? 1 : 0;
        $requirement->hdmf_status =($request->hdmf_status) ? 1 : 0;
        $requirement->saln_status =($request->saln_status) ? 1 : 0;
        $requirement->assumption_status =($request->assumption_status) ? 1 : 0;
        $requirement->position_description_status =($request->position_description_status) ? 1 : 0;
        $requirement->land_bank_status =($request->land_bank_status) ? 1 : 0;
        $requirement->original_status =($request->original_status) ? 1 : 0;
        $requirement->passport_status =($request->passport_status) ? 1 : 0;
        $requirement->bday_status =($request->bday_status) ? 1 : 0;
        $requirement->clearance_status = ($request->clearance_status) ? 1 : 0;
        $requirement->service_record_status = ($request->service_record_status) ? 1 : 0;
        $requirement->disbursement_voucher_status = ($request->disbursement_voucher_status) ? 1 : 0;
        $requirement->certificate_allowance_status = ($request->certificate_allowance_status) ? 1 : 0;
        $requirement->cert_leave_balances_status = ($request->cert_leave_balances_status) ? 1 : 0;
        $requirement->id_picture_status = ($request->id_picture_status) ? 1 : 0;
        $requirement->original_copies = ($request->_original_copies) ? $request->_original_copies : $request->original_copies;

        $requirement->save();
        // $requirement->saveDocumentFileNames($request);
        return redirect()
            ->route('appointment-requirements.edit',[
                'id' => $requirement->id
            ])
            ->with('success', 'The Appointment - Requirements was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        AppointmentRequirement::where('applicant_id',$id)->delete();
        return redirect('/appointment-requirements')->with('success', 'Appointment - Requirements data deleted!');
    }
}
