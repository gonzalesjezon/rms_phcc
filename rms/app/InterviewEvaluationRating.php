<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Database\Eloquent\SoftDeletes;
class InterviewEvaluationRating extends Model
{
    use SoftDeletes;
    /**
     * The upload storage path for images
     * @var string
     *
     */
    public $imageStorage = 'images/';
    /**
     * The upload storage path for documents
     * @var string
     *
     */
    public $documentStorage = 'documents/';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'interview_evaluation_ratings';
    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'applicant_id',
        'rated_by',
        'interview_date',
        'status',
        'attachment_path',
        'image_path',
        'rating_one',
        'rating_two',
        'rating_three',
        'rating_four',
        'rating_five',
        'rating_six',
        'rating_seven',
        'rating_eight',
        'rating_nine',
        'rating_ten',
    ];
    /**
     * List of applicant images for upload
     *
     * @var array
     */
    protected $images = [
        'image_path',
    ];
    /**
     * List of applicant documents for upload
     *
     * @var array
     */
    protected $documents = [
        'attachment_path',
    ];


    /**
     * Saves the image file name to model object
     * deletes existing media file
     *
     * @param object $request Illuminate\Http\Request object
     *
     * @return object App\Applicant model
     *
     */
    public function saveImageFileNames($request)
    {
        foreach ($this->images as $image) {
            if ($request->hasFile($image)) {
                $file = $request->file($image);
                $md5Name = md5_file($file->getRealPath());
                $this->$image = $md5Name . '.'
                    . $file->getClientOriginalExtension();
            }
        }

        return $this;
    }

    /**
     * Delete applicant media files
     *
     * @param object $request Illuminate\Http\Request
     * @param array $oldAttributes media object attribute names
     *
     * @return int number of deleted files
     */
    public function deleteMediaFiles($request, $oldAttributes = []): int
    {
        $count = 0;
        // delete images
        foreach ($this->images as $image) {
            if ($request->hasFile($image)) {
                if ($this->deleteMediaFile($this->imageStorage . $oldAttributes[$image])) {
                    $count++;
                }

            }
        }

        // delete documents
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                if ($this->deleteMediaFile($this->documentStorage . $oldAttributes[$document])) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param string $fileName path and name of media file
     *
     * @return bool
     */
    public function deleteMediaFile($fileName)
    {
        if (Storage::delete($fileName)) {
            return true;
        }

        return false;
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param array $oldAttributes media object attribute names
     *
     * @return int number of deleted files
     */
    public function deleteAllMediaFiles($oldAttributes = []): int
    {
        $count = 0;
        // delete images
        foreach ($this->images as $image) {
            if ($this->deleteMediaFile($this->imageStorage . $oldAttributes[$image])) {
                $count++;
            }
        }

        // delete documents
        foreach ($this->documents as $document) {
            if ($this->deleteMediaFile($this->documentStorage . $oldAttributes[$document])) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Saves the document file name to model object
     * deletes existing media file
     *
     * @param object $request Illuminate\Http\Request object
     *
     * @return object App\Applicant model
     */
    public function saveDocumentFileNames($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $file = $request->file($document);
                $md5Name = md5_file($file->getRealPath());
                $this->$document = $md5Name . '.'
                    . $file->getClientOriginalExtension();
            }
        }

        return $this;
    }

    /**
     * Uploads the image file to the storage container
     *
     * @param object $request Illuminate\Http\Request object
     *
     */
    public function uploadImageFiles($request)
    {
        foreach ($this->images as $image) {
            if ($request->hasFile($image)) {
                $request->file($image)->storeAs(
                    $this->imageStorage,
                    $this->$image
                );
            }
        }
    }

    /**
     * Uploads the document file to the storage container
     *
     * @param object $request Illuminate\Http\Request object
     *
     */
    public function uploadDocumentFiles($request)
    {
        foreach ($this->documents as $document) {
            if ($request->hasFile($document)) {
                $request->file($document)->storeAs(
                    $this->documentStorage,
                    $this->$document
                );
            }
        }
    }

    public function applicant()
    {
			return $this->belongsTo('App\Applicant','applicant_id');    	
    }

    public function ratedBy()
    {
			return $this->belongsTo('App\Employee','rated_by');    	
    }

 

}
