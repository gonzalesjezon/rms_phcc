<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class MatrixQualification extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'matrix_qualifications';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'applicant_id',
        'status',
        'training_remarks',
        'remarks',
        'ips_rating',
        'semester',
        'matrix_year',
        'total_hours'
    ];

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Applicant', 'applicant_id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /**
     * Relation: one is to one
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorUpdate()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    /**
     * Relation: one is to one
     * Matrix has one Evaluation
     * @return mixed
     *
     */
    public function evaluation()
    {
        return $this->hasOne('App\Evaluation', 'applicant_id', 'applicant_id');
    }
}
