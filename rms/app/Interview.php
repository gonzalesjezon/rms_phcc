<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Interview extends Model
{
		use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'interviews';
    protected $fillable = [

		'applicant_id',
		'job_id',
		'interview_date',
		'interview_time',
		'interview_location',
		'resched_interview_date',
		'resched_interview_time',
		'interview_status',
		'notify',
		'noftiy_resched_interview',
		'confirmed',

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant','applicant_id');
    }
}
