<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'recommendations';

    protected $fillable = [
		'applicant_id',
		'sign_one',
		'sign_two',
		'sign_three',
		'sign_four',
		'sign_five',
		'prepared_by',
		'recommend_status'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }

}
