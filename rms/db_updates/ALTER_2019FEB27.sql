ALTER TABLE `jobs`
	ADD COLUMN `compentency_4` TEXT NULL AFTER `compentency_3`;

ALTER TABLE `jobs`
	ADD COLUMN `compentency_5` TEXT NULL AFTER `compentency_4`;

ALTER TABLE `jobs`
	ADD COLUMN `publication` TINYINT(1) NOT NULL DEFAULT '0' AFTER `publish`;

ALTER TABLE `examinations`
	ADD COLUMN `confirmed` INT(11) NULL DEFAULT NULL AFTER `notify`;