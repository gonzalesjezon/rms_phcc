-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rms_pids.applicants
DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` timestamp NULL DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `qualified` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.applicants: ~2 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `active`, `qualified`, `remarks`, `gwa`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 6, '5c62946dabd4d', 'Jezon', 'A', 'Gonzales', NULL, NULL, 'gonzalesjezon@gmail.com', '34567890', NULL, NULL, 'csc_bulletin', '2019-02-12 00:00:00', 'Manila', 'male', 'single', 'PH', 0, 0, '232', '323', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Manila', 'Manila', 'PH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '80', 1, 1, '2019-02-12 09:39:57', '2019-02-14 11:31:19', NULL),
	(5, 1, '5c653b7b3bd2e', 'Reid', 'M', 'Lim', NULL, NULL, 'lim@gmail.com', '3456789', NULL, NULL, 'agency', '2019-01-29 00:00:00', 'Manila', 'male', 'single', 'PH', 0, 0, '232', '232', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Manila', 'Manila', 'PH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3e4b7c736087d83866f393b4453a1a63.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 1, '2019-02-14 09:57:15', '2019-02-15 02:05:29', NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table rms_pids.appointments
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `educ_remarks` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp_remarks` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_remarks` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_remarks` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_remarks` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_employee_status` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_nature_appointment` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_appointing_authority` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_pds` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_eligibility` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_position_description` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_01` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_02` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_03` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_04` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_05` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.appointments: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` (`id`, `applicant_id`, `educ_qualification`, `educ_remarks`, `educ_check`, `exp_qualification`, `exp_remarks`, `exp_check`, `training_qualification`, `training_remarks`, `training_check`, `eligibility_qualification`, `eligibility_remarks`, `eligibility_check`, `other_qualification`, `other_remarks`, `other_check`, `ra_form_33`, `ra_employee_status`, `ra_nature_appointment`, `ra_appointing_authority`, `ra_date_sign`, `ra_date_publication`, `ra_certification`, `ra_pds`, `ra_eligibility`, `ra_position_description`, `ar_01`, `ar_02`, `ar_03`, `ar_04`, `ar_05`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-02-13 02:32:13', '2019-02-13 07:25:17', NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table rms_pids.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_pids.appointment_forms: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table rms_pids.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.assumptions: ~1 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, 1, 1, '2019-02-12 09:39:57', '2019-02-12 09:40:54', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table rms_pids.attestations
DROP TABLE IF EXISTS `attestations`;
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.attestations: ~1 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
INSERT INTO `attestations` (`id`, `applicant_id`, `agency_receiving_offer`, `action_employee_status`, `period_from`, `period_to`, `date_action`, `date_release`, `date_issuance`, `publication_from`, `publication_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-02-12 09:39:58', '2019-02-12 09:40:54', NULL);
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table rms_pids.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.config: ~12 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `name`, `value`, `description`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 'application_recipient_name', 'John Doe', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(2, 'application_recipient_title', 'Department Manager', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(3, 'application_recipient_department', 'Human Resources Department', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(4, 'application_recipient_organization', 'Organization Name', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(5, 'application_recipient_address', '123 street corner ABC avenue, Philippines', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(6, 'application_requirements', 'Letter of Application, Latest Personal Data Sheet', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(7, 'url_rms', 'http://rms-url-here', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(8, 'url_pis', 'http://pis-url-here', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(9, 'url_pms', 'http://pms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(10, 'url_ams', 'http://ams-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(11, 'url_ldms', 'http://ldms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(12, 'url_spms', 'http://spms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for table rms_pids.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.countries: ~242 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `name`) VALUES
	(1, 'PH', 'Philippines'),
	(2, 'AF', 'Afghanistan'),
	(3, 'AL', 'Albania'),
	(4, 'DZ', 'Algeria'),
	(5, 'AS', 'American Samoa'),
	(6, 'AD', 'Andorra'),
	(7, 'AO', 'Angola'),
	(8, 'AI', 'Anguilla'),
	(9, 'AQ', 'Antarctica'),
	(10, 'AG', 'Antigua and/or Barbuda'),
	(11, 'AR', 'Argentina'),
	(12, 'AM', 'Armenia'),
	(13, 'AW', 'Aruba'),
	(14, 'AU', 'Australia'),
	(15, 'AT', 'Austria'),
	(16, 'AZ', 'Azerbaijan'),
	(17, 'BS', 'Bahamas'),
	(18, 'BH', 'Bahrain'),
	(19, 'BD', 'Bangladesh'),
	(20, 'BB', 'Barbados'),
	(21, 'BY', 'Belarus'),
	(22, 'BE', 'Belgium'),
	(23, 'BZ', 'Belize'),
	(24, 'BJ', 'Benin'),
	(25, 'BM', 'Bermuda'),
	(26, 'BT', 'Bhutan'),
	(27, 'BO', 'Bolivia'),
	(28, 'BA', 'Bosnia and Herzegovina'),
	(29, 'BW', 'Botswana'),
	(30, 'BV', 'Bouvet Island'),
	(31, 'BR', 'Brazil'),
	(32, 'IO', 'British lndian Ocean Territory'),
	(33, 'BN', 'Brunei Darussalam'),
	(34, 'BG', 'Bulgaria'),
	(35, 'BF', 'Burkina Faso'),
	(36, 'BI', 'Burundi'),
	(37, 'KH', 'Cambodia'),
	(38, 'CM', 'Cameroon'),
	(39, 'CA', 'Canada'),
	(40, 'CV', 'Cape Verde'),
	(41, 'KY', 'Cayman Islands'),
	(42, 'CF', 'Central African Republic'),
	(43, 'TD', 'Chad'),
	(44, 'CL', 'Chile'),
	(45, 'CN', 'China'),
	(46, 'CX', 'Christmas Island'),
	(47, 'CC', 'Cocos (Keeling) Islands'),
	(48, 'CO', 'Colombia'),
	(49, 'KM', 'Comoros'),
	(50, 'CG', 'Congo'),
	(51, 'CK', 'Cook Islands'),
	(52, 'CR', 'Costa Rica'),
	(53, 'HR', 'Croatia (Hrvatska)'),
	(54, 'CU', 'Cuba'),
	(55, 'CY', 'Cyprus'),
	(56, 'CZ', 'Czech Republic'),
	(57, 'CD', 'Democratic Republic of Congo'),
	(58, 'DK', 'Denmark'),
	(59, 'DJ', 'Djibouti'),
	(60, 'DM', 'Dominica'),
	(61, 'DO', 'Dominican Republic'),
	(62, 'TP', 'East Timor'),
	(63, 'EC', 'Ecudaor'),
	(64, 'EG', 'Egypt'),
	(65, 'SV', 'El Salvador'),
	(66, 'GQ', 'Equatorial Guinea'),
	(67, 'ER', 'Eritrea'),
	(68, 'EE', 'Estonia'),
	(69, 'ET', 'Ethiopia'),
	(70, 'FK', 'Falkland Islands (Malvinas)'),
	(71, 'FO', 'Faroe Islands'),
	(72, 'FJ', 'Fiji'),
	(73, 'FI', 'Finland'),
	(74, 'FR', 'France'),
	(75, 'FX', 'France, Metropolitan'),
	(76, 'GF', 'French Guiana'),
	(77, 'PF', 'French Polynesia'),
	(78, 'TF', 'French Southern Territories'),
	(79, 'GA', 'Gabon'),
	(80, 'GM', 'Gambia'),
	(81, 'GE', 'Georgia'),
	(82, 'DE', 'Germany'),
	(83, 'GH', 'Ghana'),
	(84, 'GI', 'Gibraltar'),
	(85, 'GR', 'Greece'),
	(86, 'GL', 'Greenland'),
	(87, 'GD', 'Grenada'),
	(88, 'GP', 'Guadeloupe'),
	(89, 'GU', 'Guam'),
	(90, 'GT', 'Guatemala'),
	(91, 'GN', 'Guinea'),
	(92, 'GW', 'Guinea-Bissau'),
	(93, 'GY', 'Guyana'),
	(94, 'HT', 'Haiti'),
	(95, 'HM', 'Heard and Mc Donald Islands'),
	(96, 'HN', 'Honduras'),
	(97, 'HK', 'Hong Kong'),
	(98, 'HU', 'Hungary'),
	(99, 'IS', 'Iceland'),
	(100, 'IN', 'India'),
	(101, 'ID', 'Indonesia'),
	(102, 'IR', 'Iran (Islamic Republic of)'),
	(103, 'IQ', 'Iraq'),
	(104, 'IE', 'Ireland'),
	(105, 'IL', 'Israel'),
	(106, 'IT', 'Italy'),
	(107, 'CI', 'Ivory Coast'),
	(108, 'JM', 'Jamaica'),
	(109, 'JP', 'Japan'),
	(110, 'JO', 'Jordan'),
	(111, 'KZ', 'Kazakhstan'),
	(112, 'KE', 'Kenya'),
	(113, 'KI', 'Kiribati'),
	(114, 'KP', 'Korea, Democratic People\'s Republic of'),
	(115, 'KR', 'Korea, Republic of'),
	(116, 'KW', 'Kuwait'),
	(117, 'KG', 'Kyrgyzstan'),
	(118, 'LA', 'Lao People\'s Democratic Republic'),
	(119, 'LV', 'Latvia'),
	(120, 'LB', 'Lebanon'),
	(121, 'LS', 'Lesotho'),
	(122, 'LR', 'Liberia'),
	(123, 'LY', 'Libyan Arab Jamahiriya'),
	(124, 'LI', 'Liechtenstein'),
	(125, 'LT', 'Lithuania'),
	(126, 'LU', 'Luxembourg'),
	(127, 'MO', 'Macau'),
	(128, 'MK', 'Macedonia'),
	(129, 'MG', 'Madagascar'),
	(130, 'MW', 'Malawi'),
	(131, 'MY', 'Malaysia'),
	(132, 'MV', 'Maldives'),
	(133, 'ML', 'Mali'),
	(134, 'MT', 'Malta'),
	(135, 'MH', 'Marshall Islands'),
	(136, 'MQ', 'Martinique'),
	(137, 'MR', 'Mauritania'),
	(138, 'MU', 'Mauritius'),
	(139, 'TY', 'Mayotte'),
	(140, 'MX', 'Mexico'),
	(141, 'FM', 'Micronesia, Federated States of'),
	(142, 'MD', 'Moldova, Republic of'),
	(143, 'MC', 'Monaco'),
	(144, 'MN', 'Mongolia'),
	(145, 'MS', 'Montserrat'),
	(146, 'MA', 'Morocco'),
	(147, 'MZ', 'Mozambique'),
	(148, 'MM', 'Myanmar'),
	(149, 'NA', 'Namibia'),
	(150, 'NR', 'Nauru'),
	(151, 'NP', 'Nepal'),
	(152, 'NL', 'Netherlands'),
	(153, 'AN', 'Netherlands Antilles'),
	(154, 'NC', 'New Caledonia'),
	(155, 'NZ', 'New Zealand'),
	(156, 'NI', 'Nicaragua'),
	(157, 'NE', 'Niger'),
	(158, 'NG', 'Nigeria'),
	(159, 'NU', 'Niue'),
	(160, 'NF', 'Norfork Island'),
	(161, 'MP', 'Northern Mariana Islands'),
	(162, 'NO', 'Norway'),
	(163, 'OM', 'Oman'),
	(164, 'PK', 'Pakistan'),
	(165, 'PW', 'Palau'),
	(166, 'PA', 'Panama'),
	(167, 'PG', 'Papua New Guinea'),
	(168, 'PY', 'Paraguay'),
	(169, 'PE', 'Peru'),
	(170, 'PN', 'Pitcairn'),
	(171, 'PL', 'Poland'),
	(172, 'PT', 'Portugal'),
	(173, 'PR', 'Puerto Rico'),
	(174, 'QA', 'Qatar'),
	(175, 'SS', 'Republic of South Sudan'),
	(176, 'RE', 'Reunion'),
	(177, 'RO', 'Romania'),
	(178, 'RU', 'Russian Federation'),
	(179, 'RW', 'Rwanda'),
	(180, 'KN', 'Saint Kitts and Nevis'),
	(181, 'LC', 'Saint Lucia'),
	(182, 'VC', 'Saint Vincent and the Grenadines'),
	(183, 'WS', 'Samoa'),
	(184, 'SM', 'San Marino'),
	(185, 'ST', 'Sao Tome and Principe'),
	(186, 'SA', 'Saudi Arabia'),
	(187, 'SN', 'Senegal'),
	(188, 'RS', 'Serbia'),
	(189, 'SC', 'Seychelles'),
	(190, 'SL', 'Sierra Leone'),
	(191, 'SG', 'Singapore'),
	(192, 'SK', 'Slovakia'),
	(193, 'SI', 'Slovenia'),
	(194, 'SB', 'Solomon Islands'),
	(195, 'SO', 'Somalia'),
	(196, 'ZA', 'South Africa'),
	(197, 'GS', 'South Georgia South Sandwich Islands'),
	(198, 'ES', 'Spain'),
	(199, 'LK', 'Sri Lanka'),
	(200, 'SH', 'St. Helena'),
	(201, 'PM', 'St. Pierre and Miquelon'),
	(202, 'SD', 'Sudan'),
	(203, 'SR', 'Suriname'),
	(204, 'SJ', 'Svalbarn and Jan Mayen Islands'),
	(205, 'SZ', 'Swaziland'),
	(206, 'SE', 'Sweden'),
	(207, 'CH', 'Switzerland'),
	(208, 'SY', 'Syrian Arab Republic'),
	(209, 'TW', 'Taiwan'),
	(210, 'TJ', 'Tajikistan'),
	(211, 'TZ', 'Tanzania, United Republic of'),
	(212, 'TH', 'Thailand'),
	(213, 'TG', 'Togo'),
	(214, 'TK', 'Tokelau'),
	(215, 'TO', 'Tonga'),
	(216, 'TT', 'Trinidad and Tobago'),
	(217, 'TN', 'Tunisia'),
	(218, 'TR', 'Turkey'),
	(219, 'TM', 'Turkmenistan'),
	(220, 'TC', 'Turks and Caicos Islands'),
	(221, 'TV', 'Tuvalu'),
	(222, 'US', 'United States'),
	(223, 'UG', 'Uganda'),
	(224, 'UA', 'Ukraine'),
	(225, 'AE', 'United Arab Emirates'),
	(226, 'GB', 'United Kingdom'),
	(227, 'UM', 'United States minor outlying islands'),
	(228, 'UY', 'Uruguay'),
	(229, 'UZ', 'Uzbekistan'),
	(230, 'VU', 'Vanuatu'),
	(231, 'VA', 'Vatican City State'),
	(232, 'VE', 'Venezuela'),
	(233, 'VN', 'Vietnam'),
	(234, 'VG', 'Virgin Islands (British)'),
	(235, 'VI', 'Virgin Islands (U.S.)'),
	(236, 'WF', 'Wallis and Futuna Islands'),
	(237, 'EH', 'Western Sahara'),
	(238, 'YE', 'Yemen'),
	(239, 'YU', 'Yugoslavia'),
	(240, 'ZR', 'Zaire'),
	(241, 'ZM', 'Zambia'),
	(242, 'ZW', 'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table rms_pids.divisions
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_pids.divisions: ~14 rows (approximately)
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;
INSERT INTO `divisions` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'General Services Division', NULL, NULL, NULL, NULL, NULL),
	(2, 'Human Resource Development Division', NULL, NULL, NULL, NULL, NULL),
	(3, 'Information and Communications Technology Division', NULL, NULL, NULL, NULL, NULL),
	(4, 'Legal Division', NULL, NULL, NULL, NULL, NULL),
	(5, 'Office of the Director', NULL, NULL, NULL, NULL, NULL),
	(6, 'Accounting Division', NULL, NULL, NULL, NULL, NULL),
	(7, 'Budget Division', NULL, NULL, NULL, NULL, NULL),
	(8, 'Corporate Planning and Management Division', NULL, NULL, NULL, NULL, NULL),
	(9, 'Monitoring and Investigation Division', NULL, NULL, NULL, NULL, NULL),
	(10, 'Adjudication Division', NULL, NULL, NULL, NULL, NULL),
	(11, 'Merger and Acquisition Division', NULL, NULL, NULL, NULL, NULL),
	(12, 'Policy Research and Development Division', NULL, NULL, NULL, NULL, NULL),
	(13, 'Knowledge Management Division', NULL, NULL, NULL, NULL, NULL),
	(14, 'Training Division', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;

-- Dumping structure for table rms_pids.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` varchar(225) DEFAULT NULL,
  `attendance_to` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_pids.educations: ~16 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` (`id`, `applicant_id`, `school_name`, `course`, `attendance_from`, `attendance_to`, `level`, `graduated`, `awards`, `educ_level`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Jezon', 'Jezon', 'Jezon', 'Jezon', 'Jezon', 'Jezon', 'Jezon', 1, NULL, NULL, '2019-02-12 09:40:54', '2019-02-12 09:40:54', NULL),
	(2, 1, 'Jezon', 'Jezon', 'Jezon', 'Jezon', 'Jezon', 'Jezon', 'Jezon', 1, NULL, NULL, '2019-02-14 11:31:19', '2019-02-14 11:31:19', NULL),
	(3, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 11:33:21', '2019-02-14 11:33:21', NULL),
	(4, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 11:42:30', '2019-02-14 11:42:30', NULL),
	(5, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 11:49:49', '2019-02-14 11:49:49', NULL),
	(6, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 12:00:14', '2019-02-14 12:00:14', NULL),
	(7, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 12:10:31', '2019-02-14 12:10:31', NULL),
	(8, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 12:12:44', '2019-02-14 12:12:44', NULL),
	(9, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 12:28:38', '2019-02-14 12:28:38', NULL),
	(10, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 12:31:52', '2019-02-14 12:31:52', NULL),
	(11, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 12:59:48', '2019-02-14 12:59:48', NULL),
	(12, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 13:01:29', '2019-02-14 13:01:29', NULL),
	(13, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 13:02:44', '2019-02-14 13:02:44', NULL),
	(14, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-14 13:06:17', '2019-02-14 13:06:17', NULL),
	(15, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-15 01:59:43', '2019-02-15 01:59:43', NULL),
	(16, 5, 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 'Reid', 1, NULL, NULL, '2019-02-15 02:05:29', '2019-02-15 02:05:29', NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table rms_pids.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table rms_pids.employee_information
DROP TABLE IF EXISTS `employee_information`;
CREATE TABLE IF NOT EXISTS `employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `hired_date` timestamp NULL DEFAULT NULL,
  `assumption_date` timestamp NULL DEFAULT NULL,
  `resigned_date` timestamp NULL DEFAULT NULL,
  `rehired_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `position_item_id` int(11) NOT NULL,
  `employee_status_id` int(11) NOT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) NOT NULL,
  `work_schedule_id` int(11) NOT NULL,
  `appointment_status_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.employee_information: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_information` ENABLE KEYS */;

-- Dumping structure for table rms_pids.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` int(10) unsigned DEFAULT NULL,
  `performance_divide` int(10) unsigned DEFAULT NULL,
  `performance_average` int(10) unsigned DEFAULT NULL,
  `performance_percent` int(10) unsigned DEFAULT NULL,
  `performance_score` int(10) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` int(10) unsigned DEFAULT NULL,
  `minimum_training_points` int(10) unsigned DEFAULT NULL,
  `education_points` int(10) unsigned DEFAULT NULL,
  `training_points` int(10) unsigned DEFAULT NULL,
  `education_training_total_points` int(10) unsigned DEFAULT NULL,
  `education_training_percent` int(10) unsigned DEFAULT NULL,
  `education_training_score` int(10) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` int(10) unsigned DEFAULT NULL,
  `additional_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_score` int(10) unsigned DEFAULT NULL,
  `potential` int(10) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` int(10) unsigned DEFAULT NULL,
  `potential_percent` int(10) unsigned DEFAULT NULL,
  `potential_score` int(10) unsigned DEFAULT NULL,
  `psychosocial` int(10) unsigned DEFAULT NULL,
  `psychosocial_average_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percent` int(10) unsigned DEFAULT NULL,
  `psychosocial_score` int(10) unsigned DEFAULT NULL,
  `total_percent` int(10) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.evaluations: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table rms_pids.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plantilla_item_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `grade` tinyint(4) DEFAULT NULL,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `annual_basic_salary` decimal(13,2) DEFAULT '0.00',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `office_id` tinyint(4) DEFAULT NULL,
  `division_id` tinyint(4) DEFAULT NULL,
  `employee_status` tinyint(4) DEFAULT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.jobs: ~198 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `plantilla_item_number`, `title`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `grade`, `monthly_basic_salary`, `annual_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `status`, `requirements`, `compentency_1`, `compentency_2`, `office_id`, `division_id`, `employee_status`, `expires`, `publish`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 'PHCC-COCH-1-2016', 'Chairman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, 0.000, 6337616.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, 1, 1, NULL, 0, '2019-02-05 17:58:33', '2019-02-12 09:36:22', 0, NULL, NULL),
	(2, 'PHCC-COM-1-2016', 'Commissioner', NULL, '', '', '', '', NULL, NULL, 18, 0.000, 4251744.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(3, 'PHCC-COM-2-2016', 'Commissioner', NULL, '', '', '', '', NULL, NULL, 18, 0.000, 4251744.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(4, 'PHCC-COM-3-2016', 'Commissioner', NULL, '', '', '', '', NULL, NULL, 18, 0.000, 4251744.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(5, 'PHCC-COM-4-2016', 'Commissioner', NULL, '', '', '', '', NULL, NULL, 18, 0.000, 4251744.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(6, 'PHCC-HEA-1-2016', 'Head Executive Assistant', '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</div><div>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</div><div>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</div><div>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</div><div>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</div><div>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>', 'Bachelor of Science', 'One (1) year of relevant experience', 'Four (4) hours of relevant training', 'Career Service (Professional)/Second Level Eligibility/ PD 907 (Honor Graduate Eligibility); and', NULL, NULL, 15, 0.000, 1470000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, 'Deliverability/Results-Oriented – shows persistence in achieving his/her targets; prepares outputs as required by the supervisor', '&nbsp;Research Conceptualization – with basic understanding of socioeconomic theories and appreciation of policy issues; can provide logistic support in the development of research proposal;&nbsp;', 1, 1, 1, NULL, 0, '2019-02-05 17:58:33', '2019-02-13 06:52:49', 0, NULL, NULL),
	(7, 'PHCC-EXA4-1-2016', 'Executive Assistant IV', NULL, 'Bachelor’s degree', '3 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '24 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, 'Deliverability/Results-Oriented (superior) – oriented towards contributing to the output from the whole system not just their own specialization - Professionalism (superior) – demonstrates a high level of personal responsibility, dependability and reliability; exhibits the values, attitudes and behaviors of the organization; establishes criteria and/or work procedures to achieve a high level of quality, productivity and service - Adaptability (superior) – anticipates the need for a change and be the champion for these changes; prepares a plan in the performance of the job to make room for adjustments - Communication (superior) – keeps his/her superiors informed about targets/priorities and accomplishments; accurately interprets policies and orders from Management; writes letters and other communication that best suits the situation/clients; edits communication matters and transforms them into a more concise, clear and relevant written communication; responds calmly and provides options to keep difficult situations under control; able to lead meetings with ease, clarifies questions, make commitments and negotiates if necessary&nbsp;', '- Research Conceptualization (superior) – superior understanding of socioeconomic theories and public policies, with expertise on specific sector/s; has ample practical experience; develops research agenda for the sector or unit - Data Gathering, Problem Solving and Analysis (superior) – has expertise in doing secondary and primary data collection and able to design data gathering systems and processes; able to capacitate staff on data documentation, processing and management of databases; able to provide in-depth analysis of data, evaluation of data quality and methods for data collection and analysis - Technical Writing (superior) – able to do complex writing tasks and deliver completed research papers with accuracy and coherence; able to deliver research papers using appropriate language, correct grammar and ethical standards; able to direct the presentation of data, concepts, policy issues, recommendations, etc. with coherence and accuracy in various forms and formats; reviews, edits and critiques technical writing outputs - Dissemination (superior) – with superior skills in translating research results into formal presentations; with expertise in delivering oral presentations to small and large audiences; able to express ideas and research results with coherence and clarity; articulate and exhibits expertise in the policy issues, concepts and research results presented - Project/Research Implementation and Management (superior) – able to manage and supervise implementation processes and procedures; has decision-making capacity for the implementation of project/research; provides workable solution to problems and issues on project implementation; able to build good partnership within the organization and with external partner', NULL, 1, 1, NULL, 0, '2019-02-05 17:58:33', '2019-02-14 11:04:36', 0, NULL, NULL),
	(8, 'PHCC-EXA4-2-2016', 'Executive Assistant IV', NULL, 'Bachelor’s degree ', '4 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '25 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(9, 'PHCC-EXA4-3-2016', 'Executive Assistant IV', NULL, 'Bachelor’s degree ', '5 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '26 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(10, 'PHCC-EXA4-4-2016', 'Executive Assistant IV', NULL, 'Bachelor’s degree ', '6 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '27 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(11, 'PHCC-EXA4-5-2016', 'Executive Assistant IV', NULL, 'Bachelor’s degree ', '7 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '28 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(12, 'PHCC-BS3-1-2016', 'Board Secretary III', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '2 years of experience in one or a combination of the ff.: secretariat support services corporate administrative legal or quasi-judicial proceedings legal / quality management and other related disciplines', '8 hours training in one or a combination of the ff.: corporate / administrative / legal / quasi-judicial proceedings communication legal / quality management or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(13, 'PHCC-EXA3-1-2016', 'Executive Assistant III', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(14, 'PHCC-EXA3-2-2016', 'Executive Assistant III', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(15, 'PHCC-EXA3-3-2016', 'Executive Assistant III', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(16, 'PHCC-EXA3-4-2016', 'Executive Assistant III', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(17, 'PHCC-EXA3-5-2016', 'Executive Assistant III', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff.: communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(18, 'PHCC-BS2-1-2016', 'Board Secretary II', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '1 year of experience in one or a combination of the ff.: secretariat support services corporate administrative legal or quasi-judicial proceedings legal / quality management and other related disciplines', '4 hours training in one or a combination of the ff.: corporate / administrative / legal / quasi-judicial proceedings communication legal / quality management or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(19, 'PHCC-BS2-2-2016', 'Board Secretary II', NULL, 'Bachelor’s degree in social sciences public administration/management legal management law or related field', '1 year of experience in one or a combination of the ff.: secretariat support services corporate administrative legal or quasi-judicial proceedings legal / quality management and other related disciplines', '4 hours training in one or a combination of the ff.: corporate / administrative / legal / quasi-judicial proceedings communication legal / quality management or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(20, 'PHCC-PSEC2-1-2016', 'Private Secretary II', NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(21, 'PHCC-PSEC2-2-2016', 'Private Secretary II', NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(22, 'PHCC-PSEC2-3-2016', 'Private Secretary II', NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(23, 'PHCC-PSEC2-4-2016', 'Private Secretary II', NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(24, 'PHCC-PSEC2-5-2016', 'Private Secretary II', NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(25, 'PHCC-SEC2-1-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(26, 'PHCC-SEC2-2-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(27, 'PHCC-SEC2-3-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(28, 'PHCC-SEC2-4-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(29, 'PHCC-CFER1-1-2016', 'Chauffeur I', NULL, 'Elementary School Graduate', '', '', 'MC 11 s. 1996 as amended - Cat. IV/Driver License', NULL, NULL, 2, 0.000, 179232.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(30, 'PHCC-DRV2-1-2016', 'Driver II', NULL, 'Elementary School Graduate', '', '', 'MC 11 s. 1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(31, 'PHCC-DRV2-2-2016', 'Driver II', NULL, 'Elementary School Graduate', '', '', 'MC 11 s. 1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(32, 'PHCC-DRV2-3-2016', 'Driver II', NULL, 'Elementary School Graduate', '', '', 'MC 11 s. 1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(33, 'PHCC-DRV2-4-2016', 'Driver II', NULL, 'Elementary School Graduate', '', '', 'MC 11 s. 1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 1, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(34, 'PHCC-EXED-1-2016', 'Executive Director', NULL, 'Master degree or Certificate of Leadership and Management from the CSC', '10 years of relevant experience in any of the fields of law, economics, commerce, management, finance or engineering; 5 years of which are supervisory/management experience', '120 hours of supervisory / management learning and development intervention undertaken within the last 5 years; OR 40 hours of MCLE, CPE, CPD and 80 hours of management training taken within the last 5 years', 'CS Professional / Second Level Eligibility; RA 1080 (CPA, Bar, Engineer)', NULL, NULL, 17, 0.000, 2642400.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 2, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(35, 'PHCC-EXA3-6-2016', 'Executive Assistant III', NULL, 'Bachelor degree in social sciences, public administration/management, legal management, law or related field', '2 years of relevant experience in one or a combination of the following: communication, public relations, corporate governance, quality management, administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff.: communication, governance, public/corporate/quality management, legal/ administrative proceedings, economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 2, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(36, 'PHCC-EXA2-1-2016', 'Executive Assistant II', NULL, 'Bachelor degree in social sciences, public administration/management, legal management, law or related field', '1 year of relevant experience in one or a combination of the following: communication, public relations, corporate governance, quality management, administrative proceedings or related disciplines', '4 hours of training in one or a combination of the ff.: communication, governance, public/corporate/quality management, legal/ administrative proceedings, economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 2, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(37, 'PHCC-PSEC1-1-2016', 'Private Secretary I', NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 2, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(38, 'PHCC-SEC2-5-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration, quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration, quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 2, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(39, 'PHCC-DRV2-5-2016', 'Driver II', NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11, s.1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 2, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(40, 'PHCC-DIR4-1-2016', 'Director IV', NULL, 'Master’s degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 16, 0.000, 2031600.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(41, 'PHCC-SEC2-6-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration, quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration, quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(42, 'PHCC-DRV2-6-2016', 'Driver II', NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11, s.1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(43, 'PHCC-CADOF-1-2016', 'Chief Administrative Officer', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(44, 'PHCC-SADOF-1-2016', 'Supervising Administrative Officer', NULL, 'Bachelor degre in social sciences, public administration / management, legal management, psychology, commerce, finance, accounting or related field', '3 years of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, personnel administration / management, organizational development, accounting, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(45, 'PHCC-ADO3-1-2016', 'Administrative Officer III', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '2 years of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '8 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, public administraion / management, organizational development, accoutning, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(46, 'PHCC-CASH3-1-2016', 'Cashier III', NULL, 'Bachelor degree in commerce, finance, accounting or related field', '2 years of relevant experience in one or a combination of the ff.: financial management, disbursement and collection, cash/check management, bookkeeping, accounting and other related disciplines', '8 hours of training n financia management, accounting, bookkeeping, disbursement and collection, cash/check management, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(47, 'PHCC-RO3-1-2016', 'Records Officer III', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '2 years of relevant experience in one or a combination of the ff.: quality / records management, office administration, systems development and other related disciplines', '8 hours of training in quality / records management, office administration, information and communications technology or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(48, 'PHCC-SU03-1-2016', 'Supply Officer III', NULL, 'Bachelor degre in social sciences, public administration / management,commerce, finance or related field', '2 years of relevant experience in one or a combination of the ff.: quality / property management, procurement, planning and other related disciplines', '8 hours of training in quality / property management, procurement, planning, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(49, 'PHCC-ADO2-1-2016', 'Administrative Officer II', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '1 year of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, public administraion / management, organizational development, accoutning, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(50, 'PHCC-ADO2-2-2016', 'Administrative Officer II', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '1 year of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, public administraion / management, organizational development, accoutning, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(51, 'PHCC-ADO2-3-2016', 'Administrative Officer II', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '1 year of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, public administraion / management, organizational development, accoutning, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(52, 'PHCC-CASH2-1-2016', 'Cashier II', NULL, 'Bachelor degree in commerce, finance, accounting or related field', '1 year of relevant experience in one or a combination of the ff.: financial management, disbursement and collection, cash/check management, bookkeeping, accounting and other related disciplines', '4 hours of training n financia management, accounting, bookkeeping, disbursement and collection, cash/check management, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(53, 'PHCC-RO2-1-2016', 'Records Officer II', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '1 year of relevant experience in one or a combination of the ff.: quality / records management, office administration, systems development and other related disciplines', '4 hours of training in quality / records management, office administration, information and communications technology or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(54, 'PHCC-SU02-1-2016', 'Supply Officer II', NULL, 'Bachelor degre in social sciences, public administration / management,commerce, finance or related field', '2 years of relevant experience in one or a combination of the ff.: quality / property management, procurement, planning and other related disciplines', '8 hours of training in quality / property management, procurement, planning, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(55, 'PHCC-CASH1-1-2016', 'Cashier I', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(56, 'PHCC-RO1-1-2016', 'Records Officer I', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(57, 'PHCC-SU01-1-2016', 'Supply Officer I', NULL, 'Bachelor degre in social sciences, public administration / management,commerce, finance or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 4, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(58, 'PHCC-CADOF-2-2016', 'Chief Administrative Officer', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(59, 'PHCC-SADOF-2-2016', 'Supervising Administrative Officer', NULL, 'Bachelor degre in social sciences, public administration / management, legal management, psychology, commerce, finance, accounting or related field', '3 years of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, personnel administration / management, organizational development, accounting, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(60, 'PHCC-HRMO3-1-2016', 'Human Resource Management Officer III', NULL, 'Bachelor degre in pyscholoygy, behavioral science, human resource management or related field', '2 years of relevant experience in one or a combination of the ff.: human resources, organizational development, personnel administration / management, competency-based recruitment process, quality management and other related disciplines', '8 hours of training in human resources, organizational development, personnel administration/management, public / quality management, competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(61, 'PHCC-HRMO3-2-2016', 'Human Resource Management Officer III', NULL, 'Bachelor degre in pyscholoygy, behavioral science, human resource management or related field', '2 years of relevant experience in one or a combination of the ff.: human resources, organizational development, personnel administration / management, competency-based recruitment process, quality management and other related disciplines', '8 hours of training in human resources, organizational development, personnel administration/management, public / quality management, competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(62, 'PHCC-HRMO2-1-2016', 'Human Resource Management Officer II', NULL, 'Bachelor degre in pyscholoygy, behavioral science, human resource management or related field', '1 year of relevant experience in one or a combination of the ff.: human resources, organizational development, personnel administration / management, competency-based recruitment process, quality management and other related disciplines', '4 hours of training in human resources, organizational development, personnel administration/management, public / quality management, competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(63, 'PHCC-HRMO2-2-2016', 'Human Resource Management Officer II', NULL, 'Bachelor degre in pyscholoygy, behavioral science, human resource management or related field', '1 year of relevant experience in one or a combination of the ff.: human resources, organizational development, personnel administration / management, competency-based recruitment process, quality management and other related disciplines', '4 hours of training in human resources, organizational development, personnel administration/management, public / quality management, competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(64, 'PHCC-HRMO1-1-2016', 'Human Resource Management Officer I', NULL, 'Bachelor degre in pyscholoygy, behavioral science, human resource management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 5, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(65, 'PHCC-ITO3-1-2016', 'Information Technology Officer III', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 6, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(66, 'PHCC-ITO2-1-2016', 'Information Technology Officer II', NULL, 'Bachelor degree in Computer Science, Information Technology, Electronics and Communications Engineering or related field', '3 years of relevant experience in one or a combination of the ff.: information and communications technology, systems development programming and other related disciplines', '16 hours of training in one or a combination of the ff.: information and communication and communications technology, systems development, programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 6, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(67, 'PHCC-ITO1-1-2016', 'Information Technology Officer I', NULL, 'Bachelor degree in Computer Science, Information Technology, Electronics and Communications Engineering or related field', '2 years of relevant experience in one or a combination of the ff.: information and communications technology, systems development programming and other related disciplines', '8 hours of training in one or a combination of the ff.: information and communication and communications technology, systems development, programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 6, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(68, 'PHCC-ITO1-2-2016', 'Information Technology Officer I', NULL, 'Bachelor degree in Computer Science, Information Technology, Electronics and Communications Engineering or related field', '2 years of relevant experience in one or a combination of the ff.: information and communications technology, systems development programming and other related disciplines', '8 hours of training in one or a combination of the ff.: information and communication and communications technology, systems development, programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 6, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(69, 'PHCC-INFOSA2-1-2016', 'Information Systems Analyst II', NULL, 'Bachelor degree in Computer Science, Information Technology, Electronics and Communications Engineering or related field', '1 year of relevant experience in one or a combination of the ff.: information and communications technology, systems development programming and other related disciplines', '4 hours of training in one or a combination of the ff.: information and communication and communications technology, systems development, programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 6, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(70, 'PHCC-INFOSA1-1-2016', 'Information Systems Analyst I', NULL, 'Bachelor degree in Computer Science, Information Technology, Electronics and Communications Engineering or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 6, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(71, 'PHCC-ATY5-1-2016', 'Attorney V', NULL, 'Bachelor of Laws, preferably with Master’s degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 14, 0.000, 1350000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(72, 'PHCC-ATY4-1-2016', 'Attorney IV', NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 12, 0.000, 886800.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(73, 'PHCC-ATY3-1-2016', 'Attorney III', NULL, 'Bachelor of Laws', '1 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '4 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(74, 'PHCC-ATY3-2-2016', 'Attorney III', NULL, 'Bachelor of Laws', '1 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '4 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(75, 'PHCC-ATY2-1-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(76, 'PHCC-ATY2-2-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(77, 'PHCC-ATY2-3-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(78, 'PHCC-ATY2-4-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(79, 'PHCC-ATY1-1-2016', 'Attorney I', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(80, 'PHCC-ATY1-2-2016', 'Attorney I', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(81, 'PHCC-LEA2-1-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(82, 'PHCC-LEA2-2-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 7, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(83, 'PHCC-DIR4-2-2016', 'Director IV', NULL, 'Master’s degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 16, 0.000, 2031600.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(84, 'PHCC-SEC2-7-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration, quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration, quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(85, 'PHCC-DRV2-7-2016', 'Driver II', NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11, s.1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(86, 'PHCC-CACT-1-2016', 'Chief Accountant', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting (CPA), preferably with Master’s degree or Certificate in Leadership and management from CSC', '4 years of supervisory/ management experience', '40 hours of supervisory/ management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (CPA)', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(87, 'PHCC-A4-1-2016', 'Accountant IV', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting', '3 years of relevant experience in one or a combination of the following: government accounting and auditing, financial management, and other related disciplines', '16 hours of training in CPE, financial management, government accounting, internal controls or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(88, 'PHCC-A3-1-2016', 'Accountant III', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting', '2 years of relevant experience in one or a combination of the following: government accounting and auditing, financial management, and other related disciplines', '8  hours training in CPE, financial management, government accounting, internal controls, or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(89, 'PHCC-A3-2-2016', 'Accountant III', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting', '2 years of relevant experience in one or a combination of the following: government accounting and auditing, financial management, and other related disciplines', '8  hours training in CPE, financial management, government accounting, internal controls, or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(90, 'PHCC-A2-1-2016', 'Accountant II', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting', '1 year of relevant experience in one or a combination of the following: government accounting and auditing, financial management, and other related disciplines', '4  hours training in CPE, financial management, government accounting, internal controls, or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(91, 'PHCC-A2-2-2016', 'Accountant II', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting', '1 year of relevant experience in one or a combination of the following: government accounting and auditing, financial management, and other related disciplines', '4  hours training in CPE, financial management, government accounting, internal controls, or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(92, 'PHCC-A1-1-2016', 'Accountant I', NULL, 'Bachelor’s degree in Commerce/ Business Administration major in Accounting', 'None required', 'None required', 'R.A. 1080 (CPA)', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 8, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(93, 'PHCC-CADOF-3-2016', 'Chief Administrative Officer', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(94, 'PHCC-SADOF-3-2016', 'Supervising Administrative Officer', NULL, 'Bachelor degre in social sciences, public administration / management, legal management, psychology, commerce, finance, accounting or related field', '3 years of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, personnel administration / management, organizational development, accounting, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(95, 'PHCC-BUDO3-1-2016', 'Budget Officer III', NULL, 'Bachelor degree in commerce, finance, accounting or related field', '2 years of relevant experience in one or a combination of the ff.: government budgeting, financial management and other related disciplines', '8 hours of training in government budgeting, financial management, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(96, 'PHCC-BUDO3-2-2016', 'Budget Officer III', NULL, 'Bachelor degree in commerce, finance, accounting or related field', '2 years of relevant experience in one or a combination of the ff.: government budgeting, financial management and other related disciplines', '8 hours of training in government budgeting, financial management, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(97, 'PHCC-BUDO2-1-2016', 'Budget Officer II', NULL, 'Bachelor degree in commerce, finance, accounting or related field', '1 year of relevant experience in one or a combination of the ff.: government budgeting, financial management and other related disciplines', '4 hours of training in government budgeting, financial management, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(98, 'PHCC-BUDO2-2-2016', 'Budget Officer II', NULL, 'Bachelor degree in commerce, finance, accounting or related field', '1 year of relevant experience in one or a combination of the ff.: government budgeting, financial management and other related disciplines', '4 hours of training in government budgeting, financial management, internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(99, 'PHCC-BUDO1-1-2016', 'Budget Officer I', NULL, '', '', '', '', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 9, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(100, 'PHCC-PLO5-1-2016', 'Planning Officer V', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(101, 'PHCC-PLO4-1-2016', 'Planning Officer IV', NULL, 'Bachelor degree in social sciences, economics, public management/ administration or related field', '3 years of relevant experiene in one or a combination of the ff.: governance/ corporate, planning, financial / quality management and other related disciplines', '16 hours of training in government / corporate, planning, financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(102, 'PHCC-ADO3-2-2016', 'Administrative Officer III', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '2 years of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '8 hour of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, public administraion / management, organizational development, accoutning, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(103, 'PHCC-PLO3-1-2016', 'Planning Officer III', NULL, 'Bachelor degree in social sciences, economics, public management/ administration or related field', '3 years of relevant experiene in one or a combination of the ff.: governance/ corporate, planning, financial / quality management and other related disciplines', '16 hours of training in government / corporate, planning, financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(104, 'PHCC-PLO3-2-2016', 'Planning Officer III', NULL, 'Bachelor degree in social sciences, economics, public management/ administration or related field', '3 years of relevant experiene in one or a combination of the ff.: governance/ corporate, planning, financial / quality management and other related disciplines', '16 hours of training in government / corporate, planning, financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(105, 'PHCC-ADO2-4-2016', 'Administrative Officer II', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', '1 year of relevant experience in one or a combination of the ff.: quality / financial / property management, procurement, human resources, personnel administration / management, organizational development, accounting, administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff.: quality / financial / property management, procurement, internal controls, human resources, public administraion / management, organizational development, accoutning, administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(106, 'PHCC-PLO2-1-2016', 'Planning Officer II', NULL, 'Bachelor degree in social sciences, economics, public management/ administration or related field', '1 year of relevant experiene in one or a combination of the ff.: governance/ corporate, planning, financial / quality management and other related disciplines', '4 hours of training in government / corporate, planning, financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(107, 'PHCC-PLO2-2-2016', 'Planning Officer II', NULL, 'Bachelor degree in social sciences, economics, public management/ administration or related field', '1 year of relevant experiene in one or a combination of the ff.: governance/ corporate, planning, financial / quality management and other related disciplines', '4 hours of training in government / corporate, planning, financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 6, 0.000, 364212.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(108, 'PHCC-ADO1-1-2016', 'Administrative Officer I', NULL, 'Bachelor degre in social sciences, public administration / management, legal management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(109, 'PHCC-PLO1-1-2016', 'Planning Officer I', NULL, 'Bachelor degree in social sciences, economics, public management/ administration or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 4, 0.000, 270384.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 10, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(110, 'PHCC-DIR4-3-2016', 'Director IV', NULL, 'Master’s degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 16, 0.000, 2031600.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(111, 'PHCC-DIR3-1-2016', 'Director III', NULL, 'Master’s degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 15, 0.000, 1470000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(112, 'PHCC-SEC2-8-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration, quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration, quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(113, 'PHCC-DRV2-8-2016', 'Driver II', NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11, s.1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(114, 'PHCC-INVA5-1-2016', 'Investigation Agent V', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(115, 'PHCC-INVA4-1-2016', 'Investigation Agent IV', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '4 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '16 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 12, 0.000, 886800.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(116, 'PHCC-INVA3-1-2016', 'Investigation Agent III', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '3 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '16 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(117, 'PHCC-INVA3-2-2016', 'Investigation Agent III', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '3 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '16 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(118, 'PHCC-INVA2-1-2016', 'Investigation Agent II', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '2 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(119, 'PHCC-INVA2-2-2016', 'Investigation Agent II', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '2 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(120, 'PHCC-INVA1-1-2016', 'Investigation Agent I', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '2 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(121, 'PHCC-INVA1-2-2016', 'Investigation Agent I', NULL, 'Bachelor degree in law, commerce/ business administration major in accounting, management, finance, engineering or related field', '2 years of relevant experience in one or a combination of the ff.: administrative, corporate, quasi-judicial, judicial, legal or investigation proceedings, accounting, auditing and other related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPE, CPD, administrative, corporate, legal proceedings, economics, trade, industry, investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar, CPA, Engineer, Criminologist)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(122, 'PHCC-LEA2-3-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(123, 'PHCC-LEA2-4-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 11, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(124, 'PHCC-ATY5-2-2016', 'Attorney V', NULL, 'Bachelor of Laws, preferably with Master’s degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 14, 0.000, 1350000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(125, 'PHCC-ATY4-2-2016', 'Attorney IV', NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 12, 0.000, 886800.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(126, 'PHCC-ATY3-3-2016', 'Attorney III', NULL, 'Bachelor of Laws', '1 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '4 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(127, 'PHCC-ATY3-4-2016', 'Attorney III', NULL, 'Bachelor of Laws', '2 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '5 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(128, 'PHCC-ATY3-5-2016', 'Attorney III', NULL, 'Bachelor of Laws', '3 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '6 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(129, 'PHCC-ATY3-6-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(130, 'PHCC-ATY2-5-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(131, 'PHCC-ATY2-6-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(132, 'PHCC-ATY2-7-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(133, 'PHCC-ATY2-8-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(134, 'PHCC-LEA2-5-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(135, 'PHCC-LEA2-6-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '5 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(136, 'PHCC-LEA2-7-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '6 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(137, 'PHCC-LEA2-8-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '7 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(138, 'PHCC-DIR4-4-2016', 'Director IV', NULL, 'Master’s degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 16, 0.000, 2031600.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(139, 'PHCC-DIR3-2-2016', 'Director III', NULL, 'Master’s degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 15, 0.000, 1470000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(140, 'PHCC-SEC2-9-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration, quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration, quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(141, 'PHCC-DRV2-9-2016', 'Driver II', NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11, s.1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(142, 'PHCC-ATY5-3-2016', 'Attorney V', NULL, 'Bachelor of Laws, preferably with Master’s degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 14, 0.000, 1350000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(143, 'PHCC-ATY4-3-2016', 'Attorney IV', NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 12, 0.000, 886800.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(144, 'PHCC-ATY3-7-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(145, 'PHCC-ATY3-8-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(146, 'PHCC-ECO3-1-2016', 'Economist III', NULL, 'Bachelor degree in Economics, Statistis, Mathematics or related field', '2 years of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy, development planning and other related economic disciplines (e.g. financial, labor, regional, industry, international, agricultural)', '8 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy, regulation, statistics, trade, industry, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(147, 'PHCC-ECO3-2-2016', 'Economist III', NULL, 'Bachelor degree in Economics, Statistis, Mathematics or related field', '2 years of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy, development planning and other related economic disciplines (e.g. financial, labor, regional, industry, international, agricultural)', '8 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy, regulation, statistics, trade, industry, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(148, 'PHCC-ATY2-9-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(149, 'PHCC-ATY2-10-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(150, 'PHCC-ECO2-1-2016', 'Economist II', NULL, 'Bachelor degree in Economics, Statistis, Mathematics or related field', '1 year of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy, development planning and other related economic disciplines (e.g. financial, labor, regional, industry, international, agricultural)', '4 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy, regulation, statistics, trade, industry, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(151, 'PHCC-ECO2-2-2016', 'Economist II', NULL, 'Bachelor degree in Economics, Statistis, Mathematics or related field', '1 year of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy, development planning and other related economic disciplines (e.g. financial, labor, regional, industry, international, agricultural)', '4 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy, regulation, statistics, trade, industry, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(152, 'PHCC-LEA2-9-2016', 'Legal Assistant II', NULL, '', '', '', '', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(153, 'PHCC-LEA2-10-2016', 'Legal Assistant II', NULL, '', '', '', '', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 13, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(154, 'PHCC-ATY5-4-2016', 'Attorney V', NULL, 'Bachelor of Laws, preferably with Master’s degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 14, 0.000, 1350000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(155, 'PHCC-ATY4-4-2016', 'Attorney IV', NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 12, 0.000, 886800.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(156, 'PHCC-ATY3-9-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(157, 'PHCC-ATY3-10-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(158, 'PHCC-ATY3-11-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(159, 'PHCC-ATY3-12-2016', 'Attorney III', NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff.: administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff.: MCLE, CPD, administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 10, 0.000, 709200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(160, 'PHCC-ATY2-11-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(161, 'PHCC-ATY2-12-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(162, 'PHCC-ATY2-13-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(163, 'PHCC-ATY2-14-2016', 'Attorney II', NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 8, 0.000, 543228.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(164, 'PHCC-LEA2-11-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '7 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(165, 'PHCC-LEA2-12-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '8 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(166, 'PHCC-LEA2-13-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '9 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(167, 'PHCC-LEA2-14-2016', 'Legal Assistant II', NULL, 'Bachelor of Science in Legal Management, AB Paralegal Studies, Law, Political Science or other allied courses', 'None required', '10 hours of training relevant to legal work, such as legal ethics, legal research and writing, or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 12, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(168, 'PHCC-DIR4-5-2016', 'Director IV', NULL, 'Master degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 16, 0.000, 2031600.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(169, 'PHCC-DIR3-3-2016', 'Director III', NULL, 'Master degree or its equivalent, or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar, CPA, Eng.)', NULL, NULL, 15, 0.000, 1470000.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(170, 'PHCC-SEC2-10-2016', 'Secretary II', NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff.: office/procedures/administration, quality management and other related disciplines', '4 hours of training in one or a combination of the ff.: office procedures / administration, quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 3, 0.000, 215700.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(171, 'PHCC-DRV2-10-2016', 'Driver II', NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11, s.1996 as amended - Cat. IV/Driver License', NULL, NULL, 1, 0.000, 169044.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 3, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(172, 'PHCC-CMPRO5-1-2016', 'Policy Research Officer V', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(173, 'PHCC-CMPRO4-1-2016', 'Policy Research Officer IV', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '3 years of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '16 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(174, 'PHCC-CMPRO3-1-2016', 'Policy Research Officer III', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '2 years of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '8 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(175, 'PHCC-CMPRO3-2-2016', 'Policy Research Officer III', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '2 years of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '8 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(176, 'PHCC-CMPRO3-3-2016', 'Policy Research Officer III', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '2 years of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '8 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(177, 'PHCC-CMPRO3-4-2016', 'Policy Research Officer III', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '2 years of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '8 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(178, 'PHCC-CMPRO3-5-2016', 'Policy Research Officer III', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '2 years of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '8 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(179, 'PHCC-CMPRO2-1-2016', 'Policy Research Officer II', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '1 year of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '4 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(180, 'PHCC-CMPRO2-2-2016', 'Policy Research Officer II', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '2 year of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '5 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(181, 'PHCC-CMPRO2-3-2016', 'Policy Research Officer II', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '3 year of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '6 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(182, 'PHCC-CMPRO2-4-2016', 'Policy Research Officer II', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', '4 year of relevant experience in one or a combination of the ff.: policy research and regulation analysis, operation of markets, development plannin, legislatin and other related disciplines', '7 hours of training in corporate governance, public / competition policy, regulation, trade, industry, economics, policy research, development planning, operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(183, 'PHCC-CMPRO1-1-2016', 'Policy Research Officer I', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(184, 'PHCC-CMPRO1-2-2016', 'Policy Research Officer I', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(185, 'PHCC-CMPRO1-3-2016', 'Policy Research Officer I', NULL, 'Bachelor degree in Economics, Public Administration/Policy or related field, preferably with degree in law', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 14, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(186, 'PHCC-INFO5-1-2016', 'Information Officer V', NULL, 'Master’s degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 13, 0.000, 1042788.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(187, 'PHCC-INFO4-1-2016', 'Information Officer IV', NULL, 'Bachelor degree in the field of Arts and Sciences, Communications, Economics, Information Technology or related field', '3 years of relevant experience in one or a combination of the ff.: communication, research and other related disciplines', '16 hours of training in one or a combination of the ff.: communication, research, economics, information technology, competition policy, corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(188, 'PHCC-INFO3-1-2016', 'Information Officer III', NULL, 'Bachelor degree in the field of Arts and Sciences, Communications, Economics, Information Technology or related field', '2 years of relevant experience in one or a combination of the ff.: communication, research and other related disciplines', '8 hours of training in one or a combination of the ff.: communication, research, economics, information technology, competition policy, corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(189, 'PHCC-INFO3-2-2016', 'Information Officer III', NULL, 'Bachelor degree in the field of Arts and Sciences, Communications, Economics, Information Technology or related field', '2 years of relevant experience in one or a combination of the ff.: communication, research and other related disciplines', '8 hours of training in one or a combination of the ff.: communication, research, economics, information technology, competition policy, corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(190, 'PHCC-INFO2-1-2016', 'Information Officer II', NULL, 'Bachelor degree in the field of Arts and Sciences, Communications, Economics, Information Technology or related field', '1 year of relevant experience in one or a combination of the ff.: communication, research and other related disciplines', '4 hours of training in one or a combination of the ff.: communication, research, economics, information technology, competition policy, corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(191, 'PHCC-INFO2-2-2016', 'Information Officer II', NULL, 'Bachelor degree in the field of Arts and Sciences, Communications, Economics, Information Technology or related field', '1 year of relevant experience in one or a combination of the ff.: communication, research and other related disciplines', '4 hours of training in one or a combination of the ff.: communication, research, economics, information technology, competition policy, corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(192, 'PHCC-INFO1-1-2016', 'Information Officer I', NULL, 'Bachelor degree in the field of Arts and Sciences, Communications, Economics, Information Technology or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 15, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(193, 'PHCC-TRNSP4-1-2016', 'Training Specialist IV', NULL, 'Bachelor degree in Communications, Economics, Law, Psychology or related field', '3 years of relevant experience in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; communication, human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 11, 0.000, 833988.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 16, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(194, 'PHCC-TRNSP3-1-2016', 'Training Specialist III', NULL, 'Bachelor degree in Communications, Economics, Law, Psychology or related field', '2 years of relevant experience in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '8 hours of training in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; communication, human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 16, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(195, 'PHCC-TRNSP3-2-2016', 'Training Specialist III', NULL, 'Bachelor degree in Communications, Economics, Law, Psychology or related field', '2 years of relevant experience in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '8 hours of training in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; communication, human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 9, 0.000, 667200.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 16, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(196, 'PHCC-TRNSP2-1-2016', 'Training Specialist II', NULL, 'Bachelor degree in Communications, Economics, Law, Psychology or related field', '1 year of relevant experience in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; communication, human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 16, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(197, 'PHCC-TRNSP2-2-2016', 'Training Specialist II', NULL, 'Bachelor degree in Communications, Economics, Law, Psychology or related field', '1 year of relevant experience in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff.: development and management of course curriculum outlines; deveopment, monitoing and attainment of resource requirements for training programs; execution of training programs; communication, human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 7, 0.000, 482028.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 16, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL),
	(198, 'PHCC-TRNSP1-1-2016', 'Training Specialist I', NULL, 'Bachelor degree in Communications, Economics, Law, Psychology or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 5, 0.000, 328968.00, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, 0, 16, 0, NULL, 0, '2019-02-05 17:58:33', NULL, 0, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table rms_pids.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.job_offers: ~1 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
INSERT INTO `job_offers` (`id`, `applicant_id`, `pera_amount`, `clothing_allowance`, `year_end_bonus`, `cash_gift`, `executive_director`, `job_offer_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-02-12 09:39:57', '2019-02-12 09:40:54', NULL);
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table rms_pids.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.migrations: ~21 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_07_13_041020_create_employees_table', 1),
	(4, '2018_07_13_042843_create_employee_information_table', 1),
	(5, '2018_08_14_084139_create_config_table', 1),
	(6, '2018_08_20_100207_create_jobs_table', 1),
	(7, '2018_09_04_093333_create_applicants_table', 1),
	(8, '2018_09_04_124945_create_countries_table', 1),
	(9, '2018_09_12_054802_add_reference_no_column_to_applicants_table', 1),
	(10, '2018_09_19_004331_create_evaluations_table', 1),
	(11, '2018_09_23_084430_alter_plantilla_item_number_column_to_varchar', 1),
	(12, '2018_09_25_031118_create_recommendations_table', 1),
	(13, '2018_09_26_081458_create_appointments_table', 1),
	(14, '2018_09_26_121939_create_job_offers_table', 1),
	(15, '2018_09_30_074257_create_assumptions_table', 1),
	(16, '2018_09_30_081027_create_attestations_table', 1),
	(17, '2018_10_06_050142_alter_columns_rating_data_evaluations_table', 1),
	(18, '2018_10_10_020636_add_column_publication_to_applicants_table', 1),
	(19, '2018_10_17_130131_alter_column_annual_basic_salary_on_jobs_table', 1),
	(20, '2018_10_18_044949_create_matrix_qualifications_table', 1),
	(21, '2018_10_31_090033_add_column_job_id_to_evaluations_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table rms_pids.offices
DROP TABLE IF EXISTS `offices`;
CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_pids.offices: ~3 rows (approximately)
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Office of the Chairman', NULL, NULL, NULL, NULL, NULL),
	(2, 'Office of the Executive Director', NULL, NULL, NULL, NULL, NULL),
	(3, 'Office of the Director', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;

-- Dumping structure for table rms_pids.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table rms_pids.preliminary_evaluation
DROP TABLE IF EXISTS `preliminary_evaluation`;
CREATE TABLE IF NOT EXISTS `preliminary_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isc_chairperson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_one` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_two` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ea_representative` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.preliminary_evaluation: ~1 rows (approximately)
/*!40000 ALTER TABLE `preliminary_evaluation` DISABLE KEYS */;
INSERT INTO `preliminary_evaluation` (`id`, `applicant_id`, `age`, `education`, `experience`, `eligibility`, `training`, `remarks`, `isc_chairperson`, `isc_member_one`, `isc_member_two`, `ea_representative`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, 0, 'Bachelor of Science in Computer Science', '10 Years Government, 2 Years Private', 'Latest Civil Service Eligibility', 'Latest Training 1, <br> Latest Training 2', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-02-12 12:05:07', '2019-02-12 12:05:07', NULL);
/*!40000 ALTER TABLE `preliminary_evaluation` ENABLE KEYS */;

-- Dumping structure for table rms_pids.recommendations
DROP TABLE IF EXISTS `recommendations`;
CREATE TABLE IF NOT EXISTS `recommendations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.recommendations: ~1 rows (approximately)
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
INSERT INTO `recommendations` (`id`, `applicant_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(3, 1, 1, NULL, '2019-02-13 07:41:21', '2019-02-13 07:41:21', NULL);
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;

-- Dumping structure for table rms_pids.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pids.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin', NULL, '$2y$10$B.ZO79UiXJyLuY706Wepa.MVM3X8BjCE/ullOFJI7lhFLX1Fva.JG', 'PMkKnPyPuD6rWoj4TGKB8frqQblHKnnHSnWInhiHNwPS6mQ0G4JpGa2ynPYX', '2019-01-26 07:32:58', '2019-01-26 07:32:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
