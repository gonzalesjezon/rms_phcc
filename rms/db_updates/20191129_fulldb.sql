-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hris.access_modules
DROP TABLE IF EXISTS `access_modules`;
CREATE TABLE IF NOT EXISTS `access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.access_modules: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_modules` ENABLE KEYS */;

-- Dumping structure for table hris.access_rights
DROP TABLE IF EXISTS `access_rights`;
CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.access_rights: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_rights` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_rights` ENABLE KEYS */;

-- Dumping structure for table hris.access_types
DROP TABLE IF EXISTS `access_types`;
CREATE TABLE IF NOT EXISTS `access_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.access_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_types` ENABLE KEYS */;

-- Dumping structure for table hris.applicants
DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `performance_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `qualified` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.applicants: ~3 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `employee_number`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `performance_path`, `active`, `qualified`, `remarks`, `gwa`, `zip_code`, `permanent_zip_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(14, 1, '5d28268b61626', NULL, 'Bing', NULL, 'Loy', NULL, NULL, 'loy@gmail.com', '567890', NULL, NULL, 'agency', '2011-06-29', 'Manila', 'male', 'single', NULL, 0, 0, '120', '40', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '374', 'Manila', 'Manila', '837', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, '1018', NULL, 1, 1, '2019-07-12 21:19:55', '2019-07-13 19:18:18', NULL),
	(15, 1, '5d296cc5412ce', NULL, 'Bingo', NULL, 'Go', NULL, NULL, 'go@gmail.com', '345678', NULL, NULL, 'agency', '2005-06-07', 'Manila', 'male', 'single', NULL, NULL, NULL, '120', '40', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', 'SM', 'Sub', '324', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, '1018', NULL, 1, NULL, '2019-07-13 20:31:49', '2019-07-13 20:46:38', NULL),
	(16, 1, '5d298cc217c34', NULL, 'Sam', NULL, 'Lyn', NULL, NULL, 'lyn@gmail.com', '2345678', NULL, NULL, 'agency', '2009-02-03', 'Manila', 'male', 'single', NULL, 0, 0, '120', '40', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', 'Mla', 'Sk', '34', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 1, 1, '2019-07-13 22:48:18', '2019-10-10 06:43:54', NULL),
	(17, 1, '5d6dfa8c3629e', NULL, 'Juan', NULL, 'Enrile', NULL, NULL, 'enrile@gmail.com', '3456789', NULL, NULL, 'agency', '1992-03-03', 'Manila', 'male', 'single', NULL, 0, 0, '120', '40', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M87', 'Roxas St', 'NA', 'Brgy 67', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52', NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table hris.appointments
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form34b_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form212_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form1_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form32_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form4_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.appointments: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` (`id`, `applicant_id`, `form33_hrmo`, `form34b_hrmo`, `form212_hrmo`, `eligibility_hrmo`, `form1_hrmo`, `form32_hrmo`, `form4_hrmo`, `form33_cscfo`, `form34b_cscfo`, `form212_cscfo`, `eligibility_cscfo`, `form1_cscfo`, `form32_cscfo`, `form4_cscfo`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-07-15 20:45:26', '2019-07-15 20:45:26', NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_casual
DROP TABLE IF EXISTS `appointment_casual`;
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_casual: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
INSERT INTO `appointment_casual` (`id`, `applicant_id`, `employee_status`, `nature_of_appointment`, `appointing_officer`, `hrmo`, `hrmo_date_sign`, `date_sign`, `period_emp_from`, `period_emp_to`, `daily_wage`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 0, 0, NULL, NULL, NULL, NULL, '2019-11-29', '2019-12-27', '300', 1, 1, '2019-11-29 03:01:49', '2019-11-29 03:06:23', NULL);
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `employee_status_id` int(11) DEFAULT NULL,
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `vice` varchar(225) DEFAULT NULL,
  `who` varchar(225) DEFAULT NULL,
  `posted_in` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `hrmo_assessment_date` date DEFAULT NULL,
  `chairperson_deliberation_date` date DEFAULT NULL,
  `form_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_forms: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
INSERT INTO `appointment_forms` (`id`, `applicant_id`, `nature_of_appointment`, `employee_status_id`, `appointing_officer`, `hrmo`, `chairperson`, `vice`, `who`, `posted_in`, `date_sign`, `publication_date_from`, `publication_date_to`, `hrmo_date_sign`, `chairperson_date_sign`, `period_emp_from`, `period_emp_to`, `date_issued`, `assessment_date`, `hrmo_assessment_date`, `chairperson_deliberation_date`, `form_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-28', '2019-11-30', NULL, NULL, 1, 1, NULL, '2019-11-28 07:50:14', '2019-11-28 07:50:14', NULL);
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_issued: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
INSERT INTO `appointment_issued` (`id`, `applicant_id`, `date_issued`, `period_of_employment_from`, `period_of_employment_to`, `nature_of_appointment`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, '2019-11-13', '2019-11-18', '2019-11-25', 2, 1, NULL, '2019-11-28 08:20:10', '2019-11-28 08:20:10', NULL);
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_processing
DROP TABLE IF EXISTS `appointment_processing`;
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_processing: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
INSERT INTO `appointment_processing` (`id`, `applicant_id`, `educ_qualification`, `educ_remarks`, `educ_check`, `exp_qualification`, `exp_remarks`, `exp_check`, `training_qualification`, `training_remarks`, `training_check`, `eligibility_qualification`, `eligibility_remarks`, `eligibility_check`, `other_qualification`, `other_remarks`, `other_check`, `ra_form_33`, `ra_employee_status`, `ra_nature_appointment`, `ra_appointing_authority`, `ra_date_sign`, `ra_date_publication`, `ra_certification`, `ra_pds`, `ra_eligibility`, `ra_position_description`, `ar_01`, `ar_02`, `ar_03`, `ar_04`, `ar_05`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, NULL, NULL, 1, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-11-28 09:43:43', '2019-11-28 09:43:43', NULL);
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_requirements
DROP TABLE IF EXISTS `appointment_requirements`;
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_status` int(11) DEFAULT '0',
  `medical_status` int(11) DEFAULT '0',
  `oath_status` int(11) DEFAULT '0',
  `nbi_status` int(11) DEFAULT '0',
  `gsis_enrollment_status` int(11) DEFAULT '0',
  `bir_form_2316_status` int(11) DEFAULT '0',
  `bir_form_2305_status` int(11) DEFAULT '0',
  `bir_form_1905_status` int(11) DEFAULT '0',
  `philhealth_status` int(11) DEFAULT '0',
  `hdmf_status` int(11) DEFAULT '0',
  `saln_status` int(11) DEFAULT '0',
  `assumption_status` int(11) DEFAULT '0',
  `position_description_status` int(11) DEFAULT '0',
  `land_bank_status` int(11) DEFAULT '0',
  `original_status` int(11) DEFAULT '0',
  `passport_status` int(11) DEFAULT '0',
  `bday_status` int(11) DEFAULT '0',
  `pds_copies` int(11) DEFAULT '0',
  `medical_copies` int(11) DEFAULT '0',
  `oath_copies` int(11) DEFAULT '0',
  `nbi_copies` int(11) DEFAULT '0',
  `gsis_enrollment_copies` int(11) DEFAULT '0',
  `bir_form_2316_copies` int(11) DEFAULT '0',
  `bir_form_2305_copies` int(11) DEFAULT '0',
  `bir_form_1905_copies` int(11) DEFAULT '0',
  `philhealth_copies` int(11) DEFAULT '0',
  `hdmf_copies` int(11) DEFAULT '0',
  `saln_copies` int(11) DEFAULT '0',
  `assumption_copies` int(11) DEFAULT '0',
  `position_description_copies` int(11) DEFAULT '0',
  `land_bank_copies` int(11) DEFAULT '0',
  `original_copies` int(11) DEFAULT '0',
  `passport_copies` int(11) DEFAULT '0',
  `bday_copies` int(11) DEFAULT '0',
  `clearance_copies` int(11) DEFAULT '0',
  `clearance_status` int(11) DEFAULT '0',
  `service_record_copies` int(11) DEFAULT '0',
  `service_record_status` int(11) DEFAULT '0',
  `disbursement_voucher_copies` int(11) DEFAULT '0',
  `disbursement_voucher_status` int(11) DEFAULT '0',
  `certificate_allowance_copies` int(11) DEFAULT '0',
  `certificate_allowance_status` int(11) DEFAULT '0',
  `cert_leave_balances_copies` int(11) DEFAULT '0',
  `cert_leave_balances_status` int(11) DEFAULT '0',
  `id_picture_copies` int(11) DEFAULT '0',
  `id_picture_status` int(11) DEFAULT '0',
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `rar_path` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_requirements: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
INSERT INTO `appointment_requirements` (`id`, `applicant_id`, `pds_status`, `medical_status`, `oath_status`, `nbi_status`, `gsis_enrollment_status`, `bir_form_2316_status`, `bir_form_2305_status`, `bir_form_1905_status`, `philhealth_status`, `hdmf_status`, `saln_status`, `assumption_status`, `position_description_status`, `land_bank_status`, `original_status`, `passport_status`, `bday_status`, `pds_copies`, `medical_copies`, `oath_copies`, `nbi_copies`, `gsis_enrollment_copies`, `bir_form_2316_copies`, `bir_form_2305_copies`, `bir_form_1905_copies`, `philhealth_copies`, `hdmf_copies`, `saln_copies`, `assumption_copies`, `position_description_copies`, `land_bank_copies`, `original_copies`, `passport_copies`, `bday_copies`, `clearance_copies`, `clearance_status`, `service_record_copies`, `service_record_status`, `disbursement_voucher_copies`, `disbursement_voucher_status`, `certificate_allowance_copies`, `certificate_allowance_status`, `cert_leave_balances_copies`, `cert_leave_balances_status`, `id_picture_copies`, `id_picture_status`, `tin_number`, `pagibig_number`, `rar_path`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, 12, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2019-11-29 02:10:12', '2019-11-29 02:24:12', NULL);
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table hris.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.assumptions: ~1 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 'Juan May', 'Dela Cruz', '2019-11-29', 1, 1, '2019-11-29 02:37:28', '2019-11-29 02:37:55', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table hris.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` date DEFAULT NULL,
  `attendance_to` date DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `ongoing` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.educations: ~12 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` (`id`, `applicant_id`, `school_name`, `course`, `attendance_from`, `attendance_to`, `level`, `graduated`, `awards`, `educ_level`, `ongoing`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(18, 14, 'STI Reco', 'BS Information Technology', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, '2019-07-12 21:19:55', '2019-07-12 21:19:55', NULL),
	(19, 14, 'UN University', 'Master in Public Administration', NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, NULL, '2019-07-12 21:19:55', '2019-07-13 19:18:19', NULL),
	(20, 15, 'Primary 1', '0', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-07-13 20:31:49', '2019-07-13 20:31:49', NULL),
	(21, 15, 'Secondary 1', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, NULL, '2019-07-13 20:31:49', '2019-07-13 20:31:49', NULL),
	(22, 15, 'FEU', 'BS Administrator', '2019-07-11', '2019-07-13', NULL, NULL, NULL, 4, 0, NULL, NULL, '2019-07-13 20:31:49', '2019-07-13 20:31:49', NULL),
	(23, 16, 'Pedro Guevarra', '0', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2019-07-13 22:48:18', '2019-07-13 22:48:18', NULL),
	(24, 16, 'Manila High School', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, NULL, '2019-07-13 22:48:18', '2019-07-13 22:48:18', NULL),
	(25, 16, 'UDM', 'BSIT', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, '2019-07-13 22:48:18', '2019-07-13 22:48:18', NULL),
	(26, 16, 'FEU', 'Master BSIT', NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, NULL, '2019-07-13 22:48:18', '2019-07-13 22:48:18', NULL),
	(27, 17, 'Elem School', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52', NULL),
	(28, 17, 'Manila High', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52', NULL),
	(29, 17, 'Universidad de Manila', 'BS Education', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52', NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table hris.eligibilities
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `eligibility_ref` varchar(225) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.eligibilities: ~3 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
INSERT INTO `eligibilities` (`id`, `applicant_id`, `eligibility_ref`, `rating`, `exam_place`, `license_number`, `license_validity`, `exam_date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(11, 14, 'CIVIL 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-12 21:19:55', '2019-07-12 21:19:55'),
	(12, 15, 'Civil 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-13 20:31:49', '2019-07-13 20:31:49'),
	(13, 16, 'Civil 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-13 22:48:18', '2019-07-13 22:48:18'),
	(14, 17, 'Eligibility I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52');
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table hris.erasure_alterations
DROP TABLE IF EXISTS `erasure_alterations`;
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.erasure_alterations: ~0 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
INSERT INTO `erasure_alterations` (`id`, `applicant_id`, `from_date`, `to_date`, `particulars`, `appointing_officer`, `sign_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, '2019-11-29', '2019-11-28', 'Sample 1', 'Officer 1', '2019-11-29', 1, 1, '2019-11-29 03:51:29', '2019-11-29 03:54:23', NULL);
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table hris.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` decimal(10,2) unsigned DEFAULT NULL,
  `performance_divide` decimal(10,2) unsigned DEFAULT NULL,
  `performance_average` decimal(10,2) unsigned DEFAULT NULL,
  `performance_percent` decimal(10,2) unsigned DEFAULT NULL,
  `performance_score` decimal(10,2) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` decimal(10,2) unsigned DEFAULT NULL,
  `minimum_training_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_points` decimal(10,2) unsigned DEFAULT NULL,
  `training_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_total_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_percent` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_score` decimal(10,2) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` decimal(10,2) unsigned DEFAULT NULL,
  `additional_points` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_score` decimal(10,2) unsigned DEFAULT NULL,
  `potential` decimal(10,2) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` decimal(10,2) unsigned DEFAULT NULL,
  `potential_percent` decimal(10,2) unsigned DEFAULT NULL,
  `potential_score` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_average_rating` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_percent` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_score` decimal(10,2) unsigned DEFAULT NULL,
  `total_percent` decimal(10,2) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.evaluations: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
INSERT INTO `evaluations` (`id`, `applicant_id`, `job_id`, `performance`, `performance_divide`, `performance_average`, `performance_percent`, `performance_score`, `eligibility`, `training`, `seminar`, `education`, `minimum_education_points`, `minimum_training_points`, `education_points`, `training_points`, `education_training_total_points`, `education_training_percent`, `education_training_score`, `relevant_positions_held`, `minimum_experience_requirement`, `additional_points`, `experience_accomplishments_total_points`, `experience_accomplishments_percent`, `experience_accomplishments_score`, `potential`, `potential_average_rating`, `potential_percentage_rating`, `potential_percent`, `potential_score`, `psychosocial`, `psychosocial_average_rating`, `psychosocial_percentage_rating`, `psychosocial_percent`, `psychosocial_score`, `total_percent`, `total_score`, `evaluated_by`, `reviewed_by`, `noted_by`, `recommended`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 1, 20.00, 40.00, 0.50, 40.00, 0.20, NULL, NULL, NULL, NULL, 10.00, 10.00, 30.00, 40.00, 90.00, 20.00, 18.00, NULL, 10.00, 20.00, 30.00, 20.00, 6.00, 40.00, 30.00, 20.00, 10.00, 2.00, 30.00, 40.00, 50.00, 10.00, 5.00, 100.00, 31.20, NULL, NULL, NULL, NULL, 1, NULL, '2019-11-28 06:28:13', '2019-11-28 06:28:13', NULL);
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table hris.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.examinations: ~0 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table hris.interviews
DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.interviews: ~0 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `applicant_id`, `job_id`, `interview_date`, `interview_time`, `interview_location`, `resched_interview_date`, `resched_interview_time`, `interview_status`, `notify`, `noftiy_resched_interview`, `confirmed`, `psb_chairperson`, `psb_secretariat`, `psb_member`, `psm_sweap_rep`, `psb_end_user`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(1, 14, 1, '2019-12-06', '07:00', 'Manila', NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-11-28 04:11:11', NULL, '2019-11-28 04:11:11');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table hris.interview_evaluation_ratings
DROP TABLE IF EXISTS `interview_evaluation_ratings`;
CREATE TABLE IF NOT EXISTS `interview_evaluation_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `rated_by` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `attachment_path` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(225) DEFAULT NULL,
  `rating_one` varchar(225) DEFAULT NULL,
  `rating_two` varchar(225) DEFAULT NULL,
  `rating_three` varchar(225) DEFAULT NULL,
  `rating_four` varchar(225) DEFAULT NULL,
  `rating_five` varchar(225) DEFAULT NULL,
  `rating_six` varchar(225) DEFAULT NULL,
  `rating_seven` varchar(225) DEFAULT NULL,
  `rating_eight` varchar(225) DEFAULT NULL,
  `rating_nine` varchar(225) DEFAULT NULL,
  `rating_ten` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table hris.interview_evaluation_ratings: ~0 rows (approximately)
/*!40000 ALTER TABLE `interview_evaluation_ratings` DISABLE KEYS */;
INSERT INTO `interview_evaluation_ratings` (`id`, `applicant_id`, `rated_by`, `interview_date`, `status`, `attachment_path`, `image_path`, `rating_one`, `rating_two`, `rating_three`, `rating_four`, `rating_five`, `rating_six`, `rating_seven`, `rating_eight`, `rating_nine`, `rating_ten`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(1, 14, 116, NULL, 1, 'd4205378969e6edd465cfd98a4529fb6.docx', NULL, '1', '2', '3', 'Select', 'Select', '4', 'Select', 'Select', 'Select', '4', 1, 1, '2019-11-28 05:03:47', NULL, '2019-11-28 05:55:54');
/*!40000 ALTER TABLE `interview_evaluation_ratings` ENABLE KEYS */;

-- Dumping structure for table hris.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `psipop_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `representation_amount` decimal(12,3) DEFAULT '0.000',
  `transportation_amount` decimal(12,3) DEFAULT '0.000',
  `eme_amount` decimal(12,3) DEFAULT '0.000',
  `communication_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `csc_education` text COLLATE utf8mb4_unicode_ci,
  `csc_experience` text COLLATE utf8mb4_unicode_ci,
  `csc_training` text COLLATE utf8mb4_unicode_ci,
  `csc_eligibility` text COLLATE utf8mb4_unicode_ci,
  `approved_date` date DEFAULT NULL,
  `other_specify` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appointer_id` int(11) DEFAULT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `posted_from` date DEFAULT NULL,
  `posted_to` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.jobs: ~1 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `psipop_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `representation_amount`, `transportation_amount`, `eme_amount`, `communication_amount`, `status`, `station`, `reporting_line`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `csc_education`, `csc_experience`, `csc_training`, `csc_eligibility`, `approved_date`, `other_specify`, `appointer_id`, `expires`, `deadline_date`, `publish_date`, `posted_from`, `posted_to`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 1, NULL, '<p>4 years College Graduate&nbsp;&nbsp;&nbsp;&nbsp;</p>', '2 years experience in the related field', 'NA', 'Civil Service Passer', NULL, NULL, 20000.000, 0.000, 24000.000, 6000.000, 20000.000, 20000.000, 5000.000, 5000.000, 5000.000, 2000.000, 2000.000, NULL, NULL, NULL, NULL, 'Sample&nbsp;<span style="color: rgb(102, 102, 102); text-align: right; white-space: nowrap;">General Functions</span>', 'Sample&nbsp;<span style="color: rgb(102, 102, 102); text-align: right; white-space: nowrap;">General&nbsp;</span><span style="color: rgb(102, 102, 102); text-align: right; white-space: nowrap;">Specific Duties &amp;</span><br style="color: rgb(102, 102, 102); text-align: right; white-space: nowrap;"><span style="color: rgb(102, 102, 102); text-align: right; white-space: nowrap;">Responsibilities</span>', NULL, NULL, NULL, '4 years College Graduate&nbsp; &nbsp;&nbsp;', '2 years experience in the related field', 'Not Applicable', 'Civil Service Passer', '2019-11-26', NULL, 3, NULL, '2019-12-28', '2019-11-26', '2019-11-26', '2019-12-25', 1, 1, 0, 0, 0, '2019-11-26 15:06:13', '2019-11-26 15:20:04', 1, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table hris.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `joboffer_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.job_offers: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
INSERT INTO `job_offers` (`id`, `applicant_id`, `pera_amount`, `clothing_allowance`, `year_end_bonus`, `cash_gift`, `executive_director`, `job_offer_date`, `joboffer_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, NULL, NULL, NULL, NULL, 'Juan Dela Cruz', '2019-11-28', 1, 1, NULL, '2019-11-28 06:47:23', '2019-11-28 06:47:23', NULL);
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table hris.matrix_qualifications
DROP TABLE IF EXISTS `matrix_qualifications`;
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `ips_rating` varchar(225) DEFAULT NULL,
  `total_hours` varchar(225) DEFAULT NULL,
  `semester` varchar(225) DEFAULT NULL,
  `matrix_year` varchar(225) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.matrix_qualifications: ~1 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
INSERT INTO `matrix_qualifications` (`id`, `applicant_id`, `job_id`, `remarks`, `training_remarks`, `ips_rating`, `total_hours`, `semester`, `matrix_year`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 1, 'Sample Remarks', NULL, '120', '48', '1st Semester', '2018', 1, 1, 1, '2019-11-26 20:16:02', '2019-11-28 01:44:23', NULL);
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table hris.oath_offices
DROP TABLE IF EXISTS `oath_offices`;
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.oath_offices: ~0 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
INSERT INTO `oath_offices` (`id`, `applicant_id`, `person_administering`, `oath_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 'Juan Dela Cruz', '2019-11-06', 1, NULL, '2019-11-29 03:38:52', '2019-11-29 03:38:52', NULL);
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table hris.psipop
DROP TABLE IF EXISTS `psipop`;
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `employment_status_id` int(11) DEFAULT NULL,
  `type_of_personnel` varchar(225) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `job_grade` int(11) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `basic_salary` decimal(10,2) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `area_code` varchar(225) DEFAULT NULL,
  `authorized` varchar(225) DEFAULT NULL,
  `actual` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `ppa_atribution` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.psipop: ~2 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
INSERT INTO `psipop` (`id`, `applicant_id`, `employment_status_id`, `type_of_personnel`, `office_id`, `position_id`, `division_id`, `department_id`, `job_grade`, `step`, `basic_salary`, `position_title`, `item_number`, `area_code`, `authorized`, `actual`, `level`, `ppa_atribution`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, NULL, 1, '1', 21, 270, 18, 0, 3, 2, 20000.00, NULL, 'UJDSAD766', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, '2019-11-26 08:34:34', '2019-11-26 08:56:17', NULL),
	(2, NULL, 1, '1', 21, 237, 11, 0, 4, 5, 20000.00, NULL, 'UJDSAD766HH', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, '2019-11-26 08:57:49', '2019-11-26 09:00:28', NULL);
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table hris.resignation_acceptance
DROP TABLE IF EXISTS `resignation_acceptance`;
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.resignation_acceptance: ~0 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
INSERT INTO `resignation_acceptance` (`id`, `applicant_id`, `letter_date`, `resignation_date`, `appointing_officer`, `sign_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, '2019-11-29', '2019-11-27', 'Officer 1', '2019-11-28', 1, NULL, '2019-11-29 03:59:21', '2019-11-29 03:59:21', NULL);
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table hris.rms_modules
DROP TABLE IF EXISTS `rms_modules`;
CREATE TABLE IF NOT EXISTS `rms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.rms_modules: ~24 rows (approximately)
/*!40000 ALTER TABLE `rms_modules` DISABLE KEYS */;
INSERT INTO `rms_modules` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'psipop', 'PSIPOP', NULL, NULL, NULL, NULL, NULL),
	(2, 'jobs', 'Jobs', NULL, NULL, NULL, NULL, NULL),
	(4, 'applicant', 'Applicant', NULL, NULL, NULL, NULL, NULL),
	(5, 'matrix-qualification', 'Matrix of Qualification', NULL, NULL, NULL, NULL, NULL),
	(6, 'interviews', 'Interviews', NULL, NULL, NULL, NULL, NULL),
	(7, 'evaluation', 'Evaluation', NULL, NULL, NULL, NULL, NULL),
	(8, 'comparative-ranking', 'Comparative Ranking', NULL, NULL, NULL, NULL, NULL),
	(9, 'recommendation', 'Recommendation', NULL, NULL, NULL, NULL, NULL),
	(10, 'joboffer', 'Job Offer', NULL, NULL, NULL, NULL, NULL),
	(11, 'appointment-form', 'Form', NULL, NULL, NULL, NULL, NULL),
	(12, 'appointment-issued', 'Transmital', NULL, NULL, NULL, NULL, NULL),
	(13, 'appointment-processing', 'Processing Checklist', NULL, NULL, NULL, NULL, NULL),
	(14, 'appointment-requirements', 'Pre Employment Requirements', NULL, NULL, NULL, NULL, NULL),
	(15, 'assumption', 'Assumption to Duty', NULL, NULL, NULL, NULL, NULL),
	(16, 'attestation', 'Transmital for Attestation', NULL, NULL, NULL, NULL, NULL),
	(17, 'appointment-casual', 'Plantilla of Casual Appointment', NULL, NULL, NULL, NULL, NULL),
	(18, 'position-descriptions', 'Position Description', NULL, NULL, NULL, NULL, NULL),
	(19, 'oath-office', 'Oath of Office', NULL, NULL, NULL, NULL, NULL),
	(20, 'erasure_alterations', 'Erasure and Alteration', NULL, NULL, NULL, NULL, NULL),
	(21, 'acceptance_resignation', 'Acceptance of Resignation', NULL, NULL, NULL, NULL, NULL),
	(22, 'boarding_applicant', 'Applicant Onboarding', NULL, NULL, NULL, NULL, NULL),
	(23, 'report', 'Report', NULL, NULL, NULL, NULL, NULL),
	(24, 'users', 'User', NULL, NULL, NULL, NULL, NULL),
	(25, 'access_types', 'Access Types', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `rms_modules` ENABLE KEYS */;

-- Dumping structure for table hris.selection_lineup
DROP TABLE IF EXISTS `selection_lineup`;
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  `job_id` int(10) unsigned DEFAULT NULL,
  `er_representative_selected` int(11) DEFAULT '0',
  `chairperson_selected` int(11) DEFAULT '0',
  `hrdd_selected` int(11) DEFAULT '0',
  `hrmo_selected` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.selection_lineup: ~0 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
INSERT INTO `selection_lineup` (`id`, `applicant_id`, `job_id`, `er_representative_selected`, `chairperson_selected`, `hrdd_selected`, `hrmo_selected`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 14, 1, 1, 1, 0, 0, 0, 1, NULL, '2019-11-28 06:28:31', '2019-11-28 06:28:31', NULL);
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table hris.trainings
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.trainings: ~6 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` (`id`, `applicant_id`, `title_learning_programs`, `inclusive_date_from`, `inclusive_date_to`, `number_hours`, `ld_type`, `sponsored_by`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(8, 14, 'Training 1', NULL, NULL, '20', NULL, NULL, NULL, NULL, '2019-07-12 21:19:55', '2019-07-12 21:19:55', NULL),
	(9, 15, 'Training 1', NULL, NULL, '48', NULL, NULL, NULL, NULL, '2019-07-13 20:31:49', '2019-07-13 20:31:49', NULL),
	(10, 16, 'Training 1', NULL, NULL, '32', NULL, NULL, NULL, NULL, '2019-07-13 22:48:18', '2019-10-10 06:43:54', NULL),
	(11, 17, 'Training 1', NULL, NULL, '48', NULL, NULL, NULL, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52', NULL),
	(12, 16, 'Training 2', NULL, NULL, '23', NULL, NULL, NULL, NULL, '2019-10-10 06:43:54', '2019-10-10 06:43:54', NULL),
	(13, 16, 'Training 3', NULL, NULL, '33', NULL, NULL, NULL, NULL, '2019-10-10 06:43:54', '2019-10-10 06:43:54', NULL);
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table hris.workexperiences
DROP TABLE IF EXISTS `workexperiences`;
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` date DEFAULT NULL,
  `inclusive_date_to` date DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` int(11) NOT NULL DEFAULT '0',
  `present_work` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.workexperiences: ~5 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
INSERT INTO `workexperiences` (`id`, `applicant_id`, `inclusive_date_from`, `inclusive_date_to`, `position_title`, `department`, `monthly_salary`, `salary_grade`, `status_of_appointment`, `govt_service`, `present_work`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(8, 14, '2018-01-01', NULL, 'Administrative 1', 'EA Sport', NULL, NULL, NULL, 1, 1, NULL, NULL, '2019-07-12 21:19:55', '2019-07-13 17:27:46', NULL),
	(9, 14, '2017-08-11', '2018-01-05', 'Admin 1', NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, '2019-07-12 21:19:55', '2019-07-12 23:49:10', NULL),
	(10, 14, '2016-03-02', '2016-08-11', 'LSB', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, '2019-07-13 00:29:29', '2019-07-13 00:29:56', NULL),
	(13, 14, '2015-08-06', '2016-08-25', 'Service Crew', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, '2019-07-13 16:56:31', '2019-07-13 16:56:31', NULL),
	(14, 15, '2018-02-06', NULL, 'Administrator 1', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2019-07-13 20:31:49', '2019-07-13 20:31:49', NULL),
	(15, 16, '2017-07-19', NULL, 'Officer 1', 'PHCC', NULL, NULL, NULL, 1, 1, NULL, NULL, '2019-07-13 22:48:18', '2019-07-13 22:48:18', NULL),
	(16, 17, '2018-02-07', '2019-09-03', 'Admin I', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, '2019-09-03 05:30:52', '2019-09-03 05:30:52', NULL);
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
