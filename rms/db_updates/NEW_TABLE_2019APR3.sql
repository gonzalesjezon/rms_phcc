-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table rms_pcw.access_modules
DROP TABLE IF EXISTS `access_modules`;
CREATE TABLE IF NOT EXISTS `access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_pcw.access_modules: ~1 rows (approximately)
/*!40000 ALTER TABLE `access_modules` DISABLE KEYS */;
INSERT INTO `access_modules` (`id`, `access_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 'HR', 1, 1, '2019-04-03 10:14:21', '2019-04-03 10:14:29');
/*!40000 ALTER TABLE `access_modules` ENABLE KEYS */;

-- Dumping structure for table rms_pcw.access_rights
DROP TABLE IF EXISTS `access_rights`;
CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_pcw.access_rights: ~4 rows (approximately)
/*!40000 ALTER TABLE `access_rights` DISABLE KEYS */;
INSERT INTO `access_rights` (`id`, `access_module_id`, `module_id`, `to_view`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 1, '2019-04-03 10:14:22', '2019-04-03 10:15:48'),
	(2, 1, 16, 1, NULL, 1, '2019-04-03 10:14:22', '2019-04-03 10:14:22'),
	(3, 1, 24, 1, NULL, 1, '2019-04-03 10:14:22', '2019-04-03 10:14:22'),
	(4, 1, 21, 1, NULL, 1, '2019-04-03 10:15:58', '2019-04-03 10:15:58');
/*!40000 ALTER TABLE `access_rights` ENABLE KEYS */;

-- Dumping structure for table rms_pcw.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_type_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_pcw.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `access_type_id`, `name`, `username`, `email`, `password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Admin', 'admin', NULL, '$2y$10$ppwBzj/fTdeTXaHswTk4TeggukgifBdCTn7CPNN.omlJ6zgI8Gogi', 'Mbx763xmRvhhUKolFE4AVAB4fYFSqfenqTzNvoRPWtvGhdvpAgjAyUxQhP9g', NULL, 1, '2019-01-26 07:32:58', '2019-04-03 10:22:28');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
