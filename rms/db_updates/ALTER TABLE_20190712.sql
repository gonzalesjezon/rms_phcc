ALTER TABLE `workexperiences`
	CHANGE COLUMN `inclusive_date_from` `inclusive_date_from` DATE NULL DEFAULT NULL AFTER `applicant_id`,
	CHANGE COLUMN `inclusive_date_to` `inclusive_date_to` DATE NULL DEFAULT NULL AFTER `inclusive_date_from`,
	CHANGE COLUMN `govt_service` `govt_service` INT NOT NULL DEFAULT '0' AFTER `status_of_appointment`;

ALTER TABLE `workexperiences`
	CHANGE COLUMN `present_day` `present_work` INT(11) NOT NULL DEFAULT '0' AFTER `govt_service`;

ALTER TABLE `matrix_qualifications`
	ADD COLUMN `training_remarks` VARCHAR(225) NULL DEFAULT NULL AFTER `remarks`;