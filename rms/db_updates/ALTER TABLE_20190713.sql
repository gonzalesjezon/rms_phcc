ALTER TABLE `educations`
	ADD COLUMN `ongoing` INT(11) NULL DEFAULT '0' AFTER `educ_level`;

ALTER TABLE `educations`
	CHANGE COLUMN `attendance_from` `attendance_from` DATE NULL DEFAULT NULL AFTER `course`,
	CHANGE COLUMN `attendance_to` `attendance_to` DATE NULL DEFAULT NULL AFTER `attendance_from`;