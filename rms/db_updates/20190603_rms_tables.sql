-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table hris.access_modules
DROP TABLE IF EXISTS `access_modules`;
CREATE TABLE IF NOT EXISTS `access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.access_modules: ~2 rows (approximately)
/*!40000 ALTER TABLE `access_modules` DISABLE KEYS */;
INSERT INTO `access_modules` (`id`, `access_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 'HR', 1, 2, '2019-04-03 10:14:21', '2019-04-04 00:01:43'),
	(2, 'Admin', 1, 1, '2019-04-04 00:45:17', '2019-04-04 00:46:45');
/*!40000 ALTER TABLE `access_modules` ENABLE KEYS */;

-- Dumping structure for table hris.access_rights
DROP TABLE IF EXISTS `access_rights`;
CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.access_rights: ~5 rows (approximately)
/*!40000 ALTER TABLE `access_rights` DISABLE KEYS */;
INSERT INTO `access_rights` (`id`, `access_module_id`, `module_id`, `to_view`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 2, '2019-04-03 10:14:22', '2019-04-04 00:01:44'),
	(2, 1, 16, 1, NULL, 2, '2019-04-03 10:14:22', '2019-04-04 00:01:44'),
	(3, 1, 24, 1, NULL, 2, '2019-04-03 10:14:22', '2019-04-04 00:01:44'),
	(4, 1, 2, 1, NULL, 2, '2019-04-03 10:15:58', '2019-04-04 00:01:44'),
	(5, 1, 4, 1, NULL, 2, '2019-04-03 22:14:06', '2019-04-04 00:01:44');
/*!40000 ALTER TABLE `access_rights` ENABLE KEYS */;

-- Dumping structure for table hris.access_types
DROP TABLE IF EXISTS `access_types`;
CREATE TABLE IF NOT EXISTS `access_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.access_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_types` ENABLE KEYS */;

-- Dumping structure for table hris.applicants
DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `qualified` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.applicants: ~1 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `employee_number`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `active`, `qualified`, `remarks`, `gwa`, `zip_code`, `permanent_zip_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 147, '5cf4a60a3f47b', NULL, 'Bing', NULL, 'Go', NULL, NULL, 'go@gmail.com', '345678', NULL, NULL, 'csc_bulletin', '2010-07-07', 'Manil', 'male', 'single', NULL, 0, 0, '120', '40', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', 'Ma', 'Sub', '389', 'Manila', 'Manila', NULL, '23', 'Ma', 'Sub', '389', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, '1018', NULL, 1, NULL, '2019-06-03 04:46:02', '2019-06-03 04:46:47', NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table hris.appointments
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form34b_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form212_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form1_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form32_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form4_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.appointments: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` (`id`, `applicant_id`, `form33_hrmo`, `form34b_hrmo`, `form212_hrmo`, `eligibility_hrmo`, `form1_hrmo`, `form32_hrmo`, `form4_hrmo`, `form33_cscfo`, `form34b_cscfo`, `form212_cscfo`, `eligibility_cscfo`, `form1_cscfo`, `form32_cscfo`, `form4_cscfo`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-06-03 04:56:26', '2019-06-03 05:05:23', NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_casual
DROP TABLE IF EXISTS `appointment_casual`;
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_casual: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `employee_status_id` int(11) DEFAULT NULL,
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `vice` varchar(225) DEFAULT NULL,
  `who` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `hrmo_assessment_date` date DEFAULT NULL,
  `chairperson_deliberation_date` date DEFAULT NULL,
  `form_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_forms: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
INSERT INTO `appointment_forms` (`id`, `applicant_id`, `nature_of_appointment`, `employee_status_id`, `appointing_officer`, `hrmo`, `chairperson`, `vice`, `who`, `date_sign`, `publication_date_from`, `publication_date_to`, `hrmo_date_sign`, `chairperson_date_sign`, `period_emp_from`, `period_emp_to`, `date_issued`, `assessment_date`, `hrmo_assessment_date`, `chairperson_deliberation_date`, `form_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2019-06-03 05:10:44', '2019-06-03 05:39:44', NULL);
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_issued: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_processing
DROP TABLE IF EXISTS `appointment_processing`;
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_processing: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table hris.appointment_requirements
DROP TABLE IF EXISTS `appointment_requirements`;
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_status` int(11) DEFAULT '0',
  `medical_status` int(11) DEFAULT '0',
  `oath_status` int(11) DEFAULT '0',
  `nbi_status` int(11) DEFAULT '0',
  `gsis_enrollment_status` int(11) DEFAULT '0',
  `bir_form_2316_status` int(11) DEFAULT '0',
  `bir_form_2305_status` int(11) DEFAULT '0',
  `bir_form_1905_status` int(11) DEFAULT '0',
  `philhealth_status` int(11) DEFAULT '0',
  `hdmf_status` int(11) DEFAULT '0',
  `saln_status` int(11) DEFAULT '0',
  `assumption_status` int(11) DEFAULT '0',
  `position_description_status` int(11) DEFAULT '0',
  `land_bank_status` int(11) DEFAULT '0',
  `original_status` int(11) DEFAULT '0',
  `passport_status` int(11) DEFAULT '0',
  `bday_status` int(11) DEFAULT '0',
  `pds_copies` int(11) DEFAULT '0',
  `medical_copies` int(11) DEFAULT '0',
  `oath_copies` int(11) DEFAULT '0',
  `nbi_copies` int(11) DEFAULT '0',
  `gsis_enrollment_copies` int(11) DEFAULT '0',
  `bir_form_2316_copies` int(11) DEFAULT '0',
  `bir_form_2305_copies` int(11) DEFAULT '0',
  `bir_form_1905_copies` int(11) DEFAULT '0',
  `philhealth_copies` int(11) DEFAULT '0',
  `hdmf_copies` int(11) DEFAULT '0',
  `saln_copies` int(11) DEFAULT '0',
  `assumption_copies` int(11) DEFAULT '0',
  `position_description_copies` int(11) DEFAULT '0',
  `land_bank_copies` int(11) DEFAULT '0',
  `original_copies` int(11) DEFAULT '0',
  `passport_copies` int(11) DEFAULT '0',
  `bday_copies` int(11) DEFAULT '0',
  `clearance_copies` int(11) DEFAULT '0',
  `clearance_status` int(11) DEFAULT '0',
  `service_record_copies` int(11) DEFAULT '0',
  `service_record_status` int(11) DEFAULT '0',
  `disbursement_voucher_copies` int(11) DEFAULT '0',
  `disbursement_voucher_status` int(11) DEFAULT '0',
  `certificate_allowance_copies` int(11) DEFAULT '0',
  `certificate_allowance_status` int(11) DEFAULT '0',
  `cert_leave_balances_copies` int(11) DEFAULT '0',
  `cert_leave_balances_status` int(11) DEFAULT '0',
  `id_picture_copies` int(11) DEFAULT '0',
  `id_picture_status` int(11) DEFAULT '0',
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `rar_path` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.appointment_requirements: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
INSERT INTO `appointment_requirements` (`id`, `applicant_id`, `pds_status`, `medical_status`, `oath_status`, `nbi_status`, `gsis_enrollment_status`, `bir_form_2316_status`, `bir_form_2305_status`, `bir_form_1905_status`, `philhealth_status`, `hdmf_status`, `saln_status`, `assumption_status`, `position_description_status`, `land_bank_status`, `original_status`, `passport_status`, `bday_status`, `pds_copies`, `medical_copies`, `oath_copies`, `nbi_copies`, `gsis_enrollment_copies`, `bir_form_2316_copies`, `bir_form_2305_copies`, `bir_form_1905_copies`, `philhealth_copies`, `hdmf_copies`, `saln_copies`, `assumption_copies`, `position_description_copies`, `land_bank_copies`, `original_copies`, `passport_copies`, `bday_copies`, `clearance_copies`, `clearance_status`, `service_record_copies`, `service_record_status`, `disbursement_voucher_copies`, `disbursement_voucher_status`, `certificate_allowance_copies`, `certificate_allowance_status`, `cert_leave_balances_copies`, `cert_leave_balances_status`, `id_picture_copies`, `id_picture_status`, `tin_number`, `pagibig_number`, `rar_path`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 1, 1, '2019-06-03 05:21:08', '2019-06-03 05:39:44', NULL);
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table hris.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.assumptions: ~1 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, 1, 1, '2019-06-03 05:21:08', '2019-06-03 05:39:44', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table hris.attestations
DROP TABLE IF EXISTS `attestations`;
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.attestations: ~1 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
INSERT INTO `attestations` (`id`, `applicant_id`, `agency_receiving_offer`, `action_employee_status`, `period_from`, `period_to`, `date_action`, `date_release`, `date_issuance`, `publication_from`, `publication_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-06-03 05:21:08', '2019-06-03 05:39:44', NULL);
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table hris.boarding_applicants
DROP TABLE IF EXISTS `boarding_applicants`;
CREATE TABLE IF NOT EXISTS `boarding_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `board_status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.boarding_applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `boarding_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `boarding_applicants` ENABLE KEYS */;

-- Dumping structure for table hris.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` varchar(225) DEFAULT NULL,
  `attendance_to` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.educations: ~3 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` (`id`, `applicant_id`, `school_name`, `course`, `attendance_from`, `attendance_to`, `level`, `graduated`, `awards`, `educ_level`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Primary 1', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-06-03 04:46:02', '2019-06-03 04:46:02', NULL),
	(2, 1, 'Secondary 1', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2019-06-03 04:46:02', '2019-06-03 04:46:02', NULL),
	(3, 1, 'College 1', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, '2019-06-03 04:46:02', '2019-06-03 04:46:02', NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table hris.eligibilities
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `eligibility_ref` int(11) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.eligibilities: ~1 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
INSERT INTO `eligibilities` (`id`, `applicant_id`, `eligibility_ref`, `rating`, `exam_place`, `license_number`, `license_validity`, `exam_date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-03 04:46:02', '2019-06-03 04:46:02');
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table hris.erasure_alterations
DROP TABLE IF EXISTS `erasure_alterations`;
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.erasure_alterations: ~1 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
INSERT INTO `erasure_alterations` (`id`, `applicant_id`, `from_date`, `to_date`, `particulars`, `appointing_officer`, `sign_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '2019-06-04', '2019-06-07', NULL, 'Sam Milby', '2019-06-03', 1, NULL, '2019-06-03 07:00:36', '2019-06-03 07:00:36', NULL);
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table hris.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` decimal(10,2) unsigned DEFAULT NULL,
  `performance_divide` decimal(10,2) unsigned DEFAULT NULL,
  `performance_average` decimal(10,2) unsigned DEFAULT NULL,
  `performance_percent` decimal(10,2) unsigned DEFAULT NULL,
  `performance_score` decimal(10,2) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` decimal(10,2) unsigned DEFAULT NULL,
  `minimum_training_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_points` decimal(10,2) unsigned DEFAULT NULL,
  `training_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_total_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_percent` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_score` decimal(10,2) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` decimal(10,2) unsigned DEFAULT NULL,
  `additional_points` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_score` decimal(10,2) unsigned DEFAULT NULL,
  `potential` decimal(10,2) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` decimal(10,2) unsigned DEFAULT NULL,
  `potential_percent` decimal(10,2) unsigned DEFAULT NULL,
  `potential_score` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_average_rating` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_percent` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_score` decimal(10,2) unsigned DEFAULT NULL,
  `total_percent` decimal(10,2) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.evaluations: ~1 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
INSERT INTO `evaluations` (`id`, `applicant_id`, `job_id`, `performance`, `performance_divide`, `performance_average`, `performance_percent`, `performance_score`, `eligibility`, `training`, `seminar`, `education`, `minimum_education_points`, `minimum_training_points`, `education_points`, `training_points`, `education_training_total_points`, `education_training_percent`, `education_training_score`, `relevant_positions_held`, `minimum_experience_requirement`, `additional_points`, `experience_accomplishments_total_points`, `experience_accomplishments_percent`, `experience_accomplishments_score`, `potential`, `potential_average_rating`, `potential_percentage_rating`, `potential_percent`, `potential_score`, `psychosocial`, `psychosocial_average_rating`, `psychosocial_percentage_rating`, `psychosocial_percent`, `psychosocial_score`, `total_percent`, `total_score`, `evaluated_by`, `reviewed_by`, `noted_by`, `recommended`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 147, 30.00, 2.00, 15.00, 40.00, 6.00, NULL, NULL, NULL, NULL, 40.00, 40.00, 23.00, 10.00, 113.00, 20.00, 22.60, NULL, 50.00, 40.00, 90.00, 20.00, 18.00, 23.00, 33.00, 44.00, 10.00, 4.40, 23.00, 33.00, 22.00, 10.00, 2.20, 100.00, 53.20, NULL, NULL, NULL, NULL, 1, NULL, '2019-06-03 05:02:33', '2019-06-03 05:04:36', NULL);
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table hris.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.examinations: ~0 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table hris.interviews
DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.interviews: ~1 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `applicant_id`, `interview_date`, `interview_time`, `interview_location`, `resched_interview_date`, `resched_interview_time`, `interview_status`, `notify`, `noftiy_resched_interview`, `confirmed`, `psb_chairperson`, `psb_secretariat`, `psb_member`, `psm_sweap_rep`, `psb_end_user`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(1, 1, NULL, '10:50PM', '3232', NULL, NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-06-03 04:56:26', NULL, '2019-06-03 05:05:23');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table hris.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plantilla_item_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `rata_amount` decimal(12,3) DEFAULT '0.000',
  `eme_amount` decimal(12,3) DEFAULT '0.000',
  `communication_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.jobs: ~159 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `plantilla_item_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `rata_amount`, `eme_amount`, `communication_amount`, `status`, `station`, `reporting_line`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `expires`, `deadline_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 531468.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 354312.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(3, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(4, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(5, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(6, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(7, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(8, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(9, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(10, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(11, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(12, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:36', NULL, 0, NULL, NULL),
	(14, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(15, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86899.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(16, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(17, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86899.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(18, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(19, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(20, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(21, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(22, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(23, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(24, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(25, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(26, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122500.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(27, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(28, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(29, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(30, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(31, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(32, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(33, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:37', NULL, 0, NULL, NULL),
	(34, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(35, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(36, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(37, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(38, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(39, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(40, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(41, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(42, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(43, 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(44, 112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(45, 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(46, 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(47, 81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(48, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(49, 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(50, 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 354312.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(51, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(52, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(53, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(54, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(55, 89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:38', NULL, 0, NULL, NULL),
	(56, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(57, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(58, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(59, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(60, 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(61, 96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(62, 82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(63, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(64, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(65, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(66, 102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(67, 105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:39', NULL, 0, NULL, NULL),
	(68, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(69, 108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(70, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(71, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(72, 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(73, 114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86899.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(74, 115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(75, 116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(76, 76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(77, 119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(78, 120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:40', NULL, 0, NULL, NULL),
	(79, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:41', NULL, 0, NULL, NULL),
	(80, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:41', NULL, 0, NULL, NULL),
	(81, 124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:42', NULL, 0, NULL, NULL),
	(82, 82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:42', NULL, 0, NULL, NULL),
	(83, 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:42', NULL, 0, NULL, NULL),
	(84, 121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 55600.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:42', NULL, 0, NULL, NULL),
	(85, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:43', NULL, 0, NULL, NULL),
	(86, 131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:43', NULL, 0, NULL, NULL),
	(87, 132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:43', NULL, 0, NULL, NULL),
	(88, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:43', NULL, 0, NULL, NULL),
	(89, 134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(90, 136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(91, 91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(92, 126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(93, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(94, 138, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(95, 139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(96, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(97, 142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(98, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(99, 144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(100, 143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(101, 146, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(102, 102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(103, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 220200.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(104, 104, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(105, 105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:44', NULL, 0, NULL, NULL),
	(106, 76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(107, 107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(108, 108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(109, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(110, 110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(111, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(112, 112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(113, 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(114, 114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(115, 115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(116, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(117, 117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(118, 141, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122500.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:45', NULL, 0, NULL, NULL),
	(119, 119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(120, 73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(121, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 354312.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(122, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(123, 123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(124, 129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(125, 125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(126, 74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(127, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(128, 127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(129, 129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(130, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:46', NULL, 0, NULL, NULL),
	(131, 131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(132, 132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(133, 133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(134, 134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(135, 140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(136, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73900.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(137, 125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 122500.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(138, 75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(139, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(140, 140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(141, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40169.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(142, 142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86899.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(143, 143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(144, 144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(145, 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:47', NULL, 0, NULL, NULL),
	(146, 167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 59100.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:48', NULL, 0, NULL, NULL),
	(147, 169, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27414.000, 0.000, 2000.000, 6000.000, 0.000, 0.000, 5000.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-06-08', 1, 1, 0, 1, 0, '2019-06-03 11:09:48', '2019-06-03 03:10:55', 0, NULL, NULL),
	(148, 170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 69499.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:49', NULL, 0, NULL, NULL),
	(149, 171, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86899.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:49', NULL, 0, NULL, NULL),
	(150, 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:49', NULL, 0, NULL, NULL),
	(151, 181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22532.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:49', NULL, 0, NULL, NULL),
	(152, 182, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14087.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:49', NULL, 0, NULL, NULL),
	(153, 183, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17975.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:49', NULL, 0, NULL, NULL),
	(154, 187, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:50', NULL, 0, NULL, NULL),
	(155, 188, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:50', NULL, 0, NULL, NULL),
	(156, 189, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30351.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:50', NULL, 0, NULL, NULL),
	(157, 193, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:51', NULL, 0, NULL, NULL),
	(158, 195, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45269.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:51', NULL, 0, NULL, NULL),
	(159, 201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86899.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-06-03 11:09:51', NULL, 0, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table hris.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `joboffer_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.job_offers: ~1 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
INSERT INTO `job_offers` (`id`, `applicant_id`, `pera_amount`, `clothing_allowance`, `year_end_bonus`, `cash_gift`, `executive_director`, `job_offer_date`, `joboffer_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2019-06-03 05:06:37', '2019-06-03 05:10:44', NULL);
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table hris.matrix_qualifications
DROP TABLE IF EXISTS `matrix_qualifications`;
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `isc_chairperson` varchar(50) DEFAULT NULL,
  `isc_member_one` varchar(50) DEFAULT NULL,
  `isc_member_two` varchar(50) DEFAULT NULL,
  `ea_representative` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.matrix_qualifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
INSERT INTO `matrix_qualifications` (`id`, `applicant_id`, `remarks`, `status`, `isc_chairperson`, `isc_member_one`, `isc_member_two`, `ea_representative`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-31 07:54:16', '2019-05-31 07:54:16');
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table hris.oath_offices
DROP TABLE IF EXISTS `oath_offices`;
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.oath_offices: ~1 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
INSERT INTO `oath_offices` (`id`, `applicant_id`, `person_administering`, `oath_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, 1, 1, '2019-06-03 05:21:08', '2019-06-03 05:39:44', NULL);
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table hris.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table hris.positionitem
DROP TABLE IF EXISTS `positionitem`;
CREATE TABLE IF NOT EXISTS `positionitem` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `PositionRefId` bigint(50) DEFAULT NULL,
  `PositionLevelRefId` bigint(50) DEFAULT NULL,
  `PositionClassificationRefId` bigint(50) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT NULL,
  `StepIncrementRefId` int(10) DEFAULT NULL,
  `SalaryAmount` decimal(15,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  `OfficeRefId` bigint(50) DEFAULT NULL,
  `JobGradeRefId` int(10) DEFAULT NULL,
  `DivisionRefId` int(10) DEFAULT NULL,
  `applicant_id` int(10) DEFAULT NULL,
  `has_occupied` int(10) DEFAULT '0',
  `EducRequirements` text,
  `WorkExpRequirements` text,
  `TrainingRequirements` text,
  `EligibilityRequirements` text,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.positionitem: ~201 rows (approximately)
/*!40000 ALTER TABLE `positionitem` DISABLE KEYS */;
INSERT INTO `positionitem` (`RefId`, `Code`, `Name`, `PositionRefId`, `PositionLevelRefId`, `PositionClassificationRefId`, `SalaryGradeRefId`, `StepIncrementRefId`, `SalaryAmount`, `Remarks`, `LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`, `OfficeRefId`, `JobGradeRefId`, `DivisionRefId`, `applicant_id`, `has_occupied`, `EducRequirements`, `WorkExpRequirements`, `TrainingRequirements`, `EligibilityRequirements`) VALUES
	(1, 'PHCC-COCH-1-2016', 'PHCC-COCH-1-2016', 1, NULL, NULL, NULL, 1, 531468.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 19, 0, 1, 1, NULL, NULL, NULL, NULL),
	(2, 'PHCC-COM-1-2016', 'PHCC-COM-1-2016', 2, NULL, NULL, NULL, 1, 354312.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 18, 0, 2, 1, NULL, NULL, NULL, NULL),
	(3, 'PHCC-COM-2-2016', 'PHCC-COM-2-2016', 2, NULL, NULL, NULL, 1, 354312.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 18, 0, NULL, 0, NULL, NULL, NULL, NULL),
	(4, 'PHCC-COM-3-2016', 'PHCC-COM-3-2016', 2, NULL, NULL, NULL, 1, 354312.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 1, 18, 0, 199, 1, NULL, NULL, NULL, NULL),
	(5, 'PHCC-DRV2-2-2016', 'PHCC-DRV2-2-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 1, 0, NULL, 0, NULL, NULL, NULL, NULL),
	(6, 'PHCC-EXA4-1-2016', 'PHCC-EXA4-1-2016', 74, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 1, 11, 0, 105, 1, NULL, NULL, NULL, NULL),
	(7, 'PHCC-TRNSP4-1-2016', 'PHCC-TRNSP4-1-2016', 75, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 10, 11, 29, 41, 1, NULL, NULL, NULL, NULL),
	(8, 'PHCC-CFER1-1-2016', 'PHCC-CFER1-1-2016', 76, NULL, NULL, NULL, 1, 14936.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 2, 0, 9, 1, NULL, NULL, NULL, NULL),
	(9, 'PHCC-DIR4-1-2016', 'PHCC-DIR4-1-2016', 77, 0, 0, NULL, 1, 169300.00, 'auto', '2019-01-14', '15:00:02', 'wralmin.hr', 'E', 21, 16, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(10, 'PHCC-HEA-1-2016', 'PHCC-HEA-1-2016', 78, NULL, NULL, NULL, 1, 122500.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 1, 15, 0, 159, 1, NULL, NULL, NULL, NULL),
	(11, 'PHCC-DIR4-5-2016', 'PHCC-DIR4-5-2016', 77, NULL, NULL, NULL, 1, 169300.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 10, 16, 4, 18, 1, NULL, NULL, NULL, NULL),
	(12, 'PHCC-DIR4-2-2016', 'PHCC-DIR4-2-2016', 77, NULL, NULL, NULL, 1, 169300.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 20, 16, 4, 13, 1, NULL, NULL, NULL, NULL),
	(13, 'PHCC-EXA4-3-2016', 'PHCC-EXA4-3-2016', 74, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 1, 11, 0, 201, 1, NULL, NULL, NULL, NULL),
	(14, 'PHCC-PSEC2-3-2016', 'PHCC-PSEC2-3-2016', 79, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 1, 6, 0, 183, 1, NULL, NULL, NULL, NULL),
	(15, 'PHCC-DIR3-1-2016', 'PHCC-DIR3-1-2016', 80, NULL, NULL, NULL, 1, 122500.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 4, 15, 4, 16, 1, NULL, NULL, NULL, NULL),
	(16, 'PHCC-DIR4-4-2016', 'PHCC-DIR4-4-2016', 77, NULL, NULL, NULL, 1, 169300.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 5, 16, 4, 17, 1, NULL, NULL, NULL, NULL),
	(17, 'PHCC-EXA3-1-2016', 'PHCC-EXA3-1-2016', 81, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 8, 0, 19, 1, NULL, NULL, NULL, NULL),
	(18, 'PHCC-CMPRO5-1-2016', 'PHCC-CMPRO5-1-2016', 82, 0, 0, NULL, 1, 86899.00, 'auto', '2019-01-14', '15:34:15', 'wralmin.hr', 'E', 10, 13, NULL, 221, 1, NULL, NULL, NULL, NULL),
	(19, 'PHCC-PSEC2-1-2016', 'PHCC-PSEC2-1-2016', 79, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 1, 6, 0, 21, 1, NULL, NULL, NULL, NULL),
	(20, 'PHCC-ATY3-4-2016', 'PHCC-ATY3-4-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 4, 10, 31, 22, 1, NULL, NULL, NULL, NULL),
	(21, 'PHCC-DRV2-9-2016', 'PHCC-DRV2-9-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 5, 1, 4, 23, 1, NULL, NULL, NULL, NULL),
	(22, 'PHCC-CADOF-1-2016', 'PHCC-CADOF-1-2016', 279, NULL, NULL, NULL, 1, 86899.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 21, 13, 8, NULL, 0, NULL, NULL, NULL, NULL),
	(23, 'PHCC-RO3-1-2016', 'PHCC-RO3-1-2016', 85, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 21, 8, 8, 173, 1, NULL, NULL, NULL, NULL),
	(24, 'PHCC-ATY2-9-2016', 'PHCC-ATY2-9-2016', 86, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 5, 8, 37, NULL, 0, NULL, NULL, NULL, NULL),
	(25, 'PHCC-INVA3-1-2016', 'PHCC-INVA3-1-2016', 87, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 4, 11, 10, 27, 1, NULL, NULL, NULL, NULL),
	(26, 'PHCC-LEA2-9-2016', 'PHCC-LEA2-9-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 5, 5, 37, 148, 1, NULL, NULL, NULL, NULL),
	(27, 'PHCC-ADO3-2-2016', 'PHCC-ADO3-2-2016', 89, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 20, 8, 11, 29, 1, NULL, NULL, NULL, NULL),
	(28, 'PHCC-ATY3-7-2016', 'PHCC-ATY3-7-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 5, 10, 37, 110, 1, NULL, NULL, NULL, NULL),
	(29, 'PHCC-INVA3-2-2016', 'PHCC-INVA3-2-2016', 87, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 4, 11, 10, 31, 1, NULL, NULL, NULL, NULL),
	(30, 'PHCC-EXA3-3-2016', 'PHCC-EXA3-3-2016', 81, 0, 0, NULL, 1, 45269.00, 'auto', '2019-01-14', '15:36:42', 'wralmin.hr', 'E', 1, 8, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(31, 'PHCC-PLO3-1-2016', 'PHCC-PLO3-1-2016', 90, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 20, 8, 11, 33, 1, NULL, NULL, NULL, NULL),
	(32, 'PHCC-INFO4-1-2016', 'PHCC-INFO4-1-2016', 91, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 10, 11, 35, 34, 1, NULL, NULL, NULL, NULL),
	(33, 'PHCC-CMPRO4-1-2016', 'PHCC-CMPRO4-1-2016', 92, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 10, 11, 30, 35, 1, NULL, NULL, NULL, NULL),
	(34, 'PHCC-CADOF-2-2016', 'PHCC-CADOF-2-2016', 279, NULL, NULL, NULL, 1, 86899.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 21, 13, 36, NULL, 0, NULL, NULL, NULL, NULL),
	(35, 'PHCC-CMPRO3-5-2016', 'PHCC-CMPRO3-5-2016', 96, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 10, 9, 30, 43, 1, NULL, NULL, NULL, NULL),
	(36, 'PHCC-DIR4-3-2016', 'PHCC-DIR4-3-2016', 77, NULL, NULL, NULL, 1, 169300.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 4, 16, 4, 38, 1, NULL, NULL, NULL, NULL),
	(37, 'PHCC-LEA2-5-2016', 'PHCC-LEA2-5-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 4, 5, 31, 218, 1, NULL, NULL, NULL, NULL),
	(38, 'PHCC-ATY4-4-2016', 'PHCC-ATY4-4-2016', 94, NULL, NULL, NULL, 1, 73900.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 5, 12, 31, 94, 1, NULL, NULL, NULL, NULL),
	(39, 'PHCC-TRNSP3-2-2016', 'PHCC-TRNSP3-2-2016', 95, 0, 0, NULL, 1, 55600.00, 'auto', '2019-01-14', '15:37:32', 'wralmin.hr', 'E', 10, 9, NULL, 64, 1, NULL, NULL, NULL, NULL),
	(40, 'PHCC-INFO5-1-2016', 'PHCC-INFO5-1-2016', 280, NULL, NULL, NULL, 1, 86899.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 10, 13, 35, NULL, 0, NULL, NULL, NULL, NULL),
	(41, 'PHCC-ATY3-9-2016', 'PHCC-ATY3-9-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 5, 10, 31, 44, 1, NULL, NULL, NULL, NULL),
	(42, 'PHCC-SADOF-3-2016', 'PHCC-SADOF-3-2016', 281, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:35', 'Admin', 'E', 20, 11, 18, NULL, 0, NULL, NULL, NULL, NULL),
	(43, 'PHCC-ATY4-1-2016', 'PHCC-ATY4-1-2016', 94, 0, 0, NULL, 1, 73900.00, 'auto', '2019-01-14', '15:35:19', 'wralmin.hr', 'E', 21, 12, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(44, 'PHCC-INFO3-2-2016', 'PHCC-INFO3-2-2016', 98, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 10, 9, 35, 47, 1, NULL, NULL, NULL, NULL),
	(45, 'PHCC-DRV2-10-2016', 'PHCC-DRV2-10-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 10, 1, 4, 48, 1, NULL, NULL, NULL, NULL),
	(46, 'PHCC-ATY5-4-2016', 'PHCC-ATY5-4-2016', 99, NULL, NULL, NULL, 1, 112500.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 5, 14, 31, 62, 1, NULL, NULL, NULL, NULL),
	(47, 'PHCC-DRV2-7-2016', 'PHCC-DRV2-7-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 20, 1, 4, 50, 1, NULL, NULL, NULL, NULL),
	(48, 'PHCC-INVA5-1-2016', 'PHCC-INVA5-1-2016', 283, NULL, NULL, NULL, 1, 86899.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 4, 13, 10, NULL, 0, NULL, NULL, NULL, NULL),
	(49, 'PHCC-HRMO3-1-2016', 'PHCC-HRMO3-1-2016', 100, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 21, 8, 36, 52, 1, NULL, NULL, NULL, NULL),
	(50, 'PHCC-INFO2-2-2016', 'PHCC-INFO2-2-2016', 101, NULL, NULL, NULL, 1, 40169.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 10, 7, 35, 53, 1, NULL, NULL, NULL, NULL),
	(51, 'PHCC-HRMO2-1-2016', 'PHCC-HRMO2-1-2016', 284, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 21, 6, 36, NULL, 0, NULL, NULL, NULL, NULL),
	(52, 'PHCC-DRV2-8-2016', 'PHCC-DRV2-8-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 4, 1, 4, 55, 1, NULL, NULL, NULL, NULL),
	(53, 'PHCC-ATY5-1-2016', 'PHCC-ATY5-1-2016', 99, NULL, NULL, NULL, 1, 112500.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 21, 14, 34, 56, 1, NULL, NULL, NULL, NULL),
	(54, 'PHCC-CMPRO3-3-2016', 'PHCC-CMPRO3-3-2016', 96, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 10, 9, 30, 57, 1, NULL, NULL, NULL, NULL),
	(55, 'PHCC-INFO1-1-2016', 'PHCC-INFO1-1-2016', 103, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 10, 5, 35, 208, 1, NULL, NULL, NULL, NULL),
	(56, 'PHCC-ECO2-1-2016', 'PHCC-ECO2-1-2016', 104, NULL, NULL, NULL, 1, 40169.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 5, 7, 37, NULL, 0, NULL, NULL, NULL, NULL),
	(57, 'PHCC-ATY5-3-2016', 'PHCC-ATY5-3-2016', 99, NULL, NULL, NULL, 1, 112500.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 5, 14, 37, 60, 1, NULL, NULL, NULL, NULL),
	(58, 'PHCC-ECO3-1-2016', 'PHCC-ECO3-1-2016', 93, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 5, 9, 37, 61, 1, NULL, NULL, NULL, NULL),
	(59, 'PHCC-ATY4-3-2016', 'PHCC-ATY4-3-2016', 94, NULL, NULL, NULL, 1, 73900.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 5, 12, 37, 163, 1, NULL, NULL, NULL, NULL),
	(60, 'PHCC-SEC2-9-2016', 'PHCC-SEC2-9-2016', 105, NULL, NULL, NULL, 1, 17975.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 5, 3, 4, 153, 1, NULL, NULL, NULL, NULL),
	(61, 'PHCC-TRNSP2-2-2016', 'PHCC-TRNSP2-2-2016', 106, NULL, NULL, NULL, 1, 40169.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 10, 7, 29, NULL, 0, NULL, NULL, NULL, NULL),
	(62, 'PHCC-SADOF-2-2016', 'PHCC-SADOF-2-2016', 97, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 21, 11, 36, 65, 1, NULL, NULL, NULL, NULL),
	(63, 'PHCC-HRMO2-2-2016', 'PHCC-HRMO2-2-2016', 284, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 21, 6, 36, NULL, 0, NULL, NULL, NULL, NULL),
	(64, 'PHCC-A2-2-2016', 'PHCC-A2-2-2016', 107, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 20, 8, 21, 93, 1, NULL, NULL, NULL, NULL),
	(65, 'PHCC-INFO3-1-2016', 'PHCC-INFO3-1-2016', 98, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:36', 'Admin', 'E', 10, 9, 35, 68, 1, NULL, NULL, NULL, NULL),
	(66, 'PHCC-TRNSP2-1-2016', 'PHCC-TRNSP2-1-2016', 106, NULL, NULL, NULL, 1, 40169.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 10, 7, 29, 102, 1, NULL, NULL, NULL, NULL),
	(67, 'PHCC-PSEC2-2-2016', 'PHCC-PSEC2-2-2016', 79, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 1, 6, 0, 79, 1, NULL, NULL, NULL, NULL),
	(68, 'PHCC-A3-1-2016', 'PHCC-A3-1-2016', 108, 0, 0, NULL, 1, 55600.00, 'auto', '2019-01-14', '13:30:12', 'wralmin.hr', 'E', 12, 9, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(69, 'PHCC-BUDO3-2-2016', 'PHCC-BUDO3-2-2016', 109, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 20, 8, 18, 72, 1, NULL, NULL, NULL, NULL),
	(70, 'PHCC-CASH3-1-2016', 'PHCC-CASH3-1-2016', 110, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 21, 8, 8, 73, 1, NULL, NULL, NULL, NULL),
	(71, 'PHCC-ATY5-2-2016', 'PHCC-ATY5-2-2016', 99, NULL, NULL, NULL, 1, 112500.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 4, 14, 31, 74, 1, NULL, NULL, NULL, NULL),
	(72, 'PHCC-CMPRO1-1-2016', 'PHCC-CMPRO1-1-2016', 111, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 10, 5, 30, NULL, 0, NULL, NULL, NULL, NULL),
	(73, 'PHCC-INVA2-2-2016', 'PHCC-INVA2-2-2016', 112, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 4, 10, 10, 198, 1, NULL, NULL, NULL, NULL),
	(74, 'PHCC-SUO1-1-2016', 'PHCC-SUO1-1-2016', 113, NULL, NULL, NULL, 1, 22532.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 21, 4, 8, 207, 1, NULL, NULL, NULL, NULL),
	(75, 'PHCC-SEC2-5-2016', 'PHCC-SEC2-5-2016', 105, 0, 0, NULL, 1, 17975.00, 'auto', '2019-01-14', '15:36:03', 'wralmin.hr', 'E', 9, 3, NULL, 220, 1, NULL, NULL, NULL, NULL),
	(76, 'PHCC-CASH2-1-2016', 'PHCC-CASH2-1-2016', 114, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 21, 6, 8, 179, 1, NULL, NULL, NULL, NULL),
	(77, 'PHCC-INVA2-1-2016', 'PHCC-INVA2-1-2016', 112, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 4, 10, 10, NULL, 0, NULL, NULL, NULL, NULL),
	(78, 'PHCC-ADO2-1-2016', 'PHCC-ADO2-1-2016', 115, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 21, 6, 8, 82, 1, NULL, NULL, NULL, NULL),
	(79, 'PHCC-HRMO3-2-2016', 'PHCC-HRMO3-2-2016', 287, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 21, 8, 36, NULL, 0, NULL, NULL, NULL, NULL),
	(80, 'PHCC-ATY3-10-2016', 'PHCC-ATY3-10-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 5, 10, 31, NULL, 0, NULL, NULL, NULL, NULL),
	(81, 'PHCC-ECO3-2-2016', 'PHCC-ECO3-2-2016', 93, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 5, 9, 37, 85, 1, NULL, NULL, NULL, NULL),
	(82, 'PHCC-A2-1-2016', 'PHCC-A2-1-2016', 107, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 20, 8, 21, 104, 1, NULL, NULL, NULL, NULL),
	(83, 'PHCC-ITO1-1-2016', 'PHCC-ITO1-1-2016', 116, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 21, 8, 23, 88, 1, NULL, NULL, NULL, NULL),
	(84, 'PHCC-COM-4-2016', 'PHCC-COM-4-2016', 2, NULL, NULL, NULL, 1, 354312.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 1, 18, 0, 90, 1, NULL, NULL, NULL, NULL),
	(85, 'PHCC-EXED-1-2016', 'PHCC-EXED-1-2016', 117, NULL, NULL, NULL, 1, 220200.00, 'auto', '2018-10-31', '11:32:34', 'Admin', 'E', 9, 17, 0, 10, 1, NULL, NULL, NULL, NULL),
	(86, 'PHCC-LEA2-11-2016', 'PHCC-LEA2-11-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 5, 5, 31, 92, 1, NULL, NULL, NULL, NULL),
	(87, 'PHCC-ATY2-10-2016', 'PHCC-ATY2-10-2016', 86, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 5, 8, 37, 192, 1, NULL, NULL, NULL, NULL),
	(88, 'PHCC-INVA4-1-2016', 'PHCC-INVA4-1-2016', 118, NULL, NULL, NULL, 1, 73900.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 4, 12, 10, 95, 1, NULL, NULL, NULL, NULL),
	(89, 'PHCC-ITO3-1-2016', 'PHCC-ITO3-1-2016', 119, NULL, NULL, NULL, 1, 86899.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 21, 13, 23, 96, 1, NULL, NULL, NULL, NULL),
	(90, 'PHCC-PLO5-1-2016', 'PHCC-PLO5-1-2016', 120, NULL, NULL, NULL, 1, 86899.00, 'auto', '2018-10-31', '11:32:37', 'Admin', 'E', 20, 13, 11, 97, 1, NULL, NULL, NULL, NULL),
	(91, 'PHCC-BUDO2-1-2016', 'PHCC-BUDO2-1-2016', 121, 0, 0, NULL, 1, 30351.00, 'auto', '2019-01-14', '15:33:30', 'wralmin.hr', 'E', 12, 6, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(92, 'PHCC-PSEC2-4-2016', 'PHCC-PSEC2-4-2016', 79, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 1, 6, 0, NULL, 0, NULL, NULL, NULL, NULL),
	(93, 'PHCC-A3-2-2016', 'PHCC-A3-2-2016', 108, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 20, 9, 21, 100, 1, NULL, NULL, NULL, NULL),
	(94, 'PHCC-PLO4-1-2016', 'PHCC-PLO4-1-2016', 122, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 20, 11, 11, 101, 1, NULL, NULL, NULL, NULL),
	(95, 'PHCC-TRNSP1-1-2016', 'PHCC-TRNSP1-1-2016', 123, 0, 0, NULL, 1, 27414.00, 'auto', '2019-01-14', '15:38:01', 'wralmin.hr', 'E', 10, 5, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(96, 'PHCC-PSEC2-5-2016', 'PHCC-PSEC2-5-2016', 79, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 1, 6, 0, 103, 1, NULL, NULL, NULL, NULL),
	(97, 'PHCC-PLO2-1-2016', 'PHCC-PLO2-1-2016', 124, 0, 0, NULL, 1, 30351.00, 'auto', '2019-01-14', '15:33:04', 'wralmin.hr', 'E', 12, 6, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(98, 'PHCC-CMPRO2-2-2016', 'PHCC-CMPRO2-2-2016', 125, 0, 0, NULL, 1, 40169.00, 'auto', '2019-01-14', '15:32:40', 'wralmin.hr', 'E', 10, 7, NULL, 223, 1, NULL, NULL, NULL, NULL),
	(99, 'PHCC-ATY2-1-2016', 'PHCC-ATY2-1-2016', 86, 0, 0, NULL, 1, 45269.00, 'auto', '2019-01-14', '15:30:40', 'wralmin.hr', 'E', 21, 8, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(100, 'PHCC-SEC2-1-2016', 'PHCC-SEC2-1-2016', 105, 0, 0, NULL, 1, 17975.00, 'auto', '2019-01-14', '15:31:40', 'wralmin.hr', 'E', 1, 3, NULL, 211, 1, NULL, NULL, NULL, NULL),
	(101, 'PHCC-ATY3-1-2016', 'PHCC-ATY3-1-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 21, 10, 34, NULL, 0, NULL, NULL, NULL, NULL),
	(102, 'PHCC-CMPRO3-1-2016', 'PHCC-CMPRO3-1-2016', 96, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 10, 9, 30, 109, 1, NULL, NULL, NULL, NULL),
	(103, 'PHCC-ATY2-11-2016', 'PHCC-ATY2-11-2016', 86, 0, 0, NULL, 1, 45269.00, 'auto', '2019-01-14', '15:29:05', 'wralmin.hr', 'E', 5, 8, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(104, 'PHCC-ATY3-5-2016', 'PHCC-ATY3-5-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 4, 10, 31, NULL, 0, NULL, NULL, NULL, NULL),
	(105, 'PHCC-ITO1-2-2016', 'PHCC-ITO1-2-2016', 116, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 8, 23, 114, 1, NULL, NULL, NULL, NULL),
	(106, 'PHCC-ATY2-2-2016', 'PHCC-ATY2-2-2016', 86, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 8, 34, NULL, 0, NULL, NULL, NULL, NULL),
	(107, 'PHCC-ITO2-1-2016', 'PHCC-ITO2-1-2016', 126, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 11, 23, NULL, 0, NULL, NULL, NULL, NULL),
	(108, 'PHCC-CMPRO3-2-2016', 'PHCC-CMPRO3-2-2016', 96, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 10, 9, 30, 118, 1, NULL, NULL, NULL, NULL),
	(109, 'PHCC-BUDO1-1-2016', 'PHCC-BUDO1-1-2016', 127, 0, 0, NULL, 1, 22532.00, 'auto', '2019-01-14', '15:21:58', 'wralmin.hr', 'E', 12, 4, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(110, 'PHCC-EXA4-2-2016', 'PHCC-EXA4-2-2016', 74, 0, 0, NULL, 1, 69499.00, 'auto', '2019-01-14', '15:17:45', 'wralmin.hr', 'E', 1, 11, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(111, 'PHCC-SADOF-1-2016', 'PHCC-SADOF-1-2016', 97, NULL, NULL, NULL, 1, 69499.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 11, 8, 121, 1, NULL, NULL, NULL, NULL),
	(112, 'PHCC-CMPRO2-3-2016', 'PHCC-CMPRO2-3-2016', 125, 0, 0, NULL, 1, 40169.00, 'auto', '2019-01-14', '15:17:14', 'wralmin.hr', 'E', 10, 7, NULL, 75, 1, NULL, NULL, NULL, NULL),
	(113, 'PHCC-INFOSA2-1-2016', 'PHCC-INFOSA2-1-2016', 128, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 6, 23, 123, 1, NULL, NULL, NULL, NULL),
	(114, 'PHCC-ADO3-1-2016', 'PHCC-ADO3-1-2016', 89, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 8, 8, 125, 1, NULL, NULL, NULL, NULL),
	(115, 'PHCC-DRV2-6-2016', 'PHCC-DRV2-6-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 21, 1, 4, 127, 1, NULL, NULL, NULL, NULL),
	(116, 'PHCC-DRV2-1-2016', 'PHCC-DRV2-1-2016', 73, NULL, NULL, NULL, 1, 14087.00, 'auto', '2018-10-31', '11:32:38', 'Admin', 'E', 1, 1, 0, 128, 1, NULL, NULL, NULL, NULL),
	(117, 'PHCC-INFOSA1-1-2016', 'PHCC-INFOSA1-1-2016', 129, 0, 0, NULL, 1, 22532.00, 'auto', '2019-01-11', '15:24:27', 'wralmin.hr', 'E', 21, 4, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(118, 'PHCC-HRMO1-1-2016', 'PHCC-HRMO1-1-2016', 288, NULL, NULL, NULL, 1, 22532.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 21, 4, 36, NULL, 0, NULL, NULL, NULL, NULL),
	(119, 'PHCC-EXA3-5-2016', 'PHCC-EXA3-5-2016', 81, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 1, 8, 0, 132, 1, NULL, NULL, NULL, NULL),
	(120, 'PHCC-CASH1-1-2016', 'PHCC-CASH1-1-2016', 131, NULL, NULL, NULL, 1, 22532.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 21, 4, 8, 133, 1, NULL, NULL, NULL, NULL),
	(121, 'PHCC-EXA3-6-2016', 'PHCC-EXA3-6-2016', 81, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 9, 8, 0, 143, 1, NULL, NULL, NULL, NULL),
	(122, 'PHCC-ATY3-3-2016', 'PHCC-ATY3-3-2016', 83, NULL, NULL, NULL, 1, 59100.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 4, 10, 31, 135, 1, NULL, NULL, NULL, NULL),
	(123, 'PHCC-PLO1-1-2016', 'PHCC-PLO1-1-2016', 132, NULL, NULL, NULL, 1, 22532.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 20, 4, 11, NULL, 0, NULL, NULL, NULL, NULL),
	(124, 'PHCC-SUO3-1-2016', 'PHCC-SUO3-1-2016', 133, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 21, 8, 8, 138, 1, NULL, NULL, NULL, NULL),
	(125, 'PHCC-ATY1-1-2016', 'PHCC-ATY1-1-2016', 134, 0, 0, NULL, 1, 40169.00, 'auto', '2019-01-14', '15:13:27', 'wralmin.hr', 'E', 21, 7, NULL, 219, 1, NULL, NULL, NULL, NULL),
	(126, 'PHCC-PLO2-2-2016', 'PHCC-PLO2-2-2016', 124, 0, 0, NULL, 1, 30351.00, 'auto', '2019-01-14', '15:15:25', 'wralmin.hr', 'E', 12, 6, NULL, 158, 1, NULL, NULL, NULL, NULL),
	(127, 'PHCC-LEA2-1-2016', 'PHCC-LEA2-1-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 21, 5, 34, 209, 1, NULL, NULL, NULL, NULL),
	(128, 'PHCC-PLO3-2-2016', 'PHCC-PLO3-2-2016', 90, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 20, 8, 11, NULL, 0, NULL, NULL, NULL, NULL),
	(129, 'PHCC-EXA2-1-2016', 'PHCC-EXA2-1-2016', 135, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 9, 6, 0, 204, 1, NULL, NULL, NULL, NULL),
	(130, 'PHCC-INVA1-1-2016', 'PHCC-INVA1-1-2016', 136, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 4, 8, 10, NULL, 0, NULL, NULL, NULL, NULL),
	(131, 'PHCC-ATY2-5-2016', 'PHCC-ATY2-5-2016', 86, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 4, 8, 31, 150, 1, NULL, NULL, NULL, NULL),
	(132, 'PHCC-ATY2-12-2016', 'PHCC-ATY2-12-2016', 86, NULL, NULL, NULL, 1, 45269.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 5, 8, 31, 151, 1, NULL, NULL, NULL, NULL),
	(133, 'PHCC-SEC2-6-2016', 'PHCC-SEC2-6-2016', 105, 0, 0, NULL, 1, 17975.00, 'auto', '2019-01-14', '15:12:05', 'wralmin.hr', 'E', 21, 3, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(134, 'PHCC-DIR3-3-2016', 'PHCC-DIR3-3-2016', 80, NULL, NULL, NULL, 1, 122500.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 10, 15, 4, 154, 1, NULL, NULL, NULL, NULL),
	(135, 'PHCC-RO1-1-2016', 'PHCC-RO1-1-2016', 137, NULL, NULL, NULL, 1, 22532.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 21, 4, 8, NULL, 0, NULL, NULL, NULL, NULL),
	(136, 'PHCC-LEA2-2-2016', 'PHCC-LEA2-2-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 21, 5, 34, 156, 1, NULL, NULL, NULL, NULL),
	(137, 'PHCC-SEC2-3-2016', 'PHCC-SEC2-3-2016', 105, NULL, NULL, NULL, 1, 17975.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 1, 3, 0, NULL, 0, NULL, NULL, NULL, NULL),
	(138, 'PHCC-BS2-1-2016', 'PHCC-BS2-1-2016', 138, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 1, 6, 0, 160, 1, NULL, NULL, NULL, NULL),
	(139, 'PHCC-ECO2-2-2016', 'PHCC-ECO2-2-2016', 104, NULL, NULL, NULL, 1, 40169.00, 'auto', '2018-10-31', '11:32:39', 'Admin', 'E', 5, 7, 37, 161, 1, NULL, NULL, NULL, NULL),
	(140, 'PHCC-ADO1-1-2016', 'PHCC-ADO1-1-2016', 139, 0, 0, NULL, 1, 22532.00, 'auto', '2019-01-14', '15:09:26', 'wralmin.hr', 'E', 12, 4, NULL, 217, 1, NULL, NULL, NULL, NULL),
	(141, 'PHCC-ATY1-2-2016', 'PHCC-ATY1-2-2016', 134, NULL, NULL, NULL, 1, 40169.00, 'auto', '2018-10-31', '11:32:41', 'Admin', 'E', 21, 7, 34, 195, 1, NULL, NULL, NULL, NULL),
	(142, 'PHCC-TRNSP3-1-2016', 'PHCC-TRNSP3-1-2016', 95, NULL, NULL, NULL, 1, 55600.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 10, 9, 29, 165, 1, NULL, NULL, NULL, NULL),
	(143, 'PHCC-BS3-1-2016', 'PHCC-BS3-1-2016', 109, 0, 0, NULL, 1, 45269.00, 'auto', '2019-01-14', '14:58:56', 'wralmin.hr', 'E', 12, 8, NULL, 169, 1, NULL, NULL, NULL, NULL),
	(144, 'PHCC-LEA2-10-2016', 'PHCC-LEA2-10-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 5, 5, 37, 168, 1, NULL, NULL, NULL, NULL),
	(145, 'PHCC-LEA2-12-2016', 'PHCC-LEA2-12-2016', 88, NULL, NULL, NULL, 1, 27414.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 5, 5, 31, NULL, 0, NULL, NULL, NULL, NULL),
	(146, 'PHCC-ADO2-4-2016', 'PHCC-ADO2-4-2016', 115, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 20, 6, 11, 170, 1, NULL, NULL, NULL, NULL),
	(147, 'PHCC-ADO2-2-2016', 'PHCC-ADO2-2-2016', 115, NULL, NULL, NULL, 1, 30351.00, 'auto', '2018-10-31', '11:32:40', 'Admin', 'E', 21, 6, 8, 171, 1, NULL, NULL, NULL, NULL),
	(148, 'PHCC-LEA2-13-2016', 'PHCC-LEA2-13-2016', 88, 0, 0, NULL, 1, 30351.00, 'auto', '2019-01-14', '14:40:30', 'wralmin.hr', 'E', 21, 6, NULL, 214, 1, NULL, NULL, NULL, NULL),
	(149, 'PHCC-CMPRO2-1-2016', 'PHCC-CMPRO2-1-2016', 125, NULL, NULL, NULL, 1, 40169.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 10, 7, 30, 136, 1, NULL, NULL, NULL, NULL),
	(150, 'PHCC-LEA2-4-2016', 'PHCC-LEA2-4-2016', 88, NULL, NULL, NULL, 1, 27414.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 4, 5, 10, NULL, 0, NULL, NULL, NULL, NULL),
	(152, 'PHCC-LEA2-6-2016', 'PHCC-LEA2-6-2016', 88, NULL, NULL, NULL, 1, 27414.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 4, 5, 31, 193, 1, NULL, NULL, NULL, NULL),
	(153, 'PHCC-LEA2-7-2016', 'PHCC-LEA2-7-2016', 88, NULL, NULL, NULL, 1, 27414.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 4, 5, 31, 175, 1, NULL, NULL, NULL, NULL),
	(154, 'PHCC-LEA2-8-2016', 'PHCC-LEA2-8-2016', 88, NULL, NULL, NULL, 1, 27414.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 4, 5, 31, 182, 1, NULL, NULL, NULL, NULL),
	(155, 'PHCC-LEA2-14-2016', 'PHCC-LEA2-14-2016', 88, 0, 0, NULL, 1, 27414.00, NULL, '2019-01-14', '14:38:51', 'wralmin.hr', 'E', 5, 5, 31, 206, 1, NULL, NULL, NULL, NULL),
	(156, 'PHCC-LEA2-3-2016', 'PHCC-LEA2-3-2016', 88, NULL, NULL, NULL, 1, 27414.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 4, 5, 10, 189, 1, NULL, NULL, NULL, NULL),
	(157, 'PHCC-ATY2-3-2016', 'PHCC-ATY2-3-2016', 86, NULL, NULL, NULL, 1, 45269.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 21, 8, 34, 184, 1, NULL, NULL, NULL, NULL),
	(158, 'PHCC-ATY2-4-2016', 'PHCC-ATY2-4-2016', 86, NULL, NULL, NULL, 1, 45269.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 21, 8, 34, 185, 1, NULL, NULL, NULL, NULL),
	(159, 'PHCC-ATY2-6-2016', 'PHCC-ATY2-6-2016', 86, NULL, NULL, NULL, 1, 45269.00, NULL, '2018-10-31', '11:32:41', 'Admin', 'E', 4, 8, 31, 191, 1, NULL, NULL, NULL, NULL),
	(160, 'PHCC-ATY2-7-2016', 'PHCC-ATY2-7-2016', 86, 0, 0, NULL, 1, 45269.00, NULL, '2019-01-14', '14:38:35', 'wralmin.hr', 'E', 21, 8, NULL, 210, 1, NULL, NULL, NULL, NULL),
	(161, 'PHCC-ATY2-8-2016', 'PHCC-ATY2-8-2016', 86, 0, 0, NULL, 1, 45269.00, NULL, '2019-01-14', '14:33:23', 'wralmin.hr', 'E', 21, 8, NULL, 216, 1, NULL, NULL, NULL, NULL),
	(162, 'PHCC-ATY2-14-2016', 'PHCC-ATY2-14-2016', 86, NULL, NULL, NULL, 1, 45269.00, NULL, '2018-10-31', '11:32:41', 'Admin', 'E', 5, 8, 31, 190, 1, NULL, NULL, NULL, NULL),
	(163, 'PHCC-ATY3-2-2016', 'PHCC-ATY3-2-2016', 83, NULL, NULL, NULL, 1, 59100.00, NULL, '2018-10-31', '11:32:38', 'Admin', 'E', 21, 10, 34, 106, 1, NULL, NULL, NULL, NULL),
	(164, 'PHCC-ATY3-6-2016', 'PHCC-ATY3-6-2016', 83, NULL, NULL, NULL, 1, 59100.00, NULL, '2018-10-31', '11:32:39', 'Admin', 'E', 4, 10, 31, 115, 1, NULL, NULL, NULL, NULL),
	(165, 'PHCC-ATY3-8-2016', 'PHCC-ATY3-8-2016', 83, 0, 0, NULL, 1, 59100.00, NULL, '2019-01-14', '14:24:58', 'wralmin.hr', 'E', 5, 10, NULL, 197, 1, NULL, NULL, NULL, NULL),
	(166, 'PHCC-ATY3-11-2016', 'PHCC-ATY3-11-2016', 83, NULL, NULL, NULL, 1, 59100.00, NULL, '2018-10-31', '11:32:35', 'Admin', 'E', 5, 10, 31, 26, 1, NULL, NULL, NULL, NULL),
	(167, 'PHCC-ATY3-12-2016', 'PHCC-ATY3-12-2016', 83, NULL, NULL, NULL, 1, 59100.00, NULL, '2018-10-31', '11:32:37', 'Admin', 'E', 5, 10, 31, NULL, 0, NULL, NULL, NULL, NULL),
	(168, 'PHCC-ATY4-2-2016', 'PHCC-ATY4-2-2016', 94, 0, 0, NULL, 1, 73900.00, NULL, '2019-01-14', '14:23:53', 'wralmin.hr', 'E', 21, 12, NULL, 177, 1, NULL, NULL, NULL, NULL),
	(169, 'PHCC-A1-1-2016', 'PHCC-A1-1-2016', 272, 0, 0, NULL, 1, 27414.00, NULL, '2018-12-21', '12:08:48', 'wralmin.hr', 'E', 12, 5, 21, NULL, 0, NULL, NULL, NULL, NULL),
	(170, 'PHCC-A4-1-2016', 'PHCC-A4-1-2016', 285, NULL, NULL, NULL, 1, 69499.00, NULL, '2018-10-31', '11:32:37', 'Admin', 'E', 20, 11, 21, NULL, 0, NULL, NULL, NULL, NULL),
	(171, 'PHCC-CACT-1-2016', 'PHCC-CACT-1-2016', 320, 0, 0, NULL, 1, 86899.00, NULL, '2019-01-14', '14:20:30', 'wralmin.hr', 'E', 12, 13, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(172, 'PHCC-EXA4-4-2016', 'PHCC-EXA4-4-2016', 74, NULL, NULL, NULL, 1, 69499.00, NULL, '2018-10-31', '11:32:36', 'Admin', 'E', 1, 11, 0, 70, 1, NULL, NULL, NULL, NULL),
	(173, 'PHCC-EXA4-5-2016', 'PHCC-EXA4-5-2016', 74, NULL, NULL, NULL, 1, 69499.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 1, 11, 0, 166, 1, NULL, NULL, NULL, NULL),
	(174, 'PHCC-BS2-2-2016', 'PHCC-BS2-2-2016', 138, NULL, NULL, NULL, 1, 30351.00, NULL, '2018-10-31', '11:32:38', 'Admin', 'E', 1, 6, 0, 107, 1, NULL, NULL, NULL, NULL),
	(175, 'PHCC-EXA3-2-2016', 'PHCC-EXA3-2-2016', 81, 0, 0, NULL, 1, 45269.00, NULL, '2019-01-14', '13:49:18', 'wralmin.hr', 'E', 1, 8, NULL, 140, 1, NULL, NULL, NULL, NULL),
	(176, 'PHCC-EXA3-4-2016', 'PHCC-EXA3-4-2016', 81, 0, 0, NULL, 1, 45269.00, NULL, '2019-01-14', '13:48:28', 'wralmin.hr', 'E', 1, 8, NULL, 222, 1, NULL, NULL, NULL, NULL),
	(177, 'PHCC-SEC2-2-2016', 'PHCC-SEC2-2-2016', 105, NULL, NULL, NULL, 1, 17975.00, NULL, '2018-10-31', '11:32:41', 'Admin', 'E', 1, 3, 0, NULL, 0, NULL, NULL, NULL, NULL),
	(178, 'PHCC-SEC2-4-2016', 'PHCC-SEC2-4-2016', 105, 0, 0, NULL, 1, 17975.00, NULL, '2019-01-14', '13:47:09', 'wralmin.hr', 'E', 1, 3, NULL, 157, 1, NULL, NULL, NULL, NULL),
	(179, 'PHCC-DRV2-3-2016', 'PHCC-DRV2-3-2016', 73, NULL, NULL, NULL, 1, 14087.00, NULL, '2018-10-31', '11:32:41', 'Admin', 'E', 1, 1, 0, 203, 1, NULL, NULL, NULL, NULL),
	(180, 'PHCC-DRV2-4-2016', 'PHCC-DRV2-4-2016', 73, 0, 0, NULL, 1, 14087.00, NULL, '2019-01-14', '13:46:16', 'wralmin.hr', 'E', 1, 1, NULL, 212, 1, NULL, NULL, NULL, NULL),
	(181, 'PHCC-PSEC1-1-2016', 'PHCC-PSEC1-1-2016', 289, NULL, NULL, NULL, 1, 22532.00, NULL, '2018-10-31', '11:32:39', 'Admin', 'E', 9, 4, 0, NULL, 0, NULL, NULL, NULL, NULL),
	(182, 'PHCC-DRV2-5-2016', 'PHCC-DRV2-5-2016', 73, 0, 0, NULL, 1, 14087.00, NULL, '2019-01-14', '13:44:11', 'wralmin.hr', 'E', 9, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(183, 'PHCC-SEC2-7-2016', 'PHCC-SEC2-7-2016', 105, 0, 0, NULL, 1, 17975.00, NULL, '2019-01-14', '13:42:51', 'wralmin.hr', 'E', 12, 3, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(184, 'PHCC-SEC2-8-2016', 'PHCC-SEC2-8-2016', 105, 0, 0, NULL, 1, 17975.00, NULL, '2019-01-14', '13:41:46', 'wralmin.hr', 'E', 4, 3, NULL, 227, 1, NULL, NULL, NULL, NULL),
	(185, 'PHCC-SEC2-10-2016', 'PHCC-SEC2-10-2016', 105, NULL, NULL, NULL, 1, 17975.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 10, 3, 4, 187, 1, NULL, NULL, NULL, NULL),
	(186, 'PHCC-CADOF-3-2016', 'PHCC-CADOF-3-2016', 84, 0, 0, NULL, 1, 86899.00, NULL, '2019-01-14', '13:40:05', 'wralmin.hr', 'E', 12, 13, NULL, 224, 1, NULL, NULL, NULL, NULL),
	(187, 'PHCC-ADO2-3-2016', 'PHCC-ADO2-3-2016', 115, 0, 0, NULL, 1, 30351.00, NULL, '2019-01-14', '13:33:11', 'wralmin.hr', 'E', 21, 6, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(188, 'PHCC-RO2-1-2016', 'PHCC-RO2-1-2016', 319, 0, 0, NULL, 1, 30351.00, NULL, '2019-01-14', '13:38:47', 'wralmin.hr', 'E', 21, 6, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(189, 'PHCC-SUO2-1-2016', 'PHCC-SUO2-1-2016', 286, NULL, NULL, NULL, 1, 30351.00, NULL, '2018-10-31', '11:32:37', 'Admin', 'E', 21, 6, 8, NULL, 0, NULL, NULL, NULL, NULL),
	(190, 'PHCC-INFOSA2-2-2016', 'PHCC-INFOSA2-2-2016', 128, NULL, NULL, NULL, 1, 30351.00, NULL, '2018-10-31', '11:32:39', 'Admin', 'E', 21, 6, 23, 129, 1, NULL, NULL, NULL, NULL),
	(191, 'PHCC-ATY2-13-2016', 'PHCC-ATY2-13-2016', 86, NULL, NULL, NULL, 1, 45269.00, NULL, '2018-10-31', '11:32:40', 'Admin', 'E', 5, 8, 31, 180, 1, NULL, NULL, NULL, NULL),
	(192, 'PHCC-BUDO2-2-2016', 'PHCC-BUDO2-2-2016', 121, NULL, NULL, NULL, 1, 30351.00, NULL, '2018-10-31', '11:32:38', 'Admin', 'E', 20, 6, 18, 119, 1, NULL, NULL, NULL, NULL),
	(193, 'PHCC-BUDO3-2-2016', 'PHCC-BUDO3-2-2016', 109, 0, 0, NULL, 1, 45269.00, NULL, '2019-01-14', '13:34:15', 'wralmin.hr', 'E', 12, 8, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(194, 'PHCC-DIR3-2-2016', 'PHCC-DIR3-2-2016', 80, NULL, NULL, NULL, 1, 122500.00, NULL, '2018-10-31', '11:32:36', 'Admin', 'E', 5, 15, 4, 49, 1, NULL, NULL, NULL, NULL),
	(195, 'PHCC-INVA1-2-2016', 'PHCC-INVA1-2-2016', 136, 0, 0, NULL, 1, 45269.00, NULL, '2019-01-14', '13:36:14', 'wralmin.hr', 'E', 4, 8, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(196, 'PHCC-CMPRO3-4-2016', 'PHCC-CMPRO3-4-2016', 96, NULL, NULL, NULL, 1, 55600.00, NULL, '2018-10-31', '11:32:37', 'Admin', 'E', 10, 9, 30, 87, 1, NULL, NULL, NULL, NULL),
	(197, 'PHCC-CMPRO2-4-2016', 'PHCC-CMPRO2-4-2016', 125, 0, 0, NULL, 1, 40169.00, NULL, '2019-01-14', '13:28:49', 'wralmin.hr', 'E', 10, 7, NULL, 141, 1, NULL, NULL, NULL, NULL),
	(198, 'PHCC-CMPRO1-2-2016', 'PHCC-CMPRO1-2-2016', 111, 0, 0, NULL, 1, 27414.00, NULL, '2019-01-14', '13:28:19', 'wralmin.hr', 'E', 10, 5, NULL, 226, 1, NULL, NULL, NULL, NULL),
	(199, 'PHCC-CMPRO1-3-2016', 'PHCC-CMPRO1-3-2016', 111, 0, 0, NULL, 1, 27414.00, NULL, '2019-01-14', '13:27:25', 'wralmin.hr', 'E', 10, 5, NULL, 215, 1, NULL, NULL, NULL, NULL),
	(200, 'PHCC-INFO2-1-2016', 'PHCC-INFO2-1-2016', 101, NULL, NULL, NULL, 1, 40169.00, NULL, '2018-10-31', '11:32:36', 'Admin', 'E', 10, 7, 35, 58, 1, NULL, NULL, NULL, NULL),
	(201, 'PHCC-TRNSP5-1-2016', 'PHCC-TRNSP5-1-2016', 282, NULL, NULL, NULL, 1, 86899.00, NULL, '2018-10-31', '11:32:35', 'Admin', 'E', 10, 13, 29, NULL, 0, NULL, NULL, NULL, NULL),
	(202, 'PHCC-BUDO3-1-2016', 'PHCC-BUDO3-1-2016', 109, 0, 0, NULL, 1, 45269.00, NULL, '2018-12-21', '12:10:18', 'wralmin.hr', 'E', 12, 8, 18, 98, 1, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `positionitem` ENABLE KEYS */;

-- Dumping structure for table hris.position_descriptions
DROP TABLE IF EXISTS `position_descriptions`;
CREATE TABLE IF NOT EXISTS `position_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `immediate_supervisor` varchar(225) DEFAULT NULL,
  `higher_supervisor` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `used_tools` text,
  `managerial` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `non_supervisor` int(11) DEFAULT NULL,
  `staff` int(11) DEFAULT NULL,
  `general_public` int(11) DEFAULT NULL,
  `other_agency` varchar(50) DEFAULT NULL,
  `other_contacts` varchar(50) DEFAULT NULL,
  `office_work` int(11) DEFAULT NULL,
  `field_work` int(11) DEFAULT NULL,
  `other_condition` varchar(50) DEFAULT NULL,
  `description_function_unit` text,
  `description_function_position` text,
  `compentency_1` text,
  `compentency_2` text,
  `percentage_work_time` varchar(50) DEFAULT NULL,
  `responsibilities` varchar(50) DEFAULT NULL,
  `supervisor_name` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.position_descriptions: ~1 rows (approximately)
/*!40000 ALTER TABLE `position_descriptions` DISABLE KEYS */;
INSERT INTO `position_descriptions` (`id`, `applicant_id`, `immediate_supervisor`, `higher_supervisor`, `item_number`, `position_title`, `used_tools`, `managerial`, `supervisor`, `non_supervisor`, `staff`, `general_public`, `other_agency`, `other_contacts`, `office_work`, `field_work`, `other_condition`, `description_function_unit`, `description_function_position`, `compentency_1`, `compentency_2`, `percentage_work_time`, `responsibilities`, `supervisor_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-06-03 06:56:37', '2019-06-03 06:56:37', NULL);
/*!40000 ALTER TABLE `position_descriptions` ENABLE KEYS */;

-- Dumping structure for table hris.preliminary_evaluation
DROP TABLE IF EXISTS `preliminary_evaluation`;
CREATE TABLE IF NOT EXISTS `preliminary_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isc_chairperson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_one` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_two` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ea_representative` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.preliminary_evaluation: ~0 rows (approximately)
/*!40000 ALTER TABLE `preliminary_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `preliminary_evaluation` ENABLE KEYS */;

-- Dumping structure for table hris.psipop
DROP TABLE IF EXISTS `psipop`;
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `plantilla_item_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `salary_grade` varchar(50) DEFAULT NULL,
  `step` varchar(50) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `code` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `type` varchar(225) DEFAULT NULL,
  `annual_authorized_salary` varchar(50) DEFAULT NULL,
  `annual_actual_salary` decimal(10,2) DEFAULT NULL,
  `ppa_attribution` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.psipop: ~0 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table hris.recommendations
DROP TABLE IF EXISTS `recommendations`;
CREATE TABLE IF NOT EXISTS `recommendations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `sign_one` varchar(225) DEFAULT NULL,
  `sign_two` varchar(225) DEFAULT NULL,
  `sign_three` varchar(225) DEFAULT NULL,
  `sign_four` varchar(225) DEFAULT NULL,
  `sign_five` varchar(225) DEFAULT NULL,
  `prepared_by` varchar(225) DEFAULT NULL,
  `recommend_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.recommendations: ~0 rows (approximately)
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;

-- Dumping structure for table hris.resignation_acceptance
DROP TABLE IF EXISTS `resignation_acceptance`;
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.resignation_acceptance: ~1 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
INSERT INTO `resignation_acceptance` (`id`, `applicant_id`, `letter_date`, `resignation_date`, `appointing_officer`, `sign_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, '2019-06-03', '2019-06-05', 'Sam Milby', '2019-06-03', 1, NULL, '2019-06-03 07:03:17', '2019-06-03 07:03:17', NULL);
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table hris.rms_modules
DROP TABLE IF EXISTS `rms_modules`;
CREATE TABLE IF NOT EXISTS `rms_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.rms_modules: ~24 rows (approximately)
/*!40000 ALTER TABLE `rms_modules` DISABLE KEYS */;
INSERT INTO `rms_modules` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'psipop', 'PSIPOP', NULL, NULL, NULL, NULL, NULL),
	(2, 'jobs', 'Jobs', NULL, NULL, NULL, NULL, NULL),
	(4, 'applicant', 'Applicant', NULL, NULL, NULL, NULL, NULL),
	(5, 'matrix-qualification', 'Matrix of Qualification', NULL, NULL, NULL, NULL, NULL),
	(6, 'interviews', 'Interviews', NULL, NULL, NULL, NULL, NULL),
	(7, 'evaluation', 'Evaluation', NULL, NULL, NULL, NULL, NULL),
	(8, 'comparative-ranking', 'Comparative Ranking', NULL, NULL, NULL, NULL, NULL),
	(9, 'recommendation', 'Recommendation', NULL, NULL, NULL, NULL, NULL),
	(10, 'joboffer', 'Job Offer', NULL, NULL, NULL, NULL, NULL),
	(11, 'appointment-form', 'Form', NULL, NULL, NULL, NULL, NULL),
	(12, 'appointment-issued', 'Transmital', NULL, NULL, NULL, NULL, NULL),
	(13, 'appointment-processing', 'Processing Checklist', NULL, NULL, NULL, NULL, NULL),
	(14, 'appointment-requirements', 'Pre Employment Requirements', NULL, NULL, NULL, NULL, NULL),
	(15, 'assumption', 'Assumption to Duty', NULL, NULL, NULL, NULL, NULL),
	(16, 'attestation', 'Transmital for Attestation', NULL, NULL, NULL, NULL, NULL),
	(17, 'appointment-casual', 'Plantilla of Casual Appointment', NULL, NULL, NULL, NULL, NULL),
	(18, 'position-descriptions', 'Position Description', NULL, NULL, NULL, NULL, NULL),
	(19, 'oath-office', 'Oath of Office', NULL, NULL, NULL, NULL, NULL),
	(20, 'erasure_alterations', 'Erasure and Alteration', NULL, NULL, NULL, NULL, NULL),
	(21, 'acceptance_resignation', 'Acceptance of Resignation', NULL, NULL, NULL, NULL, NULL),
	(22, 'boarding_applicant', 'Applicant Onboarding', NULL, NULL, NULL, NULL, NULL),
	(23, 'report', 'Report', NULL, NULL, NULL, NULL, NULL),
	(24, 'users', 'User', NULL, NULL, NULL, NULL, NULL),
	(25, 'access_types', 'Access Types', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `rms_modules` ENABLE KEYS */;

-- Dumping structure for table hris.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.sections: ~0 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table hris.selection_lineup
DROP TABLE IF EXISTS `selection_lineup`;
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  `job_id` int(10) unsigned DEFAULT NULL,
  `er_representative_selected` int(11) DEFAULT '0',
  `chairperson_selected` int(11) DEFAULT '0',
  `hrdd_selected` int(11) DEFAULT '0',
  `hrmo_selected` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hris.selection_lineup: ~1 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
INSERT INTO `selection_lineup` (`id`, `applicant_id`, `job_id`, `er_representative_selected`, `chairperson_selected`, `hrdd_selected`, `hrmo_selected`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, 147, 1, 1, 0, 1, 0, 1, NULL, '2019-06-03 05:06:30', '2019-06-03 05:06:37', NULL);
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table hris.trainings
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table hris.trainings: ~1 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` (`id`, `applicant_id`, `title_learning_programs`, `inclusive_date_from`, `inclusive_date_to`, `number_hours`, `ld_type`, `sponsored_by`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Training 1', NULL, NULL, '48', NULL, NULL, NULL, NULL, '2019-05-31 07:52:20', '2019-05-31 07:52:20', NULL),
	(2, 1, 'Training 1', NULL, NULL, '23', NULL, NULL, NULL, NULL, '2019-06-03 04:46:02', '2019-06-03 04:46:02', NULL);
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table hris.workexperiences
DROP TABLE IF EXISTS `workexperiences`;
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` varchar(50) DEFAULT NULL,
  `inclusive_date_to` varchar(50) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table hris.workexperiences: ~0 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

ALTER TABLE `jobs`
  ADD COLUMN `employee_status_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `plantilla_item_id`;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
