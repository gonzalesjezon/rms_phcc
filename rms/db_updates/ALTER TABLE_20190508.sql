ALTER TABLE `jobs`
	ADD COLUMN `rata_amount` DECIMAL(12,3) NULL DEFAULT '0.000' AFTER `cashgift_amount`,
	ADD COLUMN `eme_amount` DECIMAL(12,3) NULL DEFAULT '0.000' AFTER `rata_amount`,
	ADD COLUMN `communication_amount` DECIMAL(12,3) NULL DEFAULT '0.000' AFTER `eme_amount`;