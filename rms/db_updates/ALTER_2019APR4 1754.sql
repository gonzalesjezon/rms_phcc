ALTER TABLE `recommendations`
	ADD COLUMN `recommend_status` INT(11) NOT NULL DEFAULT '0' AFTER `prepared_by`;

ALTER TABLE `job_offers`
	ADD COLUMN `joboffer_status` INT(11) NOT NULL DEFAULT '0' AFTER `job_offer_date`;