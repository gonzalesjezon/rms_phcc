ALTER TABLE `jobs`
	ADD COLUMN `approved_date` DATE NULL DEFAULT NULL AFTER `compentency_5`,
	ADD COLUMN `other_specify` VARCHAR(225) NULL DEFAULT NULL AFTER `approved_date`,
	ADD COLUMN `appointer_id` INT NULL DEFAULT NULL AFTER `other_specify`;