-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for rms_phcc
DROP DATABASE IF EXISTS `rms_phcc`;
CREATE DATABASE IF NOT EXISTS `rms_phcc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rms_phcc`;

-- Dumping structure for table rms_phcc.access_modules
CREATE TABLE IF NOT EXISTS `access_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.access_modules: ~2 rows (approximately)
/*!40000 ALTER TABLE `access_modules` DISABLE KEYS */;
INSERT INTO `access_modules` (`id`, `access_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 'HR', 1, 2, '2019-04-03 10:14:21', '2019-04-04 00:01:43'),
	(2, 'Admin', 1, 1, '2019-04-04 00:45:17', '2019-04-04 00:46:45');
/*!40000 ALTER TABLE `access_modules` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.access_rights
CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_module_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `to_view` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.access_rights: ~5 rows (approximately)
/*!40000 ALTER TABLE `access_rights` DISABLE KEYS */;
INSERT INTO `access_rights` (`id`, `access_module_id`, `module_id`, `to_view`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 1, 1, 2, '2019-04-03 10:14:22', '2019-04-04 00:01:44'),
	(2, 1, 16, 1, NULL, 2, '2019-04-03 10:14:22', '2019-04-04 00:01:44'),
	(3, 1, 24, 1, NULL, 2, '2019-04-03 10:14:22', '2019-04-04 00:01:44'),
	(4, 1, 2, 1, NULL, 2, '2019-04-03 10:15:58', '2019-04-04 00:01:44'),
	(5, 1, 4, 1, NULL, 2, '2019-04-03 22:14:06', '2019-04-04 00:01:44');
/*!40000 ALTER TABLE `access_rights` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.applicants
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `qualified` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.applicants: ~4 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `active`, `qualified`, `remarks`, `gwa`, `zip_code`, `permanent_zip_code`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 195, '5cd28f08d419a', 'Lito', NULL, 'Lapid', NULL, NULL, 'lapid@gmail.com', '23456789', NULL, NULL, 'agency', '2019-05-08', 'Manila', 'male', 'single', NULL, 0, 0, '120', '50', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', 'St', 'Manila', 'Manila', 'Manila', 'Manila', NULL, '23', 'St', 'Manila', 'Manila', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, 1, 1, '2019-05-08 08:10:48', '2019-05-09 04:05:18', NULL),
	(2, 103, '5cd3998de8684', 'JV', NULL, 'Ejercito', NULL, NULL, 'jv@gmail.com', '34567890', NULL, NULL, 'csc_bulletin', '1969-12-22', 'Manila', 'male', 'single', NULL, 0, 0, '120', '17', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', 'St', 'Sub', '578', 'Manila', 'Manila', NULL, '23', 'St', 'Sub', '578', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, 1, '2019-05-09 03:07:57', '2019-05-09 05:31:51', NULL),
	(3, 103, '5cd39bb01f870', 'Bong', NULL, 'Revilla', NULL, NULL, 'revilla@gmail.com', '2345678', NULL, NULL, 'csc_bulletin', '2019-05-09', 'Manila', 'male', 'single', NULL, 0, 0, '120', '80', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '67', 'St', 'Sub', '890', 'Manila', 'Manila', NULL, '67', 'St', 'Sub', '890', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-09 03:17:04', '2019-05-09 03:17:04', NULL),
	(4, 103, '5cd3bdc697a54', 'Nancy', NULL, 'Binay', NULL, NULL, 'binay@gmail.com', '3456789', NULL, NULL, 'csc_bulletin', '1974-06-12', 'Manila', 'male', 'single', NULL, 0, 0, '180', '40', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', 'St', 'Sub', '768', 'Manila', 'Manila', NULL, '23', 'St', 'Sub', '768', 'Manila', 'Manila', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-09 05:42:30', '2019-05-09 05:42:30', NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.appointments
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form34b_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form212_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eligibility_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form1_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form32_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form4_cscfo` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.appointments: ~2 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` (`id`, `applicant_id`, `form33_hrmo`, `form34b_hrmo`, `form212_hrmo`, `eligibility_hrmo`, `form1_hrmo`, `form32_hrmo`, `form4_hrmo`, `form33_cscfo`, `form34b_cscfo`, `form212_cscfo`, `eligibility_cscfo`, `form1_cscfo`, `form32_cscfo`, `form4_cscfo`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-08 08:48:37', '2019-05-08 08:48:37', NULL),
	(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-05-09 03:08:36', '2019-05-09 03:08:36', NULL),
	(3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-05-09 03:17:41', '2019-05-09 03:17:41', NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.appointment_casual
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.appointment_casual: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.appointment_forms
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `hrmo_assessment_date` date DEFAULT NULL,
  `chairperson_deliberation_date` date DEFAULT NULL,
  `form_status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.appointment_forms: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
INSERT INTO `appointment_forms` (`id`, `applicant_id`, `employee_status`, `nature_of_appointment`, `appointing_officer`, `hrmo`, `chairperson`, `date_sign`, `publication_date_from`, `publication_date_to`, `hrmo_date_sign`, `chairperson_date_sign`, `period_emp_from`, `period_emp_to`, `date_issued`, `assessment_date`, `hrmo_assessment_date`, `chairperson_deliberation_date`, `form_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 2, 1, 1, NULL, 'Juan Dela Cruz II', 'Juan Dela Cruz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2019-05-10 04:57:10', '2019-05-10 05:40:22', NULL);
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.appointment_issued
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.appointment_issued: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
INSERT INTO `appointment_issued` (`id`, `applicant_id`, `employee_status`, `date_issued`, `period_of_employment_from`, `period_of_employment_to`, `nature_of_appointment`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 1, '2019-05-09', '2019-05-22', '2019-05-23', 1, 1, 1, '2019-05-09 07:30:39', '2019-05-09 08:29:38', NULL);
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.appointment_processing
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.appointment_processing: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
INSERT INTO `appointment_processing` (`id`, `applicant_id`, `educ_qualification`, `educ_remarks`, `educ_check`, `exp_qualification`, `exp_remarks`, `exp_check`, `training_qualification`, `training_remarks`, `training_check`, `eligibility_qualification`, `eligibility_remarks`, `eligibility_check`, `other_qualification`, `other_remarks`, `other_check`, `ra_form_33`, `ra_employee_status`, `ra_nature_appointment`, `ra_appointing_authority`, `ra_date_sign`, `ra_date_publication`, `ra_certification`, `ra_pds`, `ra_eligibility`, `ra_position_description`, `ar_01`, `ar_02`, `ar_03`, `ar_04`, `ar_05`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 2, NULL, 'Qualified', 1, NULL, 'Qualified', 1, NULL, 'Qualified', 1, NULL, 'Qualified', 1, NULL, 'Qualified', 1, 'Complied', 'Permanent', 'Original', 'Chairman Arsenio M. Balisacan', NULL, NULL, 'Yes', 'Completely filled-up', 'Complied', 'Complied', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 1, NULL, '2019-05-10 04:59:27', '2019-05-10 04:59:27', NULL);
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.appointment_requirements
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_status` int(11) DEFAULT '0',
  `medical_status` int(11) DEFAULT '0',
  `oath_status` int(11) DEFAULT '0',
  `nbi_status` int(11) DEFAULT '0',
  `gsis_enrollment_status` int(11) DEFAULT '0',
  `bir_form_2316_status` int(11) DEFAULT '0',
  `bir_form_2305_status` int(11) DEFAULT '0',
  `bir_form_1905_status` int(11) DEFAULT '0',
  `philhealth_status` int(11) DEFAULT '0',
  `hdmf_status` int(11) DEFAULT '0',
  `saln_status` int(11) DEFAULT '0',
  `assumption_status` int(11) DEFAULT '0',
  `position_description_status` int(11) DEFAULT '0',
  `land_bank_status` int(11) DEFAULT '0',
  `original_status` int(11) DEFAULT '0',
  `passport_status` int(11) DEFAULT '0',
  `bday_status` int(11) DEFAULT '0',
  `pds_copies` int(11) DEFAULT '0',
  `medical_copies` int(11) DEFAULT '0',
  `oath_copies` int(11) DEFAULT '0',
  `nbi_copies` int(11) DEFAULT '0',
  `gsis_enrollment_copies` int(11) DEFAULT '0',
  `bir_form_2316_copies` int(11) DEFAULT '0',
  `bir_form_2305_copies` int(11) DEFAULT '0',
  `bir_form_1905_copies` int(11) DEFAULT '0',
  `philhealth_copies` int(11) DEFAULT '0',
  `hdmf_copies` int(11) DEFAULT '0',
  `saln_copies` int(11) DEFAULT '0',
  `assumption_copies` int(11) DEFAULT '0',
  `position_description_copies` int(11) DEFAULT '0',
  `land_bank_copies` int(11) DEFAULT '0',
  `original_copies` int(11) DEFAULT '0',
  `passport_copies` int(11) DEFAULT '0',
  `bday_copies` int(11) DEFAULT '0',
  `clearance_copies` int(11) DEFAULT '0',
  `clearance_status` int(11) DEFAULT '0',
  `service_record_copies` int(11) DEFAULT '0',
  `service_record_status` int(11) DEFAULT '0',
  `disbursement_voucher_copies` int(11) DEFAULT '0',
  `disbursement_voucher_status` int(11) DEFAULT '0',
  `certificate_allowance_copies` int(11) DEFAULT '0',
  `certificate_allowance_status` int(11) DEFAULT '0',
  `cert_leave_balances_copies` int(11) DEFAULT '0',
  `cert_leave_balances_status` int(11) DEFAULT '0',
  `id_picture_copies` int(11) DEFAULT '0',
  `id_picture_status` int(11) DEFAULT '0',
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `rar_path` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.appointment_requirements: ~2 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
INSERT INTO `appointment_requirements` (`id`, `applicant_id`, `pds_status`, `medical_status`, `oath_status`, `nbi_status`, `gsis_enrollment_status`, `bir_form_2316_status`, `bir_form_2305_status`, `bir_form_1905_status`, `philhealth_status`, `hdmf_status`, `saln_status`, `assumption_status`, `position_description_status`, `land_bank_status`, `original_status`, `passport_status`, `bday_status`, `pds_copies`, `medical_copies`, `oath_copies`, `nbi_copies`, `gsis_enrollment_copies`, `bir_form_2316_copies`, `bir_form_2305_copies`, `bir_form_1905_copies`, `philhealth_copies`, `hdmf_copies`, `saln_copies`, `assumption_copies`, `position_description_copies`, `land_bank_copies`, `original_copies`, `passport_copies`, `bday_copies`, `clearance_copies`, `clearance_status`, `service_record_copies`, `service_record_status`, `disbursement_voucher_copies`, `disbursement_voucher_status`, `certificate_allowance_copies`, `certificate_allowance_status`, `cert_leave_balances_copies`, `cert_leave_balances_status`, `id_picture_copies`, `id_picture_status`, `tin_number`, `pagibig_number`, `rar_path`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 1, 1, '2019-05-09 06:33:19', '2019-05-09 08:59:32', NULL),
	(2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 1, '2019-05-10 05:40:22', '2019-05-10 05:40:22', NULL);
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.assumptions
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.assumptions: ~2 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, NULL, NULL, NULL, 1, NULL, '2019-05-09 08:59:32', '2019-05-09 08:59:32', NULL),
	(3, 2, NULL, NULL, NULL, NULL, 1, '2019-05-10 05:40:22', '2019-05-10 05:40:22', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.attestations
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.attestations: ~1 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
INSERT INTO `attestations` (`id`, `applicant_id`, `agency_receiving_offer`, `action_employee_status`, `period_from`, `period_to`, `date_action`, `date_release`, `date_issuance`, `publication_from`, `publication_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-09 08:59:33', '2019-05-09 08:59:33', NULL),
	(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2019-05-10 05:40:50', '2019-05-10 05:40:50', NULL);
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.boarding_applicants
CREATE TABLE IF NOT EXISTS `boarding_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `board_status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.boarding_applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `boarding_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `boarding_applicants` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.config
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.config: ~12 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `name`, `value`, `description`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 'application_recipient_name', 'John Doe', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(2, 'application_recipient_title', 'Department Manager', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(3, 'application_recipient_department', 'Human Resources Department', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(4, 'application_recipient_organization', 'Organization Name', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(5, 'application_recipient_address', '123 street corner ABC avenue, Philippines', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(6, 'application_requirements', 'Letter of Application, Latest Personal Data Sheet', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(7, 'url_rms', 'http://rms-url-here', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(8, 'url_pis', 'http://pis-url-here', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(9, 'url_pms', 'http://pms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(10, 'url_ams', 'http://ams-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(11, 'url_ldms', 'http://ldms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(12, 'url_spms', 'http://spms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.countries: ~242 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `name`) VALUES
	(1, 'PH', 'Philippines'),
	(2, 'AF', 'Afghanistan'),
	(3, 'AL', 'Albania'),
	(4, 'DZ', 'Algeria'),
	(5, 'AS', 'American Samoa'),
	(6, 'AD', 'Andorra'),
	(7, 'AO', 'Angola'),
	(8, 'AI', 'Anguilla'),
	(9, 'AQ', 'Antarctica'),
	(10, 'AG', 'Antigua and/or Barbuda'),
	(11, 'AR', 'Argentina'),
	(12, 'AM', 'Armenia'),
	(13, 'AW', 'Aruba'),
	(14, 'AU', 'Australia'),
	(15, 'AT', 'Austria'),
	(16, 'AZ', 'Azerbaijan'),
	(17, 'BS', 'Bahamas'),
	(18, 'BH', 'Bahrain'),
	(19, 'BD', 'Bangladesh'),
	(20, 'BB', 'Barbados'),
	(21, 'BY', 'Belarus'),
	(22, 'BE', 'Belgium'),
	(23, 'BZ', 'Belize'),
	(24, 'BJ', 'Benin'),
	(25, 'BM', 'Bermuda'),
	(26, 'BT', 'Bhutan'),
	(27, 'BO', 'Bolivia'),
	(28, 'BA', 'Bosnia and Herzegovina'),
	(29, 'BW', 'Botswana'),
	(30, 'BV', 'Bouvet Island'),
	(31, 'BR', 'Brazil'),
	(32, 'IO', 'British lndian Ocean Territory'),
	(33, 'BN', 'Brunei Darussalam'),
	(34, 'BG', 'Bulgaria'),
	(35, 'BF', 'Burkina Faso'),
	(36, 'BI', 'Burundi'),
	(37, 'KH', 'Cambodia'),
	(38, 'CM', 'Cameroon'),
	(39, 'CA', 'Canada'),
	(40, 'CV', 'Cape Verde'),
	(41, 'KY', 'Cayman Islands'),
	(42, 'CF', 'Central African Republic'),
	(43, 'TD', 'Chad'),
	(44, 'CL', 'Chile'),
	(45, 'CN', 'China'),
	(46, 'CX', 'Christmas Island'),
	(47, 'CC', 'Cocos (Keeling) Islands'),
	(48, 'CO', 'Colombia'),
	(49, 'KM', 'Comoros'),
	(50, 'CG', 'Congo'),
	(51, 'CK', 'Cook Islands'),
	(52, 'CR', 'Costa Rica'),
	(53, 'HR', 'Croatia (Hrvatska)'),
	(54, 'CU', 'Cuba'),
	(55, 'CY', 'Cyprus'),
	(56, 'CZ', 'Czech Republic'),
	(57, 'CD', 'Democratic Republic of Congo'),
	(58, 'DK', 'Denmark'),
	(59, 'DJ', 'Djibouti'),
	(60, 'DM', 'Dominica'),
	(61, 'DO', 'Dominican Republic'),
	(62, 'TP', 'East Timor'),
	(63, 'EC', 'Ecudaor'),
	(64, 'EG', 'Egypt'),
	(65, 'SV', 'El Salvador'),
	(66, 'GQ', 'Equatorial Guinea'),
	(67, 'ER', 'Eritrea'),
	(68, 'EE', 'Estonia'),
	(69, 'ET', 'Ethiopia'),
	(70, 'FK', 'Falkland Islands (Malvinas)'),
	(71, 'FO', 'Faroe Islands'),
	(72, 'FJ', 'Fiji'),
	(73, 'FI', 'Finland'),
	(74, 'FR', 'France'),
	(75, 'FX', 'France, Metropolitan'),
	(76, 'GF', 'French Guiana'),
	(77, 'PF', 'French Polynesia'),
	(78, 'TF', 'French Southern Territories'),
	(79, 'GA', 'Gabon'),
	(80, 'GM', 'Gambia'),
	(81, 'GE', 'Georgia'),
	(82, 'DE', 'Germany'),
	(83, 'GH', 'Ghana'),
	(84, 'GI', 'Gibraltar'),
	(85, 'GR', 'Greece'),
	(86, 'GL', 'Greenland'),
	(87, 'GD', 'Grenada'),
	(88, 'GP', 'Guadeloupe'),
	(89, 'GU', 'Guam'),
	(90, 'GT', 'Guatemala'),
	(91, 'GN', 'Guinea'),
	(92, 'GW', 'Guinea-Bissau'),
	(93, 'GY', 'Guyana'),
	(94, 'HT', 'Haiti'),
	(95, 'HM', 'Heard and Mc Donald Islands'),
	(96, 'HN', 'Honduras'),
	(97, 'HK', 'Hong Kong'),
	(98, 'HU', 'Hungary'),
	(99, 'IS', 'Iceland'),
	(100, 'IN', 'India'),
	(101, 'ID', 'Indonesia'),
	(102, 'IR', 'Iran (Islamic Republic of)'),
	(103, 'IQ', 'Iraq'),
	(104, 'IE', 'Ireland'),
	(105, 'IL', 'Israel'),
	(106, 'IT', 'Italy'),
	(107, 'CI', 'Ivory Coast'),
	(108, 'JM', 'Jamaica'),
	(109, 'JP', 'Japan'),
	(110, 'JO', 'Jordan'),
	(111, 'KZ', 'Kazakhstan'),
	(112, 'KE', 'Kenya'),
	(113, 'KI', 'Kiribati'),
	(114, 'KP', 'Korea, Democratic People\'s Republic of'),
	(115, 'KR', 'Korea, Republic of'),
	(116, 'KW', 'Kuwait'),
	(117, 'KG', 'Kyrgyzstan'),
	(118, 'LA', 'Lao People\'s Democratic Republic'),
	(119, 'LV', 'Latvia'),
	(120, 'LB', 'Lebanon'),
	(121, 'LS', 'Lesotho'),
	(122, 'LR', 'Liberia'),
	(123, 'LY', 'Libyan Arab Jamahiriya'),
	(124, 'LI', 'Liechtenstein'),
	(125, 'LT', 'Lithuania'),
	(126, 'LU', 'Luxembourg'),
	(127, 'MO', 'Macau'),
	(128, 'MK', 'Macedonia'),
	(129, 'MG', 'Madagascar'),
	(130, 'MW', 'Malawi'),
	(131, 'MY', 'Malaysia'),
	(132, 'MV', 'Maldives'),
	(133, 'ML', 'Mali'),
	(134, 'MT', 'Malta'),
	(135, 'MH', 'Marshall Islands'),
	(136, 'MQ', 'Martinique'),
	(137, 'MR', 'Mauritania'),
	(138, 'MU', 'Mauritius'),
	(139, 'TY', 'Mayotte'),
	(140, 'MX', 'Mexico'),
	(141, 'FM', 'Micronesia, Federated States of'),
	(142, 'MD', 'Moldova, Republic of'),
	(143, 'MC', 'Monaco'),
	(144, 'MN', 'Mongolia'),
	(145, 'MS', 'Montserrat'),
	(146, 'MA', 'Morocco'),
	(147, 'MZ', 'Mozambique'),
	(148, 'MM', 'Myanmar'),
	(149, 'NA', 'Namibia'),
	(150, 'NR', 'Nauru'),
	(151, 'NP', 'Nepal'),
	(152, 'NL', 'Netherlands'),
	(153, 'AN', 'Netherlands Antilles'),
	(154, 'NC', 'New Caledonia'),
	(155, 'NZ', 'New Zealand'),
	(156, 'NI', 'Nicaragua'),
	(157, 'NE', 'Niger'),
	(158, 'NG', 'Nigeria'),
	(159, 'NU', 'Niue'),
	(160, 'NF', 'Norfork Island'),
	(161, 'MP', 'Northern Mariana Islands'),
	(162, 'NO', 'Norway'),
	(163, 'OM', 'Oman'),
	(164, 'PK', 'Pakistan'),
	(165, 'PW', 'Palau'),
	(166, 'PA', 'Panama'),
	(167, 'PG', 'Papua New Guinea'),
	(168, 'PY', 'Paraguay'),
	(169, 'PE', 'Peru'),
	(170, 'PN', 'Pitcairn'),
	(171, 'PL', 'Poland'),
	(172, 'PT', 'Portugal'),
	(173, 'PR', 'Puerto Rico'),
	(174, 'QA', 'Qatar'),
	(175, 'SS', 'Republic of South Sudan'),
	(176, 'RE', 'Reunion'),
	(177, 'RO', 'Romania'),
	(178, 'RU', 'Russian Federation'),
	(179, 'RW', 'Rwanda'),
	(180, 'KN', 'Saint Kitts and Nevis'),
	(181, 'LC', 'Saint Lucia'),
	(182, 'VC', 'Saint Vincent and the Grenadines'),
	(183, 'WS', 'Samoa'),
	(184, 'SM', 'San Marino'),
	(185, 'ST', 'Sao Tome and Principe'),
	(186, 'SA', 'Saudi Arabia'),
	(187, 'SN', 'Senegal'),
	(188, 'RS', 'Serbia'),
	(189, 'SC', 'Seychelles'),
	(190, 'SL', 'Sierra Leone'),
	(191, 'SG', 'Singapore'),
	(192, 'SK', 'Slovakia'),
	(193, 'SI', 'Slovenia'),
	(194, 'SB', 'Solomon Islands'),
	(195, 'SO', 'Somalia'),
	(196, 'ZA', 'South Africa'),
	(197, 'GS', 'South Georgia South Sandwich Islands'),
	(198, 'ES', 'Spain'),
	(199, 'LK', 'Sri Lanka'),
	(200, 'SH', 'St. Helena'),
	(201, 'PM', 'St. Pierre and Miquelon'),
	(202, 'SD', 'Sudan'),
	(203, 'SR', 'Suriname'),
	(204, 'SJ', 'Svalbarn and Jan Mayen Islands'),
	(205, 'SZ', 'Swaziland'),
	(206, 'SE', 'Sweden'),
	(207, 'CH', 'Switzerland'),
	(208, 'SY', 'Syrian Arab Republic'),
	(209, 'TW', 'Taiwan'),
	(210, 'TJ', 'Tajikistan'),
	(211, 'TZ', 'Tanzania, United Republic of'),
	(212, 'TH', 'Thailand'),
	(213, 'TG', 'Togo'),
	(214, 'TK', 'Tokelau'),
	(215, 'TO', 'Tonga'),
	(216, 'TT', 'Trinidad and Tobago'),
	(217, 'TN', 'Tunisia'),
	(218, 'TR', 'Turkey'),
	(219, 'TM', 'Turkmenistan'),
	(220, 'TC', 'Turks and Caicos Islands'),
	(221, 'TV', 'Tuvalu'),
	(222, 'US', 'United States'),
	(223, 'UG', 'Uganda'),
	(224, 'UA', 'Ukraine'),
	(225, 'AE', 'United Arab Emirates'),
	(226, 'GB', 'United Kingdom'),
	(227, 'UM', 'United States minor outlying islands'),
	(228, 'UY', 'Uruguay'),
	(229, 'UZ', 'Uzbekistan'),
	(230, 'VU', 'Vanuatu'),
	(231, 'VA', 'Vatican City State'),
	(232, 'VE', 'Venezuela'),
	(233, 'VN', 'Vietnam'),
	(234, 'VG', 'Virgin Islands (British)'),
	(235, 'VI', 'Virgin Islands (U.S.)'),
	(236, 'WF', 'Wallis and Futuna Islands'),
	(237, 'EH', 'Western Sahara'),
	(238, 'YE', 'Yemen'),
	(239, 'YU', 'Yugoslavia'),
	(240, 'ZR', 'Zaire'),
	(241, 'ZM', 'Zambia'),
	(242, 'ZW', 'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.departments: ~0 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'OTHER EXECUTIVE OFFICES', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.divisions
CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.divisions: ~16 rows (approximately)
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;
INSERT INTO `divisions` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Office of the Chairman', NULL, NULL, NULL, NULL, NULL),
	(2, 'Office of the Executive Director', NULL, NULL, NULL, NULL, NULL),
	(3, 'Office of the Director', NULL, NULL, NULL, NULL, NULL),
	(4, 'General Services Division', NULL, NULL, NULL, NULL, NULL),
	(5, 'Human Resource Development Division', NULL, NULL, NULL, NULL, NULL),
	(6, 'Information and Communications Technology Division', NULL, NULL, NULL, NULL, NULL),
	(7, 'Legal Division', NULL, NULL, NULL, NULL, NULL),
	(8, 'Accounting Division', NULL, NULL, NULL, NULL, NULL),
	(9, 'Budget Division', NULL, NULL, NULL, NULL, NULL),
	(10, 'Corporate Planning and Management Division', NULL, NULL, NULL, NULL, NULL),
	(11, 'Monitoring and Investigation Division', NULL, NULL, NULL, NULL, NULL),
	(12, 'Adjudication Division', NULL, NULL, NULL, NULL, NULL),
	(13, 'Merger and Acquisition Division', NULL, NULL, NULL, NULL, NULL),
	(14, 'Policy Research and Development Division', NULL, NULL, NULL, NULL, NULL),
	(15, 'Knowledge Management Division', NULL, NULL, NULL, NULL, NULL),
	(16, 'Training Division', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.educations
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` varchar(225) DEFAULT NULL,
  `attendance_to` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.educations: ~12 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
INSERT INTO `educations` (`id`, `applicant_id`, `school_name`, `course`, `attendance_from`, `attendance_to`, `level`, `graduated`, `awards`, `educ_level`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-05-08 08:10:48', '2019-05-08 08:10:48', NULL),
	(2, 1, 'Secondary', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2019-05-08 08:10:48', '2019-05-08 08:10:48', NULL),
	(3, 1, 'College1', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, '2019-05-08 08:10:49', '2019-05-08 08:10:49', NULL),
	(4, 2, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-05-09 03:07:58', '2019-05-09 03:07:58', NULL),
	(5, 2, 'Secondary', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2019-05-09 03:07:58', '2019-05-09 03:07:58', NULL),
	(6, 2, 'College 2', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, '2019-05-09 03:07:58', '2019-05-09 03:07:58', NULL),
	(7, 3, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-05-09 03:17:04', '2019-05-09 03:17:04', NULL),
	(8, 3, 'Secondary', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2019-05-09 03:17:04', '2019-05-09 03:17:04', NULL),
	(9, 3, 'College 3', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, '2019-05-09 03:17:04', '2019-05-09 03:17:04', NULL),
	(10, 4, 'Primary', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-05-09 05:42:30', '2019-05-09 05:42:30', NULL),
	(11, 4, 'Secondary', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '2019-05-09 05:42:30', '2019-05-09 05:42:30', NULL),
	(12, 4, 'College 5', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, '2019-05-09 05:42:30', '2019-05-09 05:42:30', NULL);
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.eligibilities
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `eligibility_ref` int(11) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.eligibilities: ~4 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
INSERT INTO `eligibilities` (`id`, `applicant_id`, `eligibility_ref`, `rating`, `exam_place`, `license_number`, `license_validity`, `exam_date`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-08 08:10:49', '2019-05-08 08:10:49'),
	(2, 2, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 03:07:58', '2019-05-09 03:07:58'),
	(3, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 03:17:04', '2019-05-09 03:17:04'),
	(4, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 04:05:18', '2019-05-09 04:05:18'),
	(6, 4, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 05:42:30', '2019-05-09 05:42:30');
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.employee_information
CREATE TABLE IF NOT EXISTS `employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `hired_date` timestamp NULL DEFAULT NULL,
  `assumption_date` timestamp NULL DEFAULT NULL,
  `resigned_date` timestamp NULL DEFAULT NULL,
  `rehired_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `position_item_id` int(11) NOT NULL,
  `employee_status_id` int(11) NOT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) NOT NULL,
  `work_schedule_id` int(11) NOT NULL,
  `appointment_status_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.employee_information: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_information` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.erasure_alterations
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.erasure_alterations: ~0 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.evaluations
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` decimal(10,2) unsigned DEFAULT NULL,
  `performance_divide` decimal(10,2) unsigned DEFAULT NULL,
  `performance_average` decimal(10,2) unsigned DEFAULT NULL,
  `performance_percent` decimal(10,2) unsigned DEFAULT NULL,
  `performance_score` decimal(10,2) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` decimal(10,2) unsigned DEFAULT NULL,
  `minimum_training_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_points` decimal(10,2) unsigned DEFAULT NULL,
  `training_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_total_points` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_percent` decimal(10,2) unsigned DEFAULT NULL,
  `education_training_score` decimal(10,2) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` decimal(10,2) unsigned DEFAULT NULL,
  `additional_points` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` decimal(10,2) unsigned DEFAULT NULL,
  `experience_accomplishments_score` decimal(10,2) unsigned DEFAULT NULL,
  `potential` decimal(10,2) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` decimal(10,2) unsigned DEFAULT NULL,
  `potential_percent` decimal(10,2) unsigned DEFAULT NULL,
  `potential_score` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_average_rating` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_percent` decimal(10,2) unsigned DEFAULT NULL,
  `psychosocial_score` decimal(10,2) unsigned DEFAULT NULL,
  `total_percent` decimal(10,2) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.evaluations: ~3 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
INSERT INTO `evaluations` (`id`, `applicant_id`, `job_id`, `performance`, `performance_divide`, `performance_average`, `performance_percent`, `performance_score`, `eligibility`, `training`, `seminar`, `education`, `minimum_education_points`, `minimum_training_points`, `education_points`, `training_points`, `education_training_total_points`, `education_training_percent`, `education_training_score`, `relevant_positions_held`, `minimum_experience_requirement`, `additional_points`, `experience_accomplishments_total_points`, `experience_accomplishments_percent`, `experience_accomplishments_score`, `potential`, `potential_average_rating`, `potential_percentage_rating`, `potential_percent`, `potential_score`, `psychosocial`, `psychosocial_average_rating`, `psychosocial_percentage_rating`, `psychosocial_percent`, `psychosocial_score`, `total_percent`, `total_score`, `evaluated_by`, `reviewed_by`, `noted_by`, `recommended`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 195, 188.00, 2.00, 94.00, 40.00, 37.60, NULL, NULL, NULL, NULL, 60.00, 20.00, 30.00, 10.00, 120.00, 20.00, 24.00, NULL, 75.00, 15.00, 90.00, 20.00, 18.00, 23.00, 33.00, 44.00, 10.00, 4.40, 23.00, 33.00, 90.00, 10.00, 9.00, 100.00, 93.00, NULL, NULL, NULL, 1, 1, 1, '2019-05-08 08:48:04', '2019-05-08 08:49:47', NULL),
	(2, 2, 103, 200.00, 2.00, 100.00, 40.00, 40.00, NULL, NULL, NULL, NULL, 60.00, 20.00, 5.00, 10.00, 95.00, 20.00, 19.00, NULL, 80.00, 15.00, 95.00, 20.00, 19.00, 23.00, 33.00, 80.00, 10.00, 8.00, 23.00, 33.00, 90.00, 10.00, 9.00, 100.00, 95.00, NULL, NULL, NULL, 1, 1, 1, '2019-05-09 03:08:17', '2019-05-09 03:12:50', NULL),
	(3, 3, 103, 150.00, 2.00, 75.00, 40.00, 30.00, NULL, NULL, NULL, NULL, 40.00, 40.00, 5.00, 5.00, 90.00, 20.00, 18.00, NULL, 50.00, 40.00, 90.00, 20.00, 18.00, 200.00, 50.00, 80.00, 10.00, 8.00, 200.00, 30.00, 70.00, 10.00, 7.00, 100.00, 81.00, NULL, NULL, NULL, NULL, 1, 1, '2019-05-09 03:17:20', '2019-05-09 03:19:44', NULL);
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.examinations
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.examinations: ~0 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.interviews
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.interviews: ~2 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `applicant_id`, `interview_date`, `interview_time`, `interview_location`, `resched_interview_date`, `resched_interview_time`, `interview_status`, `notify`, `noftiy_resched_interview`, `confirmed`, `psb_chairperson`, `psb_secretariat`, `psb_member`, `psm_sweap_rep`, `psb_end_user`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(1, 1, NULL, '232', '3232', NULL, NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-08 08:48:37', NULL, '2019-05-08 08:48:37'),
	(2, 2, NULL, '232', '3232', NULL, NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-09 03:08:36', NULL, '2019-05-09 03:08:36'),
	(3, 3, NULL, '232', '3232', NULL, NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-09 03:17:41', NULL, '2019-05-09 03:17:41');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `psipop_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `rata_amount` decimal(12,3) DEFAULT '0.000',
  `eme_amount` decimal(12,3) DEFAULT '0.000',
  `communication_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.jobs: ~194 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `psipop_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `rata_amount`, `eme_amount`, `communication_amount`, `status`, `station`, `reporting_line`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `expires`, `deadline_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 6, NULL, 'Bachelors degree ', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:43', NULL, 0, NULL, NULL),
	(2, 7, NULL, 'Bachelors degree ', '3 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '24 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:43', NULL, 0, NULL, NULL),
	(3, 8, NULL, 'Bachelors degree ', '4 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '25 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(4, 9, NULL, 'Bachelors degree ', '5 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '26 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(5, 10, NULL, 'Bachelors degree ', '6 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '27 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(6, 11, NULL, 'Bachelors degree ', '7 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '28 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(7, 12, NULL, 'Bachelors Degree in Social Sciences Public Administration/ Management Legal Management Law or related field', '2 years of experience in one or a combination of the ff secretariat support services corporate administrative legal or quasi-judicial proceedings legal / quality management and other related disciplines', '8 hours training in one or a combination of the ff corporate / administrative / legal / quasi-judicial proceedings communication legal / quality management or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(8, 13, NULL, 'Bachelors degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(9, 14, NULL, 'Bachelors degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(10, 15, NULL, 'Bachelors degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(12, 17, NULL, 'Bachelors degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(13, 18, NULL, 'Bachelors degree in social sciences public Administration/ Management Legal Management Law or related field', '1 year of experience in one or a combination of the ff secretariat support services corporate administrative legal or quasi-judicial proceedings legal / quality management and other related disciplines', '4 hours training in one or a combination of the ff corporate / administrative / legal / quasi-judicial proceedings communication legal / quality management or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(14, 19, NULL, 'Bachelors degree in social sciences public Administration/ Management Legal Management Law or related field', '1 year of experience in one or a combination of the ff secretariat support services corporate administrative legal or quasi-judicial proceedings legal / quality management and other related disciplines', '4 hours training in one or a combination of the ff corporate / administrative / legal / quasi-judicial proceedings communication legal / quality management or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(15, 20, NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(16, 21, NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(17, 22, NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(18, 23, NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(19, 24, NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(20, 25, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(21, 26, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(22, 27, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:44', NULL, 0, NULL, NULL),
	(23, 28, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(24, 29, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(25, 30, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(26, 31, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(27, 32, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(28, 33, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(29, 34, NULL, 'Masters degree or Certificate of Leadership and Management from the CSC', '10 years of relevant experience in any of the fields of law economics commerce management finance or engineering; 5 years of which are supervisory/management experience', '120 hours of supervisory / management learning and development intervention undertaken within the last 5 years; OR 40 hours of MCLE CPE CPD and 80 hours of management training taken within the last 5 years', 'CS Professional / Second Level Eligibility; RA 1080 (CPA Bar Engineer)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(30, 35, NULL, 'Bachelors degree in social sciences public administration/management legal management law or related field', '2 years of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '16 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(31, 36, NULL, 'Bachelors degree in social sciences public administration/management legal management law or related field', '1 year of relevant experience in one or a combination of the following: communication public relations corporate governance quality management administrative proceedings or related disciplines', '4 hours of training in one or a combination of the ff communication governance public/corporate/quality management legal/ administrative proceedings economics or other relevant training', 'CS Professional / Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(32, 37, NULL, 'Completion of two years in college', 'None required', 'None required', 'None required', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(33, 38, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(34, 39, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(35, 40, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(36, 41, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(37, 42, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(38, 43, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(39, 44, NULL, 'Bachelors degre in social sciences public administration / management legal management psychology commerce finance accounting or related field', '3 years of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources personnel administration / management organizational development accounting administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(40, 45, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '2 years of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '8 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources public administraion / management organizational development accoutning administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(41, 46, NULL, 'Bachelors degree in commerce finance accounting or related field', '2 years of relevant experience in one or a combination of the ff financial management disbursement and collection cash/check management bookkeeping accounting and other related disciplines', '8 hours of training n financia management accounting bookkeeping disbursement and collection cash/check management internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(42, 47, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '2 years of relevant experience in one or a combination of the ff quality / records management office administration systems development and other related disciplines', '8 hours of training in quality / records management office administration information and communications technology or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(43, 48, NULL, 'Bachelors degre in social sciences public administration / managementcommerce finance or related field', '2 years of relevant experience in one or a combination of the ff quality / property management procurement planning and other related disciplines', '8 hours of training in quality / property management procurement planning internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(44, 49, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '1 year of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources public administraion / management organizational development accoutning administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(45, 50, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '1 year of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources public administraion / management organizational development accoutning administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(46, 51, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '1 year of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources public administraion / management organizational development accoutning administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(47, 52, NULL, 'Bachelors degree in commerce finance accounting or related field', '1 year of relevant experience in one or a combination of the ff financial management disbursement and collection cash/check management bookkeeping accounting and other related disciplines', '4 hours of training n financia management accounting bookkeeping disbursement and collection cash/check management internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(48, 53, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '1 year of relevant experience in one or a combination of the ff quality / records management office administration systems development and other related disciplines', '4 hours of training in quality / records management office administration information and communications technology or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(49, 54, NULL, 'Bachelors degre in social sciences public administration / managementcommerce finance or related field', '2 years of relevant experience in one or a combination of the ff quality / property management procurement planning and other related disciplines', '8 hours of training in quality / property management procurement planning internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:45', NULL, 0, NULL, NULL),
	(50, 55, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(51, 56, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(52, 57, NULL, 'Bachelors degre in social sciences public administration / managementcommerce finance or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(53, 58, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(54, 59, NULL, 'Bachelors degre in social sciences public administration / management legal management psychology commerce finance accounting or related field', '3 years of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources personnel administration / management organizational development accounting administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(55, 60, NULL, 'Bachelors degre in pyscholoygy behavioral science human resource management or related field', '2 years of relevant experience in one or a combination of the ff human resources organizational development personnel administration / management competency-based recruitment process quality management and other related disciplines', '8 hours of training in human resources organizational development personnel administration/management public / quality management competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(56, 61, NULL, 'Bachelors degre in pyscholoygy behavioral science human resource management or related field', '2 years of relevant experience in one or a combination of the ff human resources organizational development personnel administration / management competency-based recruitment process quality management and other related disciplines', '8 hours of training in human resources organizational development personnel administration/management public / quality management competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(57, 62, NULL, 'Bachelors degre in pyscholoygy behavioral science human resource management or related field', '1 year of relevant experience in one or a combination of the ff human resources organizational development personnel administration / management competency-based recruitment process quality management and other related disciplines', '4 hours of training in human resources organizational development personnel administration/management public / quality management competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(58, 63, NULL, 'Bachelors degre in pyscholoygy behavioral science human resource management or related field', '1 year of relevant experience in one or a combination of the ff human resources organizational development personnel administration / management competency-based recruitment process quality management and other related disciplines', '4 hours of training in human resources organizational development personnel administration/management public / quality management competency-based recruitment or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(59, 64, NULL, 'Bachelors degre in pyscholoygy behavioral science human resource management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(60, 65, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(61, 66, NULL, 'Bachelors degree in Computer Science Information Technology Electronics and Communications Engineering or related field', '3 years of relevant experience in one or a combination of the ff information and communications technology systems development programming and other related disciplines', '16 hours of training in one or a combination of the ff information and communication and communications technology systems development programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(62, 67, NULL, 'Bachelors degree in Computer Science Information Technology Electronics and Communications Engineering or related field', '2 years of relevant experience in one or a combination of the ff information and communications technology systems development programming and other related disciplines', '8 hours of training in one or a combination of the ff information and communication and communications technology systems development programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(63, 68, NULL, 'Bachelors degree in Computer Science Information Technology Electronics and Communications Engineering or related field', '2 years of relevant experience in one or a combination of the ff information and communications technology systems development programming and other related disciplines', '8 hours of training in one or a combination of the ff information and communication and communications technology systems development programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(64, 69, NULL, 'Bachelors degree in Computer Science Information Technology Electronics and Communications Engineering or related field', '1 year of relevant experience in one or a combination of the ff information and communications technology systems development programming and other related disciplines', '4 hours of training in one or a combination of the ff information and communication and communications technology systems development programming or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(65, 70, NULL, 'Bachelors degree in Computer Science Information Technology Electronics and Communications Engineering or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(66, 71, NULL, 'Bachelor of Laws preferably with Masters degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(67, 72, NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(68, 73, NULL, 'Bachelor of Laws', '1 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '4 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(69, 74, NULL, 'Bachelor of Laws', '1 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '4 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(70, 75, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:46', NULL, 0, NULL, NULL),
	(71, 76, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(72, 77, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(73, 78, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(74, 79, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(75, 80, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(76, 81, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(77, 82, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(78, 83, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(79, 84, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(80, 85, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(81, 86, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting (CPA) preferably with Masters degree or Certificate in Leadership and management from CSC', '4 years of supervisory/ management experience', '40 hours of supervisory/ management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(82, 87, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting', '3 years of relevant experience in one or a combination of the following: government accounting and auditing financial management and other related disciplines', '16 hours of training in CPE financial management government accounting internal controls or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(83, 88, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting', '2 years of relevant experience in one or a combination of the following: government accounting and auditing financial management and other related disciplines', '8  hours training in CPE financial management government accounting internal controls or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(84, 89, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting', '2 years of relevant experience in one or a combination of the following: government accounting and auditing financial management and other related disciplines', '8  hours training in CPE financial management government accounting internal controls or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(85, 90, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting', '1 year of relevant experience in one or a combination of the following: government accounting and auditing financial management and other related disciplines', '4  hours training in CPE financial management government accounting internal controls or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(86, 91, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting', '1 year of relevant experience in one or a combination of the following: government accounting and auditing financial management and other related disciplines', '4  hours training in CPE financial management government accounting internal controls or other relevant training', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(87, 92, NULL, 'Bachelors degree in Commerce/ Business Administration major in Accounting', 'None required', 'None required', 'R.A. 1080 (CPA)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(88, 93, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(89, 94, NULL, 'Bachelors degre in social sciences public administration / management legal management psychology commerce finance accounting or related field', '3 years of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources personnel administration / management organizational development accounting administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:47', NULL, 0, NULL, NULL),
	(90, 95, NULL, 'Bachelors degree in commerce finance accounting or related field', '2 years of relevant experience in one or a combination of the ff government budgeting financial management and other related disciplines', '8 hours of training in government budgeting financial management internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(91, 96, NULL, 'Bachelors degree in commerce finance accounting or related field', '2 years of relevant experience in one or a combination of the ff government budgeting financial management and other related disciplines', '8 hours of training in government budgeting financial management internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(92, 97, NULL, 'Bachelors degree in commerce finance accounting or related field', '1 year of relevant experience in one or a combination of the ff government budgeting financial management and other related disciplines', '4 hours of training in government budgeting financial management internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(93, 98, NULL, 'Bachelors degree in commerce finance accounting or related field', '1 year of relevant experience in one or a combination of the ff government budgeting financial management and other related disciplines', '4 hours of training in government budgeting financial management internal controls or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(94, 99, NULL, '', '', '', '', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(95, 100, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(96, 101, NULL, 'Bachelors degree in social sciences economics public management/ administration or related field', '3 years of relevant experiene in one or a combination of the ff governance/ corporate planning financial / quality management and other related disciplines', '16 hours of training in government / corporate planning financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(97, 102, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '2 years of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '8 hour of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources public administraion / management organizational development accoutning administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(98, 103, NULL, 'Bachelors degree in social sciences economics public management/ administration or related field', '3 years of relevant experiene in one or a combination of the ff governance/ corporate planning financial / quality management and other related disciplines', '16 hours of training in government / corporate planning financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(99, 104, NULL, 'Bachelors degree in social sciences economics public management/ administration or related field', '3 years of relevant experiene in one or a combination of the ff governance/ corporate planning financial / quality management and other related disciplines', '16 hours of training in government / corporate planning financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(100, 105, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', '1 year of relevant experience in one or a combination of the ff quality / financial / property management procurement human resources personnel administration / management organizational development accounting administrative / legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff quality / financial / property management procurement internal controls human resources public administraion / management organizational development accoutning administrative proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(101, 106, NULL, 'Bachelors degree in social sciences economics public management/ administration or related field', '1 year of relevant experiene in one or a combination of the ff governance/ corporate planning financial / quality management and other related disciplines', '4 hours of training in government / corporate planning financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(102, 107, NULL, 'Bachelors degree in social sciences economics public management/ administration or related field', '1 year of relevant experiene in one or a combination of the ff governance/ corporate planning financial / quality management and other related disciplines', '4 hours of training in government / corporate planning financial / quality management or other relevant training ', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(103, 108, NULL, 'Bachelors degre in social sciences public administration / management legal management or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 22532.000, 0.000, 2000.000, 6000.000, 22532.000, 22532.000, 5000.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-31', 1, 0, 0, 1, 0, '2019-05-08 15:43:48', '2019-05-10 05:33:52', 0, NULL, NULL),
	(104, 109, NULL, 'Bachelors degree in social sciences economics public management/ administration or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(105, 110, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(106, 111, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(107, 112, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(108, 113, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(109, 114, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(110, 115, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '4 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '16 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(111, 116, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '3 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '16 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(112, 117, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '3 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '16 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:48', NULL, 0, NULL, NULL),
	(113, 118, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '2 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '8 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:49', NULL, 0, NULL, NULL),
	(114, 119, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '2 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '8 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:49', NULL, 0, NULL, NULL),
	(115, 120, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '2 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '8 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:49', NULL, 0, NULL, NULL),
	(116, 121, NULL, 'Bachelors degree in law commerce/ business administration major in accounting management finance engineering or related field', '2 years of relevant experience in one or a combination of the ff administrative corporate quasi-judicial judicial legal or investigation proceedings accounting auditing and other related disciplines', '8 hours of training in one or a combination of the ff MCLE CPE CPD administrative corporate legal proceedings economics trade industry investigation or other related disciplines', 'Career  Service Professional/ Second Level Eligibility; RA 1080 (Bar CPA Engineer Criminologist)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:49', NULL, 0, NULL, NULL),
	(117, 122, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:49', NULL, 0, NULL, NULL),
	(118, 123, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:49', NULL, 0, NULL, NULL),
	(119, 124, NULL, 'Bachelor of Laws preferably with Masters degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(120, 125, NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(121, 126, NULL, 'Bachelor of Laws', '1 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '4 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(122, 127, NULL, 'Bachelor of Laws', '2 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '5 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(123, 128, NULL, 'Bachelor of Laws', '3 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '6 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(124, 129, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(125, 130, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(126, 131, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(127, 132, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(128, 133, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(129, 134, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '4 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(130, 135, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '5 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(131, 136, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '6 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(132, 137, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '7 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:50', NULL, 0, NULL, NULL),
	(133, 138, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:51', NULL, 0, NULL, NULL),
	(134, 139, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:51', NULL, 0, NULL, NULL),
	(135, 140, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:51', NULL, 0, NULL, NULL),
	(136, 141, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:51', NULL, 0, NULL, NULL),
	(137, 142, NULL, 'Bachelor of Laws preferably with Masters degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:51', NULL, 0, NULL, NULL),
	(138, 143, NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:51', NULL, 0, NULL, NULL),
	(139, 144, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(140, 145, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(141, 146, NULL, 'Bachelors degree in Economics Statistis Mathematics or related field', '2 years of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy development planning and other related economic disciplines (e.g. financial labor regional industry international agricultural)', '8 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy regulation statistics trade industry operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(142, 147, NULL, 'Bachelors degree in Economics Statistis Mathematics or related field', '2 years of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy development planning and other related economic disciplines (e.g. financial labor regional industry international agricultural)', '8 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy regulation statistics trade industry operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(143, 148, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(144, 149, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(145, 150, NULL, 'Bachelors degree in Economics Statistis Mathematics or related field', '1 year of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy development planning and other related economic disciplines (e.g. financial labor regional industry international agricultural)', '4 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy regulation statistics trade industry operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(146, 151, NULL, 'Bachelors degree in Economics Statistis Mathematics or related field', '1 year of relevant experience in one or a combination of the following: economic research / studies / analysis / economic projections / interpretation / analysis of statistical data on specific sector of the economy development planning and other related economic disciplines (e.g. financial labor regional industry international agricultural)', '4 hours training in one or a combination of the following: economic / statistical analysis tools / public / competition policy regulation statistics trade industry operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(147, 152, NULL, '', '', '', '', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(148, 153, NULL, '', '', '', '', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(149, 154, NULL, 'Bachelor of Laws preferably with Masters degree or Certificate in Leadership and Management from the CSC', '4 years of supervisory/management experience', '40 hours of supervisory/management learning and development intervention undertaken within the last 5 years', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(150, 155, NULL, 'Bachelor of Laws', '2 years of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '8 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(151, 156, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(152, 157, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(153, 158, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(154, 159, NULL, 'Bachelor of Laws', '4 year of relevant experience in one or a combination of the ff administrative / corporate / legal / judicial or quasi-judicial proceedings and othe related disciplines', '7 hours of training in one or a combination of the ff MCLE CPD administrative / corporate/ legal or other relevant training', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(155, 160, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(156, 161, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(157, 162, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(158, 163, NULL, 'Bachelor of Laws', 'None required', 'None required', 'R.A. 1080 (Bar)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(159, 164, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '7 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(160, 165, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '8 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(161, 166, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '9 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(162, 167, NULL, 'Bachelor of Science in Legal Management AB Paralegal Studies Law Political Science or other allied courses', 'None required', '10 hours of training relevant to legal work such as legal ethics legal research and writing or legal procedure', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(163, 168, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(164, 169, NULL, 'Masters degree or its equivalent or Certificate of Leadership and Management from the CSC', '5 years of supervisory/management experience', '120 hours of supervisory/management learning and development intervention undertaken within the last 5 years; OR 40 hours MCLE/CPE/CPD and 80 hours management training taken within the last 5 years', 'CS Professional/Second Level Eligibility/RA 1080 (Bar CPA Eng.)', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(165, 170, NULL, 'Completion of two years in college', '1 year of relevant experience in one or a combination of the ff office/procedures/administration quality management and other related disciplines', '4 hours of training in one or a combination of the ff office procedures / administration quality management or relevant training', 'CS Sub-Professional / First Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:52', NULL, 0, NULL, NULL),
	(166, 171, NULL, 'Elementary School Graduate', 'None required', 'None required', 'MC 11 s.1996 as amended - Cat. IV/Drivers License', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(167, 172, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(168, 173, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '3 years of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '16 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(169, 174, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '2 years of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '8 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(170, 175, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '2 years of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '8 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(171, 176, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '2 years of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '8 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(172, 177, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '2 years of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '8 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(173, 178, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '2 years of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '8 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(174, 179, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '1 year of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '4 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(175, 180, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '2 year of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '5 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(176, 181, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '3 year of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '6 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(177, 182, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', '4 year of relevant experience in one or a combination of the ff policy research and regulation analysis operation of markets development plannin legislatin and other related disciplines', '7 hours of training in corporate governance public / competition policy regulation trade industry economics policy research development planning operations of market or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(178, 183, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(179, 184, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(180, 185, NULL, 'Bachelors degree in Economics Public Administration/Policy or related field preferably with degree in law', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(181, 186, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(182, 187, NULL, 'Bachelors degree in the field of Arts and Sciences Communications Economics Information Technology or related field', '3 years of relevant experience in one or a combination of the ff communication research and other related disciplines', '16 hours of training in one or a combination of the ff communication research economics information technology competition policy corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(183, 188, NULL, 'Bachelors degree in the field of Arts and Sciences Communications Economics Information Technology or related field', '2 years of relevant experience in one or a combination of the ff communication research and other related disciplines', '8 hours of training in one or a combination of the ff communication research economics information technology competition policy corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(184, 189, NULL, 'Bachelors degree in the field of Arts and Sciences Communications Economics Information Technology or related field', '2 years of relevant experience in one or a combination of the ff communication research and other related disciplines', '8 hours of training in one or a combination of the ff communication research economics information technology competition policy corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(185, 190, NULL, 'Bachelors degree in the field of Arts and Sciences Communications Economics Information Technology or related field', '1 year of relevant experience in one or a combination of the ff communication research and other related disciplines', '4 hours of training in one or a combination of the ff communication research economics information technology competition policy corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(186, 191, NULL, 'Bachelors degree in the field of Arts and Sciences Communications Economics Information Technology or related field', '1 year of relevant experience in one or a combination of the ff communication research and other related disciplines', '4 hours of training in one or a combination of the ff communication research economics information technology competition policy corporate / administrative/ legal proceedings or other relevant training', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(187, 192, NULL, 'Bachelors degree in the field of Arts and Sciences Communications Economics Information Technology or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(188, 193, NULL, 'Masters degree OR Certificate of Leadership and Management from the CSC', ' 4 years of supervisory/ management experience', ' 40 hours of supervisory / management learning and development intervention undertaken within the last 5 years', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(189, 194, NULL, 'Bachelors degree in Communications Economics Law Psychology or related field', '3 years of relevant experience in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '16 hours of training in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; communication human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(190, 195, NULL, 'Bachelors degree in Communications Economics Law Psychology or related field', '2 years of relevant experience in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '8 hours of training in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; communication human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(191, 196, NULL, 'Bachelors degree in Communications Economics Law Psychology or related field', '2 years of relevant experience in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '8 hours of training in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; communication human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:53', NULL, 0, NULL, NULL),
	(192, 197, NULL, 'Bachelors degree in Communications Economics Law Psychology or related field', '1 year of relevant experience in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; communication human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:54', NULL, 0, NULL, NULL),
	(193, 198, NULL, 'Bachelors degree in Communications Economics Law Psychology or related field', '1 year of relevant experience in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; human resources; organizational development; corporate/administrative/legal proceedings and other related disciplines', '4 hours of training in one or a combination of the ff development and management of course curriculum outlines; deveopment monitoing and attainment of resource requirements for training programs; execution of training programs; communication human resources; organizational development; corporate/administrative/legal proceedings or other relevant trainings', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:54', NULL, 0, NULL, NULL),
	(194, 199, NULL, 'Bachelors degree in Communications Economics Law Psychology or related field', 'None required', 'None required', 'Career  Service Professional/ Second Level Eligibility', NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-05-08 15:43:54', NULL, 0, NULL, NULL),
	(195, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 2000.000, 6000.000, 45269.000, 45269.000, 5000.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-31', 1, 0, 0, 1, 0, '2019-05-08 08:08:38', '2019-05-08 08:08:49', 1, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.job_offers
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `joboffer_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.job_offers: ~1 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
INSERT INTO `job_offers` (`id`, `applicant_id`, `pera_amount`, `clothing_allowance`, `year_end_bonus`, `cash_gift`, `executive_director`, `job_offer_date`, `joboffer_status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(4, 2, 2000.00, 6000.00, 22532.00, NULL, NULL, NULL, 1, 1, 1, '2019-05-10 04:15:27', '2019-05-10 04:57:10', NULL);
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.matrix_qualifications
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `isc_chairperson` varchar(50) DEFAULT NULL,
  `isc_member_one` varchar(50) DEFAULT NULL,
  `isc_member_two` varchar(50) DEFAULT NULL,
  `ea_representative` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.matrix_qualifications: ~4 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
INSERT INTO `matrix_qualifications` (`id`, `applicant_id`, `remarks`, `status`, `isc_chairperson`, `isc_member_one`, `isc_member_two`, `ea_representative`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-08 08:48:03', '2019-05-08 08:48:03'),
	(2, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, '2019-05-09 03:08:17', '2019-05-09 03:08:17'),
	(3, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, '2019-05-09 03:17:20', '2019-05-09 03:17:20'),
	(4, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, '2019-05-09 05:42:48', '2019-05-09 05:42:48');
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.migrations: ~21 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_07_13_041020_create_employees_table', 1),
	(4, '2018_07_13_042843_create_employee_information_table', 1),
	(5, '2018_08_14_084139_create_config_table', 1),
	(6, '2018_08_20_100207_create_jobs_table', 1),
	(7, '2018_09_04_093333_create_applicants_table', 1),
	(8, '2018_09_04_124945_create_countries_table', 1),
	(9, '2018_09_12_054802_add_reference_no_column_to_applicants_table', 1),
	(10, '2018_09_19_004331_create_evaluations_table', 1),
	(11, '2018_09_23_084430_alter_plantilla_item_number_column_to_varchar', 1),
	(12, '2018_09_25_031118_create_recommendations_table', 1),
	(13, '2018_09_26_081458_create_appointments_table', 1),
	(14, '2018_09_26_121939_create_job_offers_table', 1),
	(15, '2018_09_30_074257_create_assumptions_table', 1),
	(16, '2018_09_30_081027_create_attestations_table', 1),
	(17, '2018_10_06_050142_alter_columns_rating_data_evaluations_table', 1),
	(18, '2018_10_10_020636_add_column_publication_to_applicants_table', 1),
	(19, '2018_10_17_130131_alter_column_annual_basic_salary_on_jobs_table', 1),
	(20, '2018_10_18_044949_create_matrix_qualifications_table', 1),
	(21, '2018_10_31_090033_add_column_job_id_to_evaluations_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `description` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.modules: ~24 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `name`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'psipop', 'PSIPOP', NULL, NULL, NULL, NULL, NULL),
	(2, 'jobs', 'Jobs', NULL, NULL, NULL, NULL, NULL),
	(4, 'applicant', 'Applicant', NULL, NULL, NULL, NULL, NULL),
	(5, 'matrix-qualification', 'Matrix of Qualification', NULL, NULL, NULL, NULL, NULL),
	(6, 'interviews', 'Interviews', NULL, NULL, NULL, NULL, NULL),
	(7, 'evaluation', 'Evaluation', NULL, NULL, NULL, NULL, NULL),
	(8, 'comparative-ranking', 'Comparative Ranking', NULL, NULL, NULL, NULL, NULL),
	(9, 'recommendation', 'Recommendation', NULL, NULL, NULL, NULL, NULL),
	(10, 'joboffer', 'Job Offer', NULL, NULL, NULL, NULL, NULL),
	(11, 'appointment-form', 'Form', NULL, NULL, NULL, NULL, NULL),
	(12, 'appointment-issued', 'Transmital', NULL, NULL, NULL, NULL, NULL),
	(13, 'appointment-processing', 'Processing Checklist', NULL, NULL, NULL, NULL, NULL),
	(14, 'appointment-requirements', 'Pre Employment Requirements', NULL, NULL, NULL, NULL, NULL),
	(15, 'assumption', 'Assumption to Duty', NULL, NULL, NULL, NULL, NULL),
	(16, 'attestation', 'Transmital for Attestation', NULL, NULL, NULL, NULL, NULL),
	(17, 'appointment-casual', 'Plantilla of Casual Appointment', NULL, NULL, NULL, NULL, NULL),
	(18, 'position-descriptions', 'Position Description', NULL, NULL, NULL, NULL, NULL),
	(19, 'oath-office', 'Oath of Office', NULL, NULL, NULL, NULL, NULL),
	(20, 'erasure_alterations', 'Erasure and Alteration', NULL, NULL, NULL, NULL, NULL),
	(21, 'acceptance_resignation', 'Acceptance of Resignation', NULL, NULL, NULL, NULL, NULL),
	(22, 'boarding_applicant', 'Applicant Onboarding', NULL, NULL, NULL, NULL, NULL),
	(23, 'report', 'Report', NULL, NULL, NULL, NULL, NULL),
	(24, 'users', 'User', NULL, NULL, NULL, NULL, NULL),
	(25, 'access_types', 'Access Types', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.oath_offices
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.oath_offices: ~2 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
INSERT INTO `oath_offices` (`id`, `applicant_id`, `person_administering`, `oath_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, 1, 1, '2019-05-09 06:33:19', '2019-05-09 08:59:32', NULL),
	(2, 2, NULL, NULL, NULL, 1, '2019-05-10 05:40:22', '2019-05-10 05:40:22', NULL);
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.offices
CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.offices: ~7 rows (approximately)
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Office of the Chairman', NULL, NULL, NULL, NULL, NULL),
	(2, 'Office of the Executive Director', NULL, NULL, NULL, NULL, NULL),
	(3, 'Administrative and Legal Office', NULL, NULL, NULL, NULL, NULL),
	(4, 'Financial Planning and Management Office', NULL, NULL, NULL, NULL, NULL),
	(5, 'Competition Enforcement Office', NULL, NULL, NULL, NULL, NULL),
	(6, 'Merger and Acquisition Office', NULL, NULL, NULL, NULL, NULL),
	(7, 'Policy Research and Knowledge Management Office', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.position_descriptions
CREATE TABLE IF NOT EXISTS `position_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `immediate_supervisor` varchar(225) DEFAULT NULL,
  `higher_supervisor` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `used_tools` text,
  `managerial` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `non_supervisor` int(11) DEFAULT NULL,
  `staff` int(11) DEFAULT NULL,
  `general_public` int(11) DEFAULT NULL,
  `other_agency` varchar(50) DEFAULT NULL,
  `other_contacts` varchar(50) DEFAULT NULL,
  `office_work` int(11) DEFAULT NULL,
  `field_work` int(11) DEFAULT NULL,
  `other_condition` varchar(50) DEFAULT NULL,
  `description_function_unit` text,
  `description_function_position` text,
  `compentency_1` text,
  `compentency_2` text,
  `percentage_work_time` varchar(50) DEFAULT NULL,
  `responsibilities` varchar(50) DEFAULT NULL,
  `supervisor_name` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.position_descriptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `position_descriptions` DISABLE KEYS */;
INSERT INTO `position_descriptions` (`id`, `applicant_id`, `immediate_supervisor`, `higher_supervisor`, `item_number`, `position_title`, `used_tools`, `managerial`, `supervisor`, `non_supervisor`, `staff`, `general_public`, `other_agency`, `other_contacts`, `office_work`, `field_work`, `other_condition`, `description_function_unit`, `description_function_position`, `compentency_1`, `compentency_2`, `percentage_work_time`, `responsibilities`, `supervisor_name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 2, 'Executive Director', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '0', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-05-10 05:44:25', '2019-05-10 05:50:53', NULL);
/*!40000 ALTER TABLE `position_descriptions` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.preliminary_evaluation
CREATE TABLE IF NOT EXISTS `preliminary_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isc_chairperson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_one` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_two` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ea_representative` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.preliminary_evaluation: ~0 rows (approximately)
/*!40000 ALTER TABLE `preliminary_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `preliminary_evaluation` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.psipop
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `employee_status` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `salary_grade` int(11) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `code` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `type` varchar(225) DEFAULT NULL,
  `annual_authorized_salary` varchar(50) DEFAULT NULL,
  `annual_actual_salary` decimal(10,2) DEFAULT NULL,
  `ppa_attribution` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.psipop: ~199 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
INSERT INTO `psipop` (`id`, `applicant_id`, `employee_status`, `office_id`, `division_id`, `department_id`, `section_id`, `salary_grade`, `step`, `position_title`, `item_number`, `code`, `level`, `type`, `annual_authorized_salary`, `annual_actual_salary`, `ppa_attribution`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, NULL, 1, 1, 1, NULL, NULL, 19, NULL, 'Chairman', 'PHCC-COCH-1-2016', NULL, NULL, NULL, NULL, 6337616.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(2, NULL, 1, 1, 1, NULL, NULL, 18, NULL, 'Commissioner', 'PHCC-COM-1-2016', NULL, NULL, NULL, NULL, 4251744.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(3, NULL, 1, 1, 1, NULL, NULL, 18, NULL, 'Commissioner', 'PHCC-COM-2-2016', NULL, NULL, NULL, NULL, 4251744.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(4, NULL, 1, 1, 1, NULL, NULL, 18, NULL, 'Commissioner', 'PHCC-COM-3-2016', NULL, NULL, NULL, NULL, 4251744.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(5, NULL, 1, 1, 1, NULL, NULL, 18, NULL, 'Commissioner', 'PHCC-COM-4-2016', NULL, NULL, NULL, NULL, 4251744.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(6, NULL, 1, 1, 1, NULL, NULL, 15, NULL, 'Head Executive Assistant', 'PHCC-HEA-1-2016', NULL, NULL, NULL, NULL, 1470000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(7, NULL, 1, 1, 1, NULL, NULL, 11, NULL, 'Executive Assistant IV', 'PHCC-EXA4-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(8, NULL, 1, 1, 1, NULL, NULL, 11, NULL, 'Executive Assistant IV', 'PHCC-EXA4-2-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(9, NULL, 1, 1, 1, NULL, NULL, 11, NULL, 'Executive Assistant IV', 'PHCC-EXA4-3-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(10, NULL, 1, 1, 1, NULL, NULL, 11, NULL, 'Executive Assistant IV', 'PHCC-EXA4-4-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(11, NULL, 1, 1, 1, NULL, NULL, 11, NULL, 'Executive Assistant IV', 'PHCC-EXA4-5-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:02', NULL, NULL),
	(12, NULL, 1, 1, 1, NULL, NULL, 8, NULL, 'Board Secretary III', 'PHCC-BS3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(13, 1, 1, 1, 1, NULL, NULL, 8, NULL, 'Executive Assistant III', 'PHCC-EXA3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', '2019-05-09 04:05:18', NULL),
	(14, NULL, 1, 1, 1, NULL, NULL, 8, NULL, 'Executive Assistant III', 'PHCC-EXA3-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(15, NULL, 1, 1, 1, NULL, NULL, 8, NULL, 'Executive Assistant III', 'PHCC-EXA3-3-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(16, NULL, 1, 1, 1, NULL, NULL, 8, NULL, 'Executive Assistant III', 'PHCC-EXA3-4-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(17, NULL, 1, 1, 1, NULL, NULL, 8, NULL, 'Executive Assistant III', 'PHCC-EXA3-5-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(18, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Board Secretary II', 'PHCC-BS2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(19, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Board Secretary II', 'PHCC-BS2-2-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(20, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Private Secretary II', 'PHCC-PSEC2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(21, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Private Secretary II', 'PHCC-PSEC2-2-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(22, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Private Secretary II', 'PHCC-PSEC2-3-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(23, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Private Secretary II', 'PHCC-PSEC2-4-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(24, NULL, 1, 1, 1, NULL, NULL, 6, NULL, 'Private Secretary II', 'PHCC-PSEC2-5-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(25, NULL, 1, 1, 1, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-1-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(26, NULL, 1, 1, 1, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-2-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(27, NULL, 1, 1, 1, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-3-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(28, NULL, 1, 1, 1, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-4-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:03', NULL, NULL),
	(29, NULL, 1, 1, 1, NULL, NULL, 2, NULL, 'Chauffeur I', 'PHCC-CFER1-1-2016', NULL, NULL, NULL, NULL, 179232.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(30, NULL, 1, 1, 1, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-1-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(31, NULL, 1, 1, 1, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-2-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(32, NULL, 1, 1, 1, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-3-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(33, NULL, 1, 1, 1, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-4-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(34, NULL, 1, 2, 2, NULL, NULL, 17, NULL, 'Executive Director', 'PHCC-EXED-1-2016', NULL, NULL, NULL, NULL, 2642400.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(35, NULL, 1, 2, 2, NULL, NULL, 8, NULL, 'Executive Assistant III', 'PHCC-EXA3-6-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(36, NULL, 1, 2, 2, NULL, NULL, 6, NULL, 'Executive Assistant II', 'PHCC-EXA2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(37, NULL, 1, 2, 2, NULL, NULL, 4, NULL, 'Private Secretary I', 'PHCC-PSEC1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(38, NULL, 1, 2, 2, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-5-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(39, NULL, 1, 2, 2, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-5-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(40, NULL, 1, 3, 3, NULL, NULL, 16, NULL, 'Director IV', 'PHCC-DIR4-1-2016', NULL, NULL, NULL, NULL, 2031600.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(41, NULL, 1, 3, 3, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-6-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(42, NULL, 1, 3, 3, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-6-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(43, NULL, 1, 3, 4, NULL, NULL, 13, NULL, 'Chief Administrative Officer', 'PHCC-CADOF-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(44, NULL, 1, 3, 4, NULL, NULL, 11, NULL, 'Supervising Administrative Officer', 'PHCC-SADOF-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(45, NULL, 1, 3, 4, NULL, NULL, 8, NULL, 'Administrative Officer III', 'PHCC-ADO3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(46, NULL, 1, 3, 4, NULL, NULL, 8, NULL, 'Cashier III', 'PHCC-CASH3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:04', NULL, NULL),
	(47, NULL, 1, 3, 4, NULL, NULL, 8, NULL, 'Records Officer III', 'PHCC-RO3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(48, NULL, 1, 3, 4, NULL, NULL, 8, NULL, 'Supply Officer III', 'PHCC-SU03-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(49, NULL, 1, 3, 4, NULL, NULL, 6, NULL, 'Administrative Officer II', 'PHCC-ADO2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(50, NULL, 1, 3, 4, NULL, NULL, 6, NULL, 'Administrative Officer II', 'PHCC-ADO2-2-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(51, NULL, 1, 3, 4, NULL, NULL, 6, NULL, 'Administrative Officer II', 'PHCC-ADO2-3-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(52, NULL, 1, 3, 4, NULL, NULL, 6, NULL, 'Cashier II', 'PHCC-CASH2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(53, NULL, 1, 3, 4, NULL, NULL, 6, NULL, 'Records Officer II', 'PHCC-RO2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(54, NULL, 1, 3, 4, NULL, NULL, 6, NULL, 'Supply Officer II', 'PHCC-SU02-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(55, NULL, 1, 3, 4, NULL, NULL, 4, NULL, 'Cashier I', 'PHCC-CASH1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(56, NULL, 1, 3, 4, NULL, NULL, 4, NULL, 'Records Officer I', 'PHCC-RO1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(57, NULL, 1, 3, 4, NULL, NULL, 4, NULL, 'Supply Officer I', 'PHCC-SU01-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(58, NULL, 1, 3, 5, NULL, NULL, 13, NULL, 'Chief Administrative Officer', 'PHCC-CADOF-2-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(59, NULL, 1, 3, 5, NULL, NULL, 11, NULL, 'Supervising Administrative Officer', 'PHCC-SADOF-2-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(60, NULL, 1, 3, 5, NULL, NULL, 8, NULL, 'Human Resource Management Officer III', 'PHCC-HRMO3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(61, NULL, 1, 3, 5, NULL, NULL, 8, NULL, 'Human Resource Management Officer III', 'PHCC-HRMO3-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(62, NULL, 1, 3, 5, NULL, NULL, 6, NULL, 'Human Resource Management Officer II', 'PHCC-HRMO2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(63, NULL, 1, 3, 5, NULL, NULL, 6, NULL, 'Human Resource Management Officer II', 'PHCC-HRMO2-2-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:05', NULL, NULL),
	(64, NULL, 1, 3, 5, NULL, NULL, 4, NULL, 'Human Resource Management Officer I', 'PHCC-HRMO1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(65, NULL, 1, 3, 6, NULL, NULL, 13, NULL, 'Information Technology Officer III', 'PHCC-ITO3-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(66, NULL, 1, 3, 6, NULL, NULL, 11, NULL, 'Information Technology Officer II', 'PHCC-ITO2-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(67, NULL, 1, 3, 6, NULL, NULL, 8, NULL, 'Information Technology Officer I', 'PHCC-ITO1-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(68, NULL, 1, 3, 6, NULL, NULL, 8, NULL, 'Information Technology Officer I', 'PHCC-ITO1-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(69, NULL, 1, 3, 6, NULL, NULL, 6, NULL, 'Information Systems Analyst II', 'PHCC-INFOSA2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(70, NULL, 1, 3, 6, NULL, NULL, 4, NULL, 'Information Systems Analyst I', 'PHCC-INFOSA1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(71, NULL, 1, 3, 7, NULL, NULL, 14, NULL, 'Attorney V', 'PHCC-ATY5-1-2016', NULL, NULL, NULL, NULL, 1350000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(72, NULL, 1, 3, 7, NULL, NULL, 12, NULL, 'Attorney IV', 'PHCC-ATY4-1-2016', NULL, NULL, NULL, NULL, 886800.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(73, NULL, 1, 3, 7, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-1-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(74, NULL, 1, 3, 7, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-2-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(75, NULL, 1, 3, 7, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(76, NULL, 1, 3, 7, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:06', NULL, NULL),
	(77, NULL, 1, 3, 7, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-3-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(78, NULL, 1, 3, 7, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-4-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(79, NULL, 1, 3, 7, NULL, NULL, 7, NULL, 'Attorney I', 'PHCC-ATY1-1-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(80, NULL, 1, 3, 7, NULL, NULL, 7, NULL, 'Attorney I', 'PHCC-ATY1-2-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(81, NULL, 1, 3, 7, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-1-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(82, NULL, 1, 3, 7, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-2-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(83, NULL, 1, 4, 3, NULL, NULL, 16, NULL, 'Director IV', 'PHCC-DIR4-2-2016', NULL, NULL, NULL, NULL, 2031600.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(84, NULL, 1, 4, 3, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-7-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(85, NULL, 1, 4, 3, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-7-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(86, NULL, 1, 4, 8, NULL, NULL, 13, NULL, 'Chief Accountant', 'PHCC-CACT-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(87, NULL, 1, 4, 8, NULL, NULL, 11, NULL, 'Accountant IV', 'PHCC-A4-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(88, NULL, 1, 4, 8, NULL, NULL, 9, NULL, 'Accountant III', 'PHCC-A3-1-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(89, NULL, 1, 4, 8, NULL, NULL, 9, NULL, 'Accountant III', 'PHCC-A3-2-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(90, NULL, 1, 4, 8, NULL, NULL, 8, NULL, 'Accountant II', 'PHCC-A2-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(91, NULL, 1, 4, 8, NULL, NULL, 8, NULL, 'Accountant II', 'PHCC-A2-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(92, NULL, 1, 4, 8, NULL, NULL, 5, NULL, 'Accountant I', 'PHCC-A1-1-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(93, NULL, 1, 4, 9, NULL, NULL, 13, NULL, 'Chief Administrative Officer', 'PHCC-CADOF-3-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:07', NULL, NULL),
	(94, NULL, 1, 4, 9, NULL, NULL, 11, NULL, 'Supervising Administrative Officer', 'PHCC-SADOF-3-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(95, NULL, 1, 4, 9, NULL, NULL, 8, NULL, 'Budget Officer III', 'PHCC-BUDO3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(96, NULL, 1, 4, 9, NULL, NULL, 8, NULL, 'Budget Officer III', 'PHCC-BUDO3-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(97, NULL, 1, 4, 9, NULL, NULL, 6, NULL, 'Budget Officer II', 'PHCC-BUDO2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(98, NULL, 1, 4, 9, NULL, NULL, 6, NULL, 'Budget Officer II', 'PHCC-BUDO2-2-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(99, NULL, 1, 4, 9, NULL, NULL, 4, NULL, 'Budget Officer I', 'PHCC-BUDO1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(100, NULL, 1, 4, 10, NULL, NULL, 13, NULL, 'Planning Officer V', 'PHCC-PLO5-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(101, NULL, 1, 4, 10, NULL, NULL, 11, NULL, 'Planning Officer IV', 'PHCC-PLO4-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(102, NULL, 1, 4, 10, NULL, NULL, 8, NULL, 'Administrative Officer III', 'PHCC-ADO3-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(103, NULL, 1, 4, 10, NULL, NULL, 8, NULL, 'Planning Officer III', 'PHCC-PLO3-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(104, NULL, 1, 4, 10, NULL, NULL, 8, NULL, 'Planning Officer III', 'PHCC-PLO3-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(105, NULL, 1, 4, 10, NULL, NULL, 6, NULL, 'Administrative Officer II', 'PHCC-ADO2-4-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(106, NULL, 1, 4, 10, NULL, NULL, 6, NULL, 'Planning Officer II', 'PHCC-PLO2-1-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(107, NULL, 1, 4, 10, NULL, NULL, 6, NULL, 'Planning Officer II', 'PHCC-PLO2-2-2016', NULL, NULL, NULL, NULL, 364212.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', NULL, NULL),
	(108, NULL, 1, 4, 10, NULL, NULL, 4, NULL, 'Administrative Officer I', 'PHCC-ADO1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:08', '2019-05-09 05:42:30', NULL),
	(109, NULL, 1, 4, 10, NULL, NULL, 4, NULL, 'Planning Officer I', 'PHCC-PLO1-1-2016', NULL, NULL, NULL, NULL, 270384.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(110, NULL, 1, 5, 3, NULL, NULL, 16, NULL, 'Director IV', 'PHCC-DIR4-3-2016', NULL, NULL, NULL, NULL, 2031600.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(111, NULL, 1, 5, 3, NULL, NULL, 15, NULL, 'Director III', 'PHCC-DIR3-1-2016', NULL, NULL, NULL, NULL, 1470000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(112, NULL, 1, 5, 3, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-8-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(113, NULL, 1, 5, 3, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-8-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(114, NULL, 1, 5, 11, NULL, NULL, 13, NULL, 'Investigation Agent V', 'PHCC-INVA5-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(115, NULL, 1, 5, 11, NULL, NULL, 12, NULL, 'Investigation Agent IV', 'PHCC-INVA4-1-2016', NULL, NULL, NULL, NULL, 886800.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(116, NULL, 1, 5, 11, NULL, NULL, 11, NULL, 'Investigation Agent III', 'PHCC-INVA3-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:09', NULL, NULL),
	(117, NULL, 1, 5, 11, NULL, NULL, 11, NULL, 'Investigation Agent III', 'PHCC-INVA3-2-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(118, NULL, 1, 5, 11, NULL, NULL, 10, NULL, 'Investigation Agent II', 'PHCC-INVA2-1-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(119, NULL, 1, 5, 11, NULL, NULL, 10, NULL, 'Investigation Agent II', 'PHCC-INVA2-2-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(120, NULL, 1, 5, 11, NULL, NULL, 8, NULL, 'Investigation Agent I', 'PHCC-INVA1-1-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(121, NULL, 1, 5, 11, NULL, NULL, 8, NULL, 'Investigation Agent I', 'PHCC-INVA1-2-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(122, NULL, 1, 5, 11, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-3-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(123, NULL, 1, 5, 11, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-4-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(124, NULL, 1, 5, 12, NULL, NULL, 14, NULL, 'Attorney V', 'PHCC-ATY5-2-2016', NULL, NULL, NULL, NULL, 1350000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(125, NULL, 1, 5, 12, NULL, NULL, 12, NULL, 'Attorney IV', 'PHCC-ATY4-2-2016', NULL, NULL, NULL, NULL, 886800.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(126, NULL, 1, 5, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-3-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(127, NULL, 1, 5, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-4-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(128, NULL, 1, 5, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-5-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(129, NULL, 1, 5, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-6-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:10', NULL, NULL),
	(130, NULL, 1, 5, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-5-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(131, NULL, 1, 5, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-6-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(132, NULL, 1, 5, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-7-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(133, NULL, 1, 5, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-8-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(134, NULL, 1, 5, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-5-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(135, NULL, 1, 5, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-6-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(136, NULL, 1, 5, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-7-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(137, NULL, 1, 5, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-8-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(138, NULL, 1, 6, 3, NULL, NULL, 16, NULL, 'Director IV', 'PHCC-DIR4-4-2016', NULL, NULL, NULL, NULL, 2031600.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(139, NULL, 1, 6, 3, NULL, NULL, 15, NULL, 'Director III', 'PHCC-DIR3-2-2016', NULL, NULL, NULL, NULL, 1470000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(140, NULL, 1, 6, 3, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-9-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(141, NULL, 1, 6, 3, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-9-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(142, NULL, 1, 6, 13, NULL, NULL, 14, NULL, 'Attorney V', 'PHCC-ATY5-3-2016', NULL, NULL, NULL, NULL, 1350000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(143, NULL, 1, 6, 13, NULL, NULL, 12, NULL, 'Attorney IV', 'PHCC-ATY4-3-2016', NULL, NULL, NULL, NULL, 886800.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(144, NULL, 1, 6, 13, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-7-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(145, NULL, 1, 6, 13, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-8-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(146, NULL, 1, 6, 13, NULL, NULL, 9, NULL, 'Economist III', 'PHCC-ECO3-1-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:11', NULL, NULL),
	(147, NULL, 1, 6, 13, NULL, NULL, 9, NULL, 'Economist III', 'PHCC-ECO3-2-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(148, NULL, 1, 6, 13, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-9-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(149, NULL, 1, 6, 13, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-10-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(150, NULL, 1, 6, 13, NULL, NULL, 7, NULL, 'Economist II', 'PHCC-ECO2-1-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(151, NULL, 1, 6, 13, NULL, NULL, 7, NULL, 'Economist II', 'PHCC-ECO2-2-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(152, NULL, 1, 6, 13, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-9-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(153, NULL, 1, 6, 13, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-10-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:12', NULL, NULL),
	(154, NULL, 1, 6, 12, NULL, NULL, 14, NULL, 'Attorney V', 'PHCC-ATY5-4-2016', NULL, NULL, NULL, NULL, 1350000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(155, NULL, 1, 6, 12, NULL, NULL, 12, NULL, 'Attorney IV', 'PHCC-ATY4-4-2016', NULL, NULL, NULL, NULL, 886800.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(156, NULL, 1, 6, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-9-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(157, NULL, 1, 6, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-10-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(158, NULL, 1, 6, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-11-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(159, NULL, 1, 6, 12, NULL, NULL, 10, NULL, 'Attorney III', 'PHCC-ATY3-12-2016', NULL, NULL, NULL, NULL, 709200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(160, NULL, 1, 6, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-11-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(161, NULL, 1, 6, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-12-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(162, NULL, 1, 6, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-13-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(163, NULL, 1, 6, 12, NULL, NULL, 8, NULL, 'Attorney II', 'PHCC-ATY2-14-2016', NULL, NULL, NULL, NULL, 543228.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(164, NULL, 1, 6, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-11-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(165, NULL, 1, 6, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-12-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(166, NULL, 1, 6, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-13-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(167, NULL, 1, 6, 12, NULL, NULL, 5, NULL, 'Legal Assistant II', 'PHCC-LEA2-14-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(168, NULL, 1, 7, 3, NULL, NULL, 16, NULL, 'Director IV', 'PHCC-DIR4-5-2016', NULL, NULL, NULL, NULL, 2031600.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(169, NULL, 1, 7, 3, NULL, NULL, 15, NULL, 'Director III', 'PHCC-DIR3-3-2016', NULL, NULL, NULL, NULL, 1470000.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(170, NULL, 1, 7, 3, NULL, NULL, 3, NULL, 'Secretary II', 'PHCC-SEC2-10-2016', NULL, NULL, NULL, NULL, 215700.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(171, NULL, 1, 7, 3, NULL, NULL, 1, NULL, 'Driver II', 'PHCC-DRV2-10-2016', NULL, NULL, NULL, NULL, 169044.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(172, NULL, 1, 7, 14, NULL, NULL, 13, NULL, 'Policy Research Officer V', 'PHCC-CMPRO5-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(173, NULL, 1, 7, 14, NULL, NULL, 11, NULL, 'Policy Research Officer IV', 'PHCC-CMPRO4-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(174, NULL, 1, 7, 14, NULL, NULL, 9, NULL, 'Policy Research Officer III', 'PHCC-CMPRO3-1-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(175, NULL, 1, 7, 14, NULL, NULL, 9, NULL, 'Policy Research Officer III', 'PHCC-CMPRO3-2-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(176, NULL, 1, 7, 14, NULL, NULL, 9, NULL, 'Policy Research Officer III', 'PHCC-CMPRO3-3-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(177, NULL, 1, 7, 14, NULL, NULL, 9, NULL, 'Policy Research Officer III', 'PHCC-CMPRO3-4-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(178, NULL, 1, 7, 14, NULL, NULL, 9, NULL, 'Policy Research Officer III', 'PHCC-CMPRO3-5-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:13', NULL, NULL),
	(179, NULL, 1, 7, 14, NULL, NULL, 7, NULL, 'Policy Research Officer II', 'PHCC-CMPRO2-1-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(180, NULL, 1, 7, 14, NULL, NULL, 7, NULL, 'Policy Research Officer II', 'PHCC-CMPRO2-2-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(181, NULL, 1, 7, 14, NULL, NULL, 7, NULL, 'Policy Research Officer II', 'PHCC-CMPRO2-3-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(182, NULL, 1, 7, 14, NULL, NULL, 7, NULL, 'Policy Research Officer II', 'PHCC-CMPRO2-4-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(183, NULL, 1, 7, 14, NULL, NULL, 5, NULL, 'Policy Research Officer I', 'PHCC-CMPRO1-1-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(184, NULL, 1, 7, 14, NULL, NULL, 5, NULL, 'Policy Research Officer I', 'PHCC-CMPRO1-2-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(185, NULL, 1, 7, 14, NULL, NULL, 5, NULL, 'Policy Research Officer I', 'PHCC-CMPRO1-3-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(186, NULL, 1, 7, 15, NULL, NULL, 13, NULL, 'Information Officer V', 'PHCC-INFO5-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(187, NULL, 1, 7, 15, NULL, NULL, 11, NULL, 'Information Officer IV', 'PHCC-INFO4-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(188, NULL, 1, 7, 15, NULL, NULL, 9, NULL, 'Information Officer III', 'PHCC-INFO3-1-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(189, NULL, 1, 7, 15, NULL, NULL, 9, NULL, 'Information Officer III', 'PHCC-INFO3-2-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(190, NULL, 1, 7, 15, NULL, NULL, 7, NULL, 'Information Officer II', 'PHCC-INFO2-1-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(191, NULL, 1, 7, 15, NULL, NULL, 7, NULL, 'Information Officer II', 'PHCC-INFO2-2-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(192, NULL, 1, 7, 15, NULL, NULL, 5, NULL, 'Information Officer I', 'PHCC-INFO1-1-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(193, NULL, 1, 7, 0, NULL, NULL, 13, NULL, 'Training Specialist V', 'PHCC-TRNSP5-1-2016', NULL, NULL, NULL, NULL, 1042788.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(194, NULL, 1, 7, 16, NULL, NULL, 11, NULL, 'Training Specialist IV', 'PHCC-TRNSP4-1-2016', NULL, NULL, NULL, NULL, 833988.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(195, NULL, 1, 7, 16, NULL, NULL, 9, NULL, 'Training Specialist III', 'PHCC-TRNSP3-1-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(196, NULL, 1, 7, 16, NULL, NULL, 9, NULL, 'Training Specialist III', 'PHCC-TRNSP3-2-2016', NULL, NULL, NULL, NULL, 667200.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(197, NULL, 1, 7, 16, NULL, NULL, 7, NULL, 'Training Specialist II', 'PHCC-TRNSP2-1-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(198, NULL, 1, 7, 16, NULL, NULL, 7, NULL, 'Training Specialist II', 'PHCC-TRNSP2-2-2016', NULL, NULL, NULL, NULL, 482028.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL),
	(199, NULL, 1, 7, 16, NULL, NULL, 5, NULL, 'Training Specialist I', 'PHCC-TRNSP1-1-2016', NULL, NULL, NULL, NULL, 328968.00, NULL, 0, NULL, NULL, '2019-05-08 15:28:14', NULL, NULL);
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.recommendations
CREATE TABLE IF NOT EXISTS `recommendations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `sign_one` varchar(225) DEFAULT NULL,
  `sign_two` varchar(225) DEFAULT NULL,
  `sign_three` varchar(225) DEFAULT NULL,
  `sign_four` varchar(225) DEFAULT NULL,
  `sign_five` varchar(225) DEFAULT NULL,
  `prepared_by` varchar(225) DEFAULT NULL,
  `recommend_status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.recommendations: ~0 rows (approximately)
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.resignation_acceptance
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.resignation_acceptance: ~0 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.sections
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.sections: ~0 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.selection_lineup
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  `job_id` int(10) unsigned DEFAULT NULL,
  `er_representative_selected` int(11) DEFAULT '0',
  `chairperson_selected` int(11) DEFAULT '0',
  `hrdd_selected` int(11) DEFAULT '0',
  `hrmo_selected` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.selection_lineup: ~2 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
INSERT INTO `selection_lineup` (`id`, `applicant_id`, `job_id`, `er_representative_selected`, `chairperson_selected`, `hrdd_selected`, `hrmo_selected`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 2, 103, 0, 0, 0, 1, 0, 1, NULL, '2019-05-10 04:15:27', '2019-05-10 04:15:27', NULL),
	(2, 3, 103, 1, 1, 0, 0, 0, 1, NULL, '2019-05-10 04:15:27', '2019-05-10 04:15:27', NULL);
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.trainings
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.trainings: ~4 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` (`id`, `applicant_id`, `title_learning_programs`, `inclusive_date_from`, `inclusive_date_to`, `number_hours`, `ld_type`, `sponsored_by`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Training 1', NULL, NULL, '23', NULL, NULL, NULL, NULL, '2019-05-08 08:10:49', '2019-05-08 08:10:49', NULL),
	(2, 2, 'Training 3', NULL, NULL, '48', NULL, NULL, NULL, NULL, '2019-05-09 03:07:58', '2019-05-09 03:07:58', NULL),
	(3, 3, 'Training 2', NULL, NULL, '32', NULL, NULL, NULL, NULL, '2019-05-09 03:17:04', '2019-05-09 03:17:04', NULL),
	(4, 4, 'Training 4', NULL, NULL, '56', NULL, NULL, NULL, NULL, '2019-05-09 05:42:30', '2019-05-09 05:42:30', NULL);
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_type_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_phcc.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `access_type_id`, `name`, `username`, `email`, `password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Admin', 'admin', NULL, '$2y$10$PNmB6Evj0H4c/6iRMx.P0.XxbT3lJc3EMFnxoEsJd13T.nX4PM70O', 'PCMmyQNaI6qWL6krEAFdrl9HhcPEiarMkOnyig3w9E3yiAcQrE9reec9YKQ1', NULL, 1, '2019-01-26 07:32:58', '2019-04-04 00:45:39'),
	(2, 1, 'Juan', 'admin01', NULL, '$2y$10$CXjhUeDVODAMxNdkBa.30.C34r/aECEkXEgg0XzZMSOuLWQBVpw.O', 'g1VZGuOoksFSkAJRE22PlnROTY2sBM4u3qaSm84c7a3AgFkh5JUG8Sg3wm4T', 1, NULL, '2019-04-03 23:38:17', '2019-04-03 23:38:17');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table rms_phcc.workexperiences
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` varchar(50) DEFAULT NULL,
  `inclusive_date_to` varchar(50) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_phcc.workexperiences: ~0 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
INSERT INTO `workexperiences` (`id`, `applicant_id`, `inclusive_date_from`, `inclusive_date_to`, `position_title`, `department`, `monthly_salary`, `salary_grade`, `status_of_appointment`, `govt_service`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(4, 2, NULL, NULL, 'Finance Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 04:05:18', '2019-05-09 04:05:18', NULL);
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
