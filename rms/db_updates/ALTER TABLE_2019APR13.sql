ALTER TABLE `applicants`
	ADD COLUMN `zip_code` VARCHAR(255) NULL DEFAULT NULL AFTER `gwa`;

ALTER TABLE `eligibilities`
	CHANGE COLUMN `name` `eligibility_ref` INT NULL DEFAULT NULL AFTER `applicant_id`;