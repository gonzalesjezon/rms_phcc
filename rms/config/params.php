<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Parameters
    |--------------------------------------------------------------------------
    |
    | Accessible via config('param.key');
    | eg: config('params._SUPER_ADMIN_ID_')
    | any other location as required by the application or its packages.
    */

    '_SUPER_ADMIN_ID_' => 1,
    'employee_status' => [
        1 => 'Permanent',
        2 => 'Project',
        3 => 'Contract of Service',
        4 => 'Contractual'
    ],
    'nature_of_appointment' => [
        1 => 'Original',
        2 => 'Promotion',
        3 => 'Re-employment'
    ],
    'publication' => [
        'agency' => 'Agency Web Site',
        'csc_bulletin' => 'CSC Bulletin of Vacant Position',
        'newspaper' => 'Newspaper',
        'others' => 'Others',
    ],
    'examination_status' => [
        1 => 'For Examination',
        2 => 'Failed Exam',
        3 => 'No Show',
        4 => 'Reschedule',
        5 => 'Withdrawn',
        6 => 'For Reference',
        7 => 'For Interview',
    ],
    'interview_status' => [
        1 => 'Set Interview',
        5 => 'Reschedule',
        4 => 'No Show',
        7 => 'For Reference',
        6 => 'Withdrawn',
    ],
    'option_1' => [
        1 => 'Occassional',
        2 => 'Frequent'
    ],
    'boarding_status' => [
        1 => 'Hired',
        2 => 'Pending for Requirements'
    ],
    'form_status' => [
        1 => 'Approved',
        2 => 'Disapproved',
        3 => 'Notice of Appointment',
        4 => 'Attestation of Appointment',
    ],
    'months' => [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    ],
    'eligibility_type' => [
        1 => 'Career Service Professional',
        2 => 'Career Service Sub-professional',
        3 => 'RA 1080 (Bar Exams)',
        4 => 'RA 1080 (Board Exams)',
        5 => 'Certified Public Accountant Exams',
        6 => 'Career Executive Service Eligible',
        7 => 'Career Service Executive Eligible',

    ],

    'applicant_status' => [
        1 => 'Qualified',
        2 => 'Not Qualified',
        3 => 'Reference',
        4 => 'Closed Position',
    ],

    'type_of_personnel' => [
        1 => 'Plantilla',
        2 => 'Nonplantilla'
    ],

    'steps' => [
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8'
    ],
    
    'job_grades' => [
        1 => 'JG 1',
        2 => 'JG 2',
        3 => 'JG 3',
        4 => 'JG 4',
        5 => 'JG 5',
        6 => 'JG 6',
        7 => 'JG 7',
        8 => 'JG 8',
        9 => 'JG 9',
        10 => 'JG 10',
        11 => 'JG 11',
        12 => 'JG 12',
        13 => 'JG 13',
        14 => 'JG 14',
        15 => 'JG 15',
        16 => 'JG 16',
        17 => 'JG 17',
        18 => 'JG 18',
        19 => 'JG 19',
        20 => 'JG 20',
        21 => 'JG 21',
        22 => 'JG 22',
        23 => 'JG 23',
        24 => 'JG 24',
        25 => 'JG 25',
        26 => 'JG 26',
        27 => 'JG 27',
        28 => 'JG 28',
        29 => 'JG 29',
        30 => 'JG 30',
        31 => 'JG 31',
        32 => 'JG 32',
        33 => 'JG 33'
    ],

    'recommendations' => [
        1 => 'Recommended',
        2 => 'Recommended with reservation/s',
        3 => 'Not Recommended'
    ],

    'rating_scales' => [
        1 => '1 - Poor',
        2 => '2 - Below Average',
        3 => '3 - Average',
        4 => '4 - Above Average',
        5 => '5 - Excellent'
    ]


];
