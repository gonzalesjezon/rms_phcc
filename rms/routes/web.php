<?php

/**
 * Public Routes
 *
 */
Route::get('/frontend', 'FronEndController@index');
Route::resources(['frontend' => 'FronEndController']);
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/menu', 'HomeController@index')->name('menu');
Route::get('/careers', 'HomeController@careers')->name('careers');
Auth::routes();

/**
 * Routes that require user to be logged in
 * filtered by middleware auth in controller
 */
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/non-plantilla', 'JobsController@nonPlantilla')->name('jobs.nonplantilla');
Route::get('jobs/publish', 'JobsController@publish')->name('jobs.publish');
Route::get('jobs/getPsipop', 'JobsController@getPsipop')->name('jobs.get-psipop');
Route::get('evaluation/rating', 'EvaluationController@rating')->name('evaluation.rating');
Route::get('evaluation/report', 'EvaluationController@evaluationReport')->name('evaluation.report');
Route::get('report/appointments_issued', 'ReportController@appointmentIssued')->name('report.appointments_issued');
Route::get('report/absence_qualified_eligible', 'ReportController@absenceQualifiedEligible')->name('report.absence_qualified_eligible');
Route::get('report/dibar', 'ReportController@dibarReport')->name('report.dibar');
Route::get('report/publication_vacant_position', 'ReportController@vacantPosition')->name('report.publication_vacant_position');
Route::get('report/appointment_form_regulated', 'ReportController@appointmentFormRegulated')->name('report.appointment_form_regulated');
Route::get('report/medical_certificate', 'ReportController@medicalCertificate')->name('report.medical_certificate');
Route::get('report/preliminary_evaluation', 'ReportController@preliminaryEvaluation')->name('report.preliminary_evaluation');
Route::get('report/checklist', 'ReportController@checklistReport')->name('report.checklist');
Route::get('report/appointments_casual', 'ReportController@appointmentCasual')->name('report.appointments_casual');
Route::get('report/accession_form', 'ReportController@accessionForm')->name('report.accession_form');
Route::get('report/separation_form', 'ReportController@sepearationForm')->name('report.separation_form');
Route::get('report/appointment-transmital', 'ReportController@appointmentTransmital')->name('report.appointment-transmital');
Route::get('report/comparative-report', 'ReportController@comparativeReport')->name('report.comparative-report');
Route::get('report/matrix_qualification', 'ReportController@matrixQReport')->name('report.matrix_qualification');
Route::get('report/psb_matrix_qualification', 'ReportController@matrixQPSBReport')->name('report.psb_matrix_qualification');
Route::get('report/matrix_qualification_3', 'ReportController@matrixQThree')->name('report.matrix_qualification_3');
Route::get('report/psb_matrix_recommended', 'ReportController@matrixRecommended')->name('report.psb_matrix_recommended');
Route::get('report/notice_of_appointment', 'ReportController@noticeOfAppointment')->name('report.notice_of_appointment');
Route::get('report/attestation', 'ReportController@attestation')->name('report.attestation');
Route::get('recommendation/report', 'RecommendationController@report')->name('recommendation.report');
Route::get('attestation/report', 'AttestationController@report')->name('attestation.report');
Route::get('comparative-ranking/report', 'ComparativeRankingController@report')->name('comparative-ranking.report');
Route::get('appointment/report', 'AppointmentController@appointmentReport')->name('appointment.report');
Route::get('appointment-checklist/report', 'AppointmentController@report')->name('appointment-checklist.report');
Route::get('appointment/create-appointment', 'AppointmentController@createAppointmentForm')->name('appointment.create-appointment');
Route::get('appointment/report-form', 'AppointmentController@reportForm')->name('appointment.report-form');
Route::get('appointment-form/report', 'AppointmentFormController@report')->name('appointment-form.report');
Route::get('assumption/report', 'AssumptionController@assumptionReport')->name('assumption.report');
Route::get('joboffer/report', 'JobOfferController@report')->name('joboffer.report');
Route::get('preliminary_evaluation/getApplicant', 'PreliminaryEvaluationController@getApplicant')->name('evaluation.getapplicant');

Route::get('examinations/getApplicant', 'ExaminationController@getApplicant')->name('examinations.get-applicant');
Route::get('interviews/getApplicant', 'InterviewController@getApplicant')->name('interviews.get-applicant');
Route::get('oath-office/report', 'OathOfficeController@oathOfficeReport')->name('oath-office.report');
Route::get('position-descriptions/report', 'PositionDescriptionController@posDescriptionReport')->name('position-descriptions.report');
Route::get('erasure_alterations/getApplicant', 'ErasureAlterationController@getApplicant')->name('erasure_alterations.get-applicant');
Route::get('erasure_alterations/report', 'ErasureAlterationController@report')->name('erasure_alterations.report');
Route::get('acceptance_resignation/report', 'AcceptanceResignationController@report')->name('acceptance_resignation.report');
Route::get('matrix-qualification/getApplicant', 'MatrixQualificationController@getApplicant')->name('matrix-qualification.get-applicant');
Route::get('appointment-processing/report', 'AppointmentProcessingController@report')->name('appointment-processing.report');
Route::get('appointment-processing/selectApplicant', 'AppointmentProcessingController@selectApplicant')->name('appointment-processing.select_applicant');

Route::get('selection-lineup/getSelected', 'SelectionLineUpController@getSelected')->name('selection-lineup.get-selected');

Route::get('selection-lineup/getApplicant', 'SelectionLineUpController@getApplicant')->name('selection-lineup.get-applicant');

Route::get('interview_evaluation_ratings/getApplicant', 'InterviewEvaluationRatingsController@getApplicant')->name('interview_evaluation_ratings.get-applicant');
Route::get('interview_evaluation_ratings/report', 'InterviewEvaluationRatingsController@report')->name('interview_evaluation_ratings.report');


Route::get('frontend/exportCSV', 'FronEndController@exportCSV')->name('frontend.export');

Route::get('attestation/sendmail', 'AttestationController@sendmail')->name('attestation.sendmail');

Route::post('evaluation/store-comparative-ranking',
    'EvaluationController@storeComparativeRanking')->name('evaluation.storeComparative');

Route::post('appointment/store-appointment-form',
    'AppointmentController@storeAppointmentForm')->name('appointment.storeAppointmentForm');

Route::post('applicant/delete',
    'ApplicantController@delete')->name('applicant.delete');

Route::post('applicant/saveToDb',
    'ApplicantController@saveToDb')->name('applicant.saveToDb');

Route::post('applicant/sendMail',
    'ApplicantController@sendMail')->name('applicant.sendEmail');

Route::post('interviews/sendMail',
    'InterviewController@sendMail')->name('interviews.sendEmail');

Route::post('appointment-form/sendMail',
    'AppointmentFormController@sendMail')->name('appointment-form.sendEmail');

Route::resources([
    'access_modules' => 'AccessModuleController',
    'users' => 'UsersController',
    'appointment-processing' => 'AppointmentProcessingController',
    'appointment-issued' => 'AppointmentIssuedController',
    'comparative-ranking' => 'ComparativeRankingController',
    'matrix-qualification' => 'MatrixQualificationController',
    'boarding_applicant' => 'BoardApplicantController',
    'acceptance_resignation' => 'AcceptanceResignationController',
    'erasure_alterations' => 'ErasureAlterationController',
    'position-descriptions' => 'PositionDescriptionController',
    'oath-office' => 'OathOfficeController',
    'appointment-casual' => 'AppointmentCasualController',
    'appointment-checklist' => 'AppointmentController',
    'appointment-requirements' => 'AppointmentRequirementController',
    'psipop' => 'PSIPOPController',
    'examinations' => 'ExaminationController',
    'interviews' => 'InterviewController',
    'interview_evaluation_ratings' => 'InterviewEvaluationRatingsController',
    'applicant' => 'ApplicantController',
    'config' => 'ConfigController',
    'preliminary_evaluation' => 'PreliminaryEvaluationController',
    'selection-lineup' => 'SelectionLineUpController',
    'evaluation' => 'EvaluationController',
    'jobs' => 'JobsController',
    'recommendation' => 'RecommendationController',
    'appointment' => 'AppointmentController',
    'appointment-form' => 'AppointmentFormController',
    'joboffer' => 'JobOfferController',
    'assumption' => 'AssumptionController',
    'attestation' => 'AttestationController',
    'report' => 'ReportController',
]);


