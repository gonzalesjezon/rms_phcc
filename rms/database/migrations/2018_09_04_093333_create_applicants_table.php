<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('applicants')) {
            Schema::create('applicants', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('job_id');
                $table->string('first_name')->nullable();
                $table->string('middle_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('extension_name')->nullable();
                $table->string('nickname')->nullable();
                $table->string('email_address')->nullable();
                $table->string('mobile_number')->nullable();
                $table->string('contact_number')->nullable();
                $table->string('telephone_number')->nullable();
                $table->timestamp('birthday')->nullable();
                $table->string('birth_place')->nullable();
                $table->string('gender')->nullable();
                $table->string('civil_status')->nullable();
                $table->string('citizenship')->nullable();
                $table->boolean('filipino')->nullable();
                $table->boolean('naturalized')->nullable();
                $table->string('height')->nullable();
                $table->string('weight')->nullable();
                $table->string('blood_type')->nullable();
                $table->string('pagibig')->nullable();
                $table->string('gsis')->nullable();
                $table->string('philhealth')->nullable();
                $table->string('tin')->nullable();
                $table->string('sss')->nullable();
                $table->string('govt_issued_id')->nullable();
                $table->string('govt_id_issued_number')->nullable();
                $table->string('govt_id_issued_place')->nullable();
                $table->timestamp('govt_id_date_issued')->nullable();
                $table->timestamp('govt_id_valid_until')->nullable();
                $table->string('house_number')->nullable();
                $table->string('street')->nullable();
                $table->string('subdivision')->nullable();
                $table->string('barangay')->nullable();
                $table->string('city')->nullable();
                $table->string('province')->nullable();
                $table->string('country')->nullable();
                $table->string('permanent_house_number')->nullable();
                $table->string('permanent_street' )->nullable();
                $table->string('permanent_subdivision')->nullable();
                $table->string('permanent_barangay')->nullable();
                $table->string('permanent_city')->nullable();
                $table->string('permanent_province')->nullable();
                $table->string('permanent_country' )->nullable();
                $table->string('permanent_telephone_number')->nullable();
                $table->string('image_path')->nullable();
                $table->string('application_letter_path')->nullable();
                $table->string('pds_path')->nullable();
                $table->string('employment_certificate_path')->nullable();
                $table->string('tor_path')->nullable();
                $table->string('coe_path')->nullable();
                $table->string('training_certificate_path')->nullable();
                $table->integer('active')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
