<?php

use Illuminate\Database\Seeder,
    App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        User::truncate();

        // And now, let's create a few articles in our database:
        User::create([
            'name' => 'storemalt',
            'email' => 'storemalt@gmail.com',
            'password' => bcrypt(123123)
        ]);
    }
}
